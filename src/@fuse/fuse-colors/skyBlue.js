const skyBlue = {
    50: '#ecfbff',
    100: '#d0f4fe',
    200: '#b0edfd',
    300: '#ff1e29',
    400: '#79e0fc',
    500: '#e41f25',
    600: '#59d6fa',
    700: '#c5161b',
    800: '#45cbf9',
    900: '#33c2f8',
    A100: '#ffffff',
    A200: '#ffffff',
    A400: '#d7f3ff',
    A700: '#beecff',
};



export default skyBlue;
