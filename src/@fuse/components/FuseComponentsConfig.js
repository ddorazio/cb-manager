import React from 'react';
import { Redirect } from 'react-router-dom';
import i18next from 'i18next';

import enFuseComponents from './i18n/enFuseComponents';
import frFuseComponents from './i18n/frFuseComponents';

i18next.addResourceBundle('en', 'customersApp', enFuseComponents);
i18next.addResourceBundle('fr', 'customersApp', frFuseComponents);

export const FuseComponentsConfig = {
    settings: {},
    routes: []
};