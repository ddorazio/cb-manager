const locale = {
    CONTAINS : "Contient",
    STARTS_WITH : "Commence par",
    ENDS_WITH : "Termine par",
    MATCHES : "Égal à",
    GREATER_THAN : "Plus grand que",
    LESS_THAN : "Plus petit que",
}

export default locale;
