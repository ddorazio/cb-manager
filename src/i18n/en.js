const locale = {
    CONTAINS : "Contains",
    STARTS_WITH : "Starts with",
    ENDS_WITH : "Ends with",
    MATCHES : "Matches",
    GREATER_THAN : "Greater than",
    LESS_THAN : "Less than",

}

export default locale;
