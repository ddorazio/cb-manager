import mock from './../mock';
import {FuseUtils} from '@fuse';

const notesDB = {
    notes : [
        {
            "id"         : "5739d1fbaac9710256574208",
            "title"      : "Test 1",
            "description": "This is the text for test 1",
            "archive"    : false,
            "created"    : "2019-03-03T18:08:32.587Z",
            "modified"   : new Date(),
            "labels"     : [
                "5725a680606588342058356d"
            ],
            "username"   : "Admin"
        },
        {
            "id"         : "5739d1fbaac9710256574202",
            "title"      : "Text 2",
            "description": "This is the text for test 2",
            "archive"    : true,
            "created"    : "2019-05-15T11:08:32.587Z",
            "modified"   : new Date(),
            "labels"     : [
                "5725a6809fdd915739187ed5"
            ],
            "username"   : "Admin"
        }
    ],
    labels: [
        {
            "id"    : "5725a6802d10e277a0f35739",
            "name"  : "Document",
            "handle": "document"
        },
        {
            "id"    : "5725a6809fdd915739187ed5",
            "name"  : "Emploi",
            "handle": "emploi"
        },
        {
            "id"    : "5725a68031fdbb1db2c1af47",
            "name"  : "Cellulaire",
            "handle": "cellulaire"
        },
        {
            "id"    : "5725a680606588342058356d",
            "name"  : "IBV",
            "handle": "ibv"
        },
        {
            "id"    : "5725a6806acf030f9341e925",
            "name"  : "NSF",
            "handle": "nsf"
        }
    ]
};

mock.onGet('/api/notes-app/notes').reply((config) => {
    return [200, notesDB.notes];
});

mock.onGet('/api/notes-app/labels').reply((config) => {
    return [200, notesDB.labels];
});

mock.onPost('/api/notes-app/create-note').reply((request) => {
    const data = JSON.parse(request.data);
    const newNote =
        {
            ...data.note,
            id: FuseUtils.generateGUID()
        };
    notesDB.notes = [
        newNote,
        ...notesDB.notes
    ];
    return [200, newNote];
});

mock.onPost('/api/notes-app/update-note').reply((request) => {
    const data = JSON.parse(request.data);

    notesDB.notes = notesDB.notes.map((note) => {
        if ( data.note.id === note.id )
        {
            return data.note
        }
        return note
    });

    return [200, data.note];
});

mock.onPost('/api/notes-app/update-labels').reply((request) => {
    const data = JSON.parse(request.data);

    notesDB.labels = data.labels;

    return [200, notesDB.labels];
});

mock.onPost('/api/notes-app/remove-note').reply((request) => {
    const data = JSON.parse(request.data);

    notesDB.notes = notesDB.notes.filter((note) => data.noteId !== note.id);

    return [200, notesDB.notes];
});

