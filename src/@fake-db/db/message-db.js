import mock from './../mock';
import {FuseUtils} from '@fuse';

var messageDB = [
    {
        'id': '561551bd7fe2ff461101c192',
        'title': 'Nouveau Client',
        'description': 'Envoyé au moment d\'un nouveau enregistrement'
    },
    {
        'id': '561551bd7fe2ff461101c122',
        'title': 'Paiement en retard',
        'description': 'Envoyé lorsque un paiment est en retard'
    },
    {
        'id': '561551bd7fe2ff461101c152',
        'title': 'Avis de prélevement',
        'description': 'Envoyé au moment d\'un nouveau enregistrement'
    }
];

mock.onGet('/api/Configuration/messages').reply((config) => {
    return [200, messageDB];
});

mock.onGet('/api/Configuration/message').reply((config) => {
    const params = config.params;
    const response = messageDB.find((message) => message.id === params.id);
    return [200, response];
});

mock.onPost('/api/Configuration/saveMessage').reply((request) => {
    const data = JSON.parse(request.data);
    if (data.id && data.id !== '') {
        const index = messageDB.findIndex((message) => message.id === data.id);
        messageDB = [
            ...messageDB.slice(0, index),
            data,
            ...messageDB.slice(index + 1)
        ]
    } else {
        messageDB = [
            ...messageDB, {
                ...data,
                id: FuseUtils.generateGUID()
            }
        ];
    }
    
    return [200, messageDB];
});