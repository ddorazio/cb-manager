import { setupSignalRConnection } from './SignalRMiddleware';
import settingsConfig from 'app/fuse-configs/settingsConfig'
import {getNotifications, setNotificationsSeen} from 'app/main/apps/notifications/store/actions'

const connectionHub = settingsConfig.apiPath+"/notificationsHub";

export const setupEventsHub = setupSignalRConnection(connectionHub, {
  "NewNotification": [getNotifications,"PREAPPROVED"],
}, ""/*getAccessToken*/);

export default () => dispatch => {
  dispatch(setupEventsHub); // dispatch is coming from Redux
};