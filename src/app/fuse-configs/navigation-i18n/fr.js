const locale = {
    'APPLICATIONS': 'Applications',
    'DASHBOARDS': 'Tableau de bord',
    'CALENDAR': 'Calendar',
    'ECOMMERCE': 'E-Commerce',
    'ACADEMY': 'Academy',
    'MAIL': 'Mail',
    'TODO': 'To-Do',
    'FILE_MANAGER': 'File Manager',
    'CONTACTS': 'Contacts',
    'CHAT': 'Chat',
    'SCRUMBOARD': 'Scrumboard',
    'NOTES': 'Notes',
    'CUSTOMERS': 'Clients',
    'REQUESTS': 'Demandes de prêt',
    'SETTINGS': 'Configuration',
    'COLLECTIONS': 'Collections',
    'DEPOSITS': 'Dépôts',
    'AGREEMENT': 'Entente de paiement',
    'REPORTS': 'Rapports'
};

export default locale;
