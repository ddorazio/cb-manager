const locale = {
    'APPLICATIONS': 'Applications',
    'DASHBOARDS': 'Dashboards',
    'CALENDAR': 'Calendar',
    'ECOMMERCE': 'E-Commerce',
    'ACADEMY': 'Academy',
    'MAIL': 'Mail',
    'TODO': 'To-Do',
    'FILE_MANAGER': 'File Manager',
    'CONTACTS': 'Contacts',
    'CHAT': 'Chat',
    'SCRUMBOARD': 'Scrumboard',
    'NOTES': 'Notes',
    'CUSTOMERS': 'Customers',
    'REQUESTS': 'Loan requests',
    'SETTINGS': 'Settings',
    'COLLECTIONS': 'Collections',
    'DEPOSITS': 'Deposits',
    'AGREEMENT': 'Payment agreements',
    'REPORTS': 'Reports'
};

export default locale;
