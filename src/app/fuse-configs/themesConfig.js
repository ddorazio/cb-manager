import { red } from '@material-ui/core/colors';
import { skyBlue, fuseDark } from '@fuse/fuse-colors';

const themesConfig = {
    default: {
        palette: {
            type: 'light',
            primary: fuseDark,
            secondary: {
                light: "#ff1e29",
                main: "#e41f25",
                dark: "#c5161b"
            },
            background: {
                paper  : "#FFFFFF",
                default: "rgba(255,255,255,0)",
            },
            error: red,
        },
        status: {
            danger: 'orange'
        }
    }
};

export default themesConfig;
