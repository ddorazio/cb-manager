import { MaterialUIComponentsNavigation } from 'app/main/documentation/material-ui-components/MaterialUIComponentsNavigation';
import { authRoles } from 'app/auth';
import i18next from 'i18next';
import en from './navigation-i18n/en';
import fr from './navigation-i18n/fr';

i18next.addResourceBundle('fr', 'navigation', fr);
i18next.addResourceBundle('en', 'navigation', en);

const navigationConfig = [
    {
        'id': 'applications',
        'title': 'Menu',
        'type': 'group',
        'icon': 'apps',
        'children': [
            /*
            {
                'id'       : 'dashboards',
                'title'    : 'Dashboard',
                'translate': 'DASHBOARDS',
                'type'     : 'item',
                'icon'     : 'dashboard',
                'url'  : '/apps/dashboards/project',
            },
            
            {
                'id'       : 'calendar',
                'title'    : 'Calendar',
                'translate': 'CALENDAR',
                'type'     : 'item',
                'icon'     : 'today',
                'url'      : '/apps/calendar'
            },*/
            {
                'id': 'customers',
                'title': 'Customers',
                'translate': 'CUSTOMERS',
                'type': 'item',
                'icon': 'group',
                'url': '/apps/customers',
            },
            {
                'id': 'requests',
                'title': 'Loan requests',
                'translate': 'REQUESTS',
                'type': 'item',
                'icon': 'attach_money',
                'url': '/apps/requests',
            },
            {
                'id': 'collections',
                'title': 'Collections',
                'translate': 'COLLECTIONS',
                'type': 'item',
                'icon': 'warning',
                'url': '/apps/collections/all'
            },
            {
                'id': 'agreement',
                'title': 'Agreement',
                'translate': 'AGREEMENT',
                'type': 'item',
                'icon': 'warning',
                'url': '/apps/agreement/all'
            },
            {
                'id': 'deposits',
                'title': 'Deposits',
                'translate': 'DEPOSITS',
                'type': 'item',
                'icon': 'account_balance',
                'url': '/apps/deposits/all'
            },
            {
                'id': 'settings',
                'title': 'Settings',
                'translate': 'SETTINGS',
                'type': 'item',
                'icon': 'settings',
                'url': '/apps/configuration'
            },
            {
                'id': 'superset',
                'title': 'Superset',
                'translate': 'REPORTS',
                'type': 'item',
                'icon': 'assessment',
                'url': '/apps/superset'
            }
            /*,/apps/deposits
            {
                'id'       : 'mail',
                'title'    : 'Mail',
                'translate': 'MAIL',
                'type'     : 'item',
                'icon'     : 'email',
                'url'      : '/apps/mail',
                'badge'    : {
                    'title': 25,
                    'bg'   : '#F44336',
                    'fg'   : '#FFFFFF'
                }
            },
            {
                'id'       : 'contacts',
                'title'    : 'Contacts',
                'translate': 'CONTACTS',
                'type'     : 'item',
                'icon'     : 'account_box',
                'url'      : '/apps/contacts/all'
            }*/
        ]
    }
];

export default navigationConfig;
