const settingsConfig = {
    layout: {
        style: 'layout1', // layout-1 layout-2 layout-3
        config: {} // checkout default layout configs at app/fuse-layouts for example  app/fuse-layouts/layout1/Layout1Config.js
    },
    customScrollbars: true,
    direction: 'ltr', //rtl, ltr
    theme: {
        main: 'default',
        navbar: 'mainThemeLight',
        toolbar: 'mainThemeLight',
        footer: 'mainThemeDark'
    },
    //signalRPath: 'https://sloanapi.financedirect.ca', // FINANCEDIRECT PROD
    //signalRPath: 'https://sloanapi.24hrcash.ca', // 24HRCASH PROD
    //signalRPath: 'https://sloanapi.pretcash2000.com', // PRETCAH2000 PROD
    //signalRPath: 'https://sloanapi.solutions500.com', // SignalR SOLUTIONS500 PROD
    //signalRPath: 'https://sloanapi.expresscreditplus.com', // SignalR EXPRESS PROD
    //signalRPath: 'https://sloanapi.alphaloans.ca', // SignalR ALPHA PROD
    //signalRPath: 'https://lmsapi.soniccash500.com', // SignalR SONIC PROD
    //signalRPath: 'https://lmsapi-demo.creditbook.ca', // SignalR ROYAL DEMO
    //signalRPath: 'https://lmsapi.royalfinances.ca', // SignalR ROYAL PROD
    signalRPath: 'http://localhost:5000', // SignalR DEV
    //apiPath: "http://creditbook-lms.mngo.co" // VERO
    //apiPath: "https://lmsapi.mngo.co" // MANGO TEST
    apiPath: 'http://localhost:5000' // LOCAL DEV
    //apiPath: 'https://lmsapi.royalfinances.ca' // ROYAL PROD
    //apiPath: 'https://lmsapi-demo.creditbook.ca' // ROYAL DEMO
    //apiPath: 'https://lmsapi.soniccash500.com' // SONIC PROD
    //apiPath: 'https://sloanapi.alphaloans.ca' // ALPHA PROD
    //apiPath: 'https://sloanapi.expresscreditplus.com' // EXPRESS PROD
    //apiPath: 'https://sloanapi.solutions500.com' // SOLUTIONS500 PROD
    //apiPath: 'https://sloanapi.pretcash2000.com' // PRETCAH2000 PROD
    //apiPath: 'https://sloanapi.24hrcash.ca' // 24HRCASH PROD
    //apiPath: 'https://sloanapi.financedirect.ca' // FINANCEDIRECT PROD
};

export default settingsConfig;
