import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import NumberFormat from 'react-number-format';

function CurrencyMask(props) {
    return <NumberFormat
        id="input-charge-type"
        {...props}
        onChange={e => props.onChange(e)}
        thousandSeparator
        isNumericString
    />
}

function CurrencyField(props) {
    return <TextField
        {...props}
        onChange={e => props.onChange(e.target.value)}
        InputProps={{
            inputComponent: CurrencyMask,
            inputProps: props,
            startAdornment: <InputAdornment position="start">$</InputAdornment>
        }}
        
    />
}

export default CurrencyField;