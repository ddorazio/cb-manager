import { jsx } from 'slate-hyperscript'
import escapeHtml from 'escape-html'
import {Text } from 'slate';
import {ELEMENT_TAGS, TEXT_TAGS} from './Types'

export const serializeEmail = (node, editor = node) => {

    if (editor === node && serializeSMS(editor) === "") {
        return ""
    }

    if (Text.isText(node)) {
        return styled(node)
    }
    
    
    const children = node.children.map(n => serializeEmail(n, editor)).join('')

    switch (node.type) {
        case 'quote':
            return `<blockquote><p>${children}</p></blockquote>`
        case 'paragraph':
            return `<p>${children}</p>`
        case 'link':
            return `<a href="${escapeHtml(node.url)}">${children}</a>`
        case 'heading-two':
            return `<h2>${children}</h2>`
        case 'heading-one':
            return `<h2>${children}</h2>`
        case 'list-item':
            return `<li>${children}</li>`
        case 'bulleted-list':
            return `<ul>${children}</ul>`
        case 'numbered-list':
            return `<ol>${children}</ol>`
        case 'mention':
            return `${node.token}`
        case 'image':
            return `<img src=${node.url}/>`
        default:
            return children
    }
}

export const serializeSMS = (node, editor = node) => {

    if (Text.isText(node)) {
        return node.text
    }

    const children = node.children.map(n => serializeSMS(n, editor)).join('')

    if (node.type === 'mention') {
        return `${node.token}`
    }

    return (editor.children.slice(1).find(n => n === node) ? "\r\n" : "") + children
}

export const deserializeEmail = (el, options) => {
    if (el.nodeType === 3) {
        const mention = options.find(mention => el.textContent.includes(mention.token));
        if (mention) {
            const index = el.textContent.indexOf(mention.token)
            const pre = el.textContent.substring(0, index)  
            const ment = jsx('element', { type: 'mention', character: mention.value, children: [{ text: '' }], token: mention.token}, [""])
            el.textContent = el.textContent.substring(index + mention.token.length, el.textContent.length)

            return [pre, ment, ...deserializeEmail(el, options)]
        } else {
            return el.textContent
        }
    } else if (el.nodeType !== 1) {
            return null
    } else if (el.nodeName === 'BR') {
        return '\n'
    }
    
    const { nodeName } = el
    let parent = el
    
    if (
        nodeName === 'PRE' &&
        el.childNodes[0] &&
        el.childNodes[0].nodeName === 'CODE'
    ) {
        parent = el.childNodes[0]
    }
    const children = parent.childNodes.length > 0 ? Array.from(parent.childNodes)
        .map(child => deserializeEmail(child, options))
        .flat() :  [""]
    
    if (el.nodeName === 'BODY') {
        return jsx('fragment', {}, children)
    }
    
    if (ELEMENT_TAGS[nodeName]) {
        const attrs = ELEMENT_TAGS[nodeName](el)
        return jsx('element', attrs, children)
    }
    
    if (TEXT_TAGS[nodeName]) {
        const attrs = TEXT_TAGS[nodeName](el)
        return children.map(child => jsx('text', attrs, child))
    }
    
    return children 
}

export const deserializeSMS = (text, options, wrapInFragment = true, wrapInParagraph = true) => {
    const separators = ["\r\n", "\n"]
    if (text.includes("\r\n") || text.includes("\n") || wrapInFragment) {
        return jsx('fragment', {}, text.split(new RegExp(separators.join('|'), 'g')).map(text => deserializeSMS(text, options, false)).flat());
        //return jsx('fragment', {}, jsx('element', {type: "paragraph"}, [text.split(new RegExp(separators.join('|'), 'g')).map(text => deserializeSMS(text, options, false)).flat()]));
    } else {
        const mention = options.find(mention => text.includes(mention.token));
        if (mention) {
            const index = text.indexOf(mention.token)
            const pre = text.substring(0, index)
            const ment = jsx('element', { type: 'mention', character: mention.value, children: [{ text: '' }], token: mention.token}, [""])
            if (wrapInParagraph) {
                return jsx('element', { type: 'paragraph'}, [pre, ment, deserializeSMS(text.substring(index + mention.token.length, text.length), options, false, false)]) 
            } else {
                return [pre, ment, deserializeSMS(text.substring(index + mention.token.length, text.length), options, false, false)]
            }
        } else {
            if (wrapInParagraph) {
                return jsx('element', { type: 'paragraph'}, [text]) 
            } else {
                return text
            }
            
        }
    }
}

const styled = node => {
    var startTag = '';
    let endTag = '';

    if (node.bold === true) {
        startTag += '<b>';
        endTag += '</b>';
    }

    if (node.italic === true) {
        startTag += '<em>';
        endTag += '</em>';
    }

    if (node.underline === true) {
        startTag += '<u>';
        endTag += '</u>';
    }

    return startTag + escapeHtml(node.text) + endTag
}

