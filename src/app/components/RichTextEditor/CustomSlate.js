import React, { useCallback, useMemo, useState, useEffect, useRef } from 'react';
import { Editable, withReact, useSlate, Slate, ReactEditor } from 'slate-react';
import { Editor, Transforms, createEditor, Range } from 'slate';
import { withHistory } from 'slate-history';
import { Button, Icon, Toolbar } from '@material-ui/core';
import { BlockButton } from './Components/BlockButton'
import { MarkButton } from './Components/MarkButton' 
import { withMentions, insertMention, MentionElement } from './Components/Mention';
import { withImages, insertImage, ImageElement } from './Components/Image';
import { Portal } from './Components/components'
import { serializeEmail, serializeSMS, deserializeEmail, deserializeSMS } from './Serialization'

import './customSlate.css';

const CustomSlate = (props) => {

    const ref = useRef()
    const [value, setValue] = useState(
        initialValue
    )
    const [target, setTarget] = useState()
    const [index, setIndex] = useState(0)
    const [search, setSearch] = useState('')

    const renderElement = useCallback(props => <Element {...props} />, [])
    const renderLeaf = useCallback(props => <Leaf {...props} />, [])

    const editor = useMemo(
        () => withImages(withMentions(withReact(withHistory(createEditor())))), 
        []
    )

    const chars = search === '' ? props.dropdown : props.dropdown.filter(c =>
        c.value.toLowerCase().startsWith(search.toLowerCase())
    ).slice(0, 10)

    useEffect(() => {
       if (props.value) {
            if (props.richText === true) {
                const document = new DOMParser().parseFromString(props.value, 'text/html')
                const deserialized = deserializeEmail(document.body, props.dropdown)
                setValue(deserialized)
            } else {
                const deserialized = deserializeSMS(props.value, props.dropdown)
                setValue(deserialized)
            }
       }
    }, [props.value]);

    useEffect(() => {
        if (target && chars.length > 0) {
          const el = ref.current
          const domRange = ReactEditor.toDOMRange(editor, target)
          const rect = domRange.getBoundingClientRect()
          el.style.top = `${rect.top + window.pageYOffset + 24}px`
          el.style.left = `${rect.left + window.pageXOffset}px`
        }
    }, [chars.length, editor, index, search, target])
  
    const onChange = (value) => {
        setValue(value);

        const { selection } = editor;
        if (selection && Range.isCollapsed(selection)) {
            const [start] = Range.edges(selection)
            const characterBefore = Editor.before(editor, start, { unit: 'character' })
            const befRange = characterBefore && Editor.range(editor, characterBefore, start)
            const befCharacter = befRange && Editor.string(editor, befRange)

            const wordBefore = Editor.before(editor, start, { unit: 'word' })
            const before = wordBefore && Editor.before(editor, wordBefore)
            const beforeRange = before && Editor.range(editor, before, start)
            const beforeText = beforeRange && Editor.string(editor, beforeRange)
            const beforeMatch = beforeText && beforeText.match(/^@(\w+)$/)
            const after = Editor.after(editor, start)
            const afterRange = Editor.range(editor, start, after)
            const afterText = Editor.string(editor, afterRange)
            const afterMatch = afterText.match(/^(\s|$)/)
  
            if (beforeMatch && afterMatch) {
                setTarget(beforeRange)
                setSearch(beforeMatch[1])
                setIndex(0)
            } 
            else if (befCharacter === "@") {
                setTarget(befRange)
                setSearch('')
                setIndex(0)
            } else {
                setTarget(null)
            }
        }

        props.onChange(props.name, props.richText ? serializeEmail(editor) : serializeSMS(editor));
    }

    const onKeyDown = useCallback(
        event => {
            if (target && chars.length > 0) {
                switch (event.key) {
                case 'ArrowDown':
                    event.preventDefault()
                    const prevIndex = index >= chars.length - 1 ? 0 : index + 1
                    setIndex(prevIndex)
                    break
                case 'ArrowUp':
                    event.preventDefault()
                    const nextIndex = index <= 0 ? chars.length - 1 : index - 1
                    setIndex(nextIndex)
                    break
                case 'Tab':
                case 'Enter':
                    event.preventDefault()
                    Transforms.select(editor, target)
                    insertMention(editor, chars[index])
                    setTarget(null)
                    break
                case 'Escape':
                    event.preventDefault()
                    setTarget(null)
                    break
                }
            } 
        },
        [index, search, target]
    )
    
    
    return (
        <div className="editor-container">
            <Slate editor={editor} value={value} onChange={onChange}>
                {props.richText === true && (
                    <Toolbar disableGutters={true} variant="dense">
                        <MarkButton format="bold" icon="format_bold" />
                        <MarkButton format="italic" icon="format_italic" />
                        <MarkButton format="underline" icon="format_underlined" />
                        <MarkButton format="code" icon="code" />
                        <BlockButton format="heading-one" icon="looks_one" />
                        <BlockButton format="heading-two" icon="looks_two" />
                        <BlockButton format="block-quote" icon="format_quote" />
                        <BlockButton format="numbered-list" icon="format_list_numbered" />
                        <BlockButton format="bulleted-list" icon="format_list_bulleted" />
                        {props.imageEnabled === true && (
                            <Button
                                onMouseDown={event => {
                                    event.preventDefault()
                                    insertImage(editor, props.imageURL)
                                }}
                            >
                                <Icon>insert_photo</Icon>
                            </Button>
                        )}
                </Toolbar>
                )}
                <Editable
                    className="editor"
                    renderElement={renderElement}
                    renderLeaf={renderLeaf}
                    placeholder={props.placeholder}
                    autoFocus
                    onKeyDown={onKeyDown}
                />
                {target && chars.length > 0 && (
                    <Portal>
                        <div
                            ref={ref}
                            style={{
                                top: '-9999px',
                                left: '-9999px',
                                position: 'absolute',
                                zIndex: 99999,
                                padding: '3px',
                                background: 'white',
                                borderRadius: '4px',
                                boxShadow: '0 1px 5px rgba(0,0,0,.2)',
                            }}
                        >
                            {chars.map((char, i) => (
                            <div
                                key={char.value}
                                style={{
                                padding: '1px 3px',
                                borderRadius: '3px',
                                background: i === index ? '#B4D5FF' : 'transparent',
                                }}
                            >
                                {char.value}
                            </div>
                            ))}
                        </div>
                    </Portal>
                )}
            </Slate>
        </div>
      
    )
}
  



const Element = props => {
    const { attributes, children, element } = props
    switch (element.type) {
        case 'block-quote':
            return <blockquote {...attributes}>{children}</blockquote>
        case 'bulleted-list':
            return <ul {...attributes}>{children}</ul>
        case 'heading-one':
            return <h1 {...attributes}>{children}</h1>
        case 'heading-two':
            return <h2 {...attributes}>{children}</h2>
        case 'list-item':
            return <li {...attributes}>{children}</li>
        case 'numbered-list':
            return <ol {...attributes}>{children}</ol>
        case 'mention':
            return <MentionElement {...props} />
        case 'image':
            return <ImageElement {...props} />
        default:
            return <p {...attributes}>{children}</p>
    }
}

const Leaf = ({ attributes, children, leaf }) => {
    if (leaf.bold) {
        children = <strong>{children}</strong>
    }

    if (leaf.code) {
        children = <code>{children}</code>
    }

    if (leaf.italic) {
        children = <em>{children}</em>
    }

    if (leaf.underline) {
        children = <u>{children}</u>
    }

    return <span {...attributes}>{children}</span>
}

const initialValue = [{
    type: 'paragraph',
    children: [{ text: '' }],
}]



export default CustomSlate