import React from 'react';
import { useSlate} from 'slate-react';
import { Button, Icon } from '@material-ui/core';
import { LIST_TYPES } from '../Types'
import { Transforms, Editor } from 'slate';

const toggleBlock = (editor, format) => {
    const isActive = isBlockActive(editor, format)
    const isList = LIST_TYPES.includes(format)

    Transforms.unwrapNodes(editor, {
        match: n => LIST_TYPES.includes(n.type),
        split: true,
    })

    Transforms.setNodes(editor, {
        type: isActive ? 'paragraph' : isList ? 'list-item' : format,
    })

    if (!isActive && isList) {
        const block = { type: format, children: [] }
        Transforms.wrapNodes(editor, block)
    }
}

const isBlockActive = (editor, format) => {
    const [match] = Editor.nodes(editor, {
    match: n => n.type === format,
    })

    return !!match
}

export const BlockButton = ({ format, icon }) => {
    const editor = useSlate()
    return (
    <Button
        active={isBlockActive(editor, format)}
        onMouseDown={event => {
            event.preventDefault()
            toggleBlock(editor, format)
        }}
    >
        <Icon>{icon}</Icon>
    </Button>
    )
}