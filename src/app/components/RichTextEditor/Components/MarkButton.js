import React from 'react';
import { useSlate} from 'slate-react';
import { Button, Icon } from '@material-ui/core';
import { Editor } from 'slate';

const toggleMark = (editor, format) => {
    const isActive = isMarkActive(editor, format)

    if (isActive) {
        Editor.removeMark(editor, format)
    } else {
        Editor.addMark(editor, format, true)
    }
}



const isMarkActive = (editor, format) => {
    const marks = Editor.marks(editor)
    return marks ? marks[format] === true : false
}

export const MarkButton = ({ format, icon }) => {
    const editor = useSlate()
    return (
    <Button
        active={isMarkActive(editor, format)}
        onMouseDown={event => {
            event.preventDefault()
            toggleMark(editor, format)
        }}
    >
        <Icon>{icon}</Icon>
    </Button>
    )
}