import React from 'react';
import {useSelected, useFocused} from 'slate-react';
import {Transforms} from 'slate';

export const withImages = editor => {
    const { insertData, isVoid } = editor
  
    editor.isVoid = element => {
      return element.type === 'image' ? true : isVoid(element)
    }
  
    editor.insertData = data => {
      const text = data.getData('text/plain')
      const { files } = data
  
      if (files && files.length > 0) {
        for (const file of files) {
          const reader = new FileReader()
          const [mime] = file.type.split('/')
  
          if (mime === 'image') {
            reader.addEventListener('load', () => {
              const url = reader.result
              insertImage(editor, url)
            })
  
            reader.readAsDataURL(file)
          }
        }
      } else if (isImageUrl(text)) {
        insertImage(editor, text)
      }
    }
  
    return editor
  }

export const ImageElement = ({ attributes, children, element }) => {
    const selected = useSelected()
    const focused = useFocused()
    return (
      <div {...attributes}>
        <div contentEditable={false}>
          <img
            src={element.url}
            style={{
                display: 'block',
                maxWidth: '100%',
                maxHeight: '20em',
                boxShadow: selected && focused ? '0 0 0 3px #B4D5FF' : 'none'
            }}
          />
        </div>
        {children}
      </div>
    )
}

export const insertImage = (editor, url) => {
    const image = { type: 'image', url, children: [{ text: '' }] }
    Transforms.insertNodes(editor, image)
    Transforms.move(editor)
}

const isImageUrl = url => {
    if (!url) return false
    if (!isUrl(url)) return false
    const ext = new URL(url).pathname.split('.').pop()
    return ['png', 'gif', 'jpg', 'jpeg'].includes(ext)
  }
  
const isUrl = str => {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

