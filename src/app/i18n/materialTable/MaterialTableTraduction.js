import { useTranslation } from 'react-i18next';

import i18next from 'i18next';

import enMaterialTable from './en';
import frMaterialTable from './fr';

i18next.addResourceBundle('en', 'materialTable', enMaterialTable);
i18next.addResourceBundle('fr', 'materialTable', frMaterialTable);

const MaterialTableTraduction = () => {
    
    const { t } = useTranslation('materialTable');

    return {
        body: {
            emptyDataSourceMessage: t("NO_DATA"),
            addTooltip: t("ADD_TOOLTIP"),
            deleteTooltip : t("DELETE_TOOLTIP"),
            editTooltip : t("EDIT_TOOLTIP"),
            filterRow: {
                filterPlaceHolder : t("FILTER_ROW_PLACE_HOLDER"),
                filterTooltip : t("FILTER_ROW_TOOLTIP")
            },
            editRow : {
                deleteText : t("EDIT_ROW_DELETE_TEXT"),
                cancelTooltip : t("EDIT_ROW_CANCEL_TOOLTIP"),
                saveTooltip : t("EDIT_ROW_SAVE_TOOLTIP"),
            }
        },
        pagination: {
            labelDisplayedRows: t("LABEL_DISPLAYED_ROWS"),
            labelRowsSelect: t("LABEL_ROWS_SELECT"),
            labelRowsPerPage : t("LABEL_ROWS_PER_PAGE"),
            firstAriaLabel: t("FIRST_ARIA_LABEL"),
            firstTooltip: t("FIRST_TOOL_TIP"),
            previousAriaLabel: t("PREVIOUS_ARIA_LABEL"),
            previousTooltip: t("PREVIOUS_TOOLTIP"),
            nextAriaLabel: t("NEXT_ARIA_LABEL"),
            nextTooltip: t("NEXT_TOOLTIP"),
            lastAriaLabel : t("LAST_ARIA_LABEL"),
            lastTooltip : t("LAST_TOOL_TIP")
        },
        toolbar: {
            searchTooltip:t("SEARCH_TOOL_TIP"),
            searchPlaceholder:t("SEARCH_PLACE_HOLDER")
        }
    }  
}
export default MaterialTableTraduction