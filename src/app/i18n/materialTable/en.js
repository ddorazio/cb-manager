const locale = {
    "ADD_TOOLTIP":"Add",
    "DELETE_TOOLTIP":"Delete",
    "EDIT_TOOLTIP":"Edit",

    "FILTER_ROW_PLACE_HOLDER":"",
    "FILTER_ROW_TOOLTIP":"Filter",

    "EDIT_ROW_DELETE_TEXT":"Are you sure delete this row?",
    "EDIT_ROW_CANCEL_TOOLTIP":"Cancel",
    "EDIT_ROW_SAVE_TOOLTIP":"Save",

    "LABEL_DISPLAYED_ROWS":"{from}-{to} of {count}",
    "LABEL_ROWS_SELECT":"rows",
    "LABEL_ROWS_PER_PAGE":"Rows per page",
    "FIRST_ARIA_LABEL":"First Page",
    "FIRST_TOOL_TIP":"First Page",
    "PREVIOUS_ARIA_LABEL":"Previous Page",
    "PREVIOUS_TOOLTIP":"Previous Page",
    "NEXT_ARIA_LABEL":"Next Page",
    "NEXT_TOOLTIP":"Next Page",
    "LAST_ARIA_LABEL":"Last Page",
    "LAST_TOOL_TIP":"Last Page",

    "SEARCH_TOOL_TIP":"Search",
    "SEARCH_PLACE_HOLDER":"Search",
    "NO_DATA":"No Data",
}

export default locale;