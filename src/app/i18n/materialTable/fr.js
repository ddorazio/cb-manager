const locale = {
    "ADD_TOOLTIP":"Ajouter",
    "DELETE_TOOLTIP":"Supprimer",
    "EDIT_TOOLTIP":"Editer",

    "FILTER_ROW_PLACE_HOLDER":"",
    "FILTER_ROW_TOOLTIP":"Filtrer",

    "EDIT_ROW_DELETE_TEXT":"Voulez-vous supprimer cette ligne?",
    "EDIT_ROW_CANCEL_TOOLTIP":"Annuler",
    "EDIT_ROW_SAVE_TOOLTIP":"Enregistrer",

    "LABEL_DISPLAYED_ROWS":"{from}-{to} de {count}",
    "LABEL_ROWS_SELECT":"lignes",
    "LABEL_ROWS_PER_PAGE":"lignes par page",
    "FIRST_ARIA_LABEL":"Première page",
    "FIRST_TOOL_TIP":"Première page",
    "PREVIOUS_ARIA_LABEL":"Précédente",
    "PREVIOUS_TOOLTIP":"Précédente",
    "NEXT_ARIA_LABEL":"Suivante",
    "NEXT_TOOLTIP":"Suivante",
    "LAST_ARIA_LABEL":"Dernière page",
    "LAST_TOOL_TIP":"Dernière page",

    "SEARCH_TOOL_TIP":"Recherche",
    "SEARCH_PLACE_HOLDER":"Recherche",
    "NO_DATA":"Aucune donnée",
}

export default locale;