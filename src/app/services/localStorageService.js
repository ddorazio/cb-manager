class localStorageService {

    static getAccessToken = () => {
        return window.localStorage.getItem('jwt_access_token');
    };

    static setLanguage = (language) => {
        return window.localStorage.setItem('language', language);
    };

    static getLanguage = () => {
        return window.localStorage.getItem('language');
    };

}


export default localStorageService;