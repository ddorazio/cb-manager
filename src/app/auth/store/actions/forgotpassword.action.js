import firebaseService from 'app/services/firebaseService';
import jwtService from 'app/services/jwtService';
import { setUserData } from './user.actions';
//import * as Actions from 'app/store/actions';
import { showMessage } from 'app/store/actions/fuse';

export const FORGOTPASSWORD_SUCCESS = 'FORGOTPASSWORD_SUCCESS';
export const FORGOTPASSWORD_FAILED = 'FORGOTPASSWORD_FAILED';
export const FORGOTPASSWORD_ERROR = 'FORGOTPASSWORD_ERROR';

export function submitForgotPassword({ email }) {
    return (dispatch) =>
        jwtService.forgotPassword(email)
            .then((response) => {

                if (response.status === "Success") {

                    dispatch(showMessage({ message: "Veuillez vérifier votre e-mail. Un nouveau mot de passe vous a été envoyé." }));

                    return dispatch({
                        type: FORGOTPASSWORD_SUCCESS
                    });
                } else {

                    dispatch(showMessage({ message: "Le courriel n'existe pas" }));

                    return dispatch({
                        type: FORGOTPASSWORD_FAILED
                    });
                }
            })
            .catch(error => {
                return dispatch({
                    type: FORGOTPASSWORD_ERROR
                });
            });
}