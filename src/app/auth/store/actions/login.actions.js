import firebaseService from 'app/services/firebaseService';
import jwtService from 'app/services/jwtService';
import { setUserData } from './user.actions';
import * as Actions from 'app/store/actions';
import { showMessage } from 'app/store/actions/fuse';
import i18n from 'i18n';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';

export function submitLogin({ email, password }) {
    return (dispatch) =>
        jwtService.signInWithEmailAndPassword(email, password)
            .then((response) => {

                if (response.status === "Success") {

                    dispatch(setUserData(response));

                    return dispatch({
                        type: LOGIN_SUCCESS
                    });

                } else {

                    dispatch(showMessage({ message: i18n.t("LOGIN_FAILED"), variant: 'error' }));

                    return dispatch({
                        type: LOGIN_FAILED
                    });
                }
            })
            .catch(error => {
                if(error === undefined) {
                    dispatch(showMessage({ message: i18n.t("LOGIN_FAILED"), variant: 'error' }));
                    return dispatch({
                        type: LOGIN_ERROR,
                        payload: error
                    });
                }
            });
}

export function resetPassword({ email, password }) {
    return (dispatch) =>
        jwtService.signInWithEmailAndPassword(email, password)
            .then((response) => {

                if (response.status === "Success") {
                    dispatch(setUserData(response));

                    return dispatch({
                        type: LOGIN_SUCCESS
                    });
                } else {
                    return dispatch({
                        type: LOGIN_FAILED,
                        message: "Login failed"
                    });
                }
            })
            .catch(error => {
                return dispatch({
                    type: LOGIN_ERROR,
                    payload: error
                });
            });
}

export function submitLoginWithFireBase({ username, password }) {
    if (!firebaseService.auth) {
        console.warn("Firebase Service didn't initialize, check your configuration");

        return () => false;
    }

    return (dispatch) =>
        firebaseService.auth.signInWithEmailAndPassword(username, password)
            .then(() => {
                return dispatch({
                    type: LOGIN_SUCCESS
                });
            })
            .catch(error => {
                console.info('error')
                const usernameErrorCodes = [
                    'auth/email-already-in-use',
                    'auth/invalid-email',
                    'auth/operation-not-allowed',
                    'auth/user-not-found',
                    'auth/user-disabled'
                ];
                const passwordErrorCodes = [
                    'auth/weak-password',
                    'auth/wrong-password'
                ];

                const response = {
                    username: usernameErrorCodes.includes(error.code) ? error.message : null,
                    password: passwordErrorCodes.includes(error.code) ? error.message : null
                };

                if (error.code === 'auth/invalid-api-key') {
                    dispatch(Actions.showMessage({ message: error.message }));
                }

                return dispatch({
                    type: LOGIN_ERROR,
                    payload: response
                });
            });
}
