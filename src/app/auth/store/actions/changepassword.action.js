import { showMessage } from 'app/store/actions/fuse';
import ChangePasswordAPI from 'app/main/login/ChangePasswordAPI';
import i18n from 'i18n';

export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILED = 'CHANGE_PASSWORD_FAILED';
export const CHANGE_PASSWORD_ERROR = 'CHANGE_PASSWORD_ERROR';
export const OPEN_CHANGE_PASSWORD = 'OPEN_CHANGE_PASSWORD';
export const CLOSE_CHANGE_PASSWORD = 'CLOSE_CHANGE_PASSWORD';

export function submitChangePassword(model) {
    
    return async dispatch => {
        try {
            const result = await ChangePasswordAPI.changePassword(model);
            return dispatch(showMessage({ message: i18n.t('CHANGE_PASSWORD_SUCCESS'), variant: 'success' }));
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
    
}

export function openChangePassword() {
    
    return async dispatch => {
        return dispatch({
            type: OPEN_CHANGE_PASSWORD
        });
    }
    
}

export function closeChangePassword() {
    
    return async dispatch => {
        return dispatch({
            type: CLOSE_CHANGE_PASSWORD
        });
    }
    
}