import * as Actions from '../actions';

const initialState = {
    success: false,
    openDialog: false
};

const change = function (state = initialState, action) {
    switch (action.type) {
        case Actions.CHANGE_PASSWORD_SUCCESS:
            {
                return {
                    ...initialState,
                    success: true
                };
            }
        case Actions.CHANGE_PASSWORD_FAILED:
            {
                return {
                    initialState,
                    success: false
                };
            }
        case Actions.OPEN_CHANGE_PASSWORD: {
                return {
                    initialState,
                    openDialog : true
                }
            }
        case Actions.CLOSE_CHANGE_PASSWORD:{
            return {
                initialState,
                openDialog : false
            }
        }
        default:
            {
                return state
            }
    }
};

export default change;