import * as Actions from '../actions';

const initialState = {
    success: false,
    error: {
        username: null,
        password: null
    }
};

const login = function (state = initialState, action) {
    switch (action.type) {
        case Actions.LOGIN_SUCCESS:
            {
                return {
                    ...initialState,
                    success: true
                };
            }
        case Actions.LOGIN_ERROR:
            {
                return {
                    ...initialState,
                    success: false,
                };
            }
        case Actions.LOGIN_FAILED:
            {
                return {
                    ...initialState,
                    success: false
                };
            }
        default:
            {
                return state
            }
    }
};

export default login;