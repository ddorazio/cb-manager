import { combineReducers } from 'redux';
import user from './user.reducer';
import login from './login.reducer';
import register from './register.reducer';
import forgotpassword from './forgotpassword.reducer';
import changepassword from './changepassword.reducer';

const authReducers = combineReducers({
    user,
    login,
    register,
    forgotpassword,
    changepassword
});

export default authReducers;