import * as Actions from '../actions';

const initialState = {
    success: false,
    error: {
        username: null
    }
};

const forgot = function (state = initialState, action) {
    switch (action.type) {
        case Actions.FORGOTPASSWORD_SUCCESS:
            {
                return {
                    ...initialState,
                    success: true
                };
            }
        case Actions.FORGOTPASSWORD_FAILED:
            {
                return {
                    initialState,
                    //...initialState,
                    success: false
                };
            }
        default:
            {
                return state
            }
    }
};

export default forgot;