import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    root: {
        '& .logo-icon': {
            width: 'auto',
            // height: 50,
            height: 'auto',
            transition: theme.transitions.create(['width', 'height'], {
                duration: theme.transitions.duration.shortest,
                easing: theme.transitions.easing.easeInOut
            })
        }
    }
}));

function Logo() {
    const classes = useStyles();

    return (
        <div className={clsx(classes.root, "flex items-center")}>
            {/* <img className="logo-icon" src="assets/images/logos/Logo_WEB.png" alt="logo" /> */}
            <img className="logo-icon mt-8 max-w-180" src="assets/images/logos/sloan_logo_solo.png" alt="logo" />
        </div>
    );
}

export default Logo;
