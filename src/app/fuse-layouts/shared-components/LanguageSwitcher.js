import React, { useState, useEffect } from 'react';
import { Button, ListItemIcon, ListItemText, Popover, MenuItem, Typography } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@material-ui/styles';
import * as Actions from 'app/store/actions';
import { Link } from 'react-router-dom';
import localStorageService from 'app/services/localStorageService';

let languages = [];

function LanguageSwitcher(props) {
    const dispatch = useDispatch();

    const theme = useTheme();
    const { i18n } = useTranslation();
    const [menu, setMenu] = useState(null);

    //const currentLng = languages.find(lng => lng.id === i18n.language);

    const [currentLng, setLang] = useState({
        id: '',
        title: '',
        flag: ''
    });

    // Initial load
    useEffect(() => {
        var _language = localStorageService.getLanguage();

        if (_language === null) {

            // Get browser language
            if (navigator.language === 'en' || navigator.language === 'en-US' || navigator.language === 'en-CA') {
                setLang({
                    id: 'en',
                    title: 'English',
                    flag: 'us'
                });
                _language = "en";

            }
            else {
                setLang({
                    id: 'fr',
                    title: 'Français',
                    flag: 'canada'
                });
                _language = "fr";
            }

            // Change language
            setLanguageMenu(_language);
            i18n.changeLanguage(_language);
            localStorageService.setLanguage(_language);
        } else {

            if (_language == "fr") {
                setLang({
                    id: 'fr',
                    title: 'Français',
                    flag: 'canada'
                });
            }
            else if (_language == "en") {
                setLang({
                    id: 'en',
                    title: 'English',
                    flag: 'us'
                });
            }

            // Change language
            setLanguageMenu(_language);
            i18n.changeLanguage(_language);
            localStorageService.setLanguage(_language);
        }
    }, []);

    const setLanguageMenu = (language) => {

        if (language == "en") {
            languages = [
                {
                    id: 'fr',
                    title: 'French',
                    flag: 'canada'
                },
                {
                    id: 'en',
                    title: 'English',
                    flag: 'us'
                }
            ];
        }
        else if (language == "fr") {
            languages = [
                {
                    id: 'fr',
                    title: 'Français',
                    flag: 'canada'
                },
                {
                    id: 'en',
                    title: 'Anglais',
                    flag: 'us'
                }
            ];
        }
    }

    const userMenuClick = event => {
        setMenu(event.currentTarget);
    };

    const userMenuClose = () => {
        setMenu(null);
    };

    function handleLanguageChange(lng) {

        //const newLangDir = i18n.dir(lng.id);

        // Update Local Storage with new language
        localStorageService.setLanguage(lng.id);
        window.location.reload();

        /*
        Change Language
         */
        //i18n.changeLanguage(lng.id);

        /*
        If necessary, change theme direction
         */
        //if (newLangDir !== theme.direction) {
        //    dispatch(Actions.setDefaultSettings({ direction: newLangDir }));
        //}

        //userMenuClose();
    }

    return (
        <React.Fragment>

            <Button className="h-64 w-64" onClick={userMenuClick}>

                <img className="mx-4 min-w-20" src={`assets/images/flags/${currentLng.flag}.png`} alt={currentLng.title} />

                <Typography className="mx-4 font-600">{currentLng.id}</Typography>
            </Button>

            <Popover
                open={Boolean(menu)}
                anchorEl={menu}
                onClose={userMenuClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center'
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
                classes={{
                    paper: "py-8"
                }}
            >
                {languages.map(lng => (
                    <MenuItem key={lng.id} onClick={() => handleLanguageChange(lng)}>
                        <ListItemIcon className="min-w-40">
                            <img className="min-w-20" src={`assets/images/flags/${lng.flag}.png`} alt={lng.title} />
                        </ListItemIcon>
                        <ListItemText primary={lng.title} />
                    </MenuItem>
                ))}
            </Popover>
        </React.Fragment>
    );
}

export default LanguageSwitcher;
