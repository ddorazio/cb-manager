import { getProtected, postProtected, putProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class ChangePasswordAPI {

    static changePassword = async (password) => {
        return await putProtected(new URL('/api/Identity/ChangePassword', settingsConfig.apiPath), password);
    }

}

export default ChangePasswordAPI