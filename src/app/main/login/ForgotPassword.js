import React, { useEffect, useRef, useState } from 'react'
import { Button, Card, CardContent, Typography, Tabs, Tab, InputAdornment, Icon } from '@material-ui/core';
import { darken } from '@material-ui/core/styles/colorManipulator';
import { FuseAnimate } from '@fuse';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import JWTLoginTab from './tabs/JWTLoginTab';
import { makeStyles } from '@material-ui/styles';
import { TextFieldFormsy } from '@fuse';
import Formsy from 'formsy-react';
import * as authActions from 'app/auth/store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    root: {
        color: theme.palette.primary.contrastText
    }
}));

function ForgotPassword() {

    const { t } = useTranslation('customersApp');
    const classes = useStyles();

    const dispatch = useDispatch();
    const login = useSelector(({ auth }) => auth.login);

    const [isFormValid, setIsFormValid] = useState(false);
    const formRef = useRef(null);

    useEffect(() => {
        if (login.error && (login.error.email || login.error.password)) {
            formRef.current.updateInputsWithError({
                ...login.error
            });
            disableButton();
        }
    }, [login.error]);

    function disableButton() {
        setIsFormValid(false);
    }

    function enableButton() {
        setIsFormValid(true);
    }

    function handleSubmit(model) {
        dispatch(authActions.submitForgotPassword(model));
    }

    return (
        <div className={clsx(classes.root, "flex flex-col flex-1 flex-shrink-0 p-24 md:flex-row md:p-0")}>

            <div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left">

                <FuseAnimate animation="transition.expandIn">
                    <img className="w-400 mb-32" src="assets/images/logos/sloan_logo2.png" alt="logo" />
                </FuseAnimate>

                {/*
                <FuseAnimate animation="transition.slideUpIn" delay={300}>
                    <Typography variant="h4" className="font-light text-black">
                        Loans Management Software
                    </Typography>

                </FuseAnimate>
                 <FuseAnimate delay={400}>
                    <Typography variant="subtitle1" color="inherit" className="text-black max-w-512 mt-16">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper nisl erat, vel convallis elit fermentum pellentesque. Sed mollis velit
                        facilisis facilisis.
                    </Typography>
                </FuseAnimate> */}
            </div>

            <FuseAnimate animation={{ translateX: [0, '100%'] }}>

                <Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>

                    <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">

                        {/* <FuseAnimate animation="transition.expandIn">
                            <img className="w-150 mb-32" src="assets/images/logos/Logo_WEB.png" alt="logo" />
                        </FuseAnimate> */}

                        <Typography variant="h6" className="text-center md:w-full mb-48">{t("FORGOT_PASSWORD_HEADER")}</Typography>

                        <div className="w-full">

                            <Formsy
                                onValidSubmit={handleSubmit}
                                onValid={enableButton}
                                onInvalid={disableButton}
                                ref={formRef}
                                className="flex flex-col justify-center w-full"
                            >
                                <TextFieldFormsy
                                    className="mb-16"
                                    type="text"
                                    name="email"
                                    label={t("FORGOT_PASSWORD_USERNAME")}
                                    value=""
                                    // validations={{
                                    //     minLength: 4
                                    // }}
                                    // validationErrors={{
                                    //     minLength: 'La longueur minimale est de 4 caractères'
                                    // }}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end"><Icon className="text-20" color="action">email</Icon></InputAdornment>
                                    }}
                                    variant="outlined"
                                    required
                                />

                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    className="w-full mx-auto mt-16 normal-case"
                                    aria-label="LOG IN"
                                    disabled={!isFormValid}
                                    value="legacy"
                                >
                                    {t("FORGOT_PASSWORD_BUTTON")}
                                </Button>

                            </Formsy>
                        </div>

                        <div className="flex items-center justify-between pt-32 pb-24">
                            <Link className="font-medium" to="Login">
                                {t("RETURN_TO_LOGIN")}
                            </Link>
                        </div>

                    </CardContent>
                </Card>
            </FuseAnimate>
        </div>
    )
}

export default ForgotPassword;