const locale = {
    LOGIN_HEADER: "LOGIN",
    LOGIN_USERNAME: "Username",
    LOGIN_FORGOT_PASSWORD: "Forgot your password?",
    LOGIN_PASSWORD: "Password",
    LOGIN_BUTTON: "LOGIN",
}

export default locale;
