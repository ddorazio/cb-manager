const locale = {
    PASSWORD_HEADER: "Changer votre mot de passe",
    PASSWORD_TEXT: "texte",
    PASSWORD_CONFIRM: "Changer",
    NEW_PASSWORD:"Nouveau mot de passe",
    NEW_CONFIRM_PASSWORD:"Confirmer le nouveau mot de passe",
    VALIDATION_SAME:"Les mots de passe doivent être identiques",

    VALIDATION_MIN_LENGTH : "La longueur minimale est de 6 caractères",
}

export default locale;