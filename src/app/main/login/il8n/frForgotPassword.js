const locale = {
    FORGOT_PASSWORD_HEADER: "RECUPEREZ VOTRE MOT DE PASSE",
    FORGOT_PASSWORD_USERNAME: "Nom d'utilisateur",
    RETURN_TO_LOGIN: "Retourner à la connexion",
    FORGOT_PASSWORD_BUTTON: "ENVOYER"
}

export default locale;