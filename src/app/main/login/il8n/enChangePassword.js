const locale = {
    PASSWORD_HEADER: "Change your password",
    PASSWORD_TEXT: "text",
    PASSWORD_CONFIRM: "Change",
    NEW_PASSWORD:"New password",
    NEW_CONFIRM_PASSWORD:"Confirm your new password",
    VALIDATION_SAME:"Passwords do not match",
    
    VALIDATION_MIN_LENGTH : "The minimum length is 6 characters",
}

export default locale;
