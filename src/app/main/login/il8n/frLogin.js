const locale = {
    LOGIN_HEADER: "CONNECTEZ-VOUS",
    LOGIN_USERNAME: "Nom d'utilisateur",
    LOGIN_FORGOT_PASSWORD: "Mot de passe oublié?",
    LOGIN_PASSWORD: "Mot de passe",
    LOGIN_BUTTON: "CONNEXION"
}

export default locale;