const locale = {
    FORGOT_PASSWORD_HEADER: "RECOVER YOUR PASSWORD",
    FORGOT_PASSWORD_USERNAME: "Username",
    RETURN_TO_LOGIN: "Return to login",
    FORGOT_PASSWORD_BUTTON: "SUBMIT"
}

export default locale;
