import React, { useEffect, useRef, useState } from 'react'
import { Button, Typography, Icon, Dialog, DialogContent  } from '@material-ui/core';
import { FuseAnimate, FusePageSimple } from '@fuse';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { TextFieldFormsy } from '@fuse';
import Formsy from 'formsy-react';
import * as authActions from 'app/auth/store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';


import enChangePassword from './il8n/enChangePassword';
import frChangePassword from './il8n/frChangePassword';

i18next.addResourceBundle('en', 'customersApp', enChangePassword);
i18next.addResourceBundle('fr', 'customersApp', frChangePassword);

const useStyles = makeStyles(theme => ({
    root: {
        color: theme.palette.primary.contrastText
    },
    textField:{
        marginTop:"30px",
        width:"100%"
    },
    dialogContent:{
        padding:"0px 20px 15px 20px"
    },
    buttonsDialog: {
        width:"100%",
        display:"flex",
        justifyContent:"space-between",
        marginTop:"30px"
    }
}));

function ChangePassword(props) {

    const { t } = useTranslation('customersApp');
    const classes = useStyles();

    const dispatch = useDispatch();

    const [isFormValid, setIsFormValid] = useState(false);
    const formRef = useRef(null);

    const open = useSelector(({ auth }) => auth.changepassword.openDialog);

    const handleClose = () => {
        dispatch(authActions.closeChangePassword())
        props.closeUserMenu()
      };

    function disableButton() {
        setIsFormValid(false);
    }

    function enableButton() {
        setIsFormValid(true);
    }

    function handleSubmit(model) {
        dispatch(authActions.submitChangePassword(model.password));
    }

    return (
      <Dialog
        onClose={handleClose}
        aria-labelledby="simple-dialog-title"
        open={open}
        
      >
        <div className="flex flex-1 items-center justify-between p-8 sm:p-24">
          <div className="flex flex-shrink items-center sm:w-400">
            <div className="flex items-center">
              <FuseAnimate animation="transition.expandIn" delay={300}>
                <Icon className="text-32 mr-12">settings</Icon>
              </FuseAnimate>
              <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                <Typography variant="h6" className="hidden sm:flex">
                  {t("PASSWORD_HEADER")}
                </Typography>
              </FuseAnimate>
            </div>
          </div>
        </div>
        <DialogContent className={classes.dialogContent}>
          <Formsy
            onValidSubmit={handleSubmit}
            onValid={enableButton}
            onInvalid={disableButton}
            ref={formRef}
            className="flex flex-col justify-center w-full"
          >
            <TextFieldFormsy
              className={classes.textField}
              type="password"
              name="password"
              label={t("NEW_PASSWORD")}
              value="password"
              validations={{
                minLength: 6,
              }}
              validationErrors={{
                minLength: t("VALIDATION_MIN_LENGTH"),
              }}
              variant="outlined"
              required
              updateImmediately
            />

            <TextFieldFormsy
              className={classes.textField}
              type="password"
              name="passwordConfirm"
              label={t("NEW_CONFIRM_PASSWORD")}
              value="confirmPassword"
              validations={{
                minLength: 6,
                equalsField: "password",
              }}
              validationErrors={{
                minLength: t("VALIDATION_MIN_LENGTH"),
                equalsField: t("VALIDATION_SAME"),
              }}
              variant="outlined"
              required
              updateImmediately
            />
            <div className={classes.buttonsDialog}>
                <Button
                variant="contained"
                color="secondary"
                className="normal-case"
                aria-label="LOG IN"
                value="legacy"
                onClick={handleClose}
                >
                {t("CANCEL")}
                </Button>

                <Button
                type="submit"
                variant="contained"
                color="primary"
                className="normal-case"
                aria-label="LOG IN"
                disabled={!isFormValid}
                value="legacy"
                onClick={handleClose}
                >
                {t("PASSWORD_CONFIRM")}
                </Button>
            </div>
            
          </Formsy>
        </DialogContent>
      </Dialog>
    );
}

export default ChangePassword;