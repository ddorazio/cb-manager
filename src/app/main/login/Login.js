import React, { useState, useEffect } from 'react'
import { Card, CardContent, Typography, Tabs, Tab } from '@material-ui/core';
import { darken } from '@material-ui/core/styles/colorManipulator';
import { FuseAnimate } from '@fuse';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import JWTLoginTab from './tabs/JWTLoginTab';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import localStorageService from 'app/services/localStorageService';

const useStyles = makeStyles(theme => ({
    root: {
        color: theme.palette.primary.contrastText
    }
}));

function Login() {

    const { i18n } = useTranslation();
    const { t } = useTranslation('customersApp');
    const classes = useStyles();
    //const [selectedTab, setSelectedTab] = useState(0);

    // function handleTabChange(event, value) {
    //     setSelectedTab(value);
    // }

    const [currentLng, setLang] = useState({
        id: '',
        title: '',
        flag: ''
    });

    // Initial load
    useEffect(() => {
        var _language = localStorageService.getLanguage();

        if (_language === null) {

            // Get browser language
            if (navigator.language === 'en' || navigator.language === 'en-US' || navigator.language === 'en-CA') {
                setLang({
                    id: 'en',
                    title: 'English',
                    flag: 'us'
                });
                _language = "en";

            } else {
                setLang({
                    id: 'fr',
                    title: 'Français',
                    flag: 'canada'
                });
                _language = "fr";
            }

            // Change language
            i18n.changeLanguage(_language);
            localStorageService.setLanguage(_language);
        } else {

            if (_language == "fr") {
                setLang({
                    id: 'fr',
                    title: 'Français',
                    flag: 'canada'
                });
            }

            if (_language == "en") {
                setLang({
                    id: 'en',
                    title: 'English',
                    flag: 'us'
                });
            }

            // Change language
            i18n.changeLanguage(_language);
            localStorageService.setLanguage(_language);
        }
    }, []);


    return (
        <div className={clsx(classes.root, "flex flex-col items-center flex-1 flex-shrink-0 p-24 md:flex-row md:p-0 login-page")}>

            <div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-center md:flex-shrink-0 md:flex-1 md:text-center">

                <FuseAnimate animation="transition.expandIn">
                    <img className="w-400 mb-32" src="assets/images/logos/sloan_logo2.png" alt="logo" />
                </FuseAnimate>

                {/*
                <FuseAnimate animation="transition.slideUpIn" delay={300}>
                    <Typography variant="h4" className="font-light text-black">
                        Loans Management Software
                    </Typography>

                </FuseAnimate>
                 <FuseAnimate delay={400}>
                    <Typography variant="subtitle1" color="inherit" className="text-black max-w-512 mt-16">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper nisl erat, vel convallis elit fermentum pellentesque. Sed mollis velit
                        facilisis facilisis.
                    </Typography>
                </FuseAnimate> */}
            </div>

            <FuseAnimate animation={{ translateX: [0, '100%'] }}>

                <Card className="w-full h-full max-w-400 mx-auto m-16 md:m-0" square>

                    <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">

                        {/* <FuseAnimate animation="transition.expandIn">
                            <img className="w-150 mb-32" src="assets/images/logos/Logo_WEB.png" alt="logo" />
                        </FuseAnimate> */}

                        <Typography variant="h6" className="text-center md:w-full mb-48">{t("LOGIN_HEADER")}</Typography>

                        <JWTLoginTab />

                        <div className="flex items-center justify-between pt-32 pb-24">
                            <Link className="font-medium" to="forgotpassword">
                                {t("LOGIN_FORGOT_PASSWORD")}
                            </Link>
                        </div>

                    </CardContent>
                </Card>
            </FuseAnimate>
        </div>
    )
}

export default Login;