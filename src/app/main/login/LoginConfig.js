import Login from './Login';
import { authRoles } from 'app/auth';
import i18next from 'i18next';

import enLogin from './il8n/enLogin';
import frLogin from './il8n/frLogin';
import enForgotPassword from './il8n/enForgotPassword';
import frForgotPassword from './il8n/frForgotPassword';

i18next.addResourceBundle('en', 'customersApp', enLogin);
i18next.addResourceBundle('fr', 'customersApp', frLogin);
i18next.addResourceBundle('en', 'customersApp', enForgotPassword);
i18next.addResourceBundle('fr', 'customersApp', frForgotPassword);

export const LoginConfig = {
    settings: {
        layout: {
            config: {
                navbar: {
                    display: false
                },
                toolbar: {
                    display: false
                },
                footer: {
                    display: false
                },
                leftSidePanel: {
                    display: false
                },
                rightSidePanel: {
                    display: false
                }
            }
        }
    },
    auth: authRoles.onlyGuest,
    routes: [
        {
            path: '/login',
            component: Login
        }
    ]
};

