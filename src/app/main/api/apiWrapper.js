import localStorageService from 'app/services/localStorageService';
import i18n from 'i18n';

const authRequestHeader = () => ({
    "Authorization": "Bearer " + localStorageService.getAccessToken(),
});

const requestHeaders = () => ({
    "Language": localStorageService.getLanguage()
});

const requestHeadersContent = () => ({
    ...requestHeaders(),
    "Content-Type": "application/json",
});

const secureHeaders = () => ({
    ...authRequestHeader(),
    ...requestHeaders()
});

const secureHeadersContent = () => ({
    ...authRequestHeader(),
    ...requestHeadersContent(),
})

const handleResponse = async (response) => {
    if (response.status === 401) {
        window.location.replace('/login');
        return;
    }

    if (response.status >= 500) {
        throw i18n.t('UNEXPECTED_ERROR')
    }

    try {
        const result = await response.json();
        if (!response.ok) {
            throw result.message;
        } else {
            return result;
        }
    } catch {
        console.warn("No body in response")
        return 
    }
    
}

export const get = async (url, headers = requestHeaders()) => {
    const response = await fetch(url, {
        method: 'GET',
        headers: headers
    });
    return await handleResponse(response);
}

export const post = async (url, data, headers = requestHeaders(), isJSON = true) => {
    const response = await fetch(url, {
        method: 'POST',
        headers: isJSON ? headers : { ...headers, "Content-Type": "multipart/form-data" },
        body: isJSON ? JSON.stringify(data) : data
    });
    return await handleResponse(response);
}

export const put = async (url, data, headers = requestHeaders()) => {
    const response = await fetch(url, {
        method: 'PUT',
        headers: headers,
        body: JSON.stringify(data)
    });
    return await handleResponse(response);
}

export const del = async (url, headers = requestHeaders()) => {
    const response = await fetch(url, {
        method: 'DELETE',
        headers: headers
    });
    return await handleResponse(response);
}

export const getProtected = async (url) => await get(url, secureHeaders());
export const postProtected = async (url, data, isJSON = true) => post(url, data, secureHeadersContent(), isJSON);
export const putProtected = async (url, data) => put(url, data, secureHeadersContent());
export const deleteProtected = async (url) => del(url, secureHeaders());
