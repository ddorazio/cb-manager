const locale = {
    CLIENT_PROFILE: "Client Profile",
    LOAN: "Loan",
    NUMBER_DAYS_SINCE_PAYMENT: "Number of days since last payment",
    PLEASE_CHOOSE: "Please choose",
    REJECTION_OF_THE_DAY: "Rejections of the day",
    FOLLOW_UP: "Need follow up",
    TRANSFERS_RECEIVABLE: "Interac e-Transfer receivable to date",
    WAITING_RESPONSE: "Waiting response",
    AMOUNT: "Amount",
    COLLECTION_FILES: "Collection Files",
    STATUS: "Status",
    COLLECTION_DEGREE: "Collection degree",
    COLLECTION_STATUS: "Collection status",
    REASON_OF_NO_PAYMENT: "Reason of non-payment",
    NO_JOB: "No job",
    TOO_MUCH_LOANS: "Too much loan",
    TOO_MUCH_NSF: "Too many NSF",
    OTHER: "Other (see comments)",
    LAST_PAYMENT_DATE: "Last payment date",
    CHECK_STEPS: "Check the steps performed and leave a note:",
    CALL_CELLPHONE: "Call cellphone",
    CALL_WORK: "Call work",
    CALL_REFERENCE: "Call references",
    CHECK_IBV: "Check the IBV",
    CHECK_CREDITBOOK_CHECK: "Check CreditBook Check",
    FIND_FACEBOOK_PROFILE: "Find FaceBook profile",
    SELECT_AGREEMENT_WITH_CLIENT: "Select the agreement made with the client",
    MAKE_A_CHOICE: "Make a choice",
    NONE: "None",
    INTERAC_TRANSFER: "Interac e-Transfer",
    DEFER_PAYMENT: "Defer payment at the end",
    MAKE_PAYMENT_AGREEMENT: "Make payment agreement",
    EXPECTED_DATE_OF_TRANSFER: "EXPECTED Date of Transfer",
    ACTUAL_DATE_OF_TRANSFER: "ACTUAL Date of Transfer",
    CONFIRMATION_EMAIL: "Confirmation email",
    SEND_SMS: "Send an SMS reminder the same day",
    ADD_NOTE: "Add a note",
    WRITE_HERE: "Write here...",

    SINCE: "Since",
    SURNAME: "First Name",
    NAME: "Last Name",
    PHONE: "Phone",
    PHONE_CELL: "Cellphone",
    EMAIL: "Email",
    SAVE: "Save",
    SEND: "Send",
    UPDATE: "Update",
    LATEST_NOTE: "Latest Note",
    NO_COLLECTION_FILES: "No collections files yet",
    DAYS: "days",
    ALL_FILES: "All files",
    FOLLOW_UP_DATE: "Follow up date",
    SETTLED: 'Settled'
}

export default locale;