const locale = {
    NEW_IBV: "Faire une NOUVELLE DEMANDE",
    IBV_LIST: "Liste d'IBV",
    IBV_LIST_NO: "No.",
    IBV_LIST_CREATED_DATE: "Date création",
    IBV_LIST_STATUS: "Statut",
    IBV_LIST_REPORT: "Rapport",
    IBV_RESEND: 'Renvoi'
}

export default locale;