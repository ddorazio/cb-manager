const locale = {
    CREDIT_CHECK_CLIENT: "Client",
    CREDIT_CHECK_TEL: "Téléphone",
    CREDIT_CHECK_EMAIL: "Courriel",
    CREDIT_CHECK_DOB: "Date de naissance",
    CREDIT_CHECK_LANGUAGE: "Langue",
    CREDIT_CHECK_REQUEST_DATE: "Date de la demande",
    CREDIT_CHECK_LENDER: "Prêteur",
    CREDIT_CHECK_AMOUNT: "Montant",
    CREDIT_CHECK_COMMENT: "Commentaires",

    CREDIT_CHECK_ADDRESS: "Adresse",
    CREDIT_CHECK_CITY: "Ville",
    CREDIT_CHECK_COUNTRY: "Pays",
    CREDIT_CHECK_PROVINCE: "Province",
    CREDIT_CHECK_POSTAL_CODE: "Code postal",
    CREDIT_CHECK_SINCE: "Depuis",

    CREDIT_CHECK_EMPLOYER: "Nom de l'employeur",
    CREDIT_CHECK_WORK_TEL: "Téléphone",
    CREDIT_CHECK_SUPERVISOR: "Nom du superviseur",
    CREDIT_CHECK_OCCUPATION: "Occupation",
    CREDIT_CHECK_PAY_FREQ: "Fréquence de paie",
    CREDIT_CHECK_NEXT_PAY: "Prochaine date de paie",
    CREDIT_CHECK_START_DATE: "Date d'embauche",

    CREDIT_CHECK_REF_FNAME: "Prénom référence ",
    CREDIT_CHECK_REF_LNAME: "Nom référence ",
    CREDIT_CHECK_REF_TEL: "Téléphone référence ",
    CREDIT_CHECK_REF_EMAIL: "Courriel référence ",
    CREDIT_CHECK_REF_RELATIONSHIP: "Lien référence ",

    CREDIT_CHECK_CLOSE_BUTTON: "Fermer",
    CREDIT_CHECK_SUBMIT_BUTTON: "Mettre à jour"
}

export default locale;