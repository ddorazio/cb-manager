const locale = {
    LOAN_LIST: "Loans list",
    NEW_LOAN_BUTTON: "NEW LOAN",

    // New Loan Dialog
    NEW_LOAN_DIALOG_TITLE: "New Loan",
    NEW_LOAN_AMOUNT: "Loan amount",
    NEW_LOAN_FREQUENCY: "Payment frequency",
    DAY_ONE: "Day #1",
    DAY_TWO: "Day #2",
    NUMBER_OF_PAYMENTS: "Number of payments",
    NEXT_PAY_DATE: "Next pay date",
    NEW_LOAN_SAVE_BUTTON: "Save",
    NEW_LOAN_CANCEL_BUTTON: "Cancel",
    NEW_LOAN_REQUEST_NO: "Request NO",
    NEW_LOAN_NO_REQUESTS: "There are no pending requests",
    LOAN_REBATE: "Rebate",

    // Modify Loan Dialog
    MODIFY_LOAN_DIALOG_TITLE: "Modify loan",
    MODIFY_LOAN_AMOUNT_HEADER: "Loan amount",
    MODIFY_LOAN_FREQUENCY_HEADER: "Payment frequency",
    MODIFY_LOAN_BALANCE_HEADER: "Current balance",
    MODIFY_LOAN_CHANGE_FREQUENCY: "Change the frequency",
    MODIFY_LOAN_FROM_DATE: "New date",
    MODIFY_LOAN_DAY_ONE: "Day #1",
    MODIFY_LOAN_DAY_TWO: "Day #2",
    MODIFY_LOAN_CHANGE_AMOUNT: "Modify the payment",
    MODIFY_LOAN_NEW_AMOUNT: "New amount",
    MODIFY_LOAN_CHANGE_PAYMENT: "Stop payments",
    MODIFY_LOAN_SAVE_BUTTON: "Save",
    MODIFY_LOAN_CANCEL_BUTTON: "Cancel",
    MODIFY_LOAN_PAYMENT_AGREEMENT: "Payment agreement",
    MODIFY_LOAN_PAYMENT_PERCEPTECH: "Perceptech",
    MODIFY_LOAN_PAYMENT_INTERAC: "Interec",
    MODIFY_LOAN_FROM_PAYMENT: "Payment to modify",

    // Modify Loan Payment Dialog
    MODIFY_LOAN_PAYMENT_DIALOG_TITLE: "Payment information",
    MODIFY_LOAN_PAYMENT_ORIGINAL_DATE: "Original date :",
    MODIFY_LOAN_PAYMENT_CHANGE_DATE_LABEL: "Change date",
    MODIFY_LOAN_PAYMENT_CHANGE_DATE_TEXT: "New date",
    MODIFY_LOAN_PAYMENT_ORIGINAL_AMOUNT: "Original amount :",
    MODIFY_LOAN_PAYMENT_CHANGE_AMOUNT_LABEL: "Modify amount",
    MODIFY_LOAN_PAYMENT_CHANGE_AMOUNT_TEXT: "New amount",
    MODIFY_LOAN_PAYMENT_ORIGINAL_BALANCE: "Original balance :",
    MODIFY_LOAN_PAYMENT_NEW_BALANCE: "New balance :",
    MODIFY_LOAN_PAYMENT_SAVE_BUTTON: "Save",
    MODIFY_LOAN_PAYMENT_CANCEL_BUTTON: "Cancel",

    // Restart Loan
    RESTART_LOAN_DIALOG_TITLE: "Modifier loan",
    RESTART_LOAN_RESTART_PAYMENTS: "Restart payments:",
    RESTART_LOAN_FROM_DATE: "From",
    RESTART_LOAN_CHANGE_FREQUENCY: "Change the frequency:",
    RESTART_LOAN_DAY_ONE: "Day #1",
    RESTART_LOAN_DAY_TWO: "Day #2",
    RESTART_LOAN_CHANGE_AMOUNT: "Modify the payment:",
    RESTART_LOAN_NEW_AMOUNT: "New amount",
    RESTART_LOAN_SAVE_BUTTON: "Save",
    RESTART_LOAN_CANCEL_BUTTON: "Cancel",
    RESTART_LOAN_PAYMENT_AGREEMENT: "Payment agreement",
    RESTART_LOAN_PAYMENT_PERCEPTECH: "Perceptech",
    RESTART_LOAN_PAYMENT_INTERAC: "Interec",

    // Send contract
    CONTRACT_DIALOG_TITLE: "Send contract",
    CONTRACT_CHOOSE_TYPE: "Choose contract to send :",
    CONTRACT_BY_FAX: "Contract - By Fax",
    CONTRACT_BY_EMAIL: "Contract - By Email",
    CONTRACT_BY_ESIGN: "Contract - eSign",
    CONTRACT_EMAIL: "Email",
    CONTRACT_SUBMIT_BUTTON: "Submit",
    CONTRACT_CANCEL_BUTTON: "Cancel",
    CONTRACT_MISSING_BANKING_INFO: "Banking info is required",
    CONTRACT_MISSING_LANGUAGE: "Client language is required",

    // Send Loan PDF
    LOANPDF_DIALOG_TITLE: "Send a statement of account in pdf",
    LOANPDF_CHOOSE_TYPE: "Choose the type of account statement",
    LOANPDF_ACCOUNT_BALANCE: "Account balance",
    LOANPDF_ACCOUNT_DETAILS: "Account details",
    CANCEL: "Cancel",
    SEND: "Send",

    // Manual payment
    MANUAL_PAYMENT_DIALOG_TITLE: "New manual payment",
    MANUAL_PAYMENT_AMOUNT: "Payment amount",
    MANUAL_PAYMENT_PAYMENT_DATE: "Payment date",
    MANUAL_PAYMENT_SAVE_BUTTON: "Save",
    MANUAL_PAYMENT_CANCEL_BUTTON: "Cancel",

    // Rebate
    REBATE_DIALOG_TITLE: "New rebate",
    REBATE_DATE: "Rebate date",
    REBATE_AMOUNT: "Rebate amount",
    REBATE_SAVE_BUTTON: "Save",
    REBATE_CANCEL_BUTTON: "Cancel",

    // Loan List Header
    LIST_LOAN_NO: "No.",
    LIST_LOAN_AMOUNT: "Amount",
    LIST_LOAN_STATUS: "Status",
    LIST_LOAN_START_DATE: "Start date",
    LIST_LOAN_END_DATE: "End date",
    LIST_LOAN_FREQUENCY: "Frequency",
    LIST_LOAN_NO_PAYMENTS: "No of payments",
    LIST_LOAN_CURRENT_BALANCE: "Current balance",

    // Loan details
    LOAN_SUMMARY_HEADER: "Loan information",
    LOAN_SUMMARY_AMOUNT: "Loan amount",
    LOAN_SUMMARY_INTEREST: "Interest",
    LOAN_SUMMARY_BROKERAGE: "Brokerage fees",
    LOAN_SUMMARY_PAYMENT: "Initial payment amount",
    LOAN_SUMMARY_FREQUENCY: "Frequency",
    LOAN_SUMMARY_CREATED_DATE: "Creation date",
    LOAN_INITIAL_NUMBER_PAYMENTS: "Initial number of payments",
    LOAN_SUMMARY_NO_NSF: "NSF payments",
    LOAN_SUMMARY_FEES: "Fees",
    LOAN_SUMMARY_DAILY_BALANCE: "Current balance",
    LOAN_SUMMARY_REBATE: "Rebates",
    LOAN_SUMMARY_TOTAL: "Total owing",
    LOAN_SUMMARY_AGENT: "Agent",
    LOAN_CONTRATC_SIGNED: "Contract signed on",

    // Loan details
    LOAN_DETAIL_NO: "No.",
    LOAN_DETAIL_DATE: "Date",
    LOAN_DETAIL_AMOUNT: "Amount",
    LOAN_DETAIL_INTEREST: "Interest",
    LOAN_DETAIL_CAPITAL: "Capital",
    LOAN_DETAIL_BALANCE: "Balance",
    LOAN_SUB_FEES: "Membership",
    LOAN_DETAIL_STATUS: "Status",
    LOAN_DETAIL_DESCRIPTION: "Description",
    LOAN_DETAIL_ACTIONS: "Actions",
    LOAN_DETAIL_REBATE_BUTTON: "REBATE",
    LOAN_DETAIL_PAYMENT_BUTTON: "PAYMENT",

    BEFORE_MEMBERSHIP: "This loan was created prior to the addition of Membership Fees",
    BEFORE_PROVIDER: "This loan was created before automated payments was activated",
    MODIFY_LOAN: "Modify loan",
    SEND_CONTRACT: "Send contract",
    MANUAL_PAYMENT: "New manual payment",
    NEW_REBATE: "New rebate",
    START_LOAN: "Start Loan",
    DELETE_LOAN: "Delete",
    ACCOUNT_STATEMENT_PDF: "Account statement",
    STOPPED_LOAN_PAYMENT: "Add payment",

    // NSF confirm Dialog
    NSF_CONFIRM_HEADER: "Confirmation",
    NSF_CONFIRM_MESSAGE: "Are you sure you want to apply a NSF payment?",
    NSF_CONFIRM_CANCEL: "Cancel",
    NSF_CONFIRM_CONFIRM: "Confirm",

    // NSF remove confirm Dialog
    NSF_REMOVE_CONFIRM_HEADER: "Confirmation",
    NSF_REMOVE_CONFIRM_MESSAGE: "Are you sure you want to remove the NSF payment?",
    NSF_REMOVE_CONFIRM_CANCEL: "Cancel",
    NSF_REMOVE_CONFIRM_CONFIRM: "Confirm",

    // Rebate remove confirm Dialog
    REBATE_REMOVE_CONFIRM_HEADER: "Confirmation",
    REBATE_REMOVE_CONFIRM_MESSAGE: "Are you sure you want to remove the Rebate?",
    REBATE_REMOVE_CONFIRM_CANCEL: "Cancel",
    REBATE_REMOVE_CONFIRM_CONFIRM: "Confirm",

    // Payment remove confirm Dialog
    PAYMENT_REMOVE_CONFIRM_HEADER: "Confirmation",
    PAYMENT_REMOVE_CONFIRM_MESSAGE: "Are you sure you want to remove the Payment?",
    PAYMENT_REMOVE_CONFIRM_CANCEL: "Cancel",
    PAYMENT_REMOVE_CONFIRM_CONFIRM: "Confirm",

    // Payment update confirm Dialog
    UPDATE_LOAN_STATUS_CONFIRM_HEADER: "Confirmation",
    UPDATE_LOAN_STATUS_CONFIRM_MESSAGE: "Are you sure you want to start this loan?",
    UPDATE_LOAN_STATUS_CONFIRM_CANCEL: "Cancel",
    UPDATE_LOAN_STATUS_CONFIRM_CONFIRM: "Confirm",

    // NSF remove confirm Dialog
    NEW_LOAN_NO_REQUESTS_HEADER: "Message",
    NEW_LOAN_NO_REQUESTS_CANCEL: "Close",
    NEW_LOAN_NO_REQUESTS_MESSAGE: "A loan can not be created. All requests already have loans associated to them or the status of the requests have not been updated.",

    // Defer Loan Payment Dialog
    DEFER_LOAN_PAYMENT_DIALOG_TITLE: "Defer payment",
    DEFER_LOAN_PAYMENT_CHANGE_DATE_TEXT: "New date",
    DEFER_LOAN_PAYMENT_CANCEL_BUTTON: "Cancel",

    DEFER_LOAN_PAYMENT_CONFIRM_HEADER: "Confirmation",
    DEFER_LOAN_PAYMENT_CONFIRM_MESSAGE: "Are you sure you want to defer this payment?",
    DEFER_LOAN_PAYMENT_CONFIRM_BUTTON: "Confirm",

    // Delete loan confirm Dialog
    DELETE_LOAN_STATUS_CONFIRM_HEADER: "Confirmation",
    DELETE_LOAN_STATUS_CONFIRM_MESSAGE: "Are you sure you want to delete this loan?",
    DELETE_LOAN_STATUS_CONFIRM_CANCEL: "Cancel",
    DELETE_LOAN_STATUS_CONFIRM_CONFIRM: "Confirm",

    // Stopped Loan Payment
    STOPPED_LOAN_PAYMENT_TITLE: "New payment",
    STOPPED_LOAN_PAYMENT_DATE: "Payment date",
    STOPPED_LOAN_PAYMENT_AMOUNT: "Payment amount",
    STOPPED_LOAN_PAYMENT_SAVE_BUTTON: "Save",
    STOPPED_LOAN_PAYMENT_CANCEL_BUTTON: "Cancel",
}

export default locale;
