const locale = {
    EXTERNAL_LOAN_HEADER: "Demandes sur les sites de prêteurs affiliés",
    EXTERNAL_LOAN_LISTE_DATE: "Date",
    EXTERNAL_LOAN_LISTE_SITE: "Prêteur",
    EXTERNAL_LOAN_LISTE_CLIENT: "Client",
    EXTERNAL_LOAN_LISTE_EMAIL: "Courriel",
    EXTERNAL_LOAN_LISTE_STATUS: "Statut",
    EXTERNAL_LOAN_REASON: "Raison",
    EXTERNAL_LOAN_NO_RESULTS_FOUND: "Aucune demandes trouvées.",

    SEARCH_CREDITBOOK_CHECK: "Synchroniser",

    REQUEST_HEADER: "Demandes",
    LOAN_AMOUNT: "Montant",
    LOAN_REQUESTED_AMOUNT: "Montant demandé",
    LOAN_APPROVED_AMOUNT: "Montant approuvé",
    LOAN_LIST_CREATED_DATE: "Date de création",
    LOAN_LIST_NO_RESULTS_FOUND: "Aucune demandes trouvées.",

    STATUSCODE_PREAPPROVED_DIALOG: "Entrer le montant pour un prêt pré-approuvé",
    STATUSCODE_ACCEPTED_DIALOG: "Entrer le montant pour un prêt acccepté",
    CANCEL_LOAN_DIALOG : "Annuler",
    SAVE_LOAN_DIALOG : "Sauvegarder",
    NO_APPROVED_LOAN : "Aucun montant",

    STATUS: "Statut",
    CHANGE_STATUS: "Modifier le statut",
    CHECKLIST_VALIDATE: "Valider",
    CHECKLIST_COMPLETED: "Terminé"
}

export default locale;