const locale = {

    DROPZONE_TEXT: "Drop your file or click",
    DOCUMENT_NAME: "Name",
    LOAN_NUMBER: "Loan No.",
    DOCUMENT_TYPE: 'Document type',
    STATUS: 'Status',
    DEPOSIT_DATE: 'Deposit Date',
    ADD_DOCUMENT: 'Add a document',
    DOCUMENTS_LIST: 'Documents List',
    UPLOAD_DOCUMENT: 'Upload a new document',
    CHOOSE_LOAN: 'Choose the related loan',
    CHOOSE_TYPE: 'Choose the document type',
    ASSOCIATED_LOAN: 'Associated loan',
    EDIT_DOCUMENT: 'Edit a document',
    CANCEL: "Cancel",
    SAVE: "Save"
}


export default locale;