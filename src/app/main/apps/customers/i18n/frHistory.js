const locale = {
    HISTORY_HEADER: "Historique",
    HISTORY_LISTE_DATE: "Date",
    HISTORY_LISTE_DESCRIPTION: "Description",
    HISTORY_LISTE_CREATOR : "Créateur",
    HISTORY_NONE_FOUND: "Aucune historique"
}

export default locale;