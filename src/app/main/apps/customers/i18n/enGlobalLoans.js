const locale = {
    EXTERNAL_LOAN_HEADER: "Requests on affiliated lender sites",
    EXTERNAL_LOAN_LISTE_DATE: "Date",
    EXTERNAL_LOAN_LISTE_SITE: "Loaner",
    EXTERNAL_LOAN_LISTE_CLIENT: "Client",
    EXTERNAL_LOAN_LISTE_EMAIL: "Email",
    EXTERNAL_LOAN_LISTE_STATUS: "Status",
    EXTERNAL_LOAN_REASON: "Reason",
    EXTERNAL_LOAN_NO_RESULTS_FOUND: "No requests found.",

    SEARCH_CREDITBOOK_CHECK: "Sync Data",

    REQUEST_HEADER: "Requests",
    LOAN_AMOUNT: "Amount",
    LOAN_REQUESTED_AMOUNT: "Amount requested",
    LOAN_APPROVED_AMOUNT: "Approved amount",
    LOAN_LIST_CREATED_DATE: "Created date",
    LOAN_LIST_NO_RESULTS_FOUND: "No requests found.",


    STATUSCODE_PREAPPROVED_DIALOG: "Enter the amount for an pre-approved loan",
    STATUSCODE_ACCEPTED_DIALOG: "Enter the amount for an accepted loan",
    CANCEL_LOAN_DIALOG : "Cancel",
    SAVE_LOAN_DIALOG : "Save",
    NO_APPROVED_LOAN : "No amount",

    STATUS: "Status",
    CHANGE_STATUS: "Change status",
    CHECKLIST_VALIDATE: "Validate",
    CHECKLIST_COMPLETED: "Completed"
}

export default locale;