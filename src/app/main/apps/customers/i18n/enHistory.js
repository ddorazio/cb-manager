const locale = {
    HISTORY_HEADER: "Logs",
    HISTORY_LISTE_DATE: "Date",
    HISTORY_LISTE_DESCRIPTION: "Description",
    HISTORY_LISTE_CREATOR : "Creator",
    HISTORY_NONE_FOUND: "No logs",
}

export default locale;