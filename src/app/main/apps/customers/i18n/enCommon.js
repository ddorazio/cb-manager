const locale = {
    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "of",
    REACT_TABLE_ROWS_TEXT: "rows",
}

export default locale;