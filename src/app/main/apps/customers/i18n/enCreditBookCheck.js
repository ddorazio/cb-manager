const locale = {
    CREDIT_CHECK_CLIENT: "Client",
    CREDIT_CHECK_TEL: "Telephone",
    CREDIT_CHECK_EMAIL: "Email",
    CREDIT_CHECK_DOB: "Date of birth",
    CREDIT_CHECK_LANGUAGE: "Language",
    CREDIT_CHECK_REQUEST_DATE: "Request Date",
    CREDIT_CHECK_LENDER: "Lender",
    CREDIT_CHECK_AMOUNT: "Amount",
    CREDIT_CHECK_COMMENT: "Comments",

    CREDIT_CHECK_ADDRESS: "Address",
    CREDIT_CHECK_CITY: "City",
    CREDIT_CHECK_COUNTRY: "Country",
    CREDIT_CHECK_PROVINCE: "Province",
    CREDIT_CHECK_POSTAL_CODE: "Postal Code",
    CREDIT_CHECK_SINCE: "Since",

    CREDIT_CHECK_EMPLOYER: "Name of Employeur",
    CREDIT_CHECK_WORK_TEL: "Telephone",
    CREDIT_CHECK_SUPERVISOR: "Supervisor Name",
    CREDIT_CHECK_OCCUPATION: "Title",
    CREDIT_CHECK_PAY_FREQ: "Pay Frequency",
    CREDIT_CHECK_NEXT_PAY: "Next Pay Date",
    CREDIT_CHECK_START_DATE: "Hiring Date",

    CREDIT_CHECK_REF_FNAME: "First Name reference ",
    CREDIT_CHECK_REF_LNAME: "Last Name reference ",
    CREDIT_CHECK_REF_TEL: "Telephone reference ",
    CREDIT_CHECK_REF_EMAIL: "Email reference ",
    CREDIT_CHECK_REF_RELATIONSHIP: "Relationship reference ",

    CREDIT_CHECK_CLOSE_BUTTON: "Close",
    CREDIT_CHECK_SUBMIT_BUTTON: "Update"
}

export default locale;