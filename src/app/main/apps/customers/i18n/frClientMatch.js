const locale = {
    MATCH_FNAME: "Prénom",
    MATCH_LNAME: "Nom",
    MATCH_CELL: "Cellulaire",
    MATCH_EMAIL: "Courriel",
    MATCH_DOB: "Date de naissance",
    MATCH_CONFIRM: "Fusionner",
    MATCH_PROFILE: "Voir profile",

    MATCH_CONFIRM_HEADER: "Confirmation",
    MATCH_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir fusionner ce profil?",
    MATCH_CONFIRM_CANCEL: "Annuler",
    MATCH_CONFIRM_CONFIRM: "Confirmer"
}

export default locale;