const locale = {
    LOAN_LIST: "Liste de Prêts",
    NEW_LOAN_BUTTON: "NOUVEAU PRÊT",

    // New Loan Dialog
    NEW_LOAN_DIALOG_TITLE: "Nouveau Prêt",
    NEW_LOAN_AMOUNT: "Montant du Prêt",
    NEW_LOAN_FREQUENCY: "Fréquence des paiements",
    DAY_ONE: "Jour #1",
    DAY_TWO: "Jour #2",
    NUMBER_OF_PAYMENTS: "Nombre de paiements",
    NEXT_PAY_DATE: "Date de la prochaine paie",
    NEW_LOAN_SAVE_BUTTON: "Enregistrer",
    NEW_LOAN_CANCEL_BUTTON: "Annuler",
    NEW_LOAN_REQUEST_NO: "No de demande ",
    NEW_LOAN_NO_REQUESTS: "Il n'y a aucune demande en attente",
    LOAN_REBATE: "Rabais",

    // Modify Loan Dialog
    MODIFY_LOAN_DIALOG_TITLE: "Modifier le prêt",
    MODIFY_LOAN_AMOUNT_HEADER: "Montant du prêt",
    MODIFY_LOAN_FREQUENCY_HEADER: "Fréquence de paiement",
    MODIFY_LOAN_BALANCE_HEADER: "Solde en cours",
    MODIFY_LOAN_CHANGE_FREQUENCY: "Changer la fréquence",
    MODIFY_LOAN_FROM_DATE: "Nouvelle date",
    MODIFY_LOAN_DAY_ONE: "Jour #1",
    MODIFY_LOAN_DAY_TWO: "Jour #2",
    MODIFY_LOAN_CHANGE_AMOUNT: "Modifier le paiement",
    MODIFY_LOAN_NEW_AMOUNT: "Nouveau montant",
    MODIFY_LOAN_CHANGE_PAYMENT: "Arrêter les paiements",
    MODIFY_LOAN_SAVE_BUTTON: "Enregistrer",
    MODIFY_LOAN_CANCEL_BUTTON: "Annuler",
    MODIFY_LOAN_PAYMENT_AGREEMENT: "Entente de paiement",
    MODIFY_LOAN_PAYMENT_PERCEPTECH: "Perceptech",
    MODIFY_LOAN_PAYMENT_INTERAC: "Interec",
    MODIFY_LOAN_FROM_PAYMENT: "Paiement à modidier",

    // Modify Loan Payment Dialog
    MODIFY_LOAN_PAYMENT_DIALOG_TITLE: "Informations sur le paiement",
    MODIFY_LOAN_PAYMENT_ORIGINAL_DATE: "Date originale :",
    MODIFY_LOAN_PAYMENT_CHANGE_DATE_LABEL: "Changer la date",
    MODIFY_LOAN_PAYMENT_CHANGE_DATE_TEXT: "Changement de date",
    MODIFY_LOAN_PAYMENT_ORIGINAL_AMOUNT: "Montant original :",
    MODIFY_LOAN_PAYMENT_CHANGE_AMOUNT_LABEL: "Modifier le montant",
    MODIFY_LOAN_PAYMENT_CHANGE_AMOUNT_TEXT: "Nouveau montant",
    MODIFY_LOAN_PAYMENT_ORIGINAL_BALANCE: "Solde original :",
    MODIFY_LOAN_PAYMENT_NEW_BALANCE: "Nouveau Solde :",
    MODIFY_LOAN_PAYMENT_SAVE_BUTTON: "Enregistrer",
    MODIFY_LOAN_PAYMENT_CANCEL_BUTTON: "Annuler",

    // Restart Loan
    RESTART_LOAN_DIALOG_TITLE: "Modifier le prêt",
    RESTART_LOAN_RESTART_PAYMENTS: "Redémarrer les paiements:",
    RESTART_LOAN_FROM_DATE: "À partir du",
    RESTART_LOAN_CHANGE_FREQUENCY: "Changer la fréquence:",
    RESTART_LOAN_DAY_ONE: "Jour #1",
    RESTART_LOAN_DAY_TWO: "Jour #2",
    RESTART_LOAN_CHANGE_AMOUNT: "Modifier le paiement:",
    RESTART_LOAN_NEW_AMOUNT: "Nouveau montant",
    RESTART_LOAN_SAVE_BUTTON: "Enregistrer",
    RESTART_LOAN_CANCEL_BUTTON: "Annuler",
    RESTART_LOAN_PAYMENT_AGREEMENT: "Entente de paiement",
    RESTART_LOAN_PAYMENT_PERCEPTECH: "Perceptech",
    RESTART_LOAN_PAYMENT_INTERAC: "Interec",

    // Send contract
    CONTRACT_DIALOG_TITLE: "Envoyer contrat",
    CONTRACT_CHOOSE_TYPE: "Choisissez le contrat à envoyer :",
    CONTRACT_BY_FAX: "Contrat - Par Fax",
    CONTRACT_BY_EMAIL: "Contrat - Par Email",
    CONTRACT_BY_ESIGN: "Contrat - eSign",
    CONTRACT_EMAIL: "Courriel",
    CONTRACT_SUBMIT_BUTTON: "Envoyer",
    CONTRACT_CANCEL_BUTTON: "Annuler",
    CONTRACT_MISSING_BANKING_INFO: "Les informations bancaires sont obligatoires",
    CONTRACT_MISSING_LANGUAGE: "La langue du clietn est obligatoire",

    // Send Loan PDF
    LOANPDF_DIALOG_TITLE: "Envoyer un état de compte en pdf",
    LOANPDF_CHOOSE_TYPE: "Choisissez le type d'état de compte",
    LOANPDF_ACCOUNT_BALANCE: "Solde du compte",
    LOANPDF_ACCOUNT_DETAILS: "Détails du compte",
    CANCEL: "Annuler",
    SEND: "Envoyer",

    // Manual payment
    MANUAL_PAYMENT_DIALOG_TITLE: "Nouveau paiement manuel",
    MANUAL_PAYMENT_AMOUNT: "Montant du paiement",
    MANUAL_PAYMENT_PAYMENT_DATE: "Date du paiement",
    MANUAL_PAYMENT_SAVE_BUTTON: "Enregistrer",
    MANUAL_PAYMENT_CANCEL_BUTTON: "Annuler",

    // Rebate
    REBATE_DIALOG_TITLE: "Nouveau rabais",
    REBATE_DATE: "Date du rabais",
    REBATE_AMOUNT: "Montant du rabais",
    REBATE_SAVE_BUTTON: "Enregistrer",
    REBATE_CANCEL_BUTTON: "Annuler",

    // Loan List Header
    LIST_LOAN_NO: "No.",
    LIST_LOAN_AMOUNT: "Montant",
    LIST_LOAN_STATUS: "Statut",
    LIST_LOAN_START_DATE: "Date début",
    LIST_LOAN_END_DATE: "Date fin",
    LIST_LOAN_FREQUENCY: "Fréquence",
    LIST_LOAN_NO_PAYMENTS: "No de paiements",
    LIST_LOAN_CURRENT_BALANCE: "Solde en cours",

    // Loan summary
    LOAN_SUMMARY_HEADER: "Informations du Prêt",
    LOAN_SUMMARY_AMOUNT: "Montant du prêt",
    LOAN_SUMMARY_INTEREST: "Intérêts",
    LOAN_SUMMARY_BROKERAGE: "Frais de courtage",
    LOAN_SUMMARY_PAYMENT: "Paiement initial",
    LOAN_SUMMARY_FREQUENCY: "Fréquence",
    LOAN_SUMMARY_CREATED_DATE: "Date de création",
    LOAN_INITIAL_NUMBER_PAYMENTS: "Nombre de paiements initial",
    LOAN_SUMMARY_NO_NSF: "Paiements en défaut",
    LOAN_SUMMARY_FEES: "Frais",
    LOAN_SUMMARY_DAILY_BALANCE: "Solde en cours",
    LOAN_SUMMARY_REBATE: "Rabais",
    LOAN_SUMMARY_TOTAL: "Total dû",
    LOAN_SUMMARY_AGENT: "Agent",
    LOAN_CONTRATC_SIGNED: "Contrat signé le",

    // Loan details
    LOAN_DETAIL_NO: "No.",
    LOAN_DETAIL_DATE: "Date",
    LOAN_DETAIL_AMOUNT: "Montant",
    LOAN_DETAIL_INTEREST: "Intérêt",
    LOAN_DETAIL_CAPITAL: "Capital",
    LOAN_DETAIL_BALANCE: "Solde",
    LOAN_SUB_FEES: "Abonnement",
    LOAN_DETAIL_STATUS: "Statut",
    LOAN_DETAIL_DESCRIPTION: "Description",
    LOAN_DETAIL_ACTIONS: "Actions",
    LOAN_DETAIL_REBATE_BUTTON: "RABAIS",
    LOAN_DETAIL_PAYMENT_BUTTON: "PAIEMENT",

    BEFORE_MEMBERSHIP: "Ce prêt a été créé avant l'ajout des frais d'abonnements",
    BEFORE_PROVIDER: "Ce prêt a été créé avant l'activation des paiements automatisés",
    MODIFY_LOAN: "Modifier le prêt",
    SEND_CONTRACT: "Envoyer le contrat",
    MANUAL_PAYMENT: "Nouveau paiement manuel",
    NEW_REBATE: "Nouveau rabais",
    START_LOAN: "Démarrer le prêt",
    DELETE_LOAN: "Supprimer",
    ACCOUNT_STATEMENT_PDF: "État de compte",
    STOPPED_LOAN_PAYMENT: "Ajouter un paiement",

    // NSF confirm Dialog
    NSF_CONFIRM_HEADER: "Confirmation",
    NSF_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir ajouter un paiement NSF?",
    NSF_CONFIRM_CANCEL: "Annuler",
    NSF_CONFIRM_CONFIRM: "Confirmer",

    // NSF remove confirm Dialog
    NSF_REMOVE_CONFIRM_HEADER: "Confirmation",
    NSF_REMOVE_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir enlever le paiement NSF?",
    NSF_REMOVE_CONFIRM_CANCEL: "Annuler",
    NSF_REMOVE_CONFIRM_CONFIRM: "Confirmer",

    // Rebate remove confirm Dialog
    REBATE_REMOVE_CONFIRM_HEADER: "Confirmation",
    REBATE_REMOVE_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir enlever le Rabais?",
    REBATE_REMOVE_CONFIRM_CANCEL: "Annuler",
    REBATE_REMOVE_CONFIRM_CONFIRM: "Confirmer",

    // Payment remove confirm Dialog
    PAYMENT_REMOVE_CONFIRM_HEADER: "Confirmation",
    PAYMENT_REMOVE_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir enlever le paiement?",
    PAYMENT_REMOVE_CONFIRM_CANCEL: "Annuler",
    PAYMENT_REMOVE_CONFIRM_CONFIRM: "Confirmer",

    // Update loan status confirm Dialog
    UPDATE_LOAN_STATUS_CONFIRM_HEADER: "Confirmation",
    UPDATE_LOAN_STATUS_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir démarrer ce prêt?",
    UPDATE_LOAN_STATUS_CONFIRM_CANCEL: "Annuler",
    UPDATE_LOAN_STATUS_CONFIRM_CONFIRM: "Confirmer",

    // No requests Dialog
    NEW_LOAN_NO_REQUESTS_HEADER: "Message",
    NEW_LOAN_NO_REQUESTS_CANCEL: "Fermer",
    NEW_LOAN_NO_REQUESTS_MESSAGE: "Un prêt ne peut pas être créé. Toutes les demandes sont déjà associés à des prêts ou le statut des demandes n'a pas été mis à jour.",

    // Defer Loan Payment Dialog
    DEFER_LOAN_PAYMENT_DIALOG_TITLE: "Reporter paiement",
    DEFER_LOAN_PAYMENT_CHANGE_DATE_TEXT: "Changement de date",
    DEFER_LOAN_PAYMENT_CANCEL_BUTTON: "Annuler",

    DEFER_LOAN_PAYMENT_CONFIRM_HEADER: "Confirmation",
    DEFER_LOAN_PAYMENT_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir reporter ce paiement?",
    DEFER_LOAN_PAYMENT_CONFIRM_BUTTON: "Confirmer",

    // Delete loan confirm Dialog
    DELETE_LOAN_STATUS_CONFIRM_HEADER: "Confirmation",
    DELETE_LOAN_STATUS_CONFIRM_MESSAGE: "Êtes-vous sûr de vouloir supprimer ce prêt?",
    DELETE_LOAN_STATUS_CONFIRM_CANCEL: "Annuler",
    DELETE_LOAN_STATUS_CONFIRM_CONFIRM: "Confirmer",

    // Stopped Loan Payment
    STOPPED_LOAN_PAYMENT_TITLE: "Nouveau paiement",
    STOPPED_LOAN_PAYMENT_DATE: "Date de paiement",
    STOPPED_LOAN_PAYMENT_AMOUNT: "Montant du paiement",
    STOPPED_LOAN_PAYMENT_SAVE_BUTTON: "Confirmer",
    STOPPED_LOAN_PAYMENT_CANCEL_BUTTON: "Annuler",
}

export default locale;
