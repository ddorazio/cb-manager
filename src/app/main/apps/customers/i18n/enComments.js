const locale = {
    COMMENTS_HEADER: "Comments",
    COMMENTS_SIDEBAR_ALL: "View All",
    COMMENTS_SIDEBAR_CATEGORIES: "Categories",
    COMMENTS_SIDEBAR_ARCHIVE: "Archive",

    COMMENTS_ADD_NEW: "Add a comment",
    COMMENTS_NONE_FOUND: "There are no comments available."
}

export default locale;