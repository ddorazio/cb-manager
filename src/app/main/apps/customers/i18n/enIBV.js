const locale = {
    NEW_IBV: "Create a NEW REQUEST",
    IBV_LIST: "IBV List",
    IBV_LIST_NO: "No.",
    IBV_LIST_CREATED_DATE: "Created date",
    IBV_LIST_STATUS: "Status",
    IBV_LIST_REPORT: "Report",
    IBV_RESEND: 'Resend',
}

export default locale;