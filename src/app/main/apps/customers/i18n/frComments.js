const locale = {
    COMMENTS_HEADER: "Commentaires",
    COMMENTS_SIDEBAR_ALL: "Voir tout",
    COMMENTS_SIDEBAR_CATEGORIES: "Catégories",
    COMMENTS_SIDEBAR_ARCHIVE: "Archive",

    COMMENTS_ADD_NEW: "Ajoutez un commentaire",
    COMMENTS_NONE_FOUND: "Il n'y a aucun commentaire pour l'instant."
}

export default locale;