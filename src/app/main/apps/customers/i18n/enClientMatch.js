const locale = {
    MATCH_FNAME: "First Name",
    MATCH_LNAME: "Last Name",
    MATCH_CELL: "Cellphone",
    MATCH_EMAIL: "Email",
    MATCH_DOB: "Date of birth",
    MATCH_CONFIRM: "Merge",
    MATCH_PROFILE: "View profile",

    MATCH_CONFIRM_HEADER: "Confirmation",
    MATCH_CONFIRM_MESSAGE: "Are you sure you want to merge this profile?",
    MATCH_CONFIRM_CANCEL: "Cancel",
    MATCH_CONFIRM_CONFIRM: "Confirm"
}

export default locale;