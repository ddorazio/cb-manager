const locale = {

    DROPZONE_TEXT: "Déposez un fichier ici ou cliquez",
    DOCUMENT_NAME: "Nom du document",
    LOAN_NUMBER: "No. Prêt",
    DOCUMENT_TYPE: "Type de document",
    STATUS: "Statut",
    DEPOSIT_DATE: "Date de dépot",
    ADD_DOCUMENT: 'Ajouter un document',
    DOCUMENTS_LIST: ' Listes des Documents',
    UPLOAD_DOCUMENT: 'Ajouter un document',
    CHOOSE_LOAN: 'Choisissez le prêt associé',
    CHOOSE_TYPE: 'Choississez le type de document',
    ASSOCIATED_LOAN: 'Prêt associé',
    EDIT_DOCUMENT: 'Modifier un document',
    CANCEL: "Annuler",
    SAVE: "Enregistrer"

}

export default locale;