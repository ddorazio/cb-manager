import React from 'react';
import { Redirect } from 'react-router-dom';
import i18next from 'i18next';
import enCustomer from './i18n/enCustomer';
import frCustomer from './i18n/frCustomer';
import enIBV from './i18n/enIBV';
import frIBV from './i18n/frIBV';
import enLoans from './i18n/enLoans';
import frLoans from './i18n/frLoans';
import enDocuments from './i18n/enDocuments';
import frDocuments from './i18n/frDocuments';
import enComments from './i18n/enComments';
import frComments from './i18n/frComments';
import enHistory from './i18n/enHistory';
import frHistory from './i18n/frHistory';
import enGlobalLoans from './i18n/enGlobalLoans';
import frGlobalLoans from './i18n/frGlobalLoans';
import enCollections from './i18n/enCollections';
import frCollections from './i18n/frCollections';
import enCommon from './i18n/enCommon';
import frCommon from './i18n/frCommon';
import enCreditBookCheck from './i18n/enCreditBookCheck';
import frCreditBookCheck from './i18n/frCreditBookCheck';
import enClientMatch from './i18n/enClientMatch';
import frClientMatch from './i18n/frClientMatch';

i18next.addResourceBundle('en', 'customersApp', enCustomer);
i18next.addResourceBundle('fr', 'customersApp', frCustomer);
i18next.addResourceBundle('en', 'customersApp', enLoans);
i18next.addResourceBundle('fr', 'customersApp', frLoans);
i18next.addResourceBundle('en', 'customersApp', enIBV);
i18next.addResourceBundle('fr', 'customersApp', frIBV);
i18next.addResourceBundle('en', 'customersApp', enDocuments);
i18next.addResourceBundle('fr', 'customersApp', frDocuments);
i18next.addResourceBundle('en', 'customersApp', enComments);
i18next.addResourceBundle('fr', 'customersApp', frComments);
i18next.addResourceBundle('en', 'customersApp', enHistory);
i18next.addResourceBundle('fr', 'customersApp', frHistory);
i18next.addResourceBundle('en', 'customersApp', enGlobalLoans);
i18next.addResourceBundle('fr', 'customersApp', frGlobalLoans);
i18next.addResourceBundle('en', 'customersApp', enCollections);
i18next.addResourceBundle('fr', 'customersApp', frCollections);
i18next.addResourceBundle('en', 'customersApp', enCommon);
i18next.addResourceBundle('fr', 'customersApp', frCommon);
i18next.addResourceBundle('en', 'customersApp', enCreditBookCheck);
i18next.addResourceBundle('fr', 'customersApp', frCreditBookCheck);
i18next.addResourceBundle('en', 'customersApp', enClientMatch);
i18next.addResourceBundle('fr', 'customersApp', frClientMatch);

export const CustomersAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes: [
        /*
           {
               path     : '/apps/customers/detail/:customerId/place/:placeId/:placeHandle?',
               component: React.lazy(() => import('./customer-detail/place/Place'))
           },  */
        {
            path: '/apps/customers/detail/:customerId/:customerHandle?',
            component: React.lazy(() => import('./customer-detail/customer/Customer'))
        },
        {
            path: '/apps/customers/:id',
            component: React.lazy(() => import('./customers-list/CustomersApp'))
        },
        {
            path: '/apps/customers',
            component: () => <Redirect to="/apps/customers/all" />
        }
    ]
};
