import { getProtected, postProtected, deleteProtected, putProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig';
import localStorageService from 'app/services/localStorageService';

class DocumentAPI {
    static getDocuments = async (clientId) => {
        return await getProtected(new URL(`/api/Document/GetClientDocuments/${clientId}`, settingsConfig.apiPath))
    }

    static createDocument = async (document) => {
        const formData = new FormData();

        formData.append('file', document.file);
        formData.append('title', document.title);
        formData.append('clientId', document.clientId);
        formData.append('documentTypeCode', document.documentTypeCode);

        if (document.clientLoanId && document.clientLoanId > 0) {
            formData.append('clientLoanId', document.clientLoanId);
        }
        if (document.note) {
            formData.append('note', document.note);
        }

        const response = await fetch(new URL('/api/Document/UploadDocument', settingsConfig.apiPath), {
            method: 'POST',
            headers: {
                "Authorization": "Bearer " + localStorageService.getAccessToken()
            },
            body: formData
        });

        return await response.json();
    }

    static deleteDocument = async (guid) => {
        return await deleteProtected(new URL(`/api/Document/DeleteDocument/${guid}`, settingsConfig.apiPath))
    }

    static updateDocument = async (data) => {
        if (data.clientLoanId == 0) {
            delete data.clientLoanId;
        }
        return await putProtected(new URL('/api/Document/UpdateDocument', settingsConfig.apiPath), data)
    }

    static downloadDocument = async (doc) => {

        const response = await fetch(new URL(`/api/Document/Download/${doc.guid}`, settingsConfig.apiPath), {
            headers: {
                "Authorization": "Bearer " + localStorageService.getAccessToken(),
            },
            responseType: 'blob'
        });
        const result = await response.blob();
        var url = window.URL.createObjectURL(result);
        var a = document.createElement('a');
        a.href = url;
        a.download = doc.filename
        document.body.appendChild(a);
        a.click();
        a.remove();
    }
}

export default DocumentAPI