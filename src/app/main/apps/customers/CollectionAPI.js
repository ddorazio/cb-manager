import { getProtected, putProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class CollectionAPI {
    static getCollection = async (collectionId) => {
        return await getProtected(new URL(`/api/Collection/${collectionId}`, settingsConfig.apiPath));
    }

    static updateCollection = async (collection) => {
        return await putProtected(new URL(`/api/Collection/`, settingsConfig.apiPath), collection);
    }

    static getRejectionsOfTheDay = async () => {
        return await getProtected(new URL(`/api/Collection/RejectionsOfTheDay`, settingsConfig.apiPath));
    }

    static getCollectionsWithFollowUp = async () => {
        return await getProtected(new URL(`/api/Collection/WithFollowUp`, settingsConfig.apiPath));
    }

    static getTransfersReceivableToDate = async () => {
        return await getProtected(new URL(`/api/Collection/TransfersReceivable`, settingsConfig.apiPath));
    }

    static getCollectionsAwaitingResponse = async () => {
        return await getProtected(new URL(`/api/Collection/AwaitingResponse`, settingsConfig.apiPath));
    }

    static getComments = async (collectionId) => {
        return await getProtected(new URL(`/api/Collection/GetComments/${collectionId}`, settingsConfig.apiPath));
    }

    static saveComment = async (comment) => {
        return await postProtected(new URL(`/api/Collection/SaveComment`, settingsConfig.apiPath), comment);
    }

    static deleteComment = async (commentId) => {
        return await deleteProtected(new URL(`/api/Collection/DeleteComment/${commentId}`, settingsConfig.apiPath));
    }

    static toggleProcedureStep = async (step) => {
        return await putProtected(new URL(`/api/Collection/ToggleProcedureStep/`, settingsConfig.apiPath), step);
    }

    static sendInteracCollectionReminders = async (collectionIds) => {
        await postProtected(new URL(`/api/Collection/SendInteracCollectionReminders`, settingsConfig.apiPath), collectionIds);
    }
}

export default CollectionAPI;