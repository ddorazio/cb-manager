import { getProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class LoanAPI {

    static getLoanNumbers = async (id) => {
        return await getProtected(new URL(`/api/Client/GetClientLoanNumbers/${id}`, settingsConfig.apiPath))
    }

    static createLoan = async (data, loan) => {
        let params = {
            LoanAmountId: loan.loanAmount,
            LoanManualAmount: loan.loanManualAmount,
            ClientId: data.id,
            InterestRate: data.interestRate,
            FrequencyId: loan.frequencyId,
            NumberOfPayments: loan.numberOfPayments,
            NextPayDate: loan.nextPayDate,
            StartDay: loan.startDate,
            EndDay: loan.endDate,
            RequestNo: loan.requestNo,
            Rebate: loan.rebate
        }
        return await postProtected(new URL("/api/Loan/NewLoan/", settingsConfig.apiPath), params)
    }

    static createNSFPayment = async (loan, payment) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: payment.id
        }
        return await postProtected(new URL("/api/Loan/LoanManualNSFPayment/", settingsConfig.apiPath), data)
    }

    static deleteNSFPayment = async (loan, payment) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: payment.id
        }
        return await postProtected(new URL("/api/Loan/LoanRemoveManualNSFPayment/", settingsConfig.apiPath), data)
    }

    static addRebate = async (loan, rebate) => {
        let data = {
            LoanId: loan._original.id,
            RebateAmount: rebate.amount,
            EffectiveDate: rebate.effectiveDate
        }
        return await postProtected(new URL("/api/Loan/LoanAddRebate/", settingsConfig.apiPath), data)
    }

    static createPayment = async (loan, payment) => {
        let data = {
            LoanId: loan._original.id,
            PaymentAmount: payment.amount,
            EffectiveDate: payment.effectiveDate
        };
        return await postProtected(new URL("/api/Loan/LoanManualPayment/", settingsConfig.apiPath), data)
    }

    static deleteRebate = async (loan, payment) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: payment.id
        }
        return await postProtected(new URL("/api/Loan/LoanRemoveRebateAndManualPayment/", settingsConfig.apiPath), data)
    }

    static updatePayment = async (loan, row, changes) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: row.id,
            NewPaymentAmount: changes.newPaymentAmount,
            NewPaymentDate: changes.originalPaymentDate
        }
        return await postProtected(new URL("/api/Loan/LoanModifyPayment/", settingsConfig.apiPath), data)
    }

    static validateTotal = async (loan, row, changes) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: row.id,
            NewPaymentAmount: changes.newPaymentAmount,
            NewPaymentDate: changes.originalPaymentDate
        }
        return await postProtected(new URL("/api/Loan/CalculateTotalAmountRemaing/", settingsConfig.apiPath), data)
    }

    static updateLoan = async (loan, changes) => {
        let data = {
            LoanId: loan._original.id,
            NewFrequencyId: changes.frequencyId,
            NewPaymentAmount: changes.newLoanAmount,
            NewPaymentFrequencyDate: changes.frequencyDate,

            StartDay: changes.startDay,
            EndDay: changes.endDay,

            IsLoanStop: changes.checkStopPayment,
            StopPaymentDate: changes.stopPaymentDate,
            PaymentAgreement: changes.paymentAgreement,
            Perceptech: changes.perceptech,
            Interac: changes.interac,
            SelectedPaymentId: changes.selectedPaymentId
        }
        return await postProtected(new URL("/api/Loan/ModifyLoan/", settingsConfig.apiPath), data)
    }

    static restartLoan = async (loan, changes) => {
        let data = {
            LoanId: loan._original.id,
            NewFrequencyId: changes.frequencyId,
            NewPaymentAmount: changes.newLoanAmount,

            StartDay: changes.startDay,
            EndDay: changes.endDay,

            RestartPaymentDate: changes.restartPaymentDate,
            PaymentAgreement: changes.paymentAgreement,
            Perceptech: changes.perceptech,
            Interac: changes.interac
        }
        return await postProtected(new URL("/api/Loan/RestartLoan/", settingsConfig.apiPath), data)
    }

    static sendContract = async (contract, loan) => {
        let data = {
            LoanId: loan._original.id,
            EmailAddress: contract.email
        }
        return await postProtected(new URL("/api/Contract/GenerateLoanContract/", settingsConfig.apiPath), data)
    }

    static updateLoanStatus = async (loanId) => {
        return await postProtected(new URL(`/api/Loan/UpdateLoanStatus/${loanId}`, settingsConfig.apiPath))
    }

    // NO LONGER USED
    static modifyPaymentDate = async (loan, row, changes) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: row.id,
            NewPaymentAmount: changes.newPaymentAmount,
            NewPaymentDate: changes.originalPaymentDate
        }
        return await postProtected(new URL("/api/Loan/DelayPaymentDate", settingsConfig.apiPath), data)
    }

    static deferPayment = async (loan, row) => {
        let data = {
            LoanId: loan._original.id,
            PaymentId: row.id
        }
        return await postProtected(new URL("/api/Loan/DeferPayment", settingsConfig.apiPath), data)
    }

    static deleteLoan = async (loanId) => {
        return await deleteProtected(new URL(`/api/Loan/DeleteLoan/${loanId}`, settingsConfig.apiPath))
    }

    static getPDFData = async (loanId) => {
        return await getProtected(new URL(`/api/Loan/GetAccountBalance/${loanId}`, settingsConfig.apiPath))
    }

    static sendLoanPDF = async (pdfString, email, typePDF, data) => {
        let model = {
            PDF: pdfString,
            LoanId: data.loanId,
            TypePDF: typePDF,
            ClientInfos: {
                Email: email,
                FirstName: data.clientInfos.firstname,
                LastName: data.clientInfos.lastname,
                LanguageClient: data.clientInfos.languageClient
            }
        }
        return await postProtected(new URL(`/api/Loan/SendLoanPDF`, settingsConfig.apiPath), model)
    }

    static addPayment = async (loan, payment) => {
        let data = {
            LoanId: loan._original.id,
            PaymentAmount: payment.amount,
            PaymentDate: payment.paymentDate
        }
        return await postProtected(new URL("/api/Loan/StoppedLoanPayment/", settingsConfig.apiPath), data)
    }

    static loadLoanConfiguration = async () => {
        return await getProtected(new URL("/api/LenderConfiguration/GetLenderLoanConfiguration/", settingsConfig.apiPath))
    }
}

export default LoanAPI