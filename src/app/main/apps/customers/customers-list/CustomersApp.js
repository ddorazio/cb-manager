import React, { useEffect, useRef } from 'react';
import { Icon, Tooltip, Fab } from '@material-ui/core';
import { FusePageSimple } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import CustomersList from './CustomersList';
import CustomersHeader from './CustomersHeader';
import CustomersSidebarContent from './CustomersSidebarContent';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import { useTranslation } from 'react-i18next';

function CustomersApp(props) {
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const pageLayout = useRef(null);
    const customersApp = useSelector(({ customersApp }) => customersApp.customers);

    useEffect(() => {
        dispatch(Actions.getCustomers(props.match.params));
    }, [dispatch, props.match.params]);

    useEffect(() => {

        if (customersApp.actionType === "REMOVE_CUSTOMER" && customersApp.actionData && customersApp.actionStatus === 1) {
            dispatch(Actions.getCustomers(props.match.params));
            customersApp.actionData = null;
            customersApp.actionStatus = null;
            customersApp.actionType = null;
        }
    }, [customersApp, dispatch, props.match.params]);

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-20 sm:pb-20 h-full black_bg",
                    content: "flex flex-col h-full dark_bg",
                    leftSidebar: "w-256 border-0",
                    header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
                }}
                header={
                    <CustomersHeader pageLayout={pageLayout} />
                }
                content={
                    <CustomersList />
                }
                /*leftSidebarContent={
                    <CustomersSidebarContent/>
                }*/
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
            <Tooltip title={t('ADD_CUSTOMER')}>
                <Fab
                    color="secondary"
                    aria-label="add"
                    className="new"
                    onClick={ev => props.history.push('/apps/customers/detail/new')}
                /*onClick={ev => dispatch(Actions.openNewCustomerDialog())}*/
                >
                    <Icon>person_add</Icon>
                </Fab>
            </Tooltip>

        </React.Fragment>
    )
}

export default withReducer('customersApp', reducer)(CustomersApp);
