import { showMessage } from 'app/store/actions/fuse';
import CustomerAPI from '../../../CustomerAPI';
import i18n from 'i18n';


export const GET_CUSTOMERS = '[CUSTOMERS APP] GET CUSTOMERS';
export const SET_SEARCH_TEXT = '[CUSTOMERS APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_CUSTOMERS = '[CUSTOMERS APP] TOGGLE IN SELECTED CUSTOMERS';
export const SELECT_ALL_CUSTOMERS = '[CUSTOMERS APP] SELECT ALL CUSTOMERS';
export const DESELECT_ALL_CUSTOMERS = '[CUSTOMERS APP] DESELECT ALL CUSTOMERS';
export const OPEN_NEW_CUSTOMER_DIALOG = '[CUSTOMERS APP] OPEN NEW CUSTOMER DIALOG';
export const CLOSE_NEW_CUSTOMER_DIALOG = '[CUSTOMERS APP] CLOSE NEW CUSTOMER DIALOG';
export const OPEN_EDIT_CUSTOMER_DIALOG = '[CUSTOMERS APP] OPEN EDIT CUSTOMER DIALOG';
export const CLOSE_EDIT_CUSTOMER_DIALOG = '[CUSTOMERS APP] CLOSE EDIT CUSTOMER DIALOG';
export const TOGGLE_STARRED_CUSTOMER = '[CUSTOMERS APP] TOGGLE STARRED CUSTOMER';
export const TOGGLE_STARRED_CUSTOMERS = '[CUSTOMERS APP] TOGGLE STARRED CUSTOMERS';
export const SET_CUSTOMERS_STARRED = '[CUSTOMERS APP] SET CUSTOMERS STARRED ';
export const REMOVE_CUSTOMER = '[CUSTOMERS APP] REMOVE CUSTOMER ';

export const DELETE_CUSTOMER_SUCCESS = 'DELETE_CUSTOMER_SUCCESS';
export const DELETE_CUSTOMER_FAILED = 'DELETE_CUSTOMER_FAILED';

export const getCustomers = () => {
    return async dispatch => {
        try {
            const customers = await CustomerAPI.getCustomers();
            dispatch({
                type: GET_CUSTOMERS,
                payload: customers
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    };
}

export const deleteCustomer = (customer) => {
    return async (dispatch) => {
        try {
            const customer = await CustomerAPI.deleteCustomer(customer);
            dispatch(showMessage({ message: i18n.t('CUSTOMER_DELETED'), variant: "success" }));
            dispatch({
                type: DELETE_CUSTOMER_SUCCESS,
                payload: customer
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    }
}

export function setSearchText(event) {
    return {
        type: SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedCustomers(customerId) {
    return {
        type: TOGGLE_IN_SELECTED_CUSTOMERS,
        customerId
    }
}

export function selectAllCustomers() {
    return {
        type: SELECT_ALL_CUSTOMERS
    }
}

export function deSelectAllCustomers() {
    return {
        type: DESELECT_ALL_CUSTOMERS
    }
}

export function openNewCustomerDialog() {
    return {
        type: OPEN_NEW_CUSTOMER_DIALOG
    }
}

export function closeNewCustomerDialog() {
    return {
        type: CLOSE_NEW_CUSTOMER_DIALOG
    }
}

export function openEditCustomerDialog(data) {
    return {
        type: OPEN_EDIT_CUSTOMER_DIALOG,
        data
    }
}

export function closeEditCustomerDialog() {
    return {
        type: CLOSE_EDIT_CUSTOMER_DIALOG
    }
}
