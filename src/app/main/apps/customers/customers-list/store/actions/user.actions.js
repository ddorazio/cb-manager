import axios from 'axios';

export const GET_USER_DATA = '[CUSTOMERS APP] GET USER DATA';

export function getUserData()
{
    const request = axios.get('/api/customers-app/user');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_USER_DATA,
                payload: response.data
            })
        );
}
