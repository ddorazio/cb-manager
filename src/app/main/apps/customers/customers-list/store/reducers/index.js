import {combineReducers} from 'redux';
import customers from './customers.reducer';
import user from './user.reducer';

const reducer = combineReducers({
    customers,
    user
});

export default reducer;
