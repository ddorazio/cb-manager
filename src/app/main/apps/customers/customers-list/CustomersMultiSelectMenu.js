import React, {useState} from 'react';
import {Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList} from '@material-ui/core';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';

function CustomersMultiSelectMenu(props)
{
    const dispatch = useDispatch();
    const selectedCustomerIds = useSelector(({customersApp}) => customersApp.customers.selectedCustomerIds);

    const [anchorEl, setAnchorEl] = useState(null);

    function openSelectedCustomerMenu(event)
    {
        setAnchorEl(event.currentTarget);
    }

    function closeSelectedCustomersMenu()
    {
        setAnchorEl(null);
    }

    return (
        <React.Fragment>
            <IconButton
                className="p-0"
                aria-owns={anchorEl ? 'selectedCustomersMenu' : null}
                aria-haspopup="true"
                onClick={openSelectedCustomerMenu}
            >
                <Icon>more_horiz</Icon>
            </IconButton>
            <Menu
                id="selectedCustomersMenu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={closeSelectedCustomersMenu}
            >
                <MenuList>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.removeCustomers(selectedCustomerIds));
                            closeSelectedCustomersMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>delete</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Remove"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.setCustomersStarred(selectedCustomerIds));
                            closeSelectedCustomersMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>star</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Starred"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.setCustomersUnstarred(selectedCustomerIds));
                            closeSelectedCustomersMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>star_border</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Unstarred"/>
                    </MenuItem>
                </MenuList>
            </Menu>
        </React.Fragment>
    );
}

export default CustomersMultiSelectMenu;

