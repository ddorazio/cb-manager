import React, { useEffect, useState } from 'react';
import { FuseUtils, FuseAnimate, FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import * as Actions from '../store/actions';
import { showMessage, openDialog, closeDialog } from 'app/store/actions/fuse';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { Avatar, Button, Typography, DialogTitle, DialogContent, DialogContentText, IconButton, DialogActions, Icon } from '@material-ui/core';

export default function ConfirmDeleteDialog(props) {

    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();

    function deleteCustomer(row) {
        dispatch(Actions.deleteCustomer(row));
    }

    return (
        {
            children: (
                <React.Fragment>
                    <DialogTitle id="alert-dialog-title">{t("DELETE_CLIENT_HEADER")}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {t("DELETE_CLIENT_MESSAGE")}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            className="whitespace-no-wrap normal-case"
                            variant="contained"
                            color="primary"
                            onClick={() => dispatch(closeDialog())}>
                            {t("DELETE_CLIENT_CANCEL")}
                        </Button>
                        <Button onClick={() => deleteCustomer(1)}
                            className="whitespace-no-wrap normal-case"
                            variant="contained"
                            color="secondary"
                            autoFocus>
                            {t("DELETE_CLIENT_CONFIRM")}
                        </Button>
                    </DialogActions>
                </React.Fragment >
            )
        }
    );
}