import React, { useEffect, useState } from 'react';
import { FuseUtils, FuseAnimate, FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import * as Actions from './store/actions';
import { showMessage, openDialog, closeDialog } from 'app/store/actions/fuse';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { Avatar, Button, Typography, DialogTitle, DialogContent, DialogContentText, IconButton, DialogActions, Icon } from '@material-ui/core';

import ConfirmDeleteDialog from './dialog/confirm.delete'

function CustomersList(props) {
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();

    const customers = useSelector(({ customersApp }) => customersApp.customers.entities);
    const searchText = useSelector(({ customersApp }) => customersApp.customers.searchText);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchText) {
            const arr = Object.keys(entities).map((id) => entities[id]);
            if (searchText.length === 0) {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if (customers) {
            setFilteredData(getFilteredArray(customers, searchText));
        }
    }, [customers, searchText]);

    function validateDate(date) {

        var testDate = new Date(date);
        var maxDt = new Date("2100-01-01");
        var minDt = new Date("1950-01-01");

        return (testDate < maxDt && testDate > minDt)
    }

    function deleteCustomer(row) {
        dispatch(Actions.deleteCustomer(row));
    }

    function deleteCustomerConfirm(row) {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("DELETE_CLIENT_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("DELETE_CLIENT_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("DELETE_CLIENT_CANCEL")}
                            </Button>
                            <Button onClick={() => deleteCustomer(row)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("DELETE_CLIENT_CONFIRM")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    if (!filteredData) {
        return <FuseLoading />;
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick: (e, handleOriginal) => {
                            if (rowInfo) {
                                props.history.push(`/apps/customers/detail/${rowInfo.original.id}`);
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        Header: t("CUSTOMER_NO"),
                        width: 100,
                        accessor: "id",
                        filterable: true,
                        Cell: row => (
                            <div><strong>{row.value.toString().padStart(7, '0')}</strong></div>
                        )
                    },
                    // {
                    //     id: "created_date",
                    //     width: 180,
                    //     Header: t("CREATED_DATE"),
                    //     accessor: d => {

                    //         if (d.creationDate && validateDate(d.creationDate)) {
                    //             return moment(d.creationDate).format("YYYY-MM-DD hh:mm A")
                    //         } else {
                    //             return "";
                    //         }
                    //     },
                    //     filterable: true
                    // },
                    {
                        Header: t("STATUS"),
                        width: 120,
                        accessor: "status",
                        filterable: true,
                        Cell: row => (<div dangerouslySetInnerHTML={{ __html: row.value }} />)
                    },
                    {
                        Header: t("SURNAME"),
                        accessor: "firstName",
                        filterable: true
                    },
                    {
                        Header: t("NAME"),
                        accessor: "lastName",
                        filterable: true
                    },
                    {
                        Header: t("CITY"),
                        accessor: "city",
                        filterable: true
                    },

                    {
                        Header: t("PROVINCE"),
                        width: 100,
                        accessor: "province",
                        filterable: true
                    },
                    {
                        Header: t("PHONE_HOME"),
                        width: 120,
                        accessor: "homePhoneNumber",
                        filterable: true
                    },
                    {
                        Header: t("PHONE_CELL"),
                        width: 120,
                        accessor: "cellPhoneNumber",
                        filterable: true
                    },
                    {
                        Header: t("EMAIL"),
                        accessor: "emailAddress",
                        filterable: true
                    },
                    // {
                    //     Header: "",
                    //     width: 50,
                    //     className: "pl-0",
                    //     Cell: row => (
                    //         <div className="flex items-center">
                    //             <IconButton
                    //                 onClick={(ev) => {
                    //                     ev.stopPropagation();
                    //                     //dispatch(Actions.removeCustomer(row.original));
                    //                     deleteCustomerConfirm(row.original);
                    //                 }}
                    //             >
                    //                 <Icon>delete</Icon>
                    //             </IconButton>
                    //         </div>
                    //     )
                    // }
                ]}
                defaultPageSize={10}
                noDataText={t("NO_CUSTOMER")}
                noDataText={t("LOAN_LIST_NO_RESULTS_FOUND")}
                pageText={t("REACT_TABLE_PAGE_TEXT")}
                ofText={t("REACT_TABLE_OF_TEXT")}
                rowsText={t("REACT_TABLE_ROWS_TEXT")}
            />
        </FuseAnimate>
    );
}

export default withRouter(CustomersList);
