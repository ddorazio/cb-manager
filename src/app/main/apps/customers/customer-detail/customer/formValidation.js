export const validateForm = (store, form) => {
    // Check first to see if anything changed
    if (areEquivalent_dumb(store, form)) {
        return false;
    }
    return validatePersonalInfo(form.basicInfo.personalInfo)
        && validateAddress(form.basicInfo.address)
        && validateRevenueSource(form.basicInfo.revenueSource)
        && validateEmploymentStatus(form.basicInfo.revenueSource.incomeTypeCode, form.basicInfo.employmentStatus)
    //&& validateBankInfo(form.basicInfo.bankInfo)
}

const validatePersonalInfo = (personalInfo) => {
    return personalInfo.firstName
        && personalInfo.lastName
        //&& personalInfo.homePhoneNumber 
        && personalInfo.cellPhoneNumber
        && personalInfo.emailAddress
        && personalInfo.dateOfBirth;
}

const validateAddress = (address) => {
    return address.civicNumber
        && address.streetName
        && address.city
        && address.provinceId
        && address.postalCode
        && address.startOfResidencyDate;
}

const validateRevenueSource = (revenueSource) => {
    switch (revenueSource.incomeTypeCode) {
        case 'EMPLOYEE':
            return revenueSource.nextPayDate
        case 'RETIREMENT_PLAN':
            return true
        case 'PARENTAL_INSURANCE':
            return revenueSource.nextDepositDate
        case 'SOCIAL_ASSISTANCE':
            return true;
        case 'EMPLOYMENT_INSURANCE':
            return revenueSource.employmentInsuranceStart
                && revenueSource.employmentInsuranceNextPayDate;
        case 'DISABILITY':
            return revenueSource.nextDepositDate;
        case 'SELF_EMPLOYED':
            return revenueSource.selfEmployedDirectDeposit != null
                && revenueSource.selfEmployedDirectDeposit != undefined
                && revenueSource.selfEmployedPhoneNumber
                && revenueSource.selfEmployedPayFrequencyId
                && revenueSource.selfEmployedStartDate
                && revenueSource.nextDepositDate;
        default:
            return false;
    }
}

const validateEmploymentStatus = (icomeStatus, employmentStatus) => {
    return (icomeStatus == "RETIREMENT_PLAN" || icomeStatus == 'EMPLOYMENT_INSURANCE') ? true :
        employmentStatus.employerName
        && employmentStatus.workPhoneNumber
        && employmentStatus.occupation
        && employmentStatus.hiringDate;
}

export const validateBankInfo = (bankInfo) => {
    return bankInfo.transitNumber
        && bankInfo.institutionNumber
        && bankInfo.accountNumber
        && (bankInfo.isBankrupt
            ? (bankInfo.bankruptEndDate)
            : true);
}

export const validateClientLanguage = (personalInfo) => {
    return personalInfo.languageId;
}

/*
    This is "dumb" because it's not 100% guaranteed to work, because
    JSON.stringify may not serialize two identical objects in the exact same way.
    All major implementations *do* however, so it might be good enough for now.
    Also it's probably slow as hell
*/
const areEquivalent_dumb = (a, b) => JSON.stringify(a) === JSON.stringify(b);