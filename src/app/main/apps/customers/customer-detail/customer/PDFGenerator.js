import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import MaterialTable from "material-table";
import jsPDF from "jspdf-react";
import * as html2canvas from "html2canvas";
import { Button } from "@material-ui/core";
import { FuseLoading } from "@fuse";
import * as Actions from "../store/actions";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  tableSummaryTd: {
    textAlign: "center",
    verticalAlign: "middle",
    padding: "2px",
  },
}));

function PDFGenerator(props) {
  const dispatch = useDispatch();
  const classes = useStyles(props);
  const [elementPDF, setElementPDF] = useState(null);
  const sendPDF = useSelector(
    ({ CustomerComp }) => CustomerComp.customer.sendPDF
  );
  const data = props.data;
  const sizePage = {
    a4: [793.706, 1122.52],
  };

  const selectedTypeReport = props.selectedTypeReport;
  const [listPayment, setListPayment] = useState(data.listPayment);

  window.html2canvas = html2canvas;

  useEffect(() => {
    if (!elementPDF) {
      setElementPDF(document.getElementById("page_pdf"));
    }
  });

  useEffect(() => {
    if (sendPDF === true) {
      saveDoc();
    }
  }, [sendPDF]);

  useEffect(() => {
    if (listPayment) {

      var copiedListPayment = data.listPayment
      if (selectedTypeReport == '0') {
        copiedListPayment = data.listPayment.filter((payment) => new Date(payment.paymentDate) > new Date(data.dateToday))
      }
      setListPayment(copiedListPayment);
    }
  }, [selectedTypeReport]);

  async function saveDoc() {
    var pdfDocument = document.getElementById("page_pdf").outerHTML
    dispatch(Actions.sendLoanPDF(pdfDocument, props.email, props.typePDF, data))
    dispatch(Actions.sendPDF(false));
  }


  if (data.clientInfos.languageClient == "EN") {
    return (
      <div>
        <div className={classes.pdfContainer}
          style={{
            position: "absolute",
            zIndex: "1000",
            width: "210mm",
            height: "297mm",
          }}
        >
          <div
            id="page_pdf"
            className={classes.pdfContent}
            style={{
              width: "auto",
              paddingTop: "0px",
              paddingLeft: "60px",
              paddingRight: "60px",
              paddingBottom: "0px",
            }}
          >
            <div className={classes.header}
              style={{
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div className={classes.firstRowHeader}
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div style={{
                  width: "200px",
                  height: "100px"
                }}>
                  <img src={data.logoPath}></img>

                </div>
              </div>
              <div className={classes.secondRowHeader}
                style={{
                  paddingTop: "20px",
                }}
              >
                <div>Account statement</div>
                <div className={classes.secondRowHeaderContent}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                  }}
                >
                  <div className={classes.secondRowHeaderLeft}
                    style={{
                      width: "65%",
                    }}
                  >
                    <div className={classes.secondRowHeaderLeftContent}
                      style={{
                        display: "flex",
                        flexWrap: "wrap",
                        marginTop: "5px",
                      }}
                    >
                      <div className={classes.secondRowHeaderLeftContentLeft}
                        style={{
                          fontWeight: "500",
                          marginRight: "10px",
                        }}
                      >
                        <div>Client Number : </div>
                        <div>Client :</div>
                      </div>
                      <div className={classes.secondRowHeaderLeftContentRight}>
                        <div>{data.idClient}</div>
                        <div className={classes.secondRowHeaderClientInfo}>
                          <div>
                            {data.clientInfos.firstname}{" "}
                            {data.clientInfos.lastname}
                          </div>
                          <div>
                            {data.address.civicNumber} {data.address.streetName}
                          </div>
                          <div>
                            {data.address.city}, {data.address.provinceName}
                          </div>
                          <div>{data.address.postalCode}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={classes.secondRowHeaderRight}
                    style={{
                      width: "35%",
                    }}
                  >
                    <div className={classes.secondRowHeaderRightContent}
                      style={{
                        float: "right",
                        display: "flex",
                        flexWrap: "wrap",
                      }}
                    >
                      <div
                        className={classes.secondRowHeaderRightContentTextLeft}
                        style={{
                          fontWeight: "500",
                          marginRight: "10px",
                        }}
                      >
                        <div>Date :</div>
                        <div>Loan Date :</div>
                        <div>Loan Amount :</div>
                      </div>
                      <div
                        className={classes.secondRowHeaderRightContentTextRight}
                      >
                        <div>{data.dateToday}</div>
                        <div>{data.startDate}</div>
                        <div>{data.amount}$</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div
              id="AccountBalance"
              className={classes.contentReport}
              style={{
                marginTop: "35px",
              }}
            >
              <table className={classes.tableLoan}
                style={{
                  borderCollapse: "collapse",
                  border: "1px solid black",
                  width: "100%",
                }}
              >
                <tr>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Date
                  </th>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Status
                  </th>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Amount
                  </th>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Interest
                  </th>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Capital
                  </th>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Balance
                  </th>
                  {(data.originalSubFeesPrice != 0 && data.chargeMembershipFee == true) &&
                    <th
                      className={classes.tableLoanTh}
                      style={{
                        border: "1px solid black",
                        padding: "2px",
                      }}
                    >
                      Subscription fee
                  </th>
                  }
                </tr>
                {listPayment.map((payment) => (
                  <tr>
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.paymentDate}
                    </td>
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.status}
                    </td>
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.amount}$
                    </td>
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.interest}$
                    </td>
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.capital}$
                    </td>
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.balance}$
                    </td>
                    {(data.originalSubFeesPrice != 0 && data.chargeMembershipFee == true) &&
                      <td
                        className={classes.tableLoanTd}
                        style={{
                          textAlign: "center",
                          verticalAlign: "middle",
                          padding: "2px",
                          height: "20px",
                        }}
                      >
                        {payment.subFeesPayment}$
                    </td>
                    }
                  </tr>
                ))}
              </table>
              <div
                className={classes.tableBottomText}
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div>
                  Interest accrued until {data.dateToday} is{" "}
                  {data.totalInterest}$
                </div>
                <div>Account balance : {data.currentDailyBalance}$</div>
              </div>
            </div>

            {selectedTypeReport == 1 && (
              <div
                id="AccountDetails"
                className={classes.contentSummary}
                style={{
                  marginTop: "35px",
                  width: "50%",
                }}
              >
                <table className={classes.tableLoan}
                  style={{
                    borderCollapse: "collapse",
                    border: "1px solid black",
                    width: "100%",
                  }}
                >
                  <tr>
                    <th
                      className={classes.tableLoanTh}
                      style={{
                        border: "1px solid black",
                        padding: "2px",
                      }}
                    >
                      Account summary
                    </th>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Loan amount
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.amount}$
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Interest
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.interestRate}
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Brokerage fees
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.brokerageFees}$
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Payments
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.originalPaymentAmount}$
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Frequency
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.frequencyValue}
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Creation date
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.createdDate}
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      NSF payments
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.defaultPayments}
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Fees
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.fees}$
                    </td>
                  </tr>
                  {(data.originalSubFeesPrice != 0 && data.chargeMembershipFee == true) &&
                    <tr
                      className={classes.tableSummaryTr}
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        padding: "0px 10px",
                      }}
                    >
                      <td
                        className={classes.tableSummaryTd}
                        style={{
                          textAlign: "center",
                          verticalAlign: "middle",
                          padding: "2px",
                        }}
                      >
                        Total subscription fees
                    </td>
                      <td
                        className={classes.tableSummaryTd}
                        style={{
                          textAlign: "center",
                          verticalAlign: "middle",
                          padding: "2px",
                        }}
                      >
                        {data.subFees}$
                    </td>
                    </tr>
                  }
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Current balance
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.currentDailyBalance}$
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Rebates
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.rebates}$
                    </td>
                  </tr>
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Total owing
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.totalAmountRemaining}$
                    </td>
                  </tr>
                </table>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className={classes.pdfContainer}
        style={{
          position: "absolute",
          zIndex: "1000",
          width: "210mm",
          height: "297mm",
        }}
      >
        <div className={classes.pdfContent}
          id="page_pdf"
          style={{
            width: "auto",
            paddingTop: "0px",
            paddingLeft: "60px",
            paddingRight: "60px",
            paddingBottom: "0px",
          }}
        >
          <div className={classes.header}
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <div className={classes.firstRowHeader}
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <div style={{
                width: "200px",
                height: "100px"
              }}>
                <img src={data.logoPath}></img>
              </div>
            </div>
            <div className={classes.secondRowHeader}
              style={{
                paddingTop: "20px",
              }}
            >
              <div>État de compte</div>
              <div className={classes.secondRowHeaderContent}
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div className={classes.secondRowHeaderLeft}
                  style={{
                    width: "65%",
                  }}
                >
                  <div className={classes.secondRowHeaderLeftContent}
                    style={{
                      display: "flex",
                      flexWrap: "wrap",
                      marginTop: "5px",
                    }}
                  >
                    <div className={classes.secondRowHeaderLeftContentLeft}
                      style={{
                        fontWeight: "500",
                        marginRight: "10px",
                      }}
                    >
                      <div>No. Client : </div>
                      <div>Client : </div>
                    </div>
                    <div className={classes.secondRowHeaderLeftContentRight}>
                      <div>{data.idClient}</div>
                      <div className={classes.secondRowHeaderClientInfo}>
                        <div>
                          {data.clientInfos.firstname}{" "}
                          {data.clientInfos.lastname}
                        </div>
                        <div>
                          {data.address.civicNumber} {data.address.streetName}
                        </div>
                        <div>
                          {data.address.city}, {data.address.provinceName}
                        </div>
                        <div>{data.address.postalCode}</div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={classes.secondRowHeaderRight}
                  style={{
                    width: "35%",
                  }}
                >
                  <div className={classes.secondRowHeaderRightContent}
                    style={{
                      float: "right",
                      display: "flex",
                      flexWrap: "wrap",
                    }}
                  >
                    <div
                      className={classes.secondRowHeaderRightContentTextLeft}
                      style={{
                        fontWeight: "500",
                        marginRight: "10px",
                      }}
                    >
                      <div>Date :</div>
                      <div>Date du prêt :</div>
                      <div>Montant du prêt:</div>
                    </div>
                    <div
                      className={classes.secondRowHeaderRightContentTextRight}
                    >
                      <div>{data.dateToday}</div>
                      <div>{data.startDate}</div>
                      <div>{data.amount}$</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div
            id="AccountBalance"
            className={classes.contentReport}
            style={{
              marginTop: "35px",
            }}
          >
            <table className={classes.tableLoan}
              style={{
                borderCollapse: "collapse",
                border: "1px solid black",
                width: "100%",
              }}
            >
              <tr>
                <th
                  className={classes.tableLoanTh}
                  style={{
                    border: "1px solid black",
                    padding: "2px",
                  }}
                >
                  Date
                </th>
                <th
                  className={classes.tableLoanTh}
                  style={{
                    border: "1px solid black",
                    padding: "2px",
                  }}
                >
                  Status
                </th>
                <th
                  className={classes.tableLoanTh}
                  style={{
                    border: "1px solid black",
                    padding: "2px",
                  }}
                >
                  Paiement
                </th>
                <th
                  className={classes.tableLoanTh}
                  style={{
                    border: "1px solid black",
                    padding: "2px",
                  }}
                >
                  Intérêts
                </th>
                <th
                  className={classes.tableLoanTh}
                  style={{
                    border: "1px solid black",
                    padding: "2px",
                  }}
                >
                  Capital Payé
                </th>
                <th
                  className={classes.tableLoanTh}
                  style={{
                    border: "1px solid black",
                    padding: "2px",
                  }}
                >
                  Solde Capital
                </th>
                {(data.originalSubFeesPrice != 0 && data.chargeMembershipFee == true) &&
                  < th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Frais de membre
                </th>
                }
              </tr>
              {listPayment.map((payment) => (
                <tr>
                  <td
                    className={classes.tableLoanTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                      height: "20px",
                    }}
                  >
                    {payment.paymentDate}
                  </td>
                  <td
                    className={classes.tableLoanTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                      height: "20px",
                    }}
                  >
                    {payment.status}
                  </td>
                  <td
                    className={classes.tableLoanTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                      height: "20px",
                    }}
                  >
                    {payment.amount}$
                  </td>
                  <td
                    className={classes.tableLoanTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                      height: "20px",
                    }}
                  >
                    {payment.interest}$
                  </td>
                  <td
                    className={classes.tableLoanTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                      height: "20px",
                    }}
                  >
                    {payment.capital}$
                  </td>
                  <td
                    className={classes.tableLoanTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                      height: "20px",
                    }}
                  >
                    {payment.balance}$
                  </td>
                  {(data.originalSubFeesPrice != 0 && data.chargeMembershipFee == true) &&
                    <td
                      className={classes.tableLoanTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                        height: "20px",
                      }}
                    >
                      {payment.subFeesPayment}$
                    </td>
                  }
                </tr>
              ))}
            </table>
            <div
              className={classes.tableBottomText}
              style={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <div>
                Intérêts accumulés jusqu'au {data.dateToday} est de{" "}
                {data.totalInterest}$
              </div>
              <div>Total dû : {data.currentDailyBalance}$</div>
            </div>
          </div>

          {selectedTypeReport == 1 && (
            <div
              id="AccountDetails"
              className={classes.contentSummary}
              style={{
                marginTop: "35px",
                width: "50%",
              }}
            >
              <table className={classes.tableLoan}
                style={{
                  borderCollapse: "collapse",
                  border: "1px solid black",
                  width: "100%",
                }}
              >
                <tr>
                  <th
                    className={classes.tableLoanTh}
                    style={{
                      border: "1px solid black",
                      padding: "2px",
                    }}
                  >
                    Sommaire du compte
                  </th>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Montant du prêt
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.amount}$
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Intérêts
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.interestRate}
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Frais de courtage
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.brokerageFees}$
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Paiement
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.originalPaymentAmount}$
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Fréquence
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.frequencyValue}
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Date de création
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.createdDate}
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Paiements en défaut
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.defaultPayments}
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Frais
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.fees}$
                  </td>
                </tr>
                {(data.originalSubFeesPrice != 0 && data.chargeMembershipFee == true) &&
                  <tr
                    className={classes.tableSummaryTr}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "0px 10px",
                    }}
                  >
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      Total frais de membre
                    </td>
                    <td
                      className={classes.tableSummaryTd}
                      style={{
                        textAlign: "center",
                        verticalAlign: "middle",
                        padding: "2px",
                      }}
                    >
                      {data.subFees}$
                    </td>
                  </tr>
                }
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Solde en cours
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.currentDailyBalance}$
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Rabais
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.rebates}$
                  </td>
                </tr>
                <tr
                  className={classes.tableSummaryTr}
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "0px 10px",
                  }}
                >
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    Total dû
                  </td>
                  <td
                    className={classes.tableSummaryTd}
                    style={{
                      textAlign: "center",
                      verticalAlign: "middle",
                      padding: "2px",
                    }}
                  >
                    {data.totalAmountRemaining}$
                  </td>
                </tr>
              </table>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default withRouter(PDFGenerator);
