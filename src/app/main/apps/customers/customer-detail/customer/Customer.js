import React, { useEffect, useState } from 'react';
import { Button, Icon, Tab, Tabs, Typography } from '@material-ui/core';
import { useTheme } from '@material-ui/styles';
import { FuseAnimate, FuseAnimateGroup, FusePageCarded, FuseLoading } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link, BrowserRouter } from 'react-router-dom';
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useHistory } from "react-router-dom";
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import Collections from './tabs/Collections';
import BasicInfo from './tabs/basic-info';
import References from './tabs/References';
import Documents from './tabs/Documents';
import Demandes from './tabs/Demandes';
import Prets from './tabs/Prets';
import IBV from './tabs/IBV';
import Comments from './tabs/Comments';
import History from './tabs/History';
import Match from './tabs/ClientMatch';
import { validateForm } from './formValidation';

const TabType = {
    COLLECTION: "collections",
    MATCH: "match",
    BASIC_INFO: "basicInfo",
    REFERENCES: "references",
    GLOBAL_REQUESTS: "requests",
    DOCUMENTS: "documents",
    IBV: "ibv",
    LOANS: "loans",
    COMMENTS: "comments",
    HISTORY: "history"
}

function Customer(props) {
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const customer = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const theme = useTheme();
    const history = useHistory();

    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);

    const [dataLoaded, setDataLoaded] = useState(false);

    var [tabs, setTabs] = useState([])

    const customerTabs = {
        [TabType.COLLECTION]: {
            tab: <Tab className="normal-case text-black" icon={<Icon>error</Icon>} label="Collections" key={TabType.COLLECTION} />,
            content: <Collections form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.MATCH]: {
            tab: <Tab className="normal-case text-black" icon={<Icon>error</Icon>} label={t("CLIENT_MATCH")} key={TabType.MATCH} />,
            content: <Match form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.BASIC_INFO]: {
            tab: <Tab className="normal-case" icon={<Icon>info</Icon>} label={t("BASIC_INFOS")} key={TabType.BASIC_INFO} />,
            content: <BasicInfo form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.REFERENCES]: {
            tab: <Tab className="normal-case" icon={<Icon>people_outline</Icon>} label={t("REFERENCES")} key={TabType.REFERENCES} />,
            content: <References form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.GLOBAL_REQUESTS]: {
            tab: <Tab className="normal-case" icon={<Icon>file_copy</Icon>} label={t("GLOBAL_REQUESTS")} key={TabType.GLOBAL_REQUESTS} />,
            content: <Demandes form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.DOCUMENTS]: {
            tab: <Tab className="normal-case" icon={<Icon>attach_file</Icon>} label="Documents" key={TabType.DOCUMENTS} />,
            content: <Documents form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.IBV]: {
            tab: <Tab className="normal-case" icon={<img className="w-48 square" src="assets/images/logos/logo-ibv.png" />} label="IBV" key={TabType.IBV} />,
            content: <IBV form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.LOANS]: {
            tab: <Tab className="normal-case" icon={<Icon>attach_money</Icon>} label={t("LOANS")} key={TabType.LOANS} />,
            content: <Prets form={form} handleChange={handleChange} setForm={setForm} customer={customer} />,
        },
        [TabType.COMMENTS]: {
            tab: <Tab className="normal-case" icon={<Icon>comment</Icon>} label={t("COMMENTS")} key={TabType.COMMENTS} />,
            content: <Comments />,
        },
        [TabType.HISTORY]: {
            tab: <Tab className="normal-case" icon={<Icon>history</Icon>} label={t("HISTORY")} key={TabType.HISTORY} />,
            content: <History />,
        }
    }

    // Read URL to select which tab to render
    useEffect(() => {
        const route = props.match.url.split('/');
        const endpoint = route[route.length - 1];
        if (!endpoint || parseInt(endpoint)) return;
        const tabIndex = tabs.indexOf(endpoint);
        if (tabIndex == -1) return;
        setTabValue(tabIndex);
    }, [props.match, tabs]);

    // When loading an existing Client
    useEffect(() => {
        if (props.match.params.customerId !== "new") {
            dispatch(Actions.getCustomer(props.match.params));
        } else {
            dispatch(Actions.newCustomer());
            const tabs = [TabType.BASIC_INFO, TabType.REFERENCES, TabType.GLOBAL_REQUESTS, TabType.DOCUMENTS]
            setTabs(tabs)
            setDataLoaded(true)
        }
    }, [props.match.params]);

    // When customer state changes
    useEffect(() => {
        if (customer.data && customer.data.id == props.match.params.customerId) {
            var tabs = [TabType.BASIC_INFO, TabType.REFERENCES, TabType.IBV, TabType.GLOBAL_REQUESTS, TabType.DOCUMENTS, TabType.LOANS, TabType.COMMENTS, TabType.HISTORY]

            if (customer.data.activeCollections && customer.data.activeCollections.length > 0) {
                tabs.splice(0, 0, TabType.COLLECTION)
            }

            // Check if Client matches exist when there is only one request
            //if (customer.data.requests && customer.data.requests.length === 1) {
            //if (customer.data.requests[0].potentialMatch === true) {
            if (customer.data.potentialMatch === true && customer.data.clientMatchList.length > 0) {
                tabs.splice(0, 0, TabType.MATCH)
            }
            //}
            setTabs(tabs)
            setDataLoaded(true)
        }
        setForm(customer.data);
    }, [customer.data]);

    useEffect(() => {
        const handle = props.match.params.customerHandle
        if (handle) {
            const index = tabs.indexOf(handle)
            if (index >= 0) {
                setTabValue(index)
            }
        }
    }, [tabs])

    // Set viewable Tab to show
    function handleChangeTab(event, tabValue) {
        const tabLocation = tabs[tabValue];
        history.push(`/apps/customers/detail/${props.match.params.customerId}/${tabLocation}`)
    }

    //Save or Update client
    function saveCustomer() {
        if (props.match.params.customerId === 'new') {
            return dispatch(Actions.saveCustomer(form))
        } else {
            return dispatch(Actions.updateCustomer(form))
        }
    }

    // Validate Save and Update
    function canBeSubmitted() {
        return validateForm(customer.data, form);
    }

    if (props.match.params.customerId !== 'new' && !dataLoaded) {
        return <FuseLoading />;
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "h-84",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/requests/all" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">{t("ALL_CUSTOMERS")}</span>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.basicInfo.social.googleId ?
                                        <img className="w-32 sm:w-48 rounded" src={form.basicInfo.social.googlePhoto} alt={form.nom} />
                                        : form.basicInfo.social.facebookId ?
                                            <img className="w-32 sm:w-48 rounded" src={form.basicInfo.social.facebookPhoto} alt={form.nom} />
                                            : <img className="w-32 sm:w-48 rounded" src="assets/images/avatars/profile.jpg" alt={form.nom} />}

                                    {/* {form.images && form.images.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 rounded" src={_.find(form.images, { id: form.featuredImageId }).url} alt={form.nom} />
                                    ) : (
                                            <img className="w-32 sm:w-48 rounded" src="assets/images/avatars/profile.jpg" alt={form.nom} />
                                        )} */}
                                </FuseAnimate>
                                <div className="flex flex-col min-w-0 mx-8 mr-64 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.basicInfo.personalInfo.firstName && form.basicInfo.personalInfo.lastName ? `${form.basicInfo.personalInfo.firstName + " " + form.basicInfo.personalInfo.lastName}` : t("NEW_CUSTOMER")}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimateGroup animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption"> {form.id ? form.id.toString().padStart(6, '0') : ""} </Typography>
                                    </FuseAnimateGroup>
                                    <FuseAnimateGroup animation="transition.slideLeftIn" delay={300}>
                                        {form.basicInfo.social.googleId &&
                                            (<Typography variant="caption"> {"Google ID: " + form.basicInfo.social.googleId} </Typography>)
                                        }

                                        {form.basicInfo.social.facebookId &&
                                            (<Typography variant="caption">{"Facebook ID: " + form.basicInfo.social.facebookId} </Typography>)
                                        }
                                    </FuseAnimateGroup>
                                </div>
                            </div>
                        </div>
                        <FuseAnimateGroup animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!canBeSubmitted()}
                                onClick={() => saveCustomer()}
                            >
                                {(props.match.params.customerId === 'new') ? t("SAVE") : t("UPDATE")}

                            </Button>
                        </FuseAnimateGroup>
                    </ div>
                )
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="secondary"
                    textColor="primary"
                    variant="scrollable"
                    className="w-full"
                >
                    {tabs.map((value, index) => {
                        return customerTabs[value].tab
                    })}
                </Tabs>
            }
            content={
                form && (
                    <div className="p-24">
                        {customerTabs[tabs[tabValue]].content}
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('CustomerComp', reducer)(Customer);