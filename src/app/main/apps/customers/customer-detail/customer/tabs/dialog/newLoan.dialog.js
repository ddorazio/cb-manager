import React, { useRef, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import * as Material from '@material-ui/core';
import { TextFieldFormsy } from '@fuse';
import Formsy from 'formsy-react';
import { withStyles } from '@material-ui/core/styles';
import * as Redux from 'react-redux';
import * as Actions from '../../../store/actions/loan.actions';
import { useTranslation } from 'react-i18next';
import * as Fuse from 'app/store/actions/fuse';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())


export default function NewLoanDialog(props) {

    const { t } = useTranslation('customersApp');
    const initialState = Redux.useSelector(({ CustomerComp }) => CustomerComp.customer);
    const revenueSource = initialState.data.basicInfo.revenueSource
    const nonLinkedRequests = Redux.useSelector(({ CustomerComp }) => CustomerComp.customer.data.nonLinkedRequests);
    const { form } = props;
    const dispatch = Redux.useDispatch();

    const [disableDates, setDisableDates] = useState(false);

    const [loan, setLoan] = useState({
        loading: false,
        loanAmount: 0,
        frequencyId: "",
        loanManualAmount: 0,
        numberOfPayments: 0,
        startDate: 0,
        endDate: 0,
        nextPayDate: null,
        loanFrequencies: [],
        numberOfPaymentsList: [],
        rebate: 0
    });

    useEffect(() => {
        findIncomeDate();
    }, [revenueSource])

    useEffect(() => {

        if (open) {
            resetForm();
        }

        setOpen(initialState.actionStatus);
    }, [initialState]);

    const findIncomeDate = () => {
        switch (revenueSource.incomeTypeCode) {
            case 'EMPLOYEE':
                setLoan({
                    ...loan,
                    nextPayDate: revenueSource.nextPayDate
                });
                break;
            case 'EMPLOYMENT_INSURANCE':
                setLoan({
                    ...loan,
                    nextPayDate: revenueSource.employmentInsuranceNextPayDate
                });
                break;
            case 'RETIREMENT_PLAN' || 'PARENTAL_INSURANCE' || 'SOCIAL_ASSISTANCE' || 'DISABILITY' || 'SELF_EMPLOYED':
                setLoan({
                    ...loan,
                    nextPayDate: revenueSource.nextDepositDate
                });
                break;
            default:
                break;
        }
    }

    const handleDateChange = (date) => {
        setLoan({
            ...loan,
            nextPayDate: date //new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)
        });
    };

    const setDateMin = () => {
        var today = new Date();
        if (form.nextPayDate && form.nextPayDate > today) {
            return new Date(form.nextPayDate.getFullYear(), form.nextPayDate.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);
        } else {
            return new Date(today.getFullYear(), today.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);
        }
    }

    const handleChangeLoanAmount = (event) => {

        // Add frequencies for the Loan Amount
        var frequencies = form.newLoanConfiguration.filter(x => x.loanAmountId === parseInt(event.target.value));

        var found = [];
        frequencies.forEach(function (element) {
            //$.each(frequencies, function (index, value) {
            var freq = {
                id: element.loanFrequencyId,
                name: element.loanFrequencyName
            }

            found.push(freq);
        });

        setLoan({
            ...loan,
            loanAmount: event.target.value,
            loanFrequencies: found,
            frequencyId: "",
            startDate: 0,
            endDate: 0,
            numberOfPayments: 0,
        });

        setDisableDates(false);
    };

    //const [frequence, setFrequence] = React.useState([]);
    const handleChangeFrequency = (event) => {

        //var index = event.nativeEvent.target.selectedIndex;
        //if (event.target[index].text.toUpperCase() === "BI-MENSUEL") {
        if (event.target.value === "4") {
            setDisableDates(true);
        } else {
            setDisableDates(false);
        }

        // Add number of payments for the Loan
        var payments = form.newLoanConfiguration.filter(x => x.loanAmountId === parseInt(loan.loanAmount) && x.loanFrequencyId == parseInt(event.target.value));

        var found = [];
        payments.forEach(function (element) {
            //$.each(frequencies, function (index, value) {
            var pay = {
                value: element.numberOfPayments
            }

            found.push(pay);
        });

        if (event.target.value === "4") {
            setLoan({
                ...loan,
                frequencyId: event.target.value,
                //numberOfPaymentsList: found,
                numberOfPayments: 0,
                startDate: 0,
                endDate: 0
            });
        } else {
            setLoan({
                ...loan,
                frequencyId: event.target.value,
                numberOfPaymentsList: found,
                numberOfPayments: 0,
                startDate: 0,
                endDate: 0
            });
        }
    };

    const handleChangeLoanManualAmount = (event) => {
        setLoan({
            ...loan,
            loanManualAmount: parseFloat(event.target.value) || 0
        });
    };

    const handleChangeDuration = (event) => {
        setLoan({
            ...loan,
            numberOfPayments: event.target.value
        });
    };

    const handleChangeStartDate = (event) => {
        setLoan({
            ...loan,
            startDate: event.target.value
        });
    };

    const handleChangeEndDate = (event) => {
        setLoan({
            ...loan,
            endDate: event.target.value
        });
    };

    const handleChangeDurationManual = (event) => {
        setLoan({
            ...loan,
            numberOfPayments: parseFloat(event.target.value) || 0
        });
    };

    const handleChangeRequestNo = (event) => {
        setLoan({
            ...loan,
            requestNo: event.target.value
        });
    };

    const handleChangeRebate = (event) => {
        setLoan({
            ...loan,
            rebate: parseFloat(event.target.value) || 0
        });
    };

    function resetForm() {
        setLoan({
            ...loan,
            loading: false,
            loanAmount: 0,
            frequencyId: "",
            loanManualAmount: 0,
            numberOfPayments: 0,
            startDate: 0,
            endDate: 0,
            nextPayDate: null,
            loanFrequencies: [],
            numberOfPaymentsList: [],
            rebate: 0,
            requestNo: ""
        });
        setDisableDates(false);
    }

    //Create new loan
    function createLoan() {
        setLoan({
            ...loan,
            loading: true
        });

        //setOpen(false);
        dispatch(Actions.createLoan(form, loan));
        //resetForm();
    }

    // Validate form submit
    function canBeSubmitted() {

        if (!loan.loading) {

            if (form.isPersonalized) {

                if (!disableDates) {
                    return loan.nextPayDate
                        && loan.frequencyId
                        && loan.loanManualAmount
                        && loan.numberOfPayments
                        && loan.requestNo
                    //&& loan.rebate;
                } else {
                    return loan.nextPayDate
                        && loan.frequencyId
                        && loan.loanManualAmount
                        && loan.startDate
                        && loan.endDate
                        && loan.numberOfPayments
                        && loan.requestNo;
                    //&& loan.rebate;
                }

            } else {

                if (!disableDates) {
                    return loan.nextPayDate
                        && loan.frequencyId
                        && loan.loanAmount
                        && loan.numberOfPayments
                        && loan.requestNo
                        && loan.rebate;
                } else {
                    return loan.nextPayDate
                        && loan.frequencyId
                        && loan.loanAmount
                        && loan.startDate
                        && loan.endDate
                        && loan.numberOfPayments
                        && loan.requestNo
                        && loan.rebate;
                }
            }
        }
    }

    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        resetForm();
    };

    // const handleSubmit = (model) => {
    //     console.info('submit', model);
    // }

    const ColorButton = withStyles(theme => ({
        root: {
            color: "#fff",
            backgroundColor: "#e41f25",
            '&:hover': {
                backgroundColor: "#000000",
            },
        },
    }))(Material.Button);

    const handleNoRequestLinks = () => {

        dispatch(Fuse.openDialog(
            {
                children: (
                    <React.Fragment>
                        <Material.DialogTitle id="alert-dialog-title">{t("NEW_LOAN_NO_REQUESTS_HEADER")}</Material.DialogTitle>
                        <Material.DialogContent>
                            <Material.DialogContentText id="alert-dialog-description">
                                {t("NEW_LOAN_NO_REQUESTS_MESSAGE")}
                            </Material.DialogContentText>
                        </Material.DialogContent>
                        <Material.DialogActions>
                            <Material.Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(Fuse.closeDialog())}>
                                {t("NEW_LOAN_NO_REQUESTS_CANCEL")}
                            </Material.Button>
                        </Material.DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    return (
        <div>
            {
                nonLinkedRequests.length == 0 ?
                    <ColorButton variant="contained" onClick={handleNoRequestLinks} endIcon={<Material.Icon>add</Material.Icon>}>{t("NEW_LOAN_BUTTON")}</ColorButton>
                    :
                    <ColorButton variant="contained" onClick={handleClickOpen} endIcon={<Material.Icon>add</Material.Icon>}>{t("NEW_LOAN_BUTTON")}</ColorButton>
            }
            <Material.Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth maxWidth='sm'>
                <Material.DialogTitle id="form-dialog-title">{t("NEW_LOAN_DIALOG_TITLE")}</Material.DialogTitle>
                <Material.DialogContent>
                    <Formsy
                        // onValidSubmit={handleSubmit}
                        // onValid={enableButton}
                        // onInvalid={disableButton}
                        // ref={formref}
                        className="flex flex-col justify-center"
                    >

                        {!form.isPersonalized &&
                            (<Material.FormControl variant="outlined" className="mt-8 mb-16 mr-16">
                                <Material.InputLabel htmlFor="montant_pret-label">{t("NEW_LOAN_AMOUNT")}</Material.InputLabel>
                                <Material.Select
                                    //native
                                    //value={montant_pret}
                                    native
                                    defaultValue={loan.loanAmount}
                                    value={loan.loanAmount}
                                    onChange={handleChangeLoanAmount}
                                    label="Montant du Prêt"
                                    validations={{ isNumeric: true }}
                                    validationErrors={{ isNumeric: 'Champs invalide' }}
                                    inputProps={{
                                        name: 'montant_pret',
                                        id: 'montant_pret-label',
                                    }}
                                >
                                    {/* <option aria-label="None" value="" /> */}
                                    {/* <option value={300}>300 $</option>
                                    <option value={500}>500 $</option>
                                    <option value={800}>800 $</option> */}
                                    {/* <MenuItem value={300}>300 $</MenuItem>
                                    <MenuItem value={500}>500 $</MenuItem>
                                    <MenuItem value={800}>800 $</MenuItem> */}

                                    <option aria-label="None" value="" />
                                    {form.loanAmounts.map((amount, index) => (
                                        <option key={index} value={amount.id}>{amount.amount} $</option>
                                    ))}

                                </Material.Select>
                            </Material.FormControl>)
                        }

                        {form.isPersonalized &&
                            (<TextFieldFormsy
                                className="mt-8 mb-16 mr-16"
                                id="montant_pret"
                                name="montant_pret"
                                label={t("NEW_LOAN_AMOUNT")}
                                type="number"
                                variant="outlined"
                                onChange={handleChangeLoanManualAmount}
                                fullwidth
                                defaultValue={loan.loanManualAmount}
                                value={loan.loanManualAmount}
                                InputProps={{
                                    endAdornment: <Material.InputAdornment>$</Material.InputAdornment>,
                                }}
                            />)
                        }

                        <Material.FormControl variant="outlined" className="mt-8 mb-16 mr-16">
                            <Material.InputLabel htmlFor="frequence-label">{t("NEW_LOAN_FREQUENCY")}</Material.InputLabel>
                            <Material.Select
                                native
                                //value={frequence}
                                value={loan.frequencyId}
                                // onChange={handleChange}
                                label="Fréquence des paiements"
                                inputProps={{
                                    name: 'frequence',
                                    id: 'frequence-label',
                                }}
                                onChange={handleChangeFrequency}
                            >
                                <option aria-label="None" value="" />
                                {/* {loan.loanFrequencies.length !== 0 &&
                                    // <option key={1} value={1}>Test</option>
                                    (loan.loanFrequencies.map((frequency, index) => (
                                        <option key={index} value={frequency.id}>{frequency.name}</option>
                                    )))
                                } */}
                                {!form.isPersonalized ?

                                    (loan.loanFrequencies.map((frequency, index) => (
                                        <option key={index} value={frequency.id}>{frequency.name}</option>
                                    )))
                                    :
                                    (form.loanFrequencies.map((frequency, index) => (
                                        <option key={index} value={frequency.id}>{frequency.name}</option>
                                    )))
                                }

                            </Material.Select>
                        </Material.FormControl>

                        {disableDates &&
                            (
                                <div className="flex">
                                    <TextFieldFormsy
                                        className="mt-8 mb-16 mr-16 w-1/2"
                                        id="montant_pret"
                                        name="montant_pret"
                                        label={t("DAY_ONE")}
                                        type="number"
                                        variant="outlined"
                                        fullwidth
                                        onChange={handleChangeStartDate}
                                        //onChange={e => handleChangeStartDate(e.target.value)}
                                        defaultValue={loan.startDate}
                                        value={loan.startDate}
                                    />

                                    <TextFieldFormsy
                                        className="mt-8 mb-16 mr-16 w-1/2"
                                        id="montant_pret"
                                        name="montant_pret"
                                        label={t("DAY_TWO")}
                                        type="number"
                                        variant="outlined"
                                        fullwidth
                                        onChange={handleChangeEndDate}
                                        defaultValue={loan.endDate}
                                        value={loan.endDate}
                                    />
                                </div>
                            )
                        }

                        {!form.isPersonalized &&
                            <Material.FormControl variant="outlined" className="mt-8 mb-16 mr-16">
                                <Material.InputLabel htmlFor="duree-label">{t("NUMBER_OF_PAYMENTS")}</Material.InputLabel>
                                <Material.Select
                                    native
                                    //value={duree}
                                    value={loan.numberOfPayments}
                                    // onChange={handleChangeDuration}
                                    label="Durée du terme (mois)"
                                    inputProps={{
                                        name: 'duree',
                                        id: 'duree-label',
                                    }}
                                    onChange={handleChangeDuration}
                                >
                                    <option aria-label="None" value="" />
                                    {/* <option value={2}>2</option>
                                    <option value={6}>6</option>
                                    <option value={12}>12</option>
                                    <option value={20}>20</option>
                                    <option value={24}>24</option> */}

                                    {loan.numberOfPaymentsList.length !== 0 &&
                                        // <option key={1} value={1}>Test</option>
                                        (loan.numberOfPaymentsList.map((number, index) => (
                                            <option key={index} value={number.value}>{number.value}</option>
                                        )))
                                    }

                                </Material.Select>
                            </Material.FormControl>
                        }


                        {form.isPersonalized &&
                            (<TextFieldFormsy
                                className="mt-8 mb-16 mr-16"
                                id="nombre_paiements"
                                name="nombre_paiements"
                                label={t("NUMBER_OF_PAYMENTS")}
                                type="number"
                                variant="outlined"
                                onChange={handleChangeDurationManual}
                                fullwidth
                                defaultValue={loan.numberOfPayments}
                                value={loan.numberOfPayments}
                            />)
                        }

                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                            <KeyboardDatePicker
                                className="mt-8 mb-16 mr-16"
                                //disableToolbar
                                minDate={setDateMin()}
                                format="DD/MM/YYYY"
                                inputVariant="outlined"
                                id="loan_date"
                                name="loan_date"
                                autoOk
                                variant="inline"
                                inputVariant="outlined"
                                InputAdornmentProps={{ position: "start" }}
                                label={t("NEXT_PAY_DATE")}
                                //value={selectedDate}
                                value={loan.nextPayDate}
                                onChange={handleDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />

                            <TextFieldFormsy
                                className="mt-8 mb-16 mr-16"
                                id="rebate"
                                name="rebate"
                                label={t("LOAN_REBATE")}
                                type="number"
                                variant="outlined"
                                onChange={handleChangeRebate}
                                fullwidth
                                defaultValue={loan.rebate}
                                value={loan.rebate}
                                InputProps={{
                                    endAdornment: <Material.InputAdornment>$</Material.InputAdornment>,
                                }}
                            />

                        </MuiPickersUtilsProvider>
                        {nonLinkedRequests.length == 0 ?
                            <span style={{ color: "red" }}>{t("NEW_LOAN_NO_REQUESTS")}</span>
                            :
                            <Material.FormControl variant="outlined" className="mt-8 mb-16 mr-16">
                                <Material.InputLabel htmlFor="frequence-label">{t("NEW_LOAN_REQUEST_NO")}</Material.InputLabel>
                                <Material.Select
                                    native
                                    value={loan.requestNo}
                                    onChange={handleChangeRequestNo}
                                >
                                    <option aria-label="None" value="" />
                                    {nonLinkedRequests.map((request, index) => (
                                        <option key={index} value={request.id}>{request.requestedDateFormat
                                            + "-"
                                            + request.clientFirstName + " " + request.clientLastName
                                            + "-"
                                            + "$"
                                            + request.loanAmount
                                            + "-"
                                            + request.status}
                                        </option>
                                    ))}
                                </Material.Select>
                            </Material.FormControl>
                        }
                    </Formsy>

                </Material.DialogContent>
                <Material.DialogActions>
                    <Material.Button onClick={handleClose} color="primary">
                        {t("NEW_LOAN_CANCEL_BUTTON")}
                    </Material.Button>
                    <Material.Button
                        onClick={() => createLoan()}
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        //disabled={!isFormValid}
                        disabled={!canBeSubmitted()}
                    >          {loan.loading &&
                        <i
                            className="fa fa-refresh fa-spin"
                            style={{ marginRight: "5px" }}
                        />
                        }
                        {t("NEW_LOAN_SAVE_BUTTON")}
                    </Material.Button>
                </Material.DialogActions>
            </Material.Dialog>
        </div>
    );
}
