import React, { Fragment, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Tooltip, Button, Icon, Input, Typography, IconButton, Fab } from '@material-ui/core';
import { FuseScrollbars } from '@fuse';
import { useForm, useUpdateEffect } from '@fuse/hooks';
import moment from 'moment';
import _ from '@lodash';
import { withRouter } from 'react-router-dom';
import NoteCreatedLabel from '../NoteCreatedLabel';
import NoteLabel from '../NoteLabel';
import NoteModel from '../model/NoteModel';
import NoteFormUploadImage from './NoteFormUploadImage';
import NoteFormLabelMenu from './NoteFormLabelMenu';
import * as Actions from '../store/actions/';
import NoteType from '../NoteType'

function NoteForm(props) {
    const dispatch = useDispatch();
    const [showList, setShowList] = useState(false);
    const { form: noteForm, handleChange, setForm } = useForm(
        _.merge(
            {},
            new NoteModel(),
            props.note,
            props.match.params.labelId ? { labels: [props.match.params.labelId] } : null,
            props.match.params.id === "archive" ? { archive: true } : null
        ));
    const { onChange } = props;

    useUpdateEffect(() => {
        if (noteForm && onChange) {
            onChange(noteForm);
        }
    }, [noteForm, onChange]);

    function handleOnCreate(event) {
        if (!props.onCreate) {
            return;
        }
        props.onCreate(noteForm);
    }


    const handleOnSave = () => {
        switch (props.type) {
            case NoteType.CLIENT: {
                dispatch(Actions.updateNote(noteForm))
                break
            }
            case NoteType.COLLECTION: {
                dispatch(Actions.updateCollectionNote(noteForm))
                break
            }
        }
        props.onClose();
    }

    function handleToggleList() {
        setShowList(!showList);
    }

    function handleDateChange(date) {
        setForm(_.setIn(noteForm, "reminder", date));
    }

    function handleAddLabel(label) {
        // Don't add labels twice
        if (noteForm.labels.some(_label => _label.id == label.id)) {
            return;
        }
        if (props.variant !== "new") {
            switch (props.type) {
                case NoteType.CLIENT: {
                    dispatch(Actions.addLabelToComment(noteForm.id, label.id));
                    break
                }

                case NoteType.COLLECTION: {
                    dispatch(Actions.addLabelToCollectionComment(noteForm.id, label.id));
                    break
                }
            }
        }
        setForm(_.setIn(noteForm, 'labels', [...noteForm.labels, label]))
    }

    function handleRemoveLabel(labelId) {
        if (props.variant !== "new") {
            switch (props.type) {
                case NoteType.CLIENT: {
                    dispatch(Actions.removeLabelFromComment(noteForm.id, labelId))
                    break
                }

                case NoteType.COLLECTION: {
                    dispatch(Actions.removeLabelFromCollectionComment(noteForm.id, labelId))
                    break
                }
            }

        }
        setForm(_.setIn(noteForm, `labels`, noteForm.labels.filter(_label => _label.id !== labelId)));
    }

    function handleArchiveToggle() {
        dispatch(Actions.updateNote({
            ...noteForm,
            archive: !noteForm.archive
        }));
        props.onClose();
    }

    function newFormButtonDisabled() {
        return noteForm.title === "" && noteForm.description === "";
    }

    if (!noteForm) {
        return null;
    }

    return (
        <div className="flex flex-col w-full">
            <FuseScrollbars className="flex flex-auto w-full max-h-640">
                <div className="w-full">
                    <div className="p-16 pb-12">
                        <Input
                            className="font-bold"
                            placeholder="Titre"
                            type="text"
                            name="title"
                            value={noteForm.title}
                            onChange={handleChange}
                            disableUnderline
                            fullWidth
                        />
                    </div>
                    <div className="p-16 pb-12">
                        <Input
                            placeholder="Écrivez ici.."
                            multiline
                            rows="4"
                            name="description"
                            value={noteForm.description}
                            onChange={handleChange}
                            disableUnderline
                            fullWidth
                            autoFocus
                        />
                    </div>

                    {(noteForm.labels || noteForm.created || noteForm.modified) && (
                        <div className="flex flex-wrap w-full p-16 pb-12 -mx-4">
                            {noteForm.created && (
                                <NoteCreatedLabel className="mt-4 mx-4" date={noteForm.created} />
                            )}
                            {noteForm.labels && noteForm.labels.map(label => (
                                <NoteLabel id={label.id} key={label.id} className="mt-4 mx-4" onDelete={() => handleRemoveLabel(label.id)} />
                            ))}
                            {noteForm.modified && (
                                <Typography color="textSecondary" className="text-12 mt-8 mx-4">
                                    Modifié le: {moment(noteForm.modified).format('MMM DD YY, h:mm A')}
                                </Typography>
                            )}
                        </div>
                    )}
                </div>
            </FuseScrollbars>

            <div className="flex flex-auto justify-between items-center h-48">
                <div className="flex items-center px-4">
                    {props.type === NoteType.CLIENT
                        ? <React.Fragment>
                            <Tooltip title="Ajouter une catégorie" placement="bottom">
                                <div>
                                    <NoteFormLabelMenu {...props} note={noteForm} onChange={handleAddLabel} />
                                </div>
                            </Tooltip>

                            <Tooltip title={noteForm.archive ? "Désarchiver" : "Archiver"} placement="bottom">
                                <div>
                                    <IconButton className="w-32 h-32 mx-4 p-0" onClick={handleArchiveToggle} disabled={newFormButtonDisabled()}>
                                        <Icon fontSize="small">
                                            {noteForm.archive ? "unarchive" : "archive"}
                                        </Icon>
                                    </IconButton>
                                </div>
                            </Tooltip>
                        </React.Fragment> : null}
                </div>
                <div className="flex items-center px-4">
                    {props.variant === "new" ? (
                        <Button
                            className="m-4"
                            onClick={handleOnCreate}
                            variant="outlined"
                            size="small"
                            disabled={newFormButtonDisabled()}
                        >
                            Ajouter
                        </Button>
                    ) : (
                            <Fragment>
                                <Tooltip title="Supprimer le commentaire" placement="bottom">
                                    <IconButton className="w-32 h-32 mx-4 p-0" onClick={props.onRemove}>
                                        <Icon fontSize="small">delete</Icon>
                                    </IconButton>
                                </Tooltip>
                                <Button
                                    className="m-4"
                                    onClick={() => handleOnSave()}
                                    variant="outlined"
                                    size="small"
                                >
                                    Enregistrer
                                </Button>
                            </Fragment>
                        )}
                </div>
            </div>
        </div>
    );
}

NoteForm.propTypes = {};
NoteForm.defaultProps = {
    variant: "edit",
    note: null
};

export default withRouter(NoteForm);
