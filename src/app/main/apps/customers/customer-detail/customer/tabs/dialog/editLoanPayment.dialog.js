import React, { useRef, useState } from 'react';
import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, FormControlLabel, Icon, IconButton, InputAdornment, TextField, Typography } from '@material-ui/core';
import { FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import * as Actions from '../../../store/actions';
import { useTranslation } from 'react-i18next';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())


export default function EditLoanPaymentDialog(props) {

    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const [locale] = useState(localStorageService.getLanguage());
    const [fullWidth, setFullWidth] = React.useState(true);
    const { loan, row, form } = props;
    const dispatch = useDispatch();



    const [state, setState] = useState({
        loading: false,
        totalValidated: true,
        originalPaymentDate: row.original.paymentDate,
        checkPaymentDate: false,

        checkPaymentAmount: false,
        newPaymentAmount: row.original.amount,
        newPaymentBalance: null
    });

    useEffect(() => {

        if (open) {
            resetForm();
        }

        setOpen(initialState.actionStatus);
    }, [initialState]);

    const handleChange = (event) => {

        switch (event.target.name) {
            case "checkPaymentDate":
                setState({
                    ...state,
                    [event.target.name]: event.target.checked

                });
                break;
            case "checkPaymentAmount":
                setState({
                    ...state,
                    [event.target.name]: event.target.checked,
                    newPaymentAmount: row.original.amount
                });
                break;
        }
    };

    const handleChangePaymentAmount = (event) => {
        setState({
            ...state,
            newPaymentAmount: parseFloat(event.target.value) || 0
        });
    };

    const handleChangePaymentDate = (event) => {
        setState({
            ...state,
            paymentDate: event,
            originalPaymentDate: event
        });
    };

    function confirmModifyPayment() {

        setState({
            ...state,
            loading: true
        });

        dispatch(Actions.updatePayment(loan, row.original, state));
    }

    function validateTotal() {

        setState({
            ...state,
            loading: true
        });

        dispatch(Actions.validateTotal(loan, row.original, state));

        // setTimeout(() => {
        //     setState({
        //         ...state,
        //         loading: false,
        //         totalValidated: true,
        //         newPaymentBalance: 100
        //     });
        // }, 2000);
    }

    // Validate form submit
    function canBeSubmitted() {

        if (!state.loading) {
            if (state.checkPaymentAmount || state.checkPaymentDate) {

                return state.newPaymentAmount
                    || state.paymentDate;
            }
        }
    }

    const setDateMin = () => {
        var index = loan._original.loanPayments.findIndex(payment => payment.id == row.original.id);
        var previousDate = new Date();
        if (index !== 0) {
            previousDate = new Date(loan._original.loanPayments[index - 1].paymentDate)
        }

        var today = new Date();
        var minDate = new Date(today.getFullYear(), today.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);

        if (minDate < previousDate) {
            return previousDate.setDate(previousDate.getDate() + 2);
        }

        return minDate;
    }

    const setDateMax = () => {
        var index = loan._original.loanPayments.findIndex(payment => payment.id == row.original.id);
        var t = loan._original.loanPayments[index + 1];
        if (loan._original.loanPayments[index + 1] !== undefined) {
            var date = new Date(loan._original.loanPayments[index + 1].paymentDate)
            return date.setDate(date.getDate());
        }
    }

    function resetForm() {
        setState({
            ...state,
            loading: false,
            totalValidated: true,
            originalPaymentDate: row.original.paymentDate,
            checkPaymentDate: false,

            checkPaymentAmount: false,
            newPaymentAmount: row.original.amount,
            newPaymentBalance: null//row.original.balance
        });
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        resetForm();
    };

    return (
        <React.Fragment>
            <IconButton onClick={handleClickOpen}><Icon>edit</Icon></IconButton>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("MODIFY_LOAN_PAYMENT_DIALOG_TITLE")}</DialogTitle>
                <DialogContent>
                    <div className="flex">
                        <div className="flex-1 mr-16 text-center">
                            <div className="bg-gray-100 p-16 ">
                                <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_PAYMENT_ORIGINAL_DATE")}</Typography>
                                <Typography className="h3" color="textPrimary">{row.original.paymentDate}</Typography>
                            </div>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.checkPaymentDate}
                                        onChange={handleChange}
                                        name="checkPaymentDate"
                                        color="secondary"
                                    />
                                }
                                label={t("MODIFY_LOAN_PAYMENT_CHANGE_DATE_LABEL")}
                            />
                            {state.checkPaymentDate &&
                                (
                                    <FuseAnimate animation="transition.fadeIn">
                                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                            <KeyboardDatePicker
                                                className="mt-8 mb-16"
                                                //disableToolbar
                                                minDate={setDateMin()}
                                                maxDate={setDateMax()}
                                                format="YYYY-MM-DD"
                                                inputVariant="outlined"
                                                id="loan_date"
                                                name="loan_date"
                                                autoOk
                                                variant="inline"
                                                inputVariant="outlined"
                                                InputAdornmentProps={{ position: "start" }}
                                                label={t("MODIFY_LOAN_PAYMENT_CHANGE_DATE_TEXT")}
                                                value={state.paymentDate}
                                                onChange={handleChangePaymentDate}
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change date',
                                                }}
                                            />
                                        </MuiPickersUtilsProvider>
                                    </FuseAnimate>
                                )
                            }
                        </div>
                        <div className="flex-1 mr-16 text-center">
                            <div className="bg-gray-100 p-16">
                                <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_PAYMENT_ORIGINAL_AMOUNT")}</Typography>
                                <Typography className="h3" color="textPrimary">{row.original.amount.toFixed(2)} $</Typography>
                            </div>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.checkPaymentAmount}
                                        onChange={handleChange}
                                        name="checkPaymentAmount"
                                        color="secondary"
                                    />
                                }
                                label={t("MODIFY_LOAN_PAYMENT_CHANGE_AMOUNT_LABEL")}
                            />
                            {state.checkPaymentAmount &&
                                (
                                    <FuseAnimate animation="transition.fadeIn">
                                        <TextField
                                            className="mt-8 bg-white"
                                            id="montant_pret"
                                            name="montant_pret"
                                            label={t("MODIFY_LOAN_PAYMENT_CHANGE_AMOUNT_TEXT")}
                                            type="number"
                                            variant="outlined"
                                            fullWidth
                                            color="primary"
                                            onChange={handleChangePaymentAmount}
                                            defaultValue={state.newPaymentAmount}
                                            value={state.newPaymentAmount}
                                            InputProps={{
                                                endAdornment: <InputAdornment>$</InputAdornment>,
                                            }}
                                        />

                                    </FuseAnimate>
                                )
                            }
                        </div>

                        <div className="flex-1 text-center">
                            <div className="bg-gray-100 p-16">
                                <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_PAYMENT_ORIGINAL_BALANCE")}</Typography>
                                <Typography className="h3" color="textPrimary">{loan._original.totalAmountRemaining.toFixed(2)} $</Typography>
                            </div>
                            {/* <Typography className="h5 mt-16" color="secondary">{t("MODIFY_LOAN_PAYMENT_NEW_BALANCE")}</Typography> */}
                            {/* <Typography className="h3" color="secondary">{state.newPaymentBalance} $</Typography> */}
                        </div>
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("MODIFY_LOAN_PAYMENT_CANCEL_BUTTON")}
                    </Button>
                    {state.totalValidated &&
                        <Button
                            //type="submit"
                            //disabled={state.loading}
                            variant="contained"
                            color="secondary"
                            aria-label="Enregistrer"
                            disabled={!canBeSubmitted()}
                            onClick={() =>
                                confirmModifyPayment()}

                        >
                            {state.loading &&
                                <i
                                    className="fa fa-refresh fa-spin"
                                    style={{ marginRight: "5px" }}
                                />
                            }
                            {t("MODIFY_LOAN_PAYMENT_SAVE_BUTTON")}
                        </Button>
                    }

                    {/* <Button onClick={(() => validateTotal())}
                        variant="contained"
                        color="secondary"
                        disabled={state.loading}
                        aria-label="Valider montant">
                        {state.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        Valider montant
                    </Button> */}
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
