import NotesAPI from '../../NotesAPI';

export const GET_NOTES = '[NOTES APP] GET NOTES';
export const SET_SEARCH_TEXT = '[NOTES APP] SET SEARCH TEXT';
export const OPEN_NOTE_DIALOG = '[NOTES APP] OPEN NOTE DIALOG';
export const CLOSE_NOTE_DIALOG = '[NOTES APP] CLOSE NOTE DIALOG';
export const CREATE_NOTE = '[NOTES APP] CREATE NOTE';
export const UPDATE_NOTE = '[NOTES APP] UPDATE NOTE';
export const REMOVE_NOTE = '[NOTES APP] REMOVE NOTE';
export const TOGGLE_VARIATE_DESC_SIZE = '[NOTES APP] TOGGLE VARIATE DESC SIZE';
export const ADD_LABEL_TO_COMMENT = 'ADD_LABEL_TO_COMMENT';
export const REMOVE_LABEL_FROM_COMMENT = 'REMOVE_LABEL_TO_COMMENT';
export const SET_SEARCH_LABEL = 'SET_SEARCH_LABEL';
export const TOGGLE_ARCHIVE = 'TOGGLE_ARCHIVE';

export function getNotes(clientId) {
    return async (dispatch) => {
        const result = await NotesAPI.getClientComments(clientId);
        dispatch({
            type: GET_NOTES,
            payload: result
        });
    }
}

export function setSearchText(event) {
    return {
        type: SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function resetSearchText() {
    return {
        type: SET_SEARCH_TEXT,
        searchText: ""
    }
}

export function toggleVariateDescSize() {
    return {
        type: TOGGLE_VARIATE_DESC_SIZE
    }
}

export function openNoteDialog(id) {
    return {
        type: OPEN_NOTE_DIALOG,
        payload: id
    }
}

export function closeNoteDialog() {
    return {
        type: CLOSE_NOTE_DIALOG
    }
}

export function setSearchLabel(labelId) {
    return {
        type: SET_SEARCH_LABEL,
        labelId
    }
}

export function toggleArchive() {
    return {
        type: TOGGLE_ARCHIVE
    }
}

export function createNote(note) {
    return async (dispatch) => {
        const result = await NotesAPI.createClientComment(note);
        dispatch({
            type: CREATE_NOTE,
            note: result
        });
    }
}

export function updateNote(note) {
    return async (dispatch) => {
        const result = await NotesAPI.updateClientComment(note);
        dispatch({
            type: UPDATE_NOTE,
            note: result
        });
    }

}

export function removeNote(commentId) {
    return async (dispatch) => {
        const result = await NotesAPI.deleteClientComment(commentId);
        dispatch({
            type: REMOVE_NOTE,
            id: commentId
        });
    }
}

export function addLabelToComment(clientCommentId, commentLabelId) {
    return async dispatch => {
        const result = await NotesAPI.addLabelToClientComment(clientCommentId, commentLabelId);
        dispatch({
            type: ADD_LABEL_TO_COMMENT,
            clientCommentId,
            commentLabelId
        });
    }
}

export function removeLabelFromComment(clientCommentId, commentLabelId) {
    return async dispatch => {
        const result = await NotesAPI.removeLabelFromClientComment(clientCommentId, commentLabelId);
        dispatch({
            type: REMOVE_LABEL_FROM_COMMENT,
            clientCommentId,
            commentLabelId
        });
    }
}
