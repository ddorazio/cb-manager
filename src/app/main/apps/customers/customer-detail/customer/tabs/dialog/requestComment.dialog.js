import React, { useEffect, useState } from 'react';
import localStorageService from 'app/services/localStorageService';

import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, Button, InputAdornment, DialogActions } from '@material-ui/core';
import { FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

function RequestCommentDialog(props) {
    const { t } = useTranslation('customersApp');
    const [row, setRow] = useState(props.row);
    const textDialog = t("ADD_COMMENT")

    const changeComment = (event) => {
        var copiedRow = props.row
        copiedRow.selectedRequest.comments = event.target.value
        setRow(copiedRow)
    }

    return (
        <div>
            <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="form-dialog-title" >
                <DialogContent style={{width:"500px"}}>
                <DialogContentText style={{fontWeight:"700", color:"black"}}>{textDialog}</DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="loanApproved"
                        type="text"
                        multiline={true}
                        rows={4}
                        variant="outlined"
                        onChange={(e) => changeComment(e) }
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                <Button onClick={props.handleClose} color="secondary">
                    {t('CANCEL_LOAN_DIALOG')}
                </Button>
                <Button onClick={props.handleSubmit} color="secondary">
                    {t('SAVE_LOAN_DIALOG')}
                </Button>
                </DialogActions>
            </Dialog>
        </div>

    )
}

export default withRouter(RequestCommentDialog);
