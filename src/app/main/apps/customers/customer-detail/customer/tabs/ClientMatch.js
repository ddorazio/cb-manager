import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import ReactTable from "react-table";
import _ from '@lodash';
import * as Actions from '../../store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import withReducer from 'app/store/withReducer';
import reducer from '../../store/reducers';
import { openDialog, closeDialog } from 'app/store/actions/fuse';
import { getClientHistory } from '../../store/actions';
import { withRouter } from 'react-router-dom';

const ColorButton = withStyles(theme => ({
    root: {
        color: "#FFFFFF",
        backgroundColor: "#e31f25",
        '&:hover': {
            backgroundColor: "#a6171b",
        },
    },
}))(Button);

function ClientMatch(props) {
    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const dispatch = useDispatch();
    const { customer } = props;

    const [state, setState] = useState({
        loading: false
    });

    useEffect(() => {
        if (initialState.actionStatus) {
            dispatch(closeDialog());
            const id = initialState.mergedClientId;
            props.history.push(`/apps/customers/detail/${id}/basicInfo`);
        }
    }, [initialState.actionStatus]);

    function completeMerge(row) {
        setState({
            ...state,
            loading: true
        });

        dispatch(Actions.mergeClientProfile(customer.data, row));
    }

    const mergeProfileConfirmation = (row) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("MATCH_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("MATCH_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("MATCH_CONFIRM_CANCEL")}
                            </Button>

                            <Button onClick={() => completeMerge(row)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {state.loading &&
                                    <i
                                        className="fa fa-refresh fa-spin"
                                        style={{ marginRight: "5px" }}
                                    />
                                }
                                {t("MATCH_CONFIRM_CONFIRM")}
                            </Button>


                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    return (

        <ReactTable
            className="-striped -highlight h-full w-full"
            data={customer.data.clientMatchList}
            // getTrProps={(state, rowData, column) => {
            //     return {
            //         className: "cursor-pointer",
            //         onClick: (e, handleOriginal) => {
            //             if (rowData) {
            //                 props.history.push(`/apps/customers/detail/${rowData.original.id}`);
            //             }
            //         }
            //     }
            // }}
            columns={[
                {
                    Header: "Id",
                    accessor: "id",
                    width: 100,
                },
                {
                    Header: t("MATCH_FNAME"),
                    accessor: "fName",
                },
                {
                    Header: t("MATCH_LNAME"),
                    accessor: "lName",
                },
                {
                    Header: t("MATCH_DOB"),
                    accessor: "birthday",
                    width: 120,
                },
                {
                    Header: t("MATCH_CELL"),
                    accessor: "celPhone",
                    width: 120,
                },
                {
                    Header: t("MATCH_EMAIL"),
                    accessor: "email",
                },
                {
                    Header: "",
                    sortable: false,
                    className: "no-pad-left",
                    Cell: row => (
                        <div className="flex items-center">
                            <Button
                                variant="outlined"
                                color="secondary"
                                aria-label="more"
                                aria-haspopup="true"
                                onClick={() => mergeProfileConfirmation(row.original)}
                            >
                                {t("MATCH_CONFIRM")}
                            </Button>
                            <Button
                                variant="outlined"
                                color="secondary"
                                aria-label="more"
                                aria-haspopup="true"
                                onClick={() => props.history.push(`/apps/customers/detail/${row.original.id}`)}
                            >
                                {t("MATCH_PROFILE")}
                            </Button>
                        </div>
                    )
                }
            ]}
            resizable={false}
            filterable={false}
            defaultPageSize={10}
            noDataText={t("emptyDataSourceMessage")}
        />
    )
}
//export default withReducer('CustomerComp', reducer)(ClientMatch);
export default withRouter(ClientMatch);
