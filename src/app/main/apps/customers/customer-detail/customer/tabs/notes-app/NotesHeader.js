import React from 'react';
import { Hidden, Icon, IconButton, Tooltip, Typography } from '@material-ui/core';
import { FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from './store/actions';
import NotesSearch from './NotesSearch';
import { useTranslation } from 'react-i18next';
import NoteType from './NoteType'
function NotesHeader(props) {

    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const variateDescSize = useSelector(({ notesApp }) => notesApp.notes.variateDescSize);

    const title = () => {
        switch (props.type) {
            case NoteType.CLIENT:
                return t("COMMENTS_HEADER")       
            case NoteType.COLLECTION:
                return "Notes"
            default:
                break;
        }
    }
    
    return (
        <div className="flex flex-1 items-center justify-between relative">

            <div className="flex flex-shrink items-center sm:w-224">
                <div className="flex items-center">
                    <Icon className="text-32">comment</Icon>
                    <Typography variant="h6" className="mx-12 hidden sm:flex">{title()}</Typography>
                </div>
            </div>
            {props.type == NoteType.CLIENT && 
                <div className="flex flex-1 items-center justify-end">
                    <NotesSearch />
                </div>
            }
            
        </div>
    );
}

export default NotesHeader;
