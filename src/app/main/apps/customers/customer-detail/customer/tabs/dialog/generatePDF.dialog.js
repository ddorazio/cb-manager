import React, { useEffect, useState } from 'react';
import localStorageService from 'app/services/localStorageService';
import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, Button, InputAdornment, FormControl, Select, InputLabel, DialogActions } from '@material-ui/core';
import { FuseLoading } from '@fuse';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import reducer from '../../../store/reducers';
import * as Actions from '../../../store/actions';
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import PDFGenerator from '../../PDFGenerator'


const useStyles = makeStyles(theme => ({
    dialogRoot: {
        overflow: "hidden"
    },

}));

function GeneratePDFDialog(props) {
    const dispatch = useDispatch();
    const classes = useStyles(props);
    const { t } = useTranslation('customersApp');
    const loanId = props.loanId
    const customer = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const [optionValue, setOptionValue] = useState(0)
    const [email, setEmail] = useState(customer.data.basicInfo.personalInfo.emailAddress)
    const open = customer.pdfGeneratorDialog.open;
    const sendPDF = customer.sendPDF;

    useEffect(() => {
        if (loanId && open) {
            dispatch(Actions.getPDFData(loanId))
        }
    }, [open]);

    const handleSubmit = () => {
        dispatch(Actions.sendPDF(true))
        dispatch(Actions.closePDFGeneratorDialog());
    }

    const handleClose = () => {
        dispatch(Actions.closePDFGeneratorDialog());
    };

    const handleTypeChange = (event) => {
        setOptionValue(parseInt(event.target.value))
    }
    const handleEmailChange = (event) => {
        setEmail(event.target.value)
    }

    if (!customer.data.pdfData) {
        return (<div></div>)
    }

    return (
        <div>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" PaperProps={{
                style: {
                    overflow: "hidden"
                }
            }}>
                <DialogContent id="PDFGeneratorDialog">
                    <DialogTitle id="alert-dialog-title">{t("LOANPDF_DIALOG_TITLE")}</DialogTitle>
                    <FormControl variant="outlined" className="mt-16 mb-16 w-full">
                        <InputLabel id="loanPDF-label">{t("LOANPDF_CHOOSE_TYPE")}</InputLabel>
                        <Select
                            native
                            labelId="loanPDF-label"
                            id="loanPDFId"
                            value={optionValue}
                            onChange={handleTypeChange}
                            label="label"
                        >
                            <option value={0}>{t("LOANPDF_ACCOUNT_BALANCE")}</option>
                            <option value={1}>{t("LOANPDF_ACCOUNT_DETAILS")}</option>
                        </Select>
                    </FormControl>
                    <TextField
                        className="mt-8 mb-16 w-full"
                        id="client_pdf_email"
                        name="client_pdf_email"
                        label="Email"
                        type="text"
                        variant="outlined"
                        value={email}
                        onChange={handleEmailChange}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        {t("CANCEL")}
                    </Button>
                    <Button onClick={handleSubmit} color="secondary">
                        {t("SEND")}
                    </Button>
                </DialogActions>
                <PDFGenerator sendPDF={sendPDF} email={email} typePDF={optionValue} selectedTypeReport={optionValue} data={customer.data.pdfData} />
            </Dialog>
        </div>

    )
}

export default withReducer('CustomerComp', reducer)(GeneratePDFDialog);
