import { getProtected, putProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class NotesAPI {
    static getClientComments = async (clientId) => {
        return await getProtected(`${settingsConfig.apiPath}/api/ClientComment/GetClientComments/${clientId}`);
    }

    static createClientComment = async (note) => {
        return await postProtected(`${settingsConfig.apiPath}/api/ClientComment/CreateClientComment`, note);
    }

    static updateClientComment = async (note) => {
        return await putProtected(`${settingsConfig.apiPath}/api/ClientComment/UpdateClientComment`, note);
    }

    static deleteClientComment = async (commentId) => {
        return await deleteProtected(`${settingsConfig.apiPath}/api/ClientComment/DeleteClientComment/${commentId}`);
    }

    static addLabelToClientComment = async (clientCommentId, commentLabelId) => {
        return await postProtected(`${settingsConfig.apiPath}/api/ClientComment/AddLabelToClientComment/${clientCommentId}/${commentLabelId}`);
    }

    static removeLabelFromClientComment = async (clientCommentId, commentLabelId) => {
        return await deleteProtected(`${settingsConfig.apiPath}/api/ClientComment/RemoveLabelFromClientComment/${clientCommentId}/${commentLabelId}`);
    }

    static getCollectionComments = async (clientId) => {
        return await getProtected(`${settingsConfig.apiPath}/api/ClientComment/GetClientComments/${clientId}`);
    }

    static createCollectionComment = async (note) => {
        return await postProtected(`${settingsConfig.apiPath}/api/ClientComment/CreateClientComment`, note);
    }

    static updateCollectionComment = async (note) => {
        return await putProtected(`${settingsConfig.apiPath}/api/ClientComment/UpdateClientComment`, note);
    }

    static deleteCollectionComment = async (commentId) => {
        return await deleteProtected(`${settingsConfig.apiPath}/api/ClientComment/DeleteClientComment/${commentId}`);
    }

    static addLabelToCollectionComment = async (clientCommentId, commentLabelId) => {
        return await postProtected(`${settingsConfig.apiPath}/api/ClientComment/AddLabelToClientComment/${clientCommentId}/${commentLabelId}`);
    }

    static removeLabelFromCollectionComment = async (clientCommentId, commentLabelId) => {
        return await deleteProtected(`${settingsConfig.apiPath}/api/ClientComment/RemoveLabelFromClientComment/${clientCommentId}/${commentLabelId}`);
    }

}

export default NotesAPI