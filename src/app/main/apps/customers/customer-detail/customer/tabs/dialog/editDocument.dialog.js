import React, { useRef, useState } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Icon, IconButton, InputLabel, MenuItem, MenuList, Paper, Popper, Select, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { withStyles } from '@material-ui/core/styles';
import frLocale from "date-fns/locale/fr";
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import withReducer from 'app/store/withReducer';
import reducer from '../../../store/reducers/documents.reducer';
import { updateDocument } from '../../../store/actions/documents.actions';
import { showMessage } from 'app/store/actions/fuse';

const localeMap = { fr: frLocale };

const useStyles = makeStyles(theme => ({
    dropzoneParagraph: {
        fontSize: "18px",
        paddingTop: "75px"
    }
}));

function EditDocumentDialog(props) {
    const classes = useStyles(props);
    const dispatch = useDispatch();
    const { t } = useTranslation('customersApp');
    const [fullWidth, setFullWidth] = React.useState(true);
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const [title, setTitle] = useState(props.data.title);
    const [clientLoanId, setClientLoanId] = useState(props.data.clientLoanId);
    const [documentTypeCode, setDocumentType] = useState(props.data.documentTypeCode);
    const [note, setNote] = useState(props.data.note);

    const handleClose = () => {
        setOpen(false);
        //resetForm();
    };

    const submit = () => {
        if (!title) {
            dispatch(showMessage({
                message: 'Veuillez assigner un titre au fichier.',
                variant: 'error'
            }));
            return;
        }
        if (!documentTypeCode) {
            dispatch(showMessage({
                message: 'Veuillez sélectionner un type de document.',
                variant: 'error'
            }));
            return;
        }
        dispatch(updateDocument({
            id: props.data.id,
            title,
            clientLoanId,
            documentTypeCode,
            note
        }));
        handleClose();
    }

    const ColorButton = withStyles(theme => ({
        root: {
            color: "#fff",
            backgroundColor: "#e41f25",
            '&:hover': {
                backgroundColor: "#000000",
            },
        },
    }))(Button);

    return (
        <React.Fragment>
            <IconButton onClick={handleClickOpen}><Icon>edit</Icon></IconButton>
            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("EDIT_DOCUMENT")}</DialogTitle>
                <DialogContent>
                    <div className="flex">
                        <div className="flex-1 mr-16 dropZoneBox">
                            <TextField
                                className="w-full mb-16"
                                id="document_name"
                                name="document_name"
                                label={t("DOCUMENT_NAME")}
                                type="text"
                                variant="outlined"
                                fullWidth
                                value={title}
                                onChange={e => {
                                    setTitle(e.target.value);
                                }}
                                error={!title}
                            />
                            <FormControl variant="outlined" className="mb-16 w-full">
                                <InputLabel id="document-label">{t("ASSOCIATED_LOAN")}</InputLabel>
                                <Select
                                    labelId="document-label"
                                    defaultValue={0}
                                    id="documentId"
                                    value={clientLoanId}
                                    onChange={e => {
                                        setClientLoanId(e.target.value);
                                    }}
                                    label={t("ASSOCIATED_LOAN")}
                                >
                                    <MenuItem value={0}>En attente</MenuItem>
                                    {props.loans.map(loan => <MenuItem value={loan.id}>{loan.loanNumber}</MenuItem>)}
                                </Select>
                            </FormControl>
                            <FormControl variant="outlined" className="mb-16 w-full">
                                <InputLabel id="documentType-label">{t("DOCUMENT_TYPE")}</InputLabel>
                                <Select
                                    labelId="documentType-label"
                                    id="documentType"
                                    value={documentTypeCode}
                                    onChange={e => {
                                        setDocumentType(e.target.value);
                                    }}
                                    label={t("DOCUMENT_TYPE")}
                                    error={!documentTypeCode}
                                >
                                    <MenuItem value={'void_check'}>Spécimen de chèque</MenuItem>
                                    <MenuItem value={'contract'}>Contrat</MenuItem>
                                    <MenuItem value={'other'}>Autre</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField
                                className="w-full"
                                id="document_note"
                                name="document_note"
                                label="Note"
                                type="text"
                                variant="outlined"
                                multiline
                                rows={3}
                                value={note}
                                onChange={e => {
                                    setNote(e.target.value);
                                }}
                                fullWidth
                            />
                        </div>
                    </div>


                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("CANCEL")}
                    </Button>
                    <Button
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        onClick={() => {
                            submit();
                        }}
                    >
                        {t("SAVE")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default withReducer('documents', reducer)(EditDocumentDialog);