import React, { useEffect, useRef } from 'react';
import { FusePageSimple } from '@fuse';
import { Card, CardContent, AppBar, Toolbar } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import NoteDialog from './dialogs/note/NoteDialog';
import LabelsDialog from './dialogs/labels/LabelsDialog';
import NoteList from './NoteList';
import NotesHeader from './NotesHeader';
import NotesSidebarContent from './NotesSidebarContent';
import NewNote from './NewNote';
import NoteType from './NoteType'

function NotesApp(props) {

    const dispatch = useDispatch();
    const client = useSelector(store => store.CustomerComp.customer.data);

    const pageLayout = useRef(null);

    useEffect(() => {
        switch (props.type) {
            case NoteType.CLIENT: dispatch(Actions.getNotes(client.id))
            case NoteType.COLLECTION: dispatch(Actions.getCollectionNotes(props.collectionId))
        }

        dispatch(Actions.getLabels());
    }, [dispatch]);

    return (
        <React.Fragment>
            <Card className="w-full mb-16">
                <AppBar position="static" elevation={0} className="black_bg">
                    <Toolbar className="pl-16 pr-8">
                        <NotesHeader {...props} />
                    </Toolbar>
                </AppBar>
                <CardContent>
                    <div className="flex">
                        {props.type == NoteType.CLIENT &&
                            <div className="flex-1 max-w-200 mr-16 mb-16">
                                <NotesSidebarContent />
                            </div>
                        }
                        <div className="flex-1">
                            <NewNote {...props} />
                            <NoteList {...props} />
                            <NoteDialog {...props} />
                            <LabelsDialog {...props} />
                        </div>

                    </div>
                </CardContent>
            </Card>
        </React.Fragment>
    )
}

export default withReducer('notesApp', reducer)(NotesApp);
