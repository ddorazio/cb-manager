import React from 'react';
import { Divider, Icon, List, ListItem, ListItemText, Paper, ListSubheader } from '@material-ui/core';
import { FuseAnimate, NavLinkAdapter } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from './store/actions';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import { setSearchLabel, toggleArchive } from './store/actions/notes.actions';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    paper: {
        [theme.breakpoints.down('md')]: {
            boxShadow: 'none'
        }
    },
    listItem: {
        color: 'inherit!important',
        textDecoration: 'none!important',
        height: 40,
        width: 'calc(100% - 16px)',
        borderRadius: '0 20px 20px 0',
        paddingLeft: 24,
        paddingRight: 12,
        '&.active': {
            backgroundColor: theme.palette.secondary.main,
            color: theme.palette.secondary.contrastText + '!important',
            pointerEvents: 'none',
            '& .list-item-icon': {
                color: 'inherit'
            }
        },
        '& .list-item-icon': {
            marginRight: 16
        }
    }
}));

function NotesSidebarContent(props) {

    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();

    const labels = useSelector(({ notesApp }) => notesApp.labels.entities);
    const labelId = useSelector(({ notesApp }) => notesApp.notes.labelId);
    const showArchived = useSelector(({ notesApp }) => notesApp.notes.showArchived);

    const classes = useStyles(props);

    const onToggleArchive = () => {
        dispatch(toggleArchive());
    }

    return (
        <div>
            <FuseAnimate animation="transition.slideLeftIn" delay={200}>
                <Paper elevation={1} className={clsx(classes.paper, "rounded-8")}>
                    <List>
                        <ListItem
                            button
                            onClick={() => dispatch(setSearchLabel(null))} // Reset filter
                            exact={true}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">label</Icon>
                            <ListItemText className="truncate" primary={t("COMMENTS_SIDEBAR_ALL")} disableTypography={true} />
                        </ListItem>
                        {/*  <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/notes/reminders'}
                            exact={true}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">notifications</Icon>
                            <ListItemText className="truncate" primary="Reminders" disableTypography={true}/>
                        </ListItem> */}
                    </List>
                    <Divider />
                    <List>
                        <ListSubheader>
                            {t("COMMENTS_SIDEBAR_CATEGORIES")}
                        </ListSubheader>
                        {Object.entries(labels).map(([key, label]) => (
                            <ListItem
                                key={label.id}
                                button
                                onClick={() => dispatch(setSearchLabel(label.id))}
                                selected={label.id === labelId}
                                exact={true}
                                activeClassName="active"
                                className={classes.listItem}
                            >
                                <Icon className="list-item-icon text-16" color="action">label</Icon>
                                <ListItemText className="truncate" primary={label.name} disableTypography={true} />
                            </ListItem>
                        ))}
                        {/*
                        <ListItem
                            button
                            className={classes.listItem}
                            onClick={ev => dispatch(Actions.openLabelsDialog())}
                        >
                            <Icon className="list-item-icon text-16" color="action">edit</Icon>
                            <ListItemText className="truncate" primary="Modifier les filtres" disableTypography={true}/>
                        </ListItem>
                        */}
                    </List>
                    <Divider />
                    <List>
                        <ListItem
                            button
                            onClick={onToggleArchive}
                            activeClassName="active"
                            className={classes.listItem}
                            selected={showArchived}
                        >
                            <Icon className="list-item-icon text-16" color="action">archive</Icon>
                            <ListItemText className="truncate" primary={t("COMMENTS_SIDEBAR_ARCHIVE")} disableTypography={true} />
                        </ListItem>
                    </List>
                </Paper>
            </FuseAnimate>
        </div>
    );
}

export default NotesSidebarContent;
