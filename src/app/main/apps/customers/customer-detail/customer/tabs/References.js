import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FuseAnimateGroup, FuseLoading } from '@fuse';
import MaterialTable from 'material-table';
import { TextField } from '@material-ui/core';
import MaskedInput from 'react-text-mask';
import { useTranslation } from 'react-i18next';
import { fetchCustomerReferences, updateClientReference, createClientReference, deleteClientReference } from '../../store/actions';

const RowField = props => {
    return (
        <TextField
            {...props}
            value={props.value}
            placeholder={props.columnDef.title + (props.required ? " *" : "")}
            error={props.required ? !props.value : false}
            onChange={e => props.onChange(e.target.value)}
        />
    );
}

const PhoneNumberField = props => {
    return <TextField
        {...props}
        value={props.value}
        onChange={e => {
            props.onChange(e.target.value)
        }}
        error={!props.value}
        InputProps={{
            inputComponent: PhoneNumberMask,
            inputProps: props
        }}
    />;
}

const PhoneNumberMask = props => {
    return <MaskedInput
        {...props}
        mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
        placeholderChar={'\u2000'}
        showMask
        onChange={e => props.onChange(e)}
    />
}

function References() {
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const clientId = useSelector(({ CustomerComp }) => CustomerComp.customer.data.id);
    const references = useSelector(({ CustomerComp }) => CustomerComp.customer.data.references);
    const creating = clientId == null | clientId == undefined; // If we don't have a clientId we are creating one

    const validateRow = rowData =>
        rowData.firstName
        && rowData.lastName
        && rowData.phoneNumber
        && rowData.relation;

    useEffect(() => {
        if (!creating && !references) {
            dispatch(fetchCustomerReferences(clientId));
        }
    }, [references]);

    if (clientId && !references) {
        return <FuseLoading />
    }

    return (
        <div className="md:flex">
            <div className="flex flex-col flex-1 md:pr-32">
                    <MaterialTable
                        title={t("REFERENCES_LIST")}
                        columns={[
                            {
                                title: "Id",
                                field: 'id',
                                hidden: true
                            },
                            {
                                title: "ClientId",
                                field: 'clientId',
                                hidden: true
                            },
                            {
                                title: t("SURNAME"),
                                field: 'firstName',
                                editComponent: props => <RowField {...props} required />
                            },
                            {
                                title: t("NAME"),
                                field: 'lastName',
                                editComponent: props => <RowField {...props} required />
                            },
                            {
                                title: t("PHONE_HOME"),
                                field: 'phoneNumber',
                                editComponent: PhoneNumberField
                            },
                            {
                                title: t("LINK"),
                                field: 'relation',
                                editComponent: props => <RowField {...props} required />
                            },
                            {
                                title: t("EMAIL"),
                                field: 'emailAddress',
                                editComponent: RowField
                            }
                        ]}
                        data={references}
                        options={{
                            actionsColumnIndex: -1
                        }}
                        editable={{
                            onRowAdd: newData => new Promise((resolve, reject) => {
                                if (!validateRow(newData)) {
                                    reject();
                                    return;
                                }
                                setTimeout(() => {   
                                    dispatch(createClientReference({
                                        clientId,
                                        ...newData
                                    }, creating));
                                    resolve();
                                }, 600);

                            }),
                            onRowUpdate: newData => new Promise((resolve, reject) => {
                                if (!validateRow(newData)) {
                                    reject();
                                    return;
                                }
                                setTimeout(() => {   
                                    dispatch(updateClientReference(newData, creating));
                                    resolve();
                                }, 600);
                            }),
                            onRowDelete: oldData => new Promise(resolve => {
                                setTimeout(() => {
                                    dispatch(deleteClientReference(oldData.id, creating));
                                resolve();
                                }, 600);
                            })
                        }}
                        localization={{
                            body: {
                                emptyDataSourceMessage: t("emptyDataSourceMessage"),
                                addTooltip: t("addTooltip"),
                                deleteTooltip: t("deleteTooltip"),
                                editTooltip: t("editTooltip"),
                                editRow: {
                                    deleteText: t("deleteText"),
                                    cancelTooltip: t("cancelTooltip"),
                                    saveTooltip: t("saveTooltip"),
                                }
                            },
                            toolbar: {
                                nRowsSelected: t("nRowsSelected"),
                                searchTooltip: t("searchTooltip"),
                                searchPlaceholder: t("searchPlaceholder"),
                            },
                            pagination: {
                                labelDisplayedRows: t("labelDisplayedRows"),
                                labelRowsSelect: t("labelRowsSelect"),
                                labelRowsPerPage: t("labelRowsPerPage"),
                                firstTooltip: t("firstTooltip"),
                                previousTooltip: t("previousTooltip"),
                                nextTooltip: t("nextTooltip"),
                                lastTooltip: t("lastTooltip"),
                            }
                        }}
                    />
            </div>
        </div>

    );
}
export default References;