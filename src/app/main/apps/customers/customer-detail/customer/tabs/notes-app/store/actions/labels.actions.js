import LabelsAPI from '../../LabelsAPI';
import settingsConfig from 'app/fuse-configs/settingsConfig';
import { showMessage, closeDialog } from 'app/store/actions/fuse';

export const GET_LABELS = '[NOTES APP] GET LABELS';
export const LABELS_DIALOG_OPEN = '[NOTES APP] LABELS DIALOG OPEN';
export const LABELS_DIALOG_CLOSE = '[NOTES APP] LABELS DIALOG CLOSE';
export const UPDATE_LABELS = '[NOTES APP] LABELS UPDATE LABELS';

export function getLabels() {
    return async (dispatch) => {
        try {
            const labels = await LabelsAPI.getLabels();
            dispatch({
                type: GET_LABELS,
                payload: labels
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export function updateLabels(labels) {
    // const request = axiosPostProtected('/api/notes-app/update-labels', { labels: Object.values(labels) });

    // return (dispatch) =>
    //     request.then((response) =>
    //         dispatch({
    //             type: UPDATE_LABELS,
    //             payload: response.data
    //         })
    //     );
}

export function openLabelsDialog() {
    return {
        type: LABELS_DIALOG_OPEN
    }
}

export function closeLabelsDialog() {
    return {
        type: LABELS_DIALOG_CLOSE
    }
}
