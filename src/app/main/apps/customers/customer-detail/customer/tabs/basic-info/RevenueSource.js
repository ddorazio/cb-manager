import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AppBar, Card, CardContent, FormControlLabel, FormControl, Icon, InputLabel, Select, TextField, Toolbar, Typography, Radio, RadioGroup } from '@material-ui/core';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

const RevenueSource = (props) => {
    const { t } = useTranslation('customersApp');
    const revenueSource = props.form.basicInfo.revenueSource;
    const { form, setForm } = props;
    const handleChange = (e) => {
        setForm({
            ...form,
            basicInfo: {
                ...form.basicInfo,
                revenueSource: {
                    ...form.basicInfo.revenueSource,
                    [e.target.name]: e.target.value
                }
            }
        });
    }

    const incomeList = [{
        code: 'EMPLOYEE',
        name: t("EMPLOYEE")
    }, {
        code: 'EMPLOYMENT_INSURANCE',
        name: t("EMPLOYMENT_INSURANCE")
    }, {
        code: 'RETIREMENT_PLAN',
        name: t("RETIREMENT_PLAN")
    }, {
        code: 'DISABILITY',
        name: t("DISABILITY")
    }, {
        code: 'SELF_EMPLOYED',
        name: t("SELF_EMPLOYED")
    }, {
        code: 'PARENTAL_INSURANCE',
        name: t("PARENTAL_INSURANCE")
    }, {
        code: 'SOCIAL_ASSISTANCE',
        name: t("SOCIAL_ASSISTANCE")
    }];

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Icon>account_box</Icon>
                    <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("SRC_OF_INCOME")}
                    </Typography>
                </Toolbar>
            </AppBar>
            <CardContent>
                <FormControl className="mt-8 mb-16 w-full" variant="outlined" >
                    <InputLabel id="incomeTypeCode">{t("SRC_OF_INCOME")}</InputLabel>
                    <Select
                        labelid="incomeTypeCode"
                        id="incomeTypeCode"
                        name="incomeTypeCode"
                        native
                        value={revenueSource.incomeTypeCode}
                        onChange={
                            e => setForm({
                                ...form,
                                basicInfo: {
                                    ...form.basicInfo,
                                    revenueSource: {
                                        ...form.basicInfo.revenueSource,
                                        incomeTypeCode: e.target.value
                                    }
                                }
                            })
                        }
                    >
                        <option aria-label="None" value="" />
                        {incomeList.map((item, index) => (
                            <option key={index} value={item.code}>{item.name}</option>
                        ))}
                    </Select>
                </FormControl>

                {revenueSource.incomeTypeCode == 'EMPLOYMENT_INSURANCE' &&
                    <div>
                        <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("EMPLOYMENT_INSURANCE_TITLE")}</Typography>
                        <div className="flex">
                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                <KeyboardDatePicker
                                    autoOk
                                    clearable="true"
                                    required
                                    disableFuture
                                    disableToolbar
                                    InputAdornmentProps={{ position: "start" }}
                                    className="mt-8 mb-16 w-full"
                                    format="DD/MM/YYYY"
                                    id="employmentInsuranceStart"
                                    name="employmentInsuranceStart"
                                    label={t("EMPLOYMENT_INSURANCE_START")}
                                    variant="inline"
                                    inputVariant="outlined"
                                    error={revenueSource.employmentInsuranceStart ? false : true}
                                    defaultValue={revenueSource.employmentInsuranceStart}
                                    value={revenueSource.employmentInsuranceStart ? revenueSource.employmentInsuranceStart : null}
                                    onChange={e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            revenueSource: {
                                                ...form.basicInfo.revenueSource,
                                                employmentInsuranceStart: e
                                            }
                                        }
                                    })}
                                />
                            </MuiPickersUtilsProvider>
                        </div>
                        <div>
                            <div className="flex">
                                <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                    <KeyboardDatePicker
                                        autoOk
                                        clearable="true"
                                        required
                                        disableToolbar
                                        InputAdornmentProps={{ position: "start" }}
                                        className="mt-8 mb-16 w-full"
                                        format="DD/MM/YYYY"
                                        id="employmentInsuranceNextPayDate"
                                        name="employmentInsuranceNextPayDate"
                                        label={t("EMPLOYMENT_NEXT_PAY_TITLE")}
                                        variant="inline"
                                        inputVariant="outlined"
                                        error={revenueSource.employmentInsuranceNextPayDate ? false : true}
                                        defaultValue={revenueSource.employmentInsuranceNextPayDate}
                                        value={revenueSource.employmentInsuranceNextPayDate ? revenueSource.employmentInsuranceNextPayDate : null}
                                        onChange={e => setForm({
                                            ...form,
                                            basicInfo: {
                                                ...form.basicInfo,
                                                revenueSource: {
                                                    ...form.basicInfo.revenueSource,
                                                    employmentInsuranceNextPayDate: e
                                                }
                                            }
                                        })}
                                    />
                                </MuiPickersUtilsProvider>
                            </div>
                        </div>
                    </div>
                }
                {(revenueSource.incomeTypeCode == 'EMPLOYEE') &&
                    <div>
                        <div className="flex">
                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                <KeyboardDatePicker
                                    autoOk
                                    clearable="true"
                                    required
                                    disableToolbar
                                    InputAdornmentProps={{ position: "start" }}
                                    className="mt-8 mb-16 w-full"
                                    format="DD/MM/YYYY"
                                    id="employmentInsuranceStart"
                                    name="employmentInsuranceStart"
                                    label={t("EMPLOYMENT_NEXT_PAY_TITLE")}
                                    variant="inline"
                                    inputVariant="outlined"
                                    error={revenueSource.nextPayDate ? false : true}
                                    defaultValue={revenueSource.nextPayDate}
                                    value={revenueSource.nextPayDate ? revenueSource.nextPayDate : null}
                                    onChange={e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            revenueSource: {
                                                ...form.basicInfo.revenueSource,
                                                nextPayDate: e
                                            }
                                        }
                                    })}
                                />
                            </MuiPickersUtilsProvider>
                        </div>
                        
                    </div>
                }
                {(revenueSource.incomeTypeCode == 'DISABILITY' || revenueSource.incomeTypeCode == 'PARENTAL_INSURANCE' || revenueSource.incomeTypeCode == 'SELF_EMPLOYED') &&
                    <div>
                        <div className="flex">
                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                <KeyboardDatePicker
                                    autoOk
                                    clearable="true"
                                    required
                                    disableToolbar
                                    InputAdornmentProps={{ position: "start" }}
                                    className="mt-8 mb-16 w-full"
                                    format="DD/MM/YYYY"
                                    id="nextDepositDate"
                                    name="nextDepositDate"
                                    label={t("EMPLOYMENT_NEXT_DEPOSIT")}
                                    variant="inline"
                                    inputVariant="outlined"
                                    error={revenueSource.nextDepositDate ? false : true}
                                    defaultValue={revenueSource.nextDepositDate}
                                    value={revenueSource.nextDepositDate ? revenueSource.nextDepositDate : null}
                                    onChange={e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            revenueSource: {
                                                ...form.basicInfo.revenueSource,
                                                nextDepositDate: e
                                            }
                                        }
                                    })}
                                />
                            </MuiPickersUtilsProvider>
                        </div>
                    </div>
                }
                {revenueSource.incomeTypeCode == 'SELF_EMPLOYED' &&
                    <div>
                        <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("SELF_EMPLOYED_DIRECT_DEPOSIT")}</Typography>
                        <FormControl component="fieldset">
                            <RadioGroup
                                style={{ display: 'flex', flexDirection: 'row' }}
                                name="selfEmployedDirectDeposit"
                                value={revenueSource.selfEmployedDirectDeposit}
                                onChange={handleChange}
                            >
                                <FormControlLabel value={true} control={<Radio />} label={t("YES")} />
                                <FormControlLabel value={false} control={<Radio />} label={t("NO")} />
                            </RadioGroup>
                        </FormControl>

                        <TextField
                            className="mt-8 mb-16 mr-16 w-full"
                            required
                            label={t("SELF_EMPLOYED_PHONE")}
                            id="selfEmployedPhoneNumber"
                            name="selfEmployedPhoneNumber"
                            value={revenueSource.selfEmployedPhoneNumber}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            error={revenueSource.selfEmployedPhoneNumber ? false : true}
                            InputProps={{
                                startAdornment: (
                                    <Icon>phone</Icon>
                                )
                            }}
                        />

                        <FormControl className="mt-8 mb-16 mr-16 w-full" variant="outlined" >
                            <InputLabel id="selfEmployedPayFrequencyId">{t("PAY_FREQUENCY")}</InputLabel>
                            <Select
                                labelid="selfEmployedPayFrequencyId"
                                id="selfEmployedPayFrequencyId"
                                name="selfEmployedPayFrequencyId"
                                required
                                native
                                value={revenueSource.selfEmployedPayFrequencyId}
                                onChange={(e) => {
                                    setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            revenueSource: {
                                                ...form.basicInfo.revenueSource,
                                                selfEmployedPayFrequencyId: e.target.value
                                            }
                                        }
                                    });
                                }}
                                error={revenueSource.selfEmployedPayFrequencyId === '' ? true : false}
                            >
                                <option aria-label="None" value="" />
                                {form.payFrequencies.map((item, index) => (
                                    <option key={index} value={item.id}>{item.name}</option>
                                ))}
                            </Select>
                        </FormControl>

                        <div className="flex">
                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                <KeyboardDatePicker
                                    autoOk
                                    clearable="true"
                                    required
                                    disableFuture
                                    disableToolbar
                                    InputAdornmentProps={{ position: "start" }}
                                    className="mt-8 mb-16 w-full"
                                    format="DD/MM/YYYY"
                                    id="selfEmployedStartDate"
                                    name="selfEmployedStartDate"
                                    label={t("EMPLOYMENT_START")}
                                    variant="inline"
                                    inputVariant="outlined"
                                    error={revenueSource.selfEmployedStartDate ? false : true}
                                    defaultValue={revenueSource.selfEmployedStartDate}
                                    value={revenueSource.selfEmployedStartDate ? revenueSource.selfEmployedStartDate : null}
                                    onChange={e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            revenueSource: {
                                                ...form.basicInfo.revenueSource,
                                                selfEmployedStartDate: e
                                            }
                                        }
                                    })}
                                />
                            </MuiPickersUtilsProvider>
                        </div>
                    </div>
                }
                {revenueSource.incomeTypeCode == 'SOCIAL_ASSISTANCE' &&
                    <div>
                        <Typography variant="subtitle2" color="secondary" className="mb-8 w-full"> {t("EMPLOYMENT_UNSUPPORTED")}</Typography>
                    </div>
                }
            </CardContent>
        </Card>
    );
}

export default RevenueSource;