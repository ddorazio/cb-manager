import React, { useEffect, useRef, useState } from 'react';
import { FuseLoading } from '@fuse';
import { Card, CardContent, CardActions, Button, ButtonGroup, AppBar, Grid, InputLabel, Select, MenuItem, FormControl, FormGroup, FormControlLabel, Checkbox, Toolbar, Icon, Typography, TextField, ClickAwayListener, Paper, } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from './notes-app/store/actions';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

import { useTranslation } from 'react-i18next';
import notesReducer from './notes-app/store/reducers';
import NotesApp from './notes-app/NotesApp';
import NoteType from './notes-app/NoteType';
import CollectionDegree from 'app/main/apps/collections/components/CollectionDegree';
import LoanStatus from 'app/main/apps/loans/components/LoanStatus';
import { makeStyles } from '@material-ui/styles';
import { selectCollection, getCollection, updateCollection, toggleProcedureStep } from '../../store/actions/collection.actions';
import { getCollectionNotes } from '../tabs/notes-app/store/actions/collectionNotes.actions';
import { sendInteracCollectionReminders } from 'app/main/apps/collections/store/actions/collections.actions';

moment.locale(localStorageService.getLanguage())

const useStyles = makeStyles({
    button: {
        cursor: 'text'
    }
});

const ActiveLoan = (props) => {
    const { t } = useTranslation('customersApp');
    return <span>
        {t('LOAN')} #{`${props.loanNumber}`.padStart(6, '0')} - <LoanStatus code={props.loanStatusCode} />
    </span>
}

function Collections(props) {
    const activeCollections = useSelector(store => store.CustomerComp.customer.data.activeCollections);
    const store = useSelector(store => store.CustomerComp.collection);

    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const classes = useStyles(props);

    const [form, setForm] = useState({});
    const handleChange = (e) => {
        const { name, value } = e.target;
        setForm(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const saveChanges = () => {
        dispatch(updateCollection({
            ...store.data,
            ...form
        }));
    }

    const getDaysSinceDate = (dateString) => {
        return Math.floor((new Date().getTime() - new Date(dateString)) / (1000 * 3600 * 24));
    }

    useEffect(() => {

        // DAVID; If you create a second NSF that goes into collections,
        // it will never go back to the database
        // Trigger loading the first collection in the list automatically
        // if (!store.collectionId && activeCollections[0]) {
        //     dispatch(selectCollection(activeCollections[0].collectionId));
        // }

        dispatch(getCollection(activeCollections[0].collectionId));
        dispatch(getCollectionNotes(activeCollections[0].collectionId));

    }, [activeCollections]);

    // DAVID: No longer needed
    /*useEffect(() => {
        if (store.collectionId) {
            dispatch(getCollection(store.collectionId));
            dispatch(getCollectionNotes(store.collectionId));
        }
    }, [store.collectionId]);*/

    useEffect(() => {
        // We only want this to trigger when we load a new collection
        // Otherwise it throws away unsaved changes
        if (store.data.id !== form.id) {
            setForm(store.data);
        }
    }, [store.data]);

    useEffect(() => {
        // Track changes to collection status and apply the proper business rules
        switch (form.collectionStatusCode) {
            case 'NEED_FOLLOW_UP':
                setForm(oldState => ({
                    ...oldState,
                    paymentAgreementCode: 'OTHER'
                }));
                break;
            case 'INTERAC_E_TRANSFER':
                setForm(oldState => ({
                    ...oldState,
                    paymentAgreementCode: 'INTERAC_E_TRANSFER'
                }));
                break;
        }
    }, [form.collectionStatusCode]);

    useEffect(() => {
        // Track changes to payment agreement and apply the proper business rules
        switch (form.paymentAgreementCode) {
            case 'DEFER_PAYMENT':
                setForm(oldState => ({
                    ...oldState,
                    collectionStatusCode: 'SETTLED'
                }));
                break;
        }
    }, [form.paymentAgreementCode]);


    function canSaveCollectionProfile() {
        return form.collectionStatusCode
            && form.nonPaymentReasonCode;
    }

    if (store.loading) {
        return <FuseLoading />
    }

    return (
        <React.Fragment>
            <div className="flex">
                <div className="flex-1 mr-16">
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <Typography variant="h6" className="mx-12 hsidden sm:flex">{t('CLIENT_PROFILE')}</Typography>
                            </Toolbar>
                        </AppBar>
                        <CardContent>
                            {activeCollections.length > 1
                                ? <Select
                                    value={store.collectionId}
                                    onChange={e => {
                                        if (e.target.value != store.collectionId) {
                                            dispatch(selectCollection(e.target.value))
                                            dispatch(getCollection(e.target.value));
                                            dispatch(getCollectionNotes(e.target.value));
                                        };
                                    }
                                    }>
                                    {activeCollections.map(coll =>
                                        <MenuItem value={coll.collectionId} key={coll.collectionId}>
                                            <ActiveLoan loanNumber={coll.loanNumber} loanStatusCode={coll.loanStatusCode} />
                                        </MenuItem>
                                    )}
                                </Select>
                                : <Typography variant="h6">
                                    <ActiveLoan loanNumber={store.data.loanNumber} loanStatusCode={store.data.loanStatusCode} />
                                </Typography>
                            }

                            {form.lastPaymentDate
                                ? <Typography variant="subtitle1">{t('NUMBER_DAYS_SINCE_PAYMENT')}: <span className="font-bold">{getDaysSinceDate(form.lastPaymentDate)} {t('DAYS')}</span></Typography>
                                : null}
                            <Typography variant="subtitle1" className="mb-16">
                                {t('COLLECTION_DEGREE')}:
                                <span className="font-bold">
                                    <CollectionDegree code={store.data.collectionDegreeCode} />
                                </span>
                            </Typography>

                            <div className="grid grid-cols-2 gap-16 mb-16">
                                <div className="col-span-1">
                                    <Typography variant="subtitle1" className="font-bold text-red">{t('COLLECTION_STATUS')} :</Typography>
                                    <FormControl variant="outlined" className="w-full" >
                                        <Select
                                            id="status"
                                            name="collectionStatusCode"
                                            displayEmpty
                                            value={form.collectionStatusCode || ''}
                                            onChange={handleChange}
                                            label="status"
                                        >
                                            <MenuItem value={''} disabled>{t('PLEASE_CHOOSE')}</MenuItem>
                                            <MenuItem value={'REJECTIONS_OF_THE_DAY'}>{t("REJECTION_OF_THE_DAY")}</MenuItem>
                                            <MenuItem value={'NEED_FOLLOW_UP'}>{t("FOLLOW_UP")}</MenuItem>
                                            <MenuItem value={'INTERAC_E_TRANSFER'}>{t("INTERAC_TRANSFER")}</MenuItem>
                                            <MenuItem value={'WAITING_FOR_RESPONSE'}>{t("WAITING_RESPONSE")}</MenuItem>
                                            <MenuItem value={'SETTLED'}>{t("SETTLED")}</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="col-span-1">
                                    <Typography variant="subtitle1" className="font-bold">{t("REASON_OF_NO_PAYMENT")} :</Typography>
                                    <FormControl variant="outlined" className="w-full" >
                                        <Select
                                            id="reason"
                                            name="nonPaymentReasonCode"
                                            displayEmpty
                                            value={form.nonPaymentReasonCode || ''}
                                            onChange={handleChange}
                                            label="reason"
                                        >
                                            <MenuItem value={''} disabled>{t('PLEASE_CHOOSE')}</MenuItem>
                                            <MenuItem value={'UNEMPLOYED'}>{t('NO_JOB')}</MenuItem>
                                            <MenuItem value={'BORROWED_TOO_MUCH'}>{t('TOO_MUCH_LOANS')}</MenuItem>
                                            <MenuItem value={'TOO_MANY_NSF_PAYMENTS'}>{t('TOO_MUCH_NSF')}</MenuItem>
                                            <MenuItem value={'OTHER'}>{t('OTHER')}</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                {form.collectionStatusCode === 'NEED_FOLLOW_UP' &&
                                    (
                                        <div class="col-span-2">
                                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                                <KeyboardDatePicker
                                                    className="w-full"
                                                    //disableToolbar
                                                    minDate={new Date()}
                                                    format="DD/MM/YYYY"
                                                    inputVariant="outlined"
                                                    id="reasonFollowUp_date"
                                                    name="followUpDate"
                                                    autoOk
                                                    variant="inline"
                                                    inputVariant="outlined"
                                                    InputAdornmentProps={{ position: "start" }}
                                                    label={t('FOLLOW_UP_DATE')}
                                                    value={form.followUpDate}
                                                    onChange={e => setForm({
                                                        ...form,
                                                        followUpDate: e
                                                    })}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change date',
                                                    }}
                                                />
                                            </MuiPickersUtilsProvider>
                                        </div>
                                    )}
                            </div>
                            <CardActions className="pl-0 pb-16">
                                <Button
                                    variant="outlined"
                                    color="secondary"
                                    disabled={!canSaveCollectionProfile()}
                                    onClick={saveChanges}
                                >
                                    {t('SAVE')}
                                </Button>
                            </CardActions>

                        </CardContent>
                    </Card>
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <Typography variant="h6" className="mx-12 hidden sm:flex">{t('CHECK_STEPS')} :</Typography>
                            </Toolbar>
                        </AppBar>
                        <CardContent>
                            <FormGroup>
                                {store.data.procedure.map((step, index) =>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                icon={<Icon>check_box_outline_blank</Icon>}
                                                checkedIcon={<Icon>check_box</Icon>} checked={step.checked}
                                                onClick={() => dispatch(toggleProcedureStep(step))}
                                            />}
                                        label={step.description}
                                        key={index}
                                    />
                                )}
                            </FormGroup>
                        </CardContent>
                    </Card>
                </div>
                <div className="flex-1">
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <Typography variant="h6" className="mx-12 hidden sm:flex">{t('SELECT_AGREEMENT_WITH_CLIENT')} :</Typography>
                            </Toolbar>
                        </AppBar>
                        <CardContent>
                            <FormControl variant="outlined" className="w-full">
                                <InputLabel id="agreement-label">{t('MAKE_A_CHOICE')}</InputLabel>
                                <Select
                                    labelId="agreement-label"
                                    id="agreement"
                                    name="paymentAgreementCode"
                                    value={form.paymentAgreementCode || ''}
                                    onChange={handleChange}
                                    label="Agreement"
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    <MenuItem value={'INTERAC_E_TRANSFER'}>{t("INTERAC_TRANSFER")}</MenuItem>
                                    <MenuItem value={'DEFER_PAYMENT'}>{t("DEFER_PAYMENT")}</MenuItem>
                                    <MenuItem value={'MAKE_PAYMENT_AGREEMENT'}>{t("MAKE_PAYMENT_AGREEMENT")}</MenuItem>
                                    <MenuItem value={'OTHER'}>{t("OTHER")}</MenuItem>
                                </Select>
                            </FormControl>

                            {form.paymentAgreementCode === 'INTERAC_E_TRANSFER' &&
                                (
                                    <React.Fragment>
                                        <div className="grid grid-cols-2 gap-16 mt-16 mb-16">
                                            <div className="col-span-1">
                                                <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                                    <KeyboardDatePicker
                                                        className="w-full"
                                                        //disableToolbar
                                                        minDate={new Date()}
                                                        format="DD/MM/YYYY"
                                                        inputVariant="outlined"
                                                        id="interacAgreement_date"
                                                        name="interacPlannedPaymentDate"
                                                        autoOk
                                                        variant="inline"
                                                        inputVariant="outlined"
                                                        InputAdornmentProps={{ position: "start" }}
                                                        label={t("EXPECTED_DATE_OF_TRANSFER")}
                                                        value={form.interacPlannedPaymentDate}
                                                        onChange={e => setForm({
                                                            ...form,
                                                            interacPlannedPaymentDate: e
                                                        })}
                                                        KeyboardButtonProps={{
                                                            'aria-label': 'change date',
                                                        }}
                                                    />
                                                </MuiPickersUtilsProvider>
                                            </div>
                                            <div className="col-span-1">
                                                <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                                    <KeyboardDatePicker
                                                        className="w-full"
                                                        //disableToolbar
                                                        //minDate={new Date()}
                                                        format="DD/MM/YYYY"
                                                        inputVariant="outlined"
                                                        id="interacAgreement_reel_date"
                                                        name="interacRealPaymentDate"
                                                        autoOk
                                                        variant="inline"
                                                        inputVariant="outlined"
                                                        InputAdornmentProps={{ position: "start" }}
                                                        label={t("ACTUAL_DATE_OF_TRANSFER")}
                                                        value={form.interacRealPaymentDate}
                                                        onChange={e => setForm({
                                                            ...form,
                                                            interacRealPaymentDate: e
                                                        })}
                                                        KeyboardButtonProps={{
                                                            'aria-label': 'change date',
                                                        }}
                                                    />
                                                </MuiPickersUtilsProvider>
                                            </div>
                                        </div>
                                        <div>
                                            <FormControlLabel
                                                control={<Checkbox icon={<Icon>check_box_outline_blank</Icon>} checkedIcon={<Icon>check_box</Icon>} />}
                                                label={t('CONFIRMATION_EMAIL')}
                                                className="mt-8"
                                            />
                                            <TextField
                                                className="mb-16 bg-white"
                                                id="confirmation_email"
                                                name="interacConfirmationEmail"
                                                label={t('EMAIL')}
                                                variant="outlined"
                                                color="primary"
                                                value={form.interacConfirmationEmail}
                                                onChange={handleChange}
                                            />
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                size="medium"
                                                className="ml-8"
                                                onClick={() => dispatch(sendInteracCollectionReminders([store.data.id]))}
                                            >
                                                {t('SEND')}
                                            </Button>
                                        </div>
                                        <div>
                                            <FormControlLabel
                                                control={<Checkbox
                                                    icon={<Icon>check_box_outline_blank</Icon>}
                                                    name="interacSmsReminderOnPaymentDate"
                                                    checked={form.interacSmsReminderOnPaymentDate}
                                                    onClick={() => setForm({
                                                        ...form,
                                                        interacSmsReminderOnPaymentDate: !form.interacSmsReminderOnPaymentDate
                                                    })}
                                                    checkedIcon={<Icon>check_box</Icon>}
                                                />}
                                                label={t('SEND_SMS')}
                                            />
                                        </div>
                                    </React.Fragment>
                                )
                            }
                        </CardContent>
                        <CardActions className="pl-16 pb-16">
                            <Button
                                variant="outlined"
                                color="secondary"
                                //disabled={!canBeSubmitted}
                                onClick={saveChanges}
                            >
                                {t('SAVE')}
                            </Button>
                        </CardActions>
                    </Card>

                    <NotesApp type={NoteType.COLLECTION} collectionId={store.collectionId} />
                </div>
            </div>

        </React.Fragment>
    )
}

export default withReducer('notesApp', notesReducer)(Collections);