import React, { useEffect, useState } from 'react';
import { Typography } from '@material-ui/core';
import { FuseUtils } from '@fuse';
import { useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Masonry from 'react-masonry-css';
import NoteListItem from './NoteListItem';
import { useTranslation } from 'react-i18next';
import NoteType from './NoteType'

function NoteList(props) {

    const { t } = useTranslation('customersApp');

    const reducer =  useSelector(({notesApp}) => {
        switch (props.type) {
            case NoteType.CLIENT: {
                return notesApp.notes
            }
            case NoteType.COLLECTION: {
                return notesApp.collectionNotes
            }
        }
    })
    
    const notes = reducer.entities
    const variateDescSize = reducer.variateDescSize
    const searchText = reducer.searchText
    const labelId = reducer.labelId
    const showArchived = reducer.showArchived

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function filterData() {
            let data = Object.keys(notes).map((id) => notes[id]);

            data = data.filter(note => note.archive === showArchived);

            if (parseInt(labelId) >= 0) {
                data = data.filter(note => note.labels.some(label => label.id === labelId));
            }

            if (searchText.length === 0) {
                return data;
            }

            data = (FuseUtils.filterArrayByString(data, searchText));

            return data;
        }

        if (notes) {
            setFilteredData(filterData())
        }

    }, [notes, searchText, labelId, showArchived]);

    return (
        (!filteredData || filteredData.length === 0) ?
            (
                <div className="flex items-center justify-center h-full">
                    <Typography color="textSecondary" variant="h5">
                        {t("COMMENTS_NONE_FOUND")}
                    </Typography>
                </div>
            ) :
            (
                <div className="flex flex-wrap w-full">

                    {filteredData
                        .map(note => (
                            <NoteListItem {...props}
                                key={note.id}
                                note={note}
                                className="w-full rounded-8 shadow-none border-1 mb-16"
                                variateDescSize={variateDescSize}
                            />
                        ))}
                </div>
            )
    )
}

export default withRouter(NoteList);
