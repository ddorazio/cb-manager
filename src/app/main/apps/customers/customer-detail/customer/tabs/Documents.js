import React, { useEffect } from 'react';
import { AppBar, Button, Card, CardContent, Icon, IconButton, Toolbar, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ReactTable from "react-table";
import { useDispatch, useSelector } from 'react-redux';
import NewDocumentDialog from './dialog/newDocument.dialog'
import EditDocumentDialog from './dialog/editDocument.dialog'
import { FuseAnimateGroup, FuseLoading } from '@fuse';
import { useTranslation } from 'react-i18next';
import reducer from '../../store/reducers/documents.reducer';
import withReducer from 'app/store/withReducer';
import { getClientDocuments, deleteDocument, getLoanNumbers, downloadDocument, RESET_DOCUMENTS_LIST } from '../../store/actions/documents.actions';


function Documents(props) {
    const { t } = useTranslation('customersApp');
    const { store, client } = useSelector(store => {
        return { store: store.documents, client: store.CustomerComp.customer.data };
    });
    const dispatch = useDispatch();

    const getDocumentTypeDesc = code => {
        switch (code) {
            case 'void_check':
                return t("VOID_CHECK");
            case 'contract':
                return t("CONTRACT");
            case 'other':
                return t("OTHER");
        }
    }

    useEffect(() => {
        dispatch(getClientDocuments(client.id));
        dispatch(getLoanNumbers(client.id));
        return () => {
            // Reset store for this component when the user navigates away from it
            dispatch({ type: RESET_DOCUMENTS_LIST });
        }
    }, []);

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Typography variant="subtitle2" color="inherit" className="flex-1">{t("DOCUMENTS_LIST")}</Typography>
                    <NewDocumentDialog loans={store.loans} clientId={client.id} />
                </Toolbar>
            </AppBar>
            <CardContent className="">
                <ReactTable
                    data={store.documents}
                    columns={[
                        {
                            show: false,
                            accessor: 'id'
                        },
                        {
                            Header: t("DOCUMENT_NAME"),
                            accessor: 'title',
                            filterable: false
                        },
                        {
                            id: 'clientLoanNumber',
                            Header: t("LOAN_NUMBER"),
                            accessor: row => row.clientLoanNumber || 'En attente...',
                            filterable: false
                        },
                        {
                            id: 'documentTypeCode',
                            Header: t("DOCUMENT_TYPE"),
                            accessor: row => getDocumentTypeDesc(row.documentTypeCode),
                            filterable: false
                        },
                        {
                            Header: t("DEPOSIT_DATE"),
                            accessor: 'uploadDateTime',
                            filterable: false
                        },
                        {
                            Header: "",
                            width: 140,
                            className: "py-0",
                            Cell: rowInfo => (
                                <div className="flex items-center">

                                    <IconButton onClick={async () => { await downloadDocument(rowInfo.original) }}>
                                        <Icon>remove_red_eye</Icon>
                                    </IconButton>
                                    <EditDocumentDialog
                                        data={store.documents.find(d => d.id === rowInfo.original.id)}
                                        loans={store.loans}
                                        clientId={client.Id}
                                    />
                                    <IconButton
                                        onClick={(ev) => {
                                            ev.stopPropagation();
                                            dispatch(deleteDocument(rowInfo.original.guid));
                                        }}
                                    >
                                        <Icon>delete</Icon>
                                    </IconButton>
                                </div>
                            )
                        }
                    ]}
                    defaultPageSize={4}
                    className="-striped white"
                    noDataText={t("NO_CUSTOMER")}
                    pageText={t("REACT_TABLE_PAGE_TEXT")}
                    ofText={t("REACT_TABLE_OF_TEXT")}
                    rowsText={t("REACT_TABLE_ROWS_TEXT")}
                />
            </CardContent>
        </Card>
    );
}
export default withReducer('documents', reducer)(Documents);