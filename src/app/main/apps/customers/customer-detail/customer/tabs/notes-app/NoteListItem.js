import React from 'react';
import {Card, Chip, Typography, Icon} from '@material-ui/core';
import {FuseAnimate} from '@fuse';
import {useDispatch} from 'react-redux';
import moment from 'moment';
import clsx from 'clsx';
import * as Actions from './store/actions';
import setDescriptionStyle from './setDescriptionStyle';
import NoteCreatedLabel from './NoteCreatedLabel';
import NoteModifiedLabel from './NoteModifiedLabel';
import NoteUserLabel from './NoteUserLabel';
import NoteLabel from './NoteLabel';
import NoteType from './NoteType'

function NoteListItem(props)
{
    const dispatch = useDispatch();

    const openNoteDialog = () => {
        switch (props.type) {
            case NoteType.CLIENT: {
                dispatch(Actions.openNoteDialog(props.note.id))
                break
            }
            case NoteType.COLLECTION: {
                dispatch(Actions.openCollectionNoteDialog(props.note.id))
                break
            }
        }
    }

    return (
        <FuseAnimate animation="transition.fadeIn" duration={400} delay={100}>
            <Card className={clsx("cursor-pointer", props.className)} onClick={() => openNoteDialog()}>

                {props.note.title && props.note.title !== "" && (
                    <Typography className="p-16 pb-8 text-14 font-bold">
                        {props.note.title}
                    </Typography>
                )}

                {props.note.description && props.note.description !== "" && (
                    <Typography
                        className="py-8 px-16"
                        component="div"
                    >
                        <div className={clsx("w-full break-words text-14")}>
                            {props.note.description}
                        </div>
                    </Typography>
                )}

                {((props.note.labels && props.note.labels.length > 0) || props.note.created || props.note.modified) && (
                    <div className="py-8 px-16 flex flex-wrap w-full -mx-2">
                        {props.type === NoteType.Client
                            ? props.note.labels.map(label => (
                                <NoteLabel {...props} id={label.id} key={label.id} className="mt-4 mx-2" linkable/>
                            ))
                            : null}
                        <NoteCreatedLabel {...props} className="mt-4 mx-2" date={props.note.created} />
                        <NoteModifiedLabel {...props} className="mt-4 mx-2" date={props.note.modified} />
                        <NoteUserLabel className="mt-4 mx-2" linkable/>
                        <Chip
                            icon={<Icon className="text-16">person</Icon>}
                            label={props.note.username}
                            classes={{
                                root      : "h-24 mt-4 mx-2",
                                label     : "px-12 py-4 text-11",
                                deleteIcon: "w-16",
                                ...props.classes
                            }}
                            variant="outlined"
                            onDelete={props.onDelete}
                        />
                    </div>
                )}
            </Card>
        </FuseAnimate>
    );
}

export default NoteListItem;
