import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AppBar, Card, CardContent, FormControl, Icon, InputLabel, Select, TextField, Toolbar, Typography } from '@material-ui/core';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

const errorText = {
    color: '#f55245'
};

const standardText = {
    color: '#7f7f7f'
};

const Address = (props) => {
    const { t } = useTranslation('customersApp');
    const address = props.form.basicInfo.address;
    const { form, setForm } = props;
    const handleChange = (e) => {
        setForm({
            ...form,
            basicInfo: {
                ...form.basicInfo,
                address: {
                    ...form.basicInfo.address,
                    [e.target.name]: e.target.value
                }
            }
        });
    }

    function handlePostalCode(event) {
        if (event.target.value.length <= 6) {
            setForm({
                ...form,
                basicInfo: {
                    ...form.basicInfo,
                    address: {
                        ...form.basicInfo.address,
                        postalCode: event.target.value
                    }
                }
            });
        }
    }

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Icon>location_on</Icon>
                    <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("MAIN_ADDRESS")}
                    </Typography>
                </Toolbar>
            </AppBar>
            <CardContent>
                <div className="flex">
                    <TextField
                        className="mt-8 mb-16 mr-16 w-1/4"
                        required
                        label="No."
                        id="civicNumber"
                        name="civicNumber"
                        value={address.civicNumber}
                        error={address.civicNumber ? false : true}
                        onChange={handleChange}
                        type="number"
                        variant="outlined"
                        fullWidth
                    />
                    <TextField
                        className="mt-8 mb-16 mr-16"
                        required
                        label={t("STREET")}
                        id="streetName"
                        name="streetName"
                        error={address.streetName ? false : true}
                        value={address.streetName}
                        onChange={handleChange}
                        variant="outlined"
                        fullWidth
                    />
                    <TextField
                        className="mt-8 mb-16 w-1/4"
                        label={t("APT")}
                        id="apartment"
                        name="apartment"
                        value={address.apartment}
                        onChange={handleChange}
                        type="number"
                        variant="outlined"
                        fullWidth
                    />
                </div>
                <div className="flex">
                    <TextField
                        className="mt-8 mb-16 mr-16 w-1/3"
                        required
                        label={t("CITY")}
                        id="city"
                        name="city"
                        value={address.city}
                        error={address.city ? false : true}
                        onChange={handleChange}
                        variant="outlined"
                        fullWidth
                    />
                    <FormControl className="mt-8 mb-16 mr-16 w-1/3" variant="outlined" >
                        <InputLabel id="province_label" required
                            style={address.provinceId ? standardText : errorText}>Province</InputLabel>
                        <Select
                            labelid="province_label"
                            native
                            id="province"
                            required
                            value={address.provinceId}
                            error={address.provinceId ? false : true}
                            onChange={e => setForm({
                                ...form,
                                basicInfo: {
                                    ...form.basicInfo,
                                    address: {
                                        ...form.basicInfo.address,
                                        provinceId: e.target.value
                                    }
                                }
                            })}
                        >
                            <option aria-label="None" value="" />
                            {form.provinces.map((item, index) => (
                                <option key={index} value={item.id}>{item.name}</option>
                            ))}

                        </Select>
                    </FormControl>
                    <TextField
                        className="mt-8 mb-16 w-1/3"
                        required
                        label={t("POSTAL_CODE")}
                        id="postalCode"
                        name="postalCode"
                        value={address.postalCode}
                        onChange={handlePostalCode}
                        variant="outlined"
                        error={address.postalCode ? false : true}
                        fullWidth
                    />
                </div>
                <div className="flex">
                    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                        <KeyboardDatePicker
                            autoOk
                            clearable="true"
                            required
                            disableToolbar
                            disableFuture
                            InputAdornmentProps={{ position: "start" }}
                            className="mt-8 mb-16"
                            format="DD/MM/YYYY"
                            id="startOfResidencyDate"
                            name="startOfResidencyDate"
                            label={t("SINCE")}
                            variant="inline"
                            error={address.startOfResidencyDate ? false : true}
                            inputVariant="outlined"
                            value={address.startOfResidencyDate ? address.startOfResidencyDate : null}
                            onChange={e => setForm({
                                ...form,
                                basicInfo: {
                                    ...form.basicInfo,
                                    address: {
                                        ...form.basicInfo.address,
                                        startOfResidencyDate: e 
                                    }
                                }
                            })}
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </CardContent>
        </Card>
    );
}

export default Address;