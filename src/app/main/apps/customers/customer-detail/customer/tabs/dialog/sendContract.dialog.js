import React, { useRef, useState, useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, InputLabel, MenuItem, MenuList, Paper, Popper, Select, TextField } from '@material-ui/core';
import frLocale from "date-fns/locale/fr";
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import * as Actions from '../../../store/actions';
import { validateBankInfo, validateClientLanguage } from '../../formValidation';
import color from '@material-ui/core/colors/amber';

const localeMap = { fr: frLocale };
export default function SendContractDialog(props) {

    const { t } = useTranslation('customersApp');
    const [fullWidth, setFullWidth] = React.useState(true);
    const dispatch = useDispatch();
    const customer = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const { loan } = props;

    const bankInfoIsValid = validateBankInfo(customer.data.basicInfo.bankInfo);
    const languageIsValid = validateClientLanguage(customer.data.basicInfo.personalInfo);

    const [contract, setContract] = useState({
        loading: false,
        submitType: null,
        email: customer.data.basicInfo.personalInfo.emailAddress
    });

    useEffect(() => {
        if (open) {
            resetForm();
        }
        onClose(customer.actionStatus);
    }, [customer]);

    const { open, onClose } = props;
    const handleClose = () => {
        onClose(false);
        resetForm();
    };

    const handleContractTypeChange = (event) => {
        setContract({
            ...contract,
            submitType: event.target.value
        });
    };

    const handleEmailChange = (event) => {
        setContract({
            ...contract,
            email: event.target.value
        });
    };

    //Submit contract
    function sendContract() {
        setContract({
            ...contract,
            loading: true
        });
        dispatch(Actions.sendContract(contract, loan));
    }

    // Validate form submit
    function canBeSubmitted() {
        return bankInfoIsValid
            && contract.submitType
            && contract.email;
    }

    function resetForm() {
        setContract({
            ...contract,
            loading: false,
            submitType: null,
            email: customer.data.basicInfo.personalInfo.emailAddress
        });
    }

    return (
        <React.Fragment>
            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("CONTRACT_DIALOG_TITLE")}</DialogTitle>
                <DialogContent>
                    <FormControl variant="outlined" className="mt-16 mb-16 w-full">
                        <InputLabel id="contract-label">{t("CONTRACT_CHOOSE_TYPE")}</InputLabel>
                        <Select
                            native
                            labelId="contract-label"
                            id="contractId"
                            value={contract.submitType}
                            onChange={handleContractTypeChange}
                            label="Choisissez le contrat à envoyer :"
                        >
                            <option aria-label="None" value="" />
                            <option value={0}>{t("CONTRACT_BY_EMAIL")}</option>
                        </Select>
                    </FormControl>
                    <TextField
                        className="mt-8 mb-16 w-full"
                        id="contract_email"
                        name="contract_email"
                        label={t("CONTRACT_EMAIL")}
                        type="text"
                        variant="outlined"
                        //defaultValue={customer.data.basicInfo.personalInfo.emailAddress}
                        value={contract.email}
                        onChange={handleEmailChange}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    {bankInfoIsValid ? null : <span style={{ color: "red" }}>{t("CONTRACT_MISSING_BANKING_INFO")}</span>}
                    {languageIsValid ? null : <span style={{ color: "red" }}>{t("CONTRACT_MISSING_LANGUAGE")}</span>}
                    <Button onClick={handleClose} color="primary">
                        {t("CONTRACT_CANCEL_BUTTON")}
                    </Button>
                    <Button
                        onClick={() => sendContract()}
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        disabled={!canBeSubmitted()}
                    >   {contract.loading &&
                        <i
                            className="fa fa-refresh fa-spin"
                            style={{ marginRight: "5px" }}
                        />
                        }
                        {t("CONTRACT_SUBMIT_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
