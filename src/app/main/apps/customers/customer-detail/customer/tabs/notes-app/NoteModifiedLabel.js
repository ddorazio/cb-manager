import React from 'react';
import {Chip, Icon} from '@material-ui/core';
import clsx from 'clsx';
import moment from 'moment';

function NoteModifiedLabel(props)
{
    if ( !props.date)
    {
        return null;
    }

    return (
        <Chip
            icon={<Icon className="text-16">update</Icon>}
            label={moment(props.date).format("YYYY-MM-DD hh:mm a")}
            classes={{
                root      : clsx("h-24", props.className),
                label     : "px-12 py-4 text-11",
                deleteIcon: "w-16",
                ...props.classes
            }}
            variant="outlined"
            onDelete={props.onDelete}
        />
    );
}

export default NoteModifiedLabel;
