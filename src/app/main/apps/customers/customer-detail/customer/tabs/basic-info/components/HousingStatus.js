import React from 'react';
import { Select } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

const options = [
    "OWNER",
    "TENANT"
];

const HousingStatus = (props) => {
    const { t } = useTranslation('customersApp');

    return (
        <Select
            labelid="housingStatus"
            id="housingStatusCode"
            native
            value={props.value}
            onChange={props.onChange}
        >
            <option aria-label="None" value="" />
            {options.map((item, index) => (
                <option key={index} value={item}>{t(item)}</option>
            ))}
        </Select>
    )
};

export default HousingStatus;