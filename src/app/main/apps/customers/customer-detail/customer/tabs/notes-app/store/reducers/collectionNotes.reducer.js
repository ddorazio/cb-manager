import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    entities       : [],
    searchText     : '',
    noteDialogId   : null,
    variateDescSize: true,
    labelId        : null,
    showArchived   : false 
};

const collectionNotesReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_COLLECTION_NOTES:
        {
            return {
                ...state,
                entities: _.keyBy(action.payload, 'id')
            };
        }
        case Actions.SET_COLLECTION_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        case Actions.TOGGLE_COLLECTION_VARIATE_DESC_SIZE:
        {
            return {
                ...state,
                variateDescSize: !state.variateDescSize
            };
        }
        case Actions.OPEN_COLLECTION_NOTE_DIALOG:
        {
            return {
                ...state,
                noteDialogId: action.payload
            };
        }
        case Actions.CLOSE_COLLECTION_NOTE_DIALOG:
        {
            return {
                ...state,
                noteDialogId: null
            };
        }
        case Actions.CREATE_COLLECTION_NOTE:
        {
            return {
                ...state,
                entities: _.cloneDeep(_.assign({[action.note.id]: action.note}, state.entities))
            };
        }
        case Actions.REMOVE_COLLECTION_NOTE:
        {
            return {
                ...state,
                entities    : _.cloneDeep(_.omit(state.entities, [action.id])),
                noteDialogId: null
            };
        }
        case Actions.UPDATE_COLLECTION_NOTE:
        {
            return {
                ...state,
                entities: _.cloneDeep(_.set(state.entities, action.note.id, action.note))
            };
        }
        case Actions.SET_COLLECTION_SEARCH_LABEL:
        {
            return {
                ...state,
                labelId: action.labelId
            }
        }
        case Actions.TOGGLE_COLLECTION_ARCHIVE:
            return {
                ...state,
                showArchived: !state.showArchived
            }
        default:
        {
            return state;
        }
    }
};

export default collectionNotesReducer;
