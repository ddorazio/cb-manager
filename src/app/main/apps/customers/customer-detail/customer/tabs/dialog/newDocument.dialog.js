import React, { useRef, useState } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, Icon, InputLabel, MenuItem, MenuList, Paper, Popper, Select, TextField } from '@material-ui/core';
import { DropzoneArea } from 'material-ui-dropzone'
import { makeStyles } from '@material-ui/styles';
import { withStyles } from '@material-ui/core/styles';
import frLocale from "date-fns/locale/fr";
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import withReducer from 'app/store/withReducer';
import reducer from '../../../store/reducers/documents.reducer';
import { createDocument } from '../../../store/actions/documents.actions';
import { showMessage } from 'app/store/actions/fuse';

const localeMap = { fr: frLocale };

const useStyles = makeStyles(theme => ({
    dropzoneParagraph: {
        fontSize: "18px",
        paddingTop: "75px"
    }
}));

function NewDocumentDialog(props) {

    const classes = useStyles(props);
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const [fullWidth, setFullWidth] = React.useState(true);
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setTitle('');
        setFile('');
        setDocumentType('');
        setClientLoanId('');
        setNote('');
    };
    const [title, setTitle] = useState('');
    const [file, setFile] = useState('');
    const [clientLoanId, setClientLoanId] = useState('');
    const [documentTypeCode, setDocumentType] = useState('');
    const [note, setNote] = useState('');

    const ColorButton = withStyles(theme => ({
        root: {
            color: "#fff",
            backgroundColor: "#e41f25",
            '&:hover': {
                backgroundColor: "#000000",
            },
        },
    }))(Button);

    const submit = () => {
        if (!file) {
            dispatch(showMessage({
                message: 'Veuillez sélectionner un fichier.',
                variant: 'error'
            }));
            return;
        }
        if (!title) {
            dispatch(showMessage({
                message: 'Veuillez assigner un titre au fichier.',
                variant: 'error'
            }));
            return;
        }
        if (!documentTypeCode) {
            dispatch(showMessage({
                message: 'Veuillez sélectionner un type de document.',
                variant: 'error'
            }));
            return;
        }
        var doc = {
            file,
            title,
            clientId: props.clientId,
            documentTypeCode,
            clientLoanId,
            note
        };
        dispatch(createDocument(doc));
        handleClose();
    }

    return (
        <React.Fragment>
            <ColorButton variant="contained" onClick={handleClickOpen} endIcon={<Icon>add</Icon>}>{t("UPLOAD_DOCUMENT")}</ColorButton>
            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("ADD_DOCUMENT")}</DialogTitle>
                <DialogContent>
                    <div className="flex">
                        <div className="flex-1 mr-16 dropZoneBox">
                            <TextField
                                className="w-full mb-16"
                                id="document_name"
                                name="document_name"
                                label={t("DOCUMENT_NAME")}
                                type="text"
                                variant="outlined"
                                error={!title}
                                fullWidth
                                value={title}
                                onChange={e => setTitle(e.target.value)}
                            />
                            <DropzoneArea
                                classes={classes}
                                acceptedFiles={['image/jpeg', 'image/png', 'image/bmp', 'application/pdf']}
                                showPreviews={true}
                                showPreviewsInDropzone={false}
                                maxFileSize={5000000}
                                showAlerts={true}
                                alertSnackbarProps={true}
                                filesLimit={1}
                                dropzoneText={t("DROPZONE_TEXT")}
                                dropzoneClass="dropZone"
                                showFileNamesInPreview={true}
                                getPreviewIcon={false}
                                previewText="Document :"
                                useChipsForPreview={false}
                                onChange={files => setFile(files[0])}
                            />
                        </div>
                        <div className="flex-1">
                            <FormControl variant="outlined" className="mb-16 w-full">
                                <InputLabel id="document-label">{t("ASSOCIATED_LOAN")}</InputLabel>
                                <Select
                                    labelId="document-label"
                                    id="documentId"
                                    value={clientLoanId}
                                    onChange={e => setClientLoanId(e.target.value)}
                                    label={t("ASSOCIATED_LOAN")}
                                    className={classes.selectEmpty}
                                >
                                    <MenuItem value={0}>En attente</MenuItem>
                                    {props.loans.map(loan => <MenuItem value={loan.id}>{loan.loanNumber}</MenuItem>)}
                                </Select>
                            </FormControl>
                            <FormControl variant="outlined" className="mb-16 w-full">
                                <InputLabel id="documentType-label">{t("DOCUMENT_TYPE")}</InputLabel>
                                <Select
                                    labelId="documentType-label"
                                    id="documentType"
                                    value={documentTypeCode}
                                    onChange={e => setDocumentType(e.target.value)}
                                    label={t("DOCUMENT_TYPE")}
                                    error={!documentTypeCode}
                                >
                                    <MenuItem value={'void_check'}>Spécimen de chèque</MenuItem>
                                    <MenuItem value={'contract'}>Contrat</MenuItem>
                                    <MenuItem value={'other'}>Autre</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField
                                className="w-full"
                                id="document_note"
                                name="document_note"
                                label="Note"
                                type="text"
                                variant="outlined"
                                multiline
                                rows={3}
                                fullWidth
                                onChange={e => setNote(e.target.value)}
                            />
                        </div>
                    </div>


                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("CANCEL")}
                    </Button>
                    <Button
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        onClick={() => {
                            submit();
                        }}
                    >
                        {t("SAVE")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}

export default withReducer('documents', reducer)(NewDocumentDialog);