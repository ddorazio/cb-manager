import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AppBar, Card, CardContent, FormControl, Icon, InputLabel, Select, TextField, Toolbar, Typography } from '@material-ui/core';
import InputMask from "react-input-mask";

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

const EmploymentStatus = (props) => {
    const { t } = useTranslation('customersApp');
    const employmentStatus = props.form.basicInfo.employmentStatus;
    const { form, setForm } = props;
    const handleChange = (e) => {
        var formValue = (e.target.id == 'workPhoneNumberHR') ? e.target.value.replace(/\D/g,'') : e.target.value

        setForm({
            ...form,
            basicInfo: {
                ...form.basicInfo,
                employmentStatus: {
                    ...form.basicInfo.employmentStatus,
                    [e.target.name]: formValue
                }
            }
        });
    }

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Icon>account_box</Icon>
                    <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("CURRENT_JOB")}
                    </Typography>
                </Toolbar>
            </AppBar>
            <CardContent>
                <div>
                    <div className="flex">
                        <TextField
                            className="mt-8 mb-16 mr-16"
                            required
                            label={t("EMPLOYER_NAME")}
                            id="employerName"
                            name="employerName"
                            value={employmentStatus.employerName}
                            onChange={handleChange}
                            variant="outlined"
                            error={employmentStatus.employerName ? false : true}
                            fullWidth
                        />
                        <TextField
                            className="mt-8 mb-16"
                            label={t("SUPERVISOR_NAME")}
                            id="supervisorName"
                            name="supervisorName"
                            value={employmentStatus.supervisorName}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <InputMask mask="(999) 999-9999"
                            maskChar=" "
                            value={employmentStatus.workPhoneNumber}
                            onChange={handleChange}>
                            {() =>
                                <TextField
                                    className="mt-8 mb-16 mr-16"
                                    required
                                    label={t("PHONE_WORK")}
                                    id="workPhoneNumber"
                                    name="workPhoneNumber"
                                    variant="outlined"
                                    fullWidth
                                    error={employmentStatus.workPhoneNumber ? false : true}
                                    InputProps={{
                                        startAdornment: (
                                            <Icon>phone</Icon>
                                        )
                                    }}
                                />}
                        </InputMask>

                        <TextField
                            className="mt-8 mb-16 w-1/3"
                            label={t("PHONE_EXT")}
                            id="workPhoneNumberExtension"
                            name="workPhoneNumberExtension"
                            value={employmentStatus.workPhoneNumberExtension}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />

                    </div>

                    <div className="flex">
                        <InputMask mask="(999) 999-9999"
                            maskChar=" "
                            value={employmentStatus.workPhoneNumberHR}
                            onChange={handleChange}>
                            {() =>
                                <TextField
                                    className="mt-8 mb-16 mr-16"
                                    label={t("PHONE_WORK_HR")}
                                    id="workPhoneNumberHR"
                                    name="workPhoneNumberHR"
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{
                                        startAdornment: (
                                            <Icon>phone</Icon>
                                        )
                                    }}
                                />}

                        </InputMask>

                        <TextField
                            className="mt-8 mb-16 w-1/3"
                            label={t("PHONE_WORK_EXT_HR")}
                            id="workPhoneNumberExtensionHR"
                            name="workPhoneNumberExtensionHR"
                            value={employmentStatus.workPhoneNumberExtensionHR}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            type="number"
                        />

                    </div>

                    <div className="flex">
                        <TextField
                            className="mt-8 mb-16 mr-16"
                            label={t("POSITION_TITLE")}
                            required
                            id="occupation"
                            name="occupation"
                            error={employmentStatus.occupation ? false : true}
                            value={employmentStatus.occupation}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                            <KeyboardDatePicker
                                autoOk
                                clearable="true"
                                disableFuture
                                required
                                disableToolbar
                                InputAdornmentProps={{ position: "start" }}
                                className="mt-8 mb-16"
                                format="DD/MM/YYYY"
                                id="hiringDate"
                                name="hiringDate"
                                label={t("HIRING_DATE")}
                                variant="inline"
                                inputVariant="outlined"
                                error={employmentStatus.hiringDate ? false : true}
                                value={employmentStatus.hiringDate ? employmentStatus.hiringDate : null}
                                onChange={e => setForm({
                                    ...form,
                                    basicInfo: {
                                        ...form.basicInfo,
                                        employmentStatus: {
                                            ...employmentStatus,
                                            hiringDate: e
                                        }
                                    }
                                })}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                    <div className="flex">
                        <FormControl className="mt-8 mb-16 mr-16 w-1/2" variant="outlined" >
                            <InputLabel id="sexe">{t("PAY_FREQUENCY")}</InputLabel>
                            <Select
                                labelid="payFrequencyId"
                                name="payFrequencyId"
                                id="payFrequencyId"
                                required
                                native
                                value={employmentStatus.payFrequencyId}
                                onChange={handleChange}
                            >
                                <option aria-label="None" value="" />
                                {form.payFrequencies.map((item, index) => (
                                    <option key={index} value={item.id}>{item.name}</option>
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                </div>
            </CardContent>
        </Card>
    );
}

export default EmploymentStatus;