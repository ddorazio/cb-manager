import React, { useState } from 'react';
import { ClickAwayListener, Paper, Typography } from '@material-ui/core';
import clsx from 'clsx';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import NoteForm from './note-form/NoteForm';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import NoteType from './NoteType';

const useStyles = makeStyles({
    button: {
        cursor: 'text'
    }
});

function NewNote(props) {
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const client = useSelector(store => store.CustomerComp.customer.data);

    const classes = useStyles(props);
    const [formOpen, setFormOpen] = useState(false);

    function handleFormOpen() {
        setFormOpen(true);
        document.addEventListener("keydown", escFunction, false);
    }

    function handleFormClose() {
        if (!formOpen) {
            return;
        }
        setFormOpen(false);
        document.removeEventListener("keydown", escFunction, false);
    }

    function handleCreate(note) {

        switch(props.type) {
            case NoteType.CLIENT: {
                dispatch(Actions.createNote({
                    ...note,
                    clientId: client.id
                }));
                break
            }
            case NoteType.COLLECTION: {
                dispatch(Actions.createCollectionNote({
                    ...note,
                    collectionId: props.collectionId
                }));
                break
            }
        }
        
        handleFormClose();
    }

    function escFunction(event) {
        if (event.keyCode === 27) {
            handleFormClose();
        }
    }

    function handleClickAway(ev) {
        const preventCloseElements = document.querySelector(".prevent-add-close");
        const preventClose = preventCloseElements ? preventCloseElements.contains(ev.target) : false;
        if (preventClose) {
            return;
        }
        handleFormClose();
    }

    const placeholder = () => {
        switch(props.type) {
            case NoteType.CLIENT: {
                return t("COMMENTS_ADD_NEW")
            }
            case NoteType.COLLECTION: {
                return t("ADD_NOTE")
            }
        }
    }

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <Paper
                className={clsx(classes.button, "flex items-center w-full mb-16 min-h-48")}
                elevation={1}
            >
                {formOpen ? (
                    <NoteForm {...props} onCreate={handleCreate} variant="new" />
                ) : (
                        <Typography
                            className="w-full px-16 py-12 font-500 text-16 w-full"
                            color="textSecondary"
                            onClick={handleFormOpen}
                        >
                            {placeholder()}
                        </Typography>
                    )}
            </Paper>
        </ClickAwayListener>
    );
}

export default NewNote;
