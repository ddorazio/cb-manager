import React, { useState } from 'react';
import clsx from 'clsx';
import _ from '@lodash';
import DateFnsUtils from '@date-io/date-fns';
import frLocale from "date-fns/locale/fr";

import { orange } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import { AppBar, Card, CardContent, FormControl, Icon, InputLabel, Select, TextField, Toolbar, Typography } from '@material-ui/core';
import InputMask from "react-input-mask";

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

const useStyles = makeStyles(theme => ({
    customerImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    customerImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    customerImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $customerImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $customerImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $customerImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));




const PersonalInfo = (props) => {
    const { t } = useTranslation('customersApp');
    const { form, setForm } = props;
    const personalInfo = props.form.basicInfo.personalInfo;
    const handleChange = (e) =>
        setForm({
            ...form,
            basicInfo: {
                ...form.basicInfo,
                personalInfo: {
                    ...form.basicInfo.personalInfo,
                    [e.target.name]: e.target.value.replace('(', '').replace(')', '').replace('-', '').replace(' ', '')
                }
            }
        });
    const classes = useStyles(props);

    function handleUploadChange(e) {
        const file = e.target.files[0];
        if (!file) {
            return;
        }
        const reader = new FileReader();
        reader.readAsBinaryString(file);

        reader.onload = () => {
            setForm(_.set({ ...form }, `images`,
                [
                    {
                        'id': '',
                        'url': `data:${file.type};base64,${btoa(reader.result)}`,
                        'type': `${file.type}`,
                        'name': file.name,
                        'data': `${btoa(reader.result)}`
                    },
                    ...form.images
                ]
            ));
        };

        if (e.target.files[0]) {
            e.target.value = "";
        }

        reader.onerror = function () {
            console.log("error on load image");
        };
    }

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Icon>account_box</Icon>
                    <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("PERSONAL_INFOS")}
                    </Typography>
                </Toolbar>
            </AppBar>
            <CardContent>
                <div>
                    <div className="flex">
                        <FormControl className="mt-8 mb-16 mr-16 w-1/2" variant="outlined" >
                            <InputLabel id="status">{t("STATUS")}</InputLabel>
                            <Select
                                labelid="statusId"
                                id="statusId"
                                native
                                value={personalInfo.statusId}
                                onChange={
                                    e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            personalInfo: {
                                                ...form.basicInfo.personalInfo,
                                                statusId: e.target.value
                                            }
                                        }
                                    })
                                }
                            >
                                <option aria-label="None" value="" />
                                {form.status.map((item, index) => (
                                    <option key={index} value={item.id}>{item.name}</option>
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                    <div className="flex">
                        {/* <input
                            accept="image/*"
                            className="hidden"
                            id="button-file"
                            type="file"
                            onChange={handleUploadChange}
                        />

                        <div className="mt-8 mb-16 mr-16">
                            <label
                                htmlFor="button-file"
                                className={
                                    clsx(
                                        classes.customerImageUpload,
                                        "flex items-center justify-center relative w-50 h-50 pt-8 overflow-hidden cursor-pointer"
                                    )}
                            >
                                <Icon fontSize="large" color="action">add_a_photo</Icon>
                            </label>
                        </div> */}
                        <TextField
                            className="mt-8 mb-16 mr-16"
                            required
                            label={t("SURNAME")}
                            autoFocus
                            id="firstName"
                            name="firstName"
                            value={personalInfo.firstName}
                            onChange={handleChange}
                            variant="outlined"
                            error={personalInfo.firstName ? false : true}
                            fullWidth
                        />
                        <TextField
                            className="mt-8 mb-16"
                            required

                            label={t("NAME")}
                            id="lastName"
                            name="lastName"
                            value={personalInfo.lastName}
                            onChange={handleChange}
                            variant="outlined"
                            error={personalInfo.lastName ? false : true}
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <InputMask mask="(999) 999-9999"
                            maskChar=" "
                            value={personalInfo.homePhoneNumber}
                            onChange={handleChange}>
                            {() =>
                                <TextField
                                    className="mt-8 mb-16 mr-16"
                                    label={t("PHONE_HOME")}
                                    id="homePhoneNumber"
                                    name="homePhoneNumber"
                                    //error={personalInfo.homePhoneNumber ? false : true}
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{
                                        startAdornment: (
                                            <Icon>phone</Icon>
                                        )
                                    }}
                                ></TextField>
                            }
                        </InputMask>

                        <InputMask mask="(999) 999-9999"
                            value={personalInfo.cellPhoneNumber}
                            onChange={handleChange}
                            maskChar=" ">
                            {() =>
                                <TextField
                                    className="mt-8 mb-16"
                                    required
                                    label={t("PHONE_CELL")}
                                    id="cellPhoneNumber"
                                    name="cellPhoneNumber"
                                    error={personalInfo.cellPhoneNumber ? false : true}
                                    variant="outlined"
                                    fullWidth
                                    InputProps={{
                                        startAdornment: (
                                            <Icon>phone_iphone</Icon>
                                        )
                                    }}
                                ></TextField>}
                        </InputMask>


                    </div>
                    <div className="flex">

                        <InputMask mask="(999) 999-9999"
                            value={personalInfo.faxNumber}
                            onChange={handleChange}
                            maskChar=" ">
                            {() =>
                                <TextField
                                    className="mt-8 mb-16 mr-16"
                                    label={t("FAX")}
                                    id="faxNumber"
                                    name="faxNumber"
                                    variant="outlined"
                                    fullWidth
                                />}
                        </InputMask>

                        <TextField
                            className="mt-8 mb-16"
                            required
                            label={t("EMAIL")}
                            id="emailAddress"
                            name="emailAddress"
                            value={personalInfo.emailAddress}
                            error={personalInfo.emailAddress ? false : true}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            InputProps={{
                                startAdornment: (
                                    <Icon>email</Icon>
                                )
                            }}
                        />

                    </div>
                    <div className="flex">

                        <FormControl className="mt-8 mb-16 mr-16 w-1/2" variant="outlined" >
                            <InputLabel id="sexe">{t("GENDER")}</InputLabel>
                            <Select
                                labelid="genderId"
                                id="genderId"
                                native
                                value={personalInfo.genderId}
                                onChange={
                                    e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            personalInfo: {
                                                ...form.basicInfo.personalInfo,
                                                genderId: e.target.value
                                            }
                                        }
                                    })
                                }
                            >
                                <option aria-label="None" value="" />
                                {form.genders.map((item, index) => (
                                    <option key={index} value={item.id}>{item.name}</option>
                                ))}
                            </Select>
                        </FormControl>
                        <FormControl className="mt-8 mb-16 w-1/2" variant="outlined" >
                            <InputLabel id="language">{t("LANGUAGE")}</InputLabel>
                            <Select
                                labelid="idlanguage"
                                id="idlanguage"
                                native
                                value={personalInfo.languageId}
                                onChange={
                                    e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            personalInfo: {
                                                ...form.basicInfo.personalInfo,
                                                languageId: e.target.value
                                            }
                                        }
                                    })
                                }
                            >

                                <option aria-label="None" value="" />
                                {form.languages.map((item, index) => (
                                    <option key={index} value={item.id}>{item.name}</option>
                                ))}

                            </Select>
                        </FormControl>
                    </div>
                    <div className="flex">
                        <TextField
                            className="mt-8 mb-16 mr-16 w-1/2"
                            label={t("SIN")}
                            id="socialInsuranceNumber"
                            type="number"
                            name="socialInsuranceNumber"
                            value={personalInfo.socialInsuranceNumber}
                            onChange={handleChange}
                            variant="outlined"
                            onInput={(e) => {
                                e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, 9)
                            }}
                        />

                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                            <KeyboardDatePicker
                                autoOk
                                clearable="true"
                                required
                                disableFuture
                                disableToolbar
                                InputAdornmentProps={{ position: "start" }}
                                className="mt-8 mb-16"
                                format="DD/MM/YYYY"
                                id="dateOfBirth"
                                name="dateOfBirth"
                                label={t("DATE_OF_BIRTH")}
                                variant="inline"
                                inputVariant="outlined"
                                //error={personalInfo.dateOfBirth ? false : true}
                                defaultValue={personalInfo.dateOfBirth}
                                value={personalInfo.dateOfBirth ? personalInfo.dateOfBirth : null}
                                onChange={e => setForm({
                                    ...form,
                                    basicInfo: {
                                        ...form.basicInfo,
                                        personalInfo: {
                                            ...form.basicInfo.personalInfo,
                                            dateOfBirth: e 
                                        }
                                    }
                                })}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
            </CardContent>
        </Card >
    );
}

export default PersonalInfo;