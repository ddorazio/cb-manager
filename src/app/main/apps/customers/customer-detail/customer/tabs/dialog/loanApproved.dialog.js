import React, { useEffect, useState } from 'react';
import localStorageService from 'app/services/localStorageService';

import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, Button, InputAdornment, DialogActions } from '@material-ui/core';
import { FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

function LoanApprovedDialog(props) {
    const { t } = useTranslation('customersApp');
    const [row, setRow] = useState(props.row);
    const [amountChange, setAmountChange] = useState(true)
    const textDialog = (props.row.statusCode == 'PREAPPROVED') ? t("STATUSCODE_PREAPPROVED_DIALOG") : t("STATUSCODE_ACCEPTED_DIALOG")

    const changeAmountApproved = (event) => {
        var reg = new RegExp(/^-?\d*\.?\d*$/)
        var amount = (event.target.value.indexOf(',') > -1) ? event.target.value.replace(/,/g, '.') : event.target.value 
        
        if(reg.test(amount)) {        
            var copiedRow = props.row
            copiedRow.selectedRequest.amountApproved = parseFloat(amount)
            setRow(copiedRow)
            setAmountChange(false)
        } else {
            setAmountChange(true)
        }
    }

    return (
        <div>
            <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="form-dialog-title">
                <DialogContent>
                <DialogContentText>{textDialog}</DialogContentText>
                <div style={{display:"flex", alignItems: "baseline"}}>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="loanApproved"
                        label={t('LOAN_APPROVED_AMOUNT')}
                        type="text"
                        onChange={(ev) => changeAmountApproved(ev)}
                        fullWidth
                        InputProps={{
                            endAdornment: <InputAdornment>$</InputAdornment>,
                        }}
                    />
                    
                </div>
                </DialogContent>
                <DialogActions>
                <Button onClick={props.handleClose} color="secondary">
                    {t('CANCEL_LOAN_DIALOG')}
                </Button>
                <Button onClick={props.handleSubmit} color="secondary" disabled={amountChange}>
                    {t('SAVE_LOAN_DIALOG')}
                </Button>
                </DialogActions>
            </Dialog>
        </div>

    )
}

export default withRouter(LoanApprovedDialog);
