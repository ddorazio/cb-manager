import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AppBar, Card, CardContent, FormControlLabel, Checkbox, Icon, TextField, Toolbar, Typography } from '@material-ui/core';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

const BankInfo = (props) => {
    const { t } = useTranslation('customersApp');
    const bankInfo = props.form.basicInfo.bankInfo;
    const { form, setForm } = props;
    const handleChange = (e) => {
        setForm({
            ...form,
            basicInfo: {
                ...form.basicInfo,
                bankInfo: {
                    ...form.basicInfo.bankInfo,
                    [e.target.name]: e.target.value
                }
            }
        });
    }

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Icon>account_balance</Icon>
                    <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("BANK_DETAILS")}</Typography>
                </Toolbar>
            </AppBar>
            <CardContent className="pb-16">
                <div>
                    <div className="flex">
                        <TextField
                            className="mt-8 mb-16 mr-16 w-1/3"
                            label={t("TRANSIT_NO")}
                            id="transitNumber"
                            type="number"
                            name="transitNumber"
                            value={bankInfo.transitNumber}
                            onChange={handleChange}
                            variant="outlined"
                            error={bankInfo.transitNumber ? bankInfo.transitNumber.length >= 5 ? false : true : true}
                            inputProps={{ minLength: 3 }}
                        /* Pour mettre une limite de caractères*/
                        // onInput={(e) => {
                        //     e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, 5)
                        // }}
                        />
                        <TextField
                            className="mt-8 mb-16 mr-16 w-1/3"
                            label={t("INSTITUTION_NO")}
                            id="institutionNumber"
                            type="number"
                            name="institutionNumber"
                            value={bankInfo.institutionNumber}
                            error={bankInfo.institutionNumber ? bankInfo.institutionNumber.length >= 3 ? false : true : true}
                            onChange={handleChange}
                            variant="outlined"
                            inputProps={{
                                maxLength: 3,
                            }}
                        />
                        <TextField
                            className="mt-8 mb-16 w-1/3"
                            label={t("ACCOUNT_NO")}
                            id="accountNumber"
                            type="number"
                            name="accountNumber"
                            error={bankInfo.accountNumber ? false : true}
                            value={bankInfo.accountNumber}
                            onChange={handleChange}
                            variant="outlined"

                        />
                    </div>

                    <div className="flex">
                        <FormControlLabel
                            className="mt-8 mb-16 mr-16 w-1/3"
                            control={
                                <Checkbox
                                    id="isBankrupt"
                                    name="isBankrupt"
                                    onChange={e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            bankInfo: {
                                                ...form.basicInfo.bankInfo,
                                                isBankrupt: e.target.checked
                                            }
                                        }
                                    })}
                                    defaultValue={bankInfo.isBankrupt}
                                    checked={bankInfo.isBankrupt}
                                />
                            }
                            label={t("BANKRUPT")} />

                        <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                            <KeyboardDatePicker
                                disabled={!bankInfo.isBankrupt}
                                autoOk
                                clearable="true"
                                disablePast
                                disableToolbar
                                InputAdornmentProps={{ position: "start" }}
                                className="mt-8 mb-16"
                                format="dd/MM/yyyy"
                                id="bankruptEndDate"
                                label={t("BANKRUPT_DATE")}
                                variant="inline"
                                inputVariant="outlined"
                                defaultValue={null}
                                value={bankInfo.bankruptEndDate ? bankInfo.bankruptEndDate : null}
                                error={bankInfo.isBankrupt && !bankInfo.bankruptEndDate}
                                onChange={e => setForm({
                                    ...form,
                                    basicInfo: {
                                        ...form.basicInfo,
                                        bankInfo: {
                                            ...form.basicInfo.bankInfo,
                                            bankruptEndDate: e
                                        }
                                    }
                                })}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </div>
            </CardContent>
        </Card>
    );
}

export default BankInfo;