import React, { useEffect } from 'react';
import { withStyles} from '@material-ui/core/styles';
import {AppBar,Button, Card, CardContent, Icon, IconButton, Toolbar, Typography} from '@material-ui/core';
import ReactTable from "react-table";
import { FuseAnimateGroup } from '@fuse';
import _ from '@lodash';
import * as Actions from '../../store/actions';
import { dark } from '@material-ui/core/styles/createPalette';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import withReducer from  'app/store/withReducer';
import reducer from '../../store/reducers';
import { getClientHistory } from '../../store/actions';

const ColorButton = withStyles(theme => ({
    root: {
        color: "#FFFFFF",
        backgroundColor: "#e31f25",
        '&:hover': {
            backgroundColor: "#a6171b",
        },
    },
}))(Button);

function History(props)
{
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const clientId = useSelector(store => store.CustomerComp.customer.data.id);
    const history = useSelector(store => store.CustomerComp.customer.data.history);

    useEffect(() => {
        if(history === undefined) {
            dispatch(getClientHistory(clientId));
        }
    })

    useEffect(() => {
        dispatch(getClientHistory(clientId));
    }, [clientId]);

    return (
            <Card className="w-full mb-16">
                <AppBar position="static" elevation={0} className="black_bg">
                    <Toolbar className="pl-16 pr-8">
                        <Typography variant="subtitle2" color="inherit" className="flex-1">{t('HISTORY_HEADER')}</Typography>
                    </Toolbar>
                </AppBar>
                <CardContent className="p-0">
                    <ReactTable
                        data={history}
                        columns={[
                                {
                                    Header: t('HISTORY_LISTE_DATE'),
                                    width : 200,
                                    accessor: 'dateTime',
                                    filterable: false,
                                },
                                {
                                    Header: t('HISTORY_LISTE_CREATOR'),
                                    width : 350,
                                    accessor: 'username',
                                    filterable: false,
                                },
                                {
                                    Header: t('HISTORY_LISTE_DESCRIPTION'),
                                    accessor: 'descriptionFr',
                                    filterable: false,
                                },
       
                                {
                                    Header: "",
                                    width : 50,
                                    className: "py-0",
                                    Cell  : row => (
                                        <div className="flex items-center">
                                            
                                            <IconButton>
                                                <Icon>remove_red_eye</Icon>
                                            </IconButton>
                                        </div>
                                    )
                                }
                            ]}
                        defaultPageSize={5}
                        className="-striped white"
                        noDataText={t('HISTORY_NONE_FOUND')}
                    />
                </CardContent>
            </Card>    
    );
}
export default withReducer('CustomerComp', reducer)(History);
