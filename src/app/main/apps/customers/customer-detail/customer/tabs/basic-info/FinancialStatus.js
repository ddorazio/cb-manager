import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AppBar, Card, CardContent, FormControl, Icon, InputLabel, Select, TextField, Toolbar, Typography } from '@material-ui/core';
import frLocale from "date-fns/locale/fr";
import localStorageService from 'app/services/localStorageService';
import CBNumberFormat from '../../../../../../../components/CBNumberFormat';
import HousingStatus from './components/HousingStatus';

const FinancialStatus = (props) => {
    const { t } = useTranslation('customersApp');
    const financialStatus = props.form.basicInfo.financialStatus || {};
    const { form, setForm } = props;

    const handleChange = (e) => {
        setForm({
            ...form,
            basicInfo: {
                ...form.basicInfo,
                financialStatus: {
                    ...form.basicInfo.financialStatus,
                    [e.target.name]: e.target.value
                }
            }
        });
    }
    const localeMap = { fr: frLocale };
    const [locale] = useState(localStorageService.getLanguage());

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Icon>attach_money</Icon>
                    <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("FINANCIAL_STATUS")}
                    </Typography>
                </Toolbar>
            </AppBar>
            <CardContent>
                <div>
                    <div className="flex">
                        <FormControl className="mt-8 mb-16 w-full mr-16" variant="outlined" >
                            <InputLabel id="housingStatus">{t("FINANCIAL_HOUSING_STATUS")}</InputLabel>
                            <HousingStatus
                                value={financialStatus.housingStatusCode}
                                onChange={
                                    e => setForm({
                                        ...form,
                                        basicInfo: {
                                            ...form.basicInfo,
                                            financialStatus: {
                                                ...form.basicInfo.financialStatus,
                                                housingStatusCode: e.target.value
                                            }
                                        }
                                    })
                                }
                            />
                        </FormControl>

                        <TextField
                            className="mt-8 mb-16"
                            label={t("FINANCIAL_GROSS_SALARY")}
                            id="grossAnnualRevenue"
                            name="grossAnnualRevenue"
                            value={financialStatus.grossAnnualRevenue}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            InputProps={{
                                inputComponent: CBNumberFormat,
                            }}
                        />
                    </div>
                    <div className="flex">
                        <TextField
                            className="mt-8 mb-16 mr-16"
                            required
                            label={t("FINANCIAL_RENT")}
                            id="monthlyHousingCost"
                            name="monthlyHousingCost"
                            value={financialStatus.monthlyHousingCost}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            error={financialStatus.monthlyHousingCost == 0 ? false : financialStatus.monthlyHousingCost ? false : true}
                            InputProps={{
                                inputComponent: CBNumberFormat,
                            }}
                        />
                        <TextField
                            className="mt-8 mb-16"
                            label={t("FINANCIAL_HEATING")}
                            id="monthlyUtilityCost"
                            name="monthlyUtilityCost"
                            value={financialStatus.monthlyUtilityCost}
                            onChange={handleChange}
                            variant="outlined"
                            error={financialStatus.monthlyUtilityCost == 0 ? false : financialStatus.monthlyUtilityCost ? false : true}
                            fullWidth
                            InputProps={{
                                inputComponent: CBNumberFormat,
                            }}
                        />
                    </div>
                    <div className="flex">
                        <TextField
                            className="mt-8 mb-16 mr-16"
                            label={t("FINANCIAL_CAR")}
                            id="monthlyCarPayment"
                            name="monthlyCarPayment"
                            value={financialStatus.monthlyCarPayment}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            error={financialStatus.monthlyCarPayment == 0 ? false : financialStatus.monthlyCarPayment ? false : true}
                            InputProps={{
                                inputComponent: CBNumberFormat,
                            }}
                        />
                        <TextField
                            className="mt-8 mb-16"
                            label={t("FINANCIAL_OTHER")}
                            id="monthlyOtherLoanPayment"
                            name="monthlyOtherLoanPayment"
                            value={financialStatus.monthlyOtherLoanPayment}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            error={financialStatus.monthlyOtherLoanPayment == 0 ? false : financialStatus.monthlyOtherLoanPayment ? false : true}
                            InputProps={{
                                inputComponent: CBNumberFormat,
                            }}
                        />

                    </div>
                </div>
            </CardContent>
        </Card>
    );
}

export default FinancialStatus;