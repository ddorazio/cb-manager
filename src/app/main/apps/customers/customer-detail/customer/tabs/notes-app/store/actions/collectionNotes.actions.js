import NotesAPI from '../../NotesAPI';
import CollectionAPI from 'app/main/apps/customers/CollectionAPI';

export const GET_COLLECTION_NOTES = '[NOTES APP] GET COLLECTION NOTES';
export const SET_COLLECTION_SEARCH_TEXT = '[NOTES APP] SET COLLECTION SEARCH TEXT';
export const OPEN_COLLECTION_NOTE_DIALOG = '[NOTES APP] OPEN COLLECTION NOTE DIALOG';
export const CLOSE_COLLECTION_NOTE_DIALOG = '[NOTES APP] CLOSE COLLECTION NOTE DIALOG';
export const CREATE_COLLECTION_NOTE = '[NOTES APP] CREATE COLLECTION NOTE';
export const UPDATE_COLLECTION_NOTE = '[NOTES APP] UPDATE COLLECTION NOTE';
export const REMOVE_COLLECTION_NOTE = '[NOTES APP] REMOVE COLLECTION NOTE';
export const TOGGLE_COLLECTION_VARIATE_DESC_SIZE = '[NOTES APP] TOGGLE COLLECTION VARIATE DESC SIZE';
export const ADD_LABEL_TO_COLLECTION_COMMENT = '[NOTES APP] ADD LABEL TO COLLECTION COMMENT';
export const REMOVE_LABEL_FROM_COLLECTION_COMMENT = '[NOTES APP] REMOVE LABEL TO COLLECTION COMMENT';
export const SET_COLLECTION_SEARCH_LABEL = '[NOTES APP] SET COLLECTION SEARCH LABEL';
export const TOGGLE_COLLECTION_ARCHIVE = '[NOTES APP] TOGGLE COLLECTION ARCHIVE';

export function getCollectionNotes(collectionId) {
    return async (dispatch) => {
        const result = await CollectionAPI.getComments(collectionId);
        dispatch({
            type: GET_COLLECTION_NOTES,
            payload: result
        });
    }
}

export function setCollectionSearchText(event) {
    return {
        type: SET_COLLECTION_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function resetCollectionSearchText() {
    return {
        type: SET_COLLECTION_SEARCH_TEXT,
        searchText: ""
    }
}

export function toggleCollectionVariateDescSize() {
    return {
        type: TOGGLE_COLLECTION_VARIATE_DESC_SIZE
    }
}

export function openCollectionNoteDialog(id) {
    return {
        type: OPEN_COLLECTION_NOTE_DIALOG,
        payload: id
    }
}

export function closeCollectionNoteDialog() {
    return {
        type: CLOSE_COLLECTION_NOTE_DIALOG
    }
}

export function setCollectionSearchLabel(labelId) {
    return {
        type: SET_COLLECTION_SEARCH_LABEL,
        labelId
    }
}

export function toggleCollectionArchive() {
    return {
        type: TOGGLE_COLLECTION_ARCHIVE
    }
}

export function createCollectionNote(note) {
    return async (dispatch) => {
        const result = await CollectionAPI.saveComment(note);
        dispatch({
            type: CREATE_COLLECTION_NOTE,
            note: result
        });
    }
}

export function updateCollectionNote(note) {
    return async (dispatch) => {
        const result = await CollectionAPI.saveComment(note);
        dispatch({
            type: UPDATE_COLLECTION_NOTE,
            note: result
        })
        dispatch({
            type:CLOSE_COLLECTION_NOTE_DIALOG
        })
    }

}

export function removeCollectionNote(commentId) {
    return async (dispatch) => {
        await CollectionAPI.deleteComment(commentId);
        dispatch({
            type: REMOVE_COLLECTION_NOTE,
            id: commentId
        });
    }
}

export function addLabelToCollectionComment(clientCommentId, commentLabelId) {
    return async dispatch => {
        const result = await NotesAPI.addLabelToCollectionComment(clientCommentId, commentLabelId);
        dispatch({
            type: ADD_LABEL_TO_COLLECTION_COMMENT,
            clientCommentId,
            commentLabelId
        });
    }
}

export function removeLabelFromCollectionComment(clientCommentId, commentLabelId) {
    return async dispatch => {
        const result = await NotesAPI.removeLabelFromCollectionComment(clientCommentId, commentLabelId);
        dispatch({
            type: REMOVE_LABEL_FROM_COLLECTION_COMMENT,
            clientCommentId,
            commentLabelId
        });
    }
}
