import { getProtected, putProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class LabelsAPI {
    static getLabels = async () => {
        return await getProtected(`${settingsConfig.apiPath}/api/ClientComment/GetCommentLabels`);
    }
}

export default LabelsAPI