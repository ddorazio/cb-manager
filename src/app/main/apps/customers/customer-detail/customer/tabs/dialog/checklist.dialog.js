import React, { useRef, useState } from 'react';
import { AppBar, Button, Dialog, DialogTitle, DialogContent, DialogActions, FormControlLabel, Grow, Grid, Icon, InputLabel, Checkbox, TextField, InputAdornment, IconButton, MenuItem, MenuList, Paper, Popper, Table, TableBody, TableContainer, TableHead, TableRow, TableCell, Toolbar, Typography } from '@material-ui/core';
import { FuseAnimate } from '@fuse';
import DateFnsUtils from '@date-io/date-fns';
import frLocale from "date-fns/locale/fr";
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import * as Actions from '../../../store/actions';
import { useTranslation } from 'react-i18next';
import localStorageService from 'app/services/localStorageService';

const localeMap = { fr: frLocale };

export default function ChecklistDialog(props) {

    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const { t } = useTranslation('customersApp');
    const [fullWidth, setFullWidth] = React.useState(true);
    const dispatch = useDispatch();
    const { row } = props;

    const [state, setState] = useState({
        loading: false,
        data: props.row.data,
        checklist: props.row.checklist
    });

    //const { open } = props;
    //const [aopen, setOpen] = React.useState(props.open);
    const handleClose = () => {
        setState({
            ...state,
            open: false
        });
    };

    // Initial load
    useEffect(() => {
        // setState({
        //     ...state,
        //     data: props.row.data,
        //     checklist: props.row.checklist,
        //     open: props.open
        // });

        if (!state.open) {
            setState({
                ...state,
                loading: false,
                data: props.row.data,
                checklist: props.row.checklist,
                open: props.open
            });
        } else {
            setState({
                ...state,
                data: props.row.data,
                checklist: props.row.checklist,
                open: props.open
            });
        }

    }, [props]);

    // When the 
    // useEffect(() => {

    //     if (state.open) {
    //         resetForm();
    //     }

    //     setState({
    //         ...state,
    //         open: initialState.actionStatus
    //     });
    //     //setOpen(initialState.actionStatus);
    // }, [initialState]);

    // Validate form submit
    function canBeSubmitted() {
        let count = state.check.length;
        state.check.forEach(function (item) {

            if (item.confirmed) {
                count--;
            };
        });

        if (count === 0) {
            return true;
        }

        return false;
    }

    const handleCheckChange = (event) => {
        let newList = state.checklist.slice()
        newList.find(x => x.code === event.target.name).confirmed = event.target.checked;

        setState({
            ...state,
            check: newList
        });
    };

    function updateRequestChecklist() {
        setState({
            ...state,
            loading: true
        });

        dispatch(Actions.updateRequestChecklist(row))
    }

    function resetForm() {
        setState({
            ...state,
            loading: false
        });
    }

    return (
        <React.Fragment>
            <Dialog open={state.open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='md'>
                {/* <DialogTitle id="form-dialog-title">{t("MODIFY_LOAN_PAYMENT_DIALOG_TITLE")}</DialogTitle> */}
                <DialogContent>

                    {state.checklist.map((item, index) => (

                        <div class="col-span-1">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={item.confirmed}
                                        onClick={(ev) => handleCheckChange(ev)}
                                        name={item.code}
                                        color="secondary"
                                    />
                                }
                                label={item.name}
                            />
                        </div>
                    ))}

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("CREDIT_CHECK_CLOSE_BUTTON")}
                    </Button>

                    <Button
                        //type="submit"
                        //disabled={state.loading}
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        // disabled={!canBeSubmitted()}
                        onClick={(ev) => {
                            ev.stopPropagation();
                            updateRequestChecklist()
                        }}

                    >
                        {state.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("CREDIT_CHECK_SUBMIT_BUTTON")}
                    </Button>

                </DialogActions>
            </Dialog>
        </React.Fragment >
    );
}
