import React from 'react';
import _ from '@lodash';
import PersonalInfo from './PersonalInfo';
import Address from './Address';
import RevenueSource from './RevenueSource';
import EmploymentStatus from './EmploymentStatus';
import BankInfo from './BankInfo';
import FinancialStatus from './FinancialStatus';

function BasicInfo(props) {
    return (
        <div className="md:flex">
            <div className="flex flex-col flex-1 md:pr-32">
                <PersonalInfo {...props} />
                <Address {...props} />
            </div>
            <div className="flex flex-col flex-1">
                <RevenueSource {...props} />
                <EmploymentStatus {...props} />
                <FinancialStatus {...props} />
                <BankInfo {...props} />
            </div>
        </div>
    );
}
export default BasicInfo;