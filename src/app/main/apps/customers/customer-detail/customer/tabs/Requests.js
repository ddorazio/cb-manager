import React, { useEffect, useState } from 'react';
import localStorageService from 'app/services/localStorageService';

import { Typography, Card, AppBar, Toolbar, CardContent, Button, Menu, MenuItem, Fade, IconButton, TextField } from '@material-ui/core';
import { FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import { fetchClientRequests, updateRequest } from '../../store/actions';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import MaterialTable from 'material-table';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { requestStatus, getStatusFullName, getStatusColor } from '../../../../requests/RequestStatus'
import ChecklistDialog from './dialog/checklist.dialog'
import LoanApprovedDialog from './dialog/loanApproved.dialog'
import RequestCommentDialog from './dialog/requestComment.dialog'

function Requests(props) {

    const language = localStorageService.getLanguage()
    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const clientId = useSelector(({ CustomerComp }) => CustomerComp.customer.data.id);
    const requests = useSelector(({ CustomerComp }) => CustomerComp.customer.data.requests);
    const creating = clientId == null | clientId == undefined;

    useEffect(() => {
        if (!creating && !requests && clientId) {
            dispatch(fetchClientRequests(clientId));
        }

        setRow({
            ...row,
            open: initialState.actionStatus
        });

    }, [initialState]);

    // useEffect(() => {
    //     setRow({
    //         ...row,
    //         open: initialState.actionStatus
    //     });

    // }, [initialState]);

    const [row, setRow] = useState({
        checklist: [],
        open: initialState.actionStatus,
        selectedRequest: null
    });

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorEl2, setAnchorEl2] = React.useState(null);
    const open = Boolean(anchorEl);

    //const [openChecklistDialog, setChecklistDialog] = React.useState(initialState.actionStatus);
    const handleChecklistCloseDialog = (value) => {
        //setChecklistDialog(value);
        setRow({
            ...row,
            open: initialState.actionStatus
        });
    };

    const [statusDialog, setstatusDialog] = React.useState(null);
    const [openDialog, setOpenDialog] = React.useState(false);

    const handleClickOpenDialog = (status) => {
        setstatusDialog(status)
        setOpenDialog(true);
    };

    const handleCloseDialog = () => {
        setstatusDialog(null)
        setOpenDialog(false);
    };

    const [openDialogComment, setOpenDialogComment] = React.useState(false);

    const handleClickOpenDialogComment = (status) => {
        setstatusDialog(status)
        setOpenDialogComment(true);
    };

    const handleCloseDialogComment = () => {
        setstatusDialog(null)
        setOpenDialogComment(false);
    };

    const handleClick = (event, rowData) => {
        setRow({
            ...row,
            selectedRequest: rowData
        });

        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const [subStatuses, setSubStatuses] = React.useState(null)

    const statusClicked = (event, request, status) => {
        if (status && status.subStatus) {
            setSubStatuses(status.subStatus)
            openSubMenu(event, status)
        } else {
            let req = { ...row.selectedRequest }; //{ ...request };
            if (statusDialog) {
                req.statusCode = statusDialog.statusCode
                if (statusDialog.statusCode != "ACCEPTED" && statusDialog.statusCode != "PREAPPROVED") {
                    req.amountApproved = null
                }
            } else {
                req.amountApproved = null
                req.statusCode = status.statusCode
            }

            dispatch(updateRequest(req));
            setAnchorEl(null);
            handleCloseDialog()
            handleMenu2Close();
            setstatusDialog(null)
            setOpenDialogComment(false);
        }
    }



    const openSubMenu = (event, status) => {
        setAnchorEl2(event.currentTarget)
    }

    const handleMenu2Close = () => {
        setAnchorEl2(null)
    }

    const subMenuClicked = (event, request, status) => {
        if (status || statusDialog) {
            let req = { ...row.selectedRequest }//{ ...request }
            req.statusCode = (statusDialog) ? statusDialog.statusCode : status.statusCode
            dispatch(updateRequest(req))
            setAnchorEl(null);
            setAnchorEl2(null);
        }
    }

    const formatLoanAmount = (loanAmount) => {
        if (loanAmount !== undefined) {
            return new Intl.NumberFormat(language + '-CA', { style: 'currency', currency: 'CAD' }).format(loanAmount)
        } else {
            return t("NO_APPROVED_LOAN")
        }

    }

    function validateDate(date) {

        var testDate = new Date(date);
        var maxDt = new Date("2100-01-01");
        var minDt = new Date("1899-01-01");

        return (testDate < maxDt && testDate > minDt)
    }

    if (!clientId) {
        return <div />
    }

    if (!requests) {
        return <FuseLoading />;
    }

    const handleMenuItemClick = (item) => {
        //setChecklistDialog(true);
        setRow({
            ...row,
            data: item,
            open: true,
            checklist: item.checklist
        });
    };

    return (
        <React.Fragment>


            <ChecklistDialog open={row.open} onClose={handleChecklistCloseDialog} row={row} />
            <LoanApprovedDialog open={openDialog} row={row} handleSubmit={statusClicked} handleClose={handleCloseDialog} />
            <RequestCommentDialog open={openDialogComment} row={row} handleSubmit={statusClicked} handleClose={handleCloseDialogComment} />
            <Card className="w-full mb-16">
                <CardContent className="p-0">
                    <MaterialTable
                        className="-striped white w-full mb-16 mr-16"
                        title={t('REQUEST_HEADER')}
                        columns={[
                            { title: '', field: 'id', hidden: true },
                            {
                                title: t('LOAN_LIST_CREATED_DATE'),
                                field: 'requestedDate',
                                type: 'date',
                                sorting: false,
                                defaultSort: 'desc',
                                cellStyle: { textAlign: 'left' }
                            },
                            {
                                title: t('LOAN_REQUESTED_AMOUNT'),
                                field: 'loanAmount',
                                sorting: false,
                                cellStyle: { textAlign: 'left' },
                                render: rowData => formatLoanAmount(rowData.loanAmount)
                            },
                            {
                                title: t('LOAN_APPROVED_AMOUNT'),
                                field: 'amountApproved',
                                sorting: false,
                                cellStyle: { textAlign: 'left' },
                                render: rowData => formatLoanAmount(rowData.amountApproved)
                            },
                            {
                                title: t('STATUS'),
                                field: 'status',
                                sorting: false,
                                cellStyle: { textAlign: 'left' },
                                render: rowData => <div className={`inline text-12 rounded-full py-4 px-8 truncate ${getStatusColor(rowData.statusCode)} text-white`}>{getStatusFullName(rowData.statusCode)}</div>
                            },
                            {
                                align: "right",
                                title: 'Action',
                                sorting: false,
                                render: rowData => {
                                    return (
                                        <div>
                                            <Button
                                                variant="outlined"
                                                color="secondary"
                                                aria-label="more"
                                                aria-controls={`menu-${rowData.id}`}
                                                aria-haspopup="true"
                                                onClick={(ev) => handleClick(ev, rowData)}
                                            >
                                                {t("CHANGE_STATUS")}
                                            </Button>
                                            <Menu
                                                id={`menu-${rowData.id}`}
                                                anchorEl={anchorEl}
                                                keepMounted
                                                open={open}
                                                onClose={handleClose}
                                                TransitionComponent={Fade}
                                            >

                                                {requestStatus.filter(el => el.statusCode !== "NEW").filter(el => el.statusCode !== "AUTODECLINED").map((status, index) => {
                                                    if (status.statusCode == 'ACCEPTED' || status.statusCode == 'PREAPPROVED') {
                                                        return <MenuItem key={`menu-${rowData.id}-${index}`} onClick={(ev) => handleClickOpenDialog(status)}>{status.name}</MenuItem>
                                                    } else {
                                                        return <MenuItem key={`menu-${rowData.id}-${index}`} onClick={(ev) => statusClicked(ev, rowData, status)}>{status.name}</MenuItem>
                                                    }
                                                })}

                                                {/* <MenuItem key={`menu-${rowData.id}-1`} onClick={handleClose}>Accepter</MenuItem>
                                            <MenuItem key={`menu-${rowData.id}-2`} onClick={(event) => openSubMenu(event, rowData, 1)}>En Attente</MenuItem>
                                            <MenuItem key={`menu-${rowData.id}-3`} onClick={(event) => openSubMenu(event, rowData, 2)}>Refuser</MenuItem>
                                            <MenuItem key={`menu-${rowData.id}-4`} onClick={handleClose}>Annuler</MenuItem> */}


                                                {subStatuses &&
                                                    <Menu
                                                        id={`menu-${rowData.id}-2`}
                                                        anchorEl={anchorEl2}
                                                        keepMounted
                                                        open={Boolean(anchorEl2)}
                                                        onClose={handleMenu2Close}
                                                    >
                                                        {subStatuses.map((status, index) => {
                                                            if (status.statusCode == 'DENIED_OTHER') {
                                                                return <MenuItem key={`menu-${rowData.id}-${index}`} onClick={(ev) => handleClickOpenDialogComment(status)}>{status.name}</MenuItem>
                                                            } else {
                                                                return <MenuItem key={`menu-${rowData.id}-${index}`} onClick={(ev) => statusClicked(ev, rowData, status)}>{status.name}</MenuItem>
                                                            }


                                                        })}
                                                    </Menu>
                                                }
                                            </Menu>

                                        </div>
                                    )
                                }
                            },
                            {
                                align: "right",
                                title: 'Checklist',
                                sorting: false,
                                render: rowData => {
                                    {
                                        if (rowData.checkListCompleted === false && rowData.statusCode === 'PREAPPROVED') {
                                            return (
                                                <div>
                                                    <Button
                                                        variant="outlined"
                                                        color="secondary"
                                                        aria-label="more"
                                                        aria-controls={`menu-${rowData.id}`}
                                                        aria-haspopup="true"
                                                        onClick={(ev) => {
                                                            ev.stopPropagation();
                                                            handleMenuItemClick(rowData)
                                                        }}
                                                    >
                                                        {t("CHECKLIST_VALIDATE")}
                                                    </Button>
                                                </div>
                                            )
                                        } else if (rowData.checkListCompleted === false && rowData.statusCode !== 'PREAPPROVED') {
                                            return (
                                                <div>
                                                    <Button
                                                        variant="outlined"
                                                        color="secondary"
                                                        aria-label="more"
                                                        aria-controls={`menu-${rowData.id}`}
                                                        aria-haspopup="true"
                                                        disabled="true"
                                                        onClick={(ev) => {
                                                            ev.stopPropagation();
                                                            handleMenuItemClick(rowData)
                                                        }}
                                                    >
                                                        {t("CHECKLIST_VALIDATE")}
                                                    </Button>
                                                </div>
                                            )
                                        } else {
                                            return (
                                                <div>
                                                    <div className={`inline text-12 rounded-full py-4 px-8 truncate bg-green text-white`}>{t("CHECKLIST_COMPLETED")}</div>
                                                </div>
                                            )
                                        }
                                    }
                                }
                            },
                        ]}
                        data={requests}
                        options={{
                            paging: false,
                            actionsColumnIndex: -1,
                            draggable: false,
                            search: false
                        }}
                        localization={{
                            body: {
                                emptyDataSourceMessage: t("LOAN_LIST_NO_RESULTS_FOUND"),
                            },
                        }}
                    />

                </CardContent>
            </Card>

        </React.Fragment>
    );
}

export default withRouter(Requests);
