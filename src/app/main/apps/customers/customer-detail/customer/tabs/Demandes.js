import React, { useState, useEffect } from 'react';
import { AppBar, Button, Card, CardContent, Icon, IconButton, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import ReactTable from "react-table";
import { FuseAnimateGroup } from '@fuse';
import _ from '@lodash';
import { useTranslation } from 'react-i18next';
import Requests from './Requests'
import * as Actions from '../../store/actions';
import { useDispatch, useSelector } from 'react-redux';
import CreditCheckDetailsDialog from './dialog/creditCheckDetails.dialog'

const useStyles = makeStyles(theme => ({
    dropzoneParagraph: {
        fontSize: "18px",
        paddingTop: "75px"
    }
}));

function Demandes(props) {
    const { t } = useTranslation('customersApp');
    const form = useSelector(({ CustomerComp }) => CustomerComp.customer.data);
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const creditBookChecks = useSelector(({ CustomerComp }) => CustomerComp.customer.data.creditBookChecks);
    //const classes = useStyles(props);

    const [row, setRow] = useState({
        job: "",
        references: [],
        address: ""
    });

    const handleMenuItemClick = (item) => {
        setCreditCheckDialog(true);
        setRow(item.original);
    };

    const [openCreditCheckDialog, setCreditCheckDialog] = React.useState(false);
    const handleCreditCheckCloseDialog = (value) => {
        setCreditCheckDialog(value);
    };

    const dispatch = useDispatch();
    const [state, setState] = useState({
        loading: false
    });

    useEffect(() => {

        setState({
            ...state,
            loading: initialState.actionStatus
        });

    }, [initialState]);

    function search() {
        setState({
            ...state,
            loading: true
        });

        dispatch(Actions.searchCreditBookCheck(form));
    }

    return (

        <div>
            <CreditCheckDetailsDialog open={openCreditCheckDialog} onClose={handleCreditCheckCloseDialog} row={row}></CreditCheckDetailsDialog>
            <div className="md:flex">
                <Requests />
            </div>
            <div className="md:flex">
                {/* <div className="flex flex-col flex-1 md:pr-32">
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <Typography variant="subtitle2" color="inherit" className="flex-1">{t("EXTERNAL_LOAN_HEADER")}</Typography>

                            </Toolbar>
                        </AppBar>
                        <CardContent className="p-0">
                            <ReactTable
                                data={[
                                    // {
                                    //     date: "2020-02-05 20:04:38",
                                    //     web: "pretheure.com",
                                    //     montant: "500$",
                                    //     statut: '<div class="inline text-12 rounded-full py-4 px-8 truncate bg-red-700 text-white">Refusé - Pas d\'emploi</div>'
                                    // },
                                    // {
                                    //     date: "2020-02-03 08:04:10",
                                    //     web: "pret911.ca",
                                    //     montant: "500$",
                                    //     statut: '<div class="inline text-12 rounded-full py-4 px-8 truncate bg-orange text-black">En Attente</div>'
                                    // },
                                    // {
                                    //     date: "2019-06-04 15:14:24",
                                    //     web: "paydayking500.com",
                                    //     montant: "1000$",
                                    //     statut: '<div class="inline text-12 rounded-full py-4 px-8 truncate bg-red-700 text-white">Refusé - Autres</div>'
                                    // }
                                ]}
                                columns={[
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_DATE"),
                                        accessor: 'date',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_SITE"),
                                        accessor: 'web',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_CLIENT"),
                                        //width: 90,
                                        accessor: 'montant',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_EMAIL"),
                                        //width: 90,
                                        accessor: 'email',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_STATUS"),
                                        accessor: 'statut',
                                        filterable: true,
                                        Cell: row => (<div dangerouslySetInnerHTML={{ __html: row.value }} />)
                                    },
                                    {
                                        Header: "",
                                        width: 50,
                                        className: "py-0",
                                        Cell: row => (
                                            <div className="flex items-center">

                                                <IconButton>
                                                    <Icon>remove_red_eye</Icon>
                                                </IconButton>
                                            </div>
                                        )
                                    }
                                ]}
                                resizable={false}
                                filterable={false}
                                defaultPageSize={5}
                                className="-striped white"
                                noDataText={t("EXTERNAL_LOAN_NO_RESULTS_FOUND")}
                                pageText={t("REACT_TABLE_PAGE_TEXT")}
                                ofText={t("REACT_TABLE_OF_TEXT")}
                                rowsText={t("REACT_TABLE_ROWS_TEXT")}
                            />
                        </CardContent>
                    </Card>
                </div> */}

                <div className="flex flex-col flex-1">
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <img src="./assets/images/logos/CreditBookCHECK.png" alt="CreditBook CHECK" className="w-128" />
                                <Button
                                    className="ml-20"
                                    //type="submit"
                                    //disabled={state.loading}
                                    variant="contained"
                                    color="secondary"
                                    aria-label="Enregistrer"
                                    // disabled={!canBeSubmitted()}
                                    onClick={() =>
                                        search()}

                                >
                                    {state.loading &&
                                        <i
                                            className="fa fa-refresh fa-spin"
                                            style={{ marginRight: "5px" }}
                                        />
                                    }
                                    {t("SEARCH_CREDITBOOK_CHECK")}
                                </Button>
                            </Toolbar>
                        </AppBar>
                        <CardContent className="p-0">
                            <ReactTable
                                data={creditBookChecks}
                                columns={[
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_DATE"),
                                        accessor: 'requestDate',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_SITE"),
                                        accessor: 'lender',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_CLIENT"),
                                        //width: 90,
                                        accessor: 'client',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_EMAIL"),
                                        //width: 90,
                                        accessor: 'email',
                                        filterable: true
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_LISTE_STATUS"),
                                        accessor: 'status',
                                        filterable: true,
                                        Cell: row => (<div dangerouslySetInnerHTML={{ __html: row.value }} />)
                                    },
                                    {
                                        Header: t("EXTERNAL_LOAN_REASON"),
                                        style: { 'whiteSpace': 'unset' },
                                        accessor: 'reason',
                                        filterable: true
                                    },
                                    {
                                        Header: "",
                                        width: 50,
                                        className: "py-0",
                                        Cell: row => (
                                            <div className="flex items-center">

                                                <IconButton
                                                    onClick={(ev) => {
                                                        // ev.stopPropagation();
                                                        handleMenuItemClick(row);
                                                    }}>
                                                    <Icon>remove_red_eye</Icon>
                                                </IconButton>
                                            </div>
                                        )
                                    }
                                ]}
                                defaultPageSize={5}
                                className="-striped white"
                                noDataText={t("EXTERNAL_LOAN_NO_RESULTS_FOUND")}
                                pageText={t("REACT_TABLE_PAGE_TEXT")}
                                ofText={t("REACT_TABLE_OF_TEXT")}
                                rowsText={t("REACT_TABLE_ROWS_TEXT")}
                            />
                        </CardContent>
                    </Card>
                </div>
            </div>
        </div>


    );
}
export default Demandes;