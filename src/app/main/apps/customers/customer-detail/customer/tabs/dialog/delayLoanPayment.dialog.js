import React, { useRef, useState } from 'react';
import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, FormControlLabel, Icon, IconButton, InputAdornment, TextField, Typography } from '@material-ui/core';
import { FuseAnimate } from '@fuse';
import { MuiPickersUtilsProvider, KeyboardDatePicker, } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import * as Actions from '../../../store/actions';
import { useTranslation } from 'react-i18next';
import localStorageService from 'app/services/localStorageService';

moment.locale(localStorageService.getLanguage())

export default function DelayLoanPaymentDialog(props) {

    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const [fullWidth, setFullWidth] = React.useState(true);
    const { loan, row } = props;
    const dispatch = useDispatch();

    const [state, setState] = useState({
        loading: false,
        originalPaymentDate: row.original.paymentDate,
        paymentDate: row.original.paymentDate
    });

    useEffect(() => {

        if (open) {
            resetForm();
        }

        setOpen(initialState.actionStatus);
    }, [initialState]);

    const handleChangePaymentDate = (event) => {

        setState({
            ...state,
            paymentDate: event,
            originalPaymentDate: event
        });
    };

    function confirmModifyPayment() {

        setState({
            ...state,
            loading: true
        });

        dispatch(Actions.modifyPaymentDate(loan, row.original, state));
    }

    // Validate form submit
    function canBeSubmitted() {
        if (!state.loading) {
            return state.paymentDate;
        }
    }

    function resetForm() {
        setState({
            ...state,
            loading: false,
            originalPaymentDate: row.original.paymentDate,
            paymentDate: row.original.paymentDate
        });
    }

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        resetForm();
    };

    return (
        <React.Fragment>
            <IconButton onClick={handleClickOpen}><Icon>calendar_today</Icon></IconButton>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='xs'>
                <DialogTitle id="form-dialog-title">{t("DEFER_LOAN_PAYMENT_DIALOG_TITLE")}</DialogTitle>
                <DialogContent>
                    <div className="flex">
                        <div className="flex-1 mr-16 text-center">
                            {/* <div className="bg-gray-100 p-16 ">
                                <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_PAYMENT_ORIGINAL_DATE")}</Typography>
                                <Typography className="h3" color="textPrimary">{row.original.paymentDate}</Typography>
                            </div> */}

                            <FuseAnimate animation="transition.fadeIn">
                                <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                    <KeyboardDatePicker
                                        className="mt-8 mb-16"
                                        //disableToolbar
                                        minDate={row.original.paymentDate}
                                        format="YYYY-MM-DD"
                                        inputVariant="outlined"
                                        id="loan_date"
                                        name="loan_date"
                                        autoOk
                                        variant="inline"
                                        inputVariant="outlined"
                                        InputAdornmentProps={{ position: "start" }}
                                        label={t("DEFER_LOAN_PAYMENT_CHANGE_DATE_TEXT")}
                                        value={state.paymentDate}
                                        onChange={handleChangePaymentDate}
                                        KeyboardButtonProps={{
                                            'aria-label': 'change date',
                                        }}
                                    />
                                </MuiPickersUtilsProvider>
                            </FuseAnimate>
                        </div>
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("DEFER_LOAN_PAYMENT_CANCEL_BUTTON")}
                    </Button>

                    <Button
                        //type="submit"
                        //disabled={state.loading}
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        disabled={!canBeSubmitted()}
                        onClick={() =>
                            confirmModifyPayment()}
                    >
                        {state.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("MODIFY_LOAN_PAYMENT_SAVE_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
