import React, { useRef, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button, ButtonGroup, Checkbox, ClickAwayListener, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormControlLabel, InputLabel, InputAdornment, Select, Switch, TextField, Typography } from '@material-ui/core';
import { FuseAnimateGroup } from '@fuse';

import { useDispatch, useSelector } from 'react-redux';
//import { useEffect } from 'react';
import * as Actions from '../../../store/actions';
import { useTranslation } from 'react-i18next';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

export default function EditLoanDialog(props) {
    //loan._original.loanPayments.findIndex(payment => payment.id == state.selectedPaymentId)
    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const { form, loan } = props;
    const [fullWidth, setFullWidth] = React.useState(true);

    const newDateToday = new Date();
    var today = new Date();
    const minDate = new Date(today.getFullYear(), today.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);
    const [minFrequencyDate, setMinFrequencyDate] = useState(minDate)

    const dispatch = useDispatch();
    const [state, setState] = useState({
        loading: false,
        stopPaymentDate: new Date(),
        disableDates: false,
        checkFrequency: false,
        checkPayment: false,
        checkStopPayment: false,
        paymentAgreement: false,
        perceptech: false,
        interac: false,
        frequencyId: loan._original.frequencyId,
        frequencyDate: null, //minDate,
        newLoanAmount: null,
        displayStopPaymentDate: minDate,
        startDay: "",
        endDay: "",
        selectedPaymentId: "",
    })

    // useEffect(() => {

    //     if (!(state.frequencyDate instanceof Date)) {
    //         const frequencyDate = new Date(state.frequencyDate)
    //         setState({
    //             ...state,
    //             frequencyDate: frequencyDate,
    //         });
    //     }

    // }, [state.frequencyDate])

    useEffect(() => {
        if (!(state.displayStopPaymentDate instanceof Date)) {
            const displayStopPaymentDate = new Date(state.displayStopPaymentDate)

            setState({
                ...state,
                displayStopPaymentDate: displayStopPaymentDate,
            });
        }
    }, [state.displayStopPaymentDate])

    useEffect(() => {
        var paymentList = loan._original.loanPayments
            .filter(item => item.sentToPerceptech === false)
            .filter(item => item.statusCode === "PEN" || item.statusCode === "PC")
            .map((item, index) => ({
                id: item.id,
                payment: `${item.paymentDate + ' ' + item.amount + "$"}`
            }));

        setState({
            ...state,
            paymentList: paymentList
        });
        setMinFrequencyDate(minDate)
    }, [loan._original.loanPayments]);

    // useEffect(() => {

    //     if (open) {
    //         resetForm();
    //     }

    //     onClose(initialState.actionStatus);
    // }, [initialState]);

    useEffect(() => {

        if (open) {
            var paymentList = initialState.data.loans
                .filter(item => item.id === loan._original.id)[0].loanPayments
                .filter(item => item.canEdit === true)
                .map((item, index) => ({
                    id: item.id,
                    payment: `${item.paymentDate + ' ' + item.amount + "$"}`
                }));

            setState({
                ...state,
                loading: false,
                stopPaymentDate: new Date(),
                disableDates: false,
                checkFrequency: false,
                checkPayment: false,
                checkStopPayment: false,
                paymentAgreement: false,
                perceptech: false,
                interac: false,
                frequencyId: loan._original.frequencyId,
                frequencyDate: null, //minDate,
                newLoanAmount: null,
                displayStopPaymentDate: minDate,
                startDay: "",
                endDay: "",
                selectedPaymentId: "",
            });
        }

        onClose(initialState.actionStatus);

    }, [initialState.data.loans]);

    const handleChange = (event) => {

        switch (event.target.name) {
            case "checkFrequency":
                setState({
                    ...state,
                    [event.target.name]: event.target.checked,
                    disableDates: loan._original.frequencyId === 4 ? true : false,
                    frequencyId: loan._original.frequencyId,
                    //frequencyDate: minDate,
                    startDay: loan._original.biMonthlyStartDay,
                    endDay: loan._original.biMonthlyEndDay,
                    checkStopPayment: false,
                    //displayStopPaymentDate: new Date()
                    displayStopPaymentDate: minDate
                });
                break;
            case "checkPayment":
                setState({
                    ...state,
                    [event.target.name]: event.target.checked,
                    newLoanAmount: null,
                    checkStopPayment: false,
                    //displayStopPaymentDate: new Date()
                    displayStopPaymentDate: minDate
                });
                break;
            case "checkStopPayment":
                setState({
                    ...state,
                    [event.target.name]: event.target.checked,
                    disableDates: false,
                    checkFrequency: false,
                    checkPayment: false,
                    paymentAgreement: false,
                    perceptech: false,
                    interac: false,
                    frequencyId: loan._original.frequencyId,
                    frequencyDate: null,
                    newLoanAmount: 0,
                    startDay: "",
                    endDay: ""
                });
                break;
            case "paymentAgreement":
                setState({
                    ...state,
                    [event.target.name]: event.target.checked,
                    checkStopPayment: false
                });
                break;
        }
    };

    const handleChangeLoanFrequency = (event) => {

        if (event.target.value === "4") {
            setState({
                ...state,
                frequencyId: event.target.value,
                disableDates: true
            });
        } else {
            setState({
                ...state,
                frequencyId: event.target.value,
                disableDates: false
            });
        }
    }

    const handleChangeLoanFrequencyDate = (event) => {
        setState({
            ...state,
            frequencyDate: event
        });
    };

    const handleChangeLoanAmount = (event) => {
        setState({
            ...state,
            newLoanAmount: parseFloat(event.target.value) || 0
        });
    };

    const handleChangeLoanStopPaymentDate = (event) => {
        setState({
            ...state,
            displayStopPaymentDate: event,
            stopPaymentDate: event
        });
    };

    const handleChangeStartDate = (event) => {
        setState({
            ...state,
            startDay: event.target.value
        });
    };

    const handleChangeEndDate = (event) => {
        setState({
            ...state,
            endDay: event.target.value
        });
    };

    const handleChangePerceptech = (event) => {
        setState({
            ...state,
            perceptech: event.target.checked,
            interac: false
        });
    };

    const handleChangeInterac = (event) => {
        setState({
            ...state,
            interac: event.target.checked,
            perceptech: false
        });
    };

    const handleChangeSelectedPayment = (event) => {

        var index = loan._original.loanPayments.findIndex(payment => payment.id == event.target.value)
        var today = new Date();
        var minDate = new Date(today.getFullYear(), today.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);

        if (index >= 1) {
            var previousDate = new Date(loan._original.loanPayments[index - 1].paymentDate)

            if (minDate < previousDate) {
                previousDate.setDate(previousDate.getDate() + 2);
                setMinFrequencyDate(previousDate);

                // setState({
                //     ...state,
                //     frequencyDate: previousDate
                // });

                // if (state.frequencyDate < date.getTime()) {
                //     setState({
                //         ...state,
                //         frequencyDate: date
                //     });
                // }
            } else {
                setMinFrequencyDate(minDate);
            }


        } else {
            var today = new Date();
            setMinFrequencyDate(minDate)
        }

        setState({
            ...state,
            frequencyDate: null,
            selectedPaymentId: event.target.value
        });
    };

    function resetForm() {
        setState({
            ...state,
            loading: false,
            stopPaymentDate: new Date(),
            disableDates: false,
            checkFrequency: false,
            checkPayment: false,
            checkStopPayment: false,
            paymentAgreement: false,
            perceptech: false,
            interac: false,
            frequencyId: loan._original.frequencyId,
            frequencyDate: null,
            newLoanAmount: null,
            displayStopPaymentDate: minDate,
            startDay: "",
            endDay: "",
            selectedPaymentId: ""
        });
    }

    const { open, onClose } = props;
    const handleClose = () => {
        onClose(false);
        resetForm();
    };

    function confirmModifyLoan() {
        setState({
            ...state,
            loading: true,
            //frequencyDate: null
        });

        dispatch(Actions.updateLoan(loan, state));
        //onClose(false);
        //resetForm();
    }

    // Validate form submit
    function canBeSubmitted() {

        if (!state.loading) {

            if (!state.disableDates) {

                if (state.paymentAgreement) {
                    return (state.perceptech
                        || state.interac)
                        && state.frequencyDate
                        && state.frequencyId
                        && state.newLoanAmount;
                }
                else if (state.checkFrequency && state.checkPayment) {
                    return state.frequencyDate
                        && state.frequencyId
                        && state.selectedPaymentId
                        && state.newLoanAmount;
                }
                else if (state.checkFrequency) {
                    return state.frequencyDate
                        && state.frequencyId
                        && state.selectedPaymentId;
                } else if (state.checkPayment) {
                    return state.frequencyDate
                        && state.newLoanAmount;
                } else if (state.checkStopPayment) {
                    return state.displayStopPaymentDate;
                }

            } else {

                if (state.paymentAgreement) {
                    return (state.perceptech
                        || state.interac)
                        && state.frequencyDate
                        && state.frequencyId
                        && state.newLoanAmount;
                }
                else if (state.checkFrequency && state.checkPayment) {
                    return state.frequencyDate
                        && state.frequencyId
                        && state.selectedPaymentId
                        && state.newLoanAmount
                        && state.startDay
                        && state.endDay;
                } else if (state.checkFrequency) {
                    return state.frequencyDate
                        && state.frequencyId
                        && state.selectedPaymentId
                        && state.startDay
                        && state.endDay;
                } else if (state.checkPayment) {
                    return state.frequencyDate
                        && state.newLoanAmount;
                } else if (state.checkStopPayment) {
                    return state.displayStopPaymentDate;
                }
            }
        }
    }

    return (
        <React.Fragment>

            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='md'>
                <DialogTitle id="form-dialog-title">{t("MODIFY_LOAN_DIALOG_TITLE")}</DialogTitle>
                <DialogContent>
                    <div class="grid grid-cols-3 gap-8">
                        <div class="col-span-1 bg-gray-100 p-16 text-center">
                            <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_AMOUNT_HEADER")} :</Typography>
                            <Typography className="h3" color="textPrimary">{loan.amount.toFixed(2)} $</Typography>
                        </div>
                        <div class="col-span-1 bg-gray-100 p-16 text-center">
                            <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_FREQUENCY_HEADER")} :</Typography>
                            <Typography className="h3" color="textPrimary">{loan.frequency}</Typography>
                        </div>
                        <div class="col-span-1 bg-gray-100 p-16 text-center">
                            <Typography className="h5" color="textSecondary">{t("MODIFY_LOAN_BALANCE_HEADER")} :</Typography>
                            <Typography className="h3" color="textPrimary">{loan._original.totalAmountRemaining.toFixed(2)} $</Typography>
                        </div>
                    </div>
                    <div class="grid grid-cols-4 gap-8 mt-10">
                        <div class="col-start-1 col-span-4">
                            <div class="grid grid-cols-4 gap-16">
                                <div class="col-span-1">
                                    {loan._original.statusCode == "PA"
                                        ?
                                        (loan._original.isPerceptech &&
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={state.checkFrequency}
                                                        onChange={handleChange}
                                                        name="checkFrequency"
                                                        color="secondary"
                                                    />
                                                }
                                                label={t("MODIFY_LOAN_CHANGE_FREQUENCY")}
                                            />
                                        )
                                        :
                                        (
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={state.checkFrequency}
                                                        onChange={handleChange}
                                                        name="checkFrequency"
                                                        color="secondary"
                                                    />
                                                }
                                                label={t("MODIFY_LOAN_CHANGE_FREQUENCY")}
                                            />
                                        )
                                    }
                                </div>

                                <div class="col-span-1">
                                    {state.checkFrequency &&
                                        (
                                            <FormControl className="w-full" variant="outlined" >
                                                <InputLabel id="status">{t("MODIFY_LOAN_FROM_PAYMENT")}</InputLabel>
                                                <Select
                                                    labelid="statusId"
                                                    id="statusId"
                                                    native
                                                    value={state.selectedPaymentId}
                                                    onChange={handleChangeSelectedPayment}
                                                >
                                                    <option aria-label="None" value="" />
                                                    {state.paymentList.map((item, index) => (
                                                        <option key={index} value={item.id}>{item.payment}</option>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        )
                                    }
                                </div>

                                <div class="col-span-1">
                                    {state.checkFrequency &&
                                        (
                                            <FormControl className="w-full" variant="outlined" >
                                                <Select
                                                    fullWidth
                                                    native
                                                    value={state.frequencyId}
                                                    label="Fréquence des paiements"
                                                    inputProps={{
                                                        name: 'frequence',
                                                        id: 'frequence-label',
                                                    }}
                                                    onChange={handleChangeLoanFrequency}
                                                >
                                                    <option aria-label="None" value="" />
                                                    {form.loanFrequencies
                                                        .map((frequency, index) => (
                                                            //.filter(item => item.id <= 3).map((frequency, index) => (
                                                            <option key={index} value={frequency.id}>{frequency.name}</option>
                                                        ))}

                                                </Select>
                                            </FormControl>
                                        )
                                    }
                                </div>
                                <div class="col-span-1">
                                    {state.checkFrequency &&
                                        (
                                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                                <KeyboardDatePicker
                                                    className="w-full"
                                                    //disableToolbar
                                                    minDate={minFrequencyDate}
                                                    format="YYYY-MM-DD"
                                                    inputVariant="outlined"
                                                    id="frequency_date"
                                                    name="frequency_date"
                                                    autoOk
                                                    variant="inline"
                                                    inputVariant="outlined"
                                                    InputAdornmentProps={{ position: "start" }}
                                                    label={t("MODIFY_LOAN_FROM_DATE")}
                                                    value={state.frequencyDate}
                                                    onChange={handleChangeLoanFrequencyDate}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change date',
                                                    }}
                                                />
                                            </MuiPickersUtilsProvider>
                                        )
                                    }
                                </div>
                                {state.checkFrequency && state.disableDates &&
                                    (
                                        <React.Fragment>
                                            <div class="col-start-2 col-span-1">
                                                <div class="grid grid-cols-2 gap-16">
                                                    <div class="col-span-1">
                                                        <TextField
                                                            id="montant_pret"
                                                            name="montant_pret"
                                                            label={t("MODIFY_LOAN_DAY_ONE")}
                                                            type="number"
                                                            variant="outlined"
                                                            fullwidth
                                                            onChange={handleChangeStartDate}
                                                            //onChange={e => handleChangeStartDate(e.target.value)}
                                                            defaultValue={state.startDay}
                                                            value={state.startDay}
                                                        />
                                                    </div>
                                                    <div class="col-span-1">
                                                        <TextField
                                                            id="montant_pret"
                                                            name="montant_pret"
                                                            label={t("MODIFY_LOAN_DAY_ONE")}
                                                            type="number"
                                                            variant="outlined"
                                                            fullwidth
                                                            onChange={handleChangeEndDate}
                                                            defaultValue={state.endDay}
                                                            value={state.endDay}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-span-1"></div>
                                        </React.Fragment>
                                    )
                                }
                            </div>
                        </div>

                        <div class="col-start-1 col-span-4">
                            <div class="grid grid-cols-4 gap-16">
                                <div class="col-span-1">
                                    {loan._original.statusCode === "PA"
                                        ?
                                        (loan._original.isPerceptech &&
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={state.checkPayment}
                                                        onChange={handleChange}
                                                        name="checkPayment"
                                                        color="secondary"
                                                    />
                                                }
                                                label={t("MODIFY_LOAN_CHANGE_AMOUNT")}
                                            />
                                        )
                                        :
                                        (
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={state.checkPayment}
                                                        onChange={handleChange}
                                                        name="checkPayment"
                                                        color="secondary"
                                                    />
                                                }
                                                label={t("MODIFY_LOAN_CHANGE_AMOUNT")}
                                            />
                                        )
                                    }
                                </div>
                                <div class="col-span-1">
                                    {state.checkPayment &&
                                        (
                                            <FormControl className="w-full" variant="outlined" >
                                                <TextField
                                                    id="montant_pret"
                                                    name="montant_pret"
                                                    label={t("MODIFY_LOAN_NEW_AMOUNT")}
                                                    type="number"
                                                    variant="outlined"
                                                    fullWidth
                                                    color="primary"
                                                    onChange={handleChangeLoanAmount}
                                                    //defaultValue={state.newLoanAmount}
                                                    value={state.newLoanAmount}
                                                    InputProps={{
                                                        endAdornment: <InputAdornment>$</InputAdornment>,
                                                    }}
                                                />
                                            </FormControl>
                                        )
                                    }
                                </div>
                                <div class="col-span-1">
                                    {state.checkPayment && !state.checkFrequency &&
                                        (
                                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                                <KeyboardDatePicker
                                                    className="w-full"
                                                    //disableToolbar
                                                    minDate={minDate}
                                                    format="YYYY-MM-DD"
                                                    inputVariant="outlined"
                                                    id="frequency_date"
                                                    name="frequency_date"
                                                    autoOk
                                                    variant="inline"
                                                    inputVariant="outlined"
                                                    InputAdornmentProps={{ position: "start" }}
                                                    label={t("MODIFY_LOAN_FROM_DATE")}
                                                    value={state.frequencyDate}
                                                    onChange={handleChangeLoanFrequencyDate}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change date',
                                                    }}
                                                />
                                            </MuiPickersUtilsProvider>
                                        )
                                    }
                                </div>

                            </div>
                        </div>

                        <div class="col-start-1 col-span-4">
                            <div class="grid grid-cols-4 gap-16">
                                <div class="col-span-1">
                                    {loan._original.statusCode === "PA"
                                        ?
                                        (loan._original.isPerceptech &&
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={state.paymentAgreement}
                                                        onChange={handleChange}
                                                        name="paymentAgreement"
                                                        color="secondary"
                                                    />
                                                }
                                                label={t("MODIFY_LOAN_PAYMENT_AGREEMENT")}
                                            />
                                        )
                                        :
                                        (
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={state.paymentAgreement}
                                                        onChange={handleChange}
                                                        name="paymentAgreement"
                                                        color="secondary"
                                                    />
                                                }
                                                label={t("MODIFY_LOAN_PAYMENT_AGREEMENT")}
                                            />
                                        )
                                    }
                                </div>
                                <div class="col-span-2">
                                    {state.paymentAgreement &&
                                        (
                                            <React.Fragment>
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={state.perceptech}
                                                            onChange={handleChangePerceptech}
                                                            name="perceptech"
                                                            color="secondary"
                                                        />
                                                    }
                                                    label={t("MODIFY_LOAN_PAYMENT_PERCEPTECH")}
                                                />
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={state.interac}
                                                            onChange={handleChangeInterac}
                                                            name="interac"
                                                            color="secondary"
                                                        />
                                                    }
                                                    label={t("MODIFY_LOAN_PAYMENT_INTERAC")}
                                                />
                                            </React.Fragment>
                                        )
                                    }
                                </div>
                            </div>
                        </div>




                        <div class="col-start-1 col-span-4">
                            <div class="grid grid-cols-4 gap-16">
                                <div class="col-span-1">
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={state.checkStopPayment}
                                                onChange={handleChange}
                                                name="checkStopPayment"
                                                color="secondary"
                                            />
                                        }
                                        label={t("MODIFY_LOAN_CHANGE_PAYMENT")}
                                    />
                                </div>
                                <div class="col-span-1">
                                    {state.checkStopPayment &&
                                        (
                                            <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                                <KeyboardDatePicker
                                                    className="w-full"
                                                    //disableToolbar
                                                    minDate={minDate}
                                                    format="YYYY-MM-DD"
                                                    inputVariant="outlined"
                                                    id="displayStopPaymentDate"
                                                    name="displayStopPaymentDate"
                                                    autoOk
                                                    variant="inline"
                                                    inputVariant="outlined"
                                                    InputAdornmentProps={{ position: "start" }}
                                                    label={t("MODIFY_LOAN_FROM_DATE")}
                                                    value={state.displayStopPaymentDate}
                                                    onChange={handleChangeLoanStopPaymentDate}
                                                    KeyboardButtonProps={{
                                                        'aria-label': 'change date',
                                                    }}
                                                />
                                            </MuiPickersUtilsProvider>
                                        )
                                    }

                                </div>
                            </div>
                        </div>





                    </div>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("MODIFY_LOAN_CANCEL_BUTTON")}
                    </Button>
                    <Button
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        onClick={() => confirmModifyLoan()}
                        disabled={!canBeSubmitted()}
                    >
                        {state.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("MODIFY_LOAN_SAVE_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog >
        </React.Fragment >
    );
}
