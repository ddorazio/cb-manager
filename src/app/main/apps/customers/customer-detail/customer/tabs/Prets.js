import React from 'react';
import * as Material from '@material-ui/core';
import ReactTable from "react-table";
import { FuseAnimateGroup } from '@fuse';
import clsx from 'clsx';
import _ from '@lodash';
import NewLoanDialog from './dialog/newLoan.dialog';
import * as Actions from '../../store/actions';
import PretDetails from './PretDetails';
import * as Redux from 'react-redux';
import { useTranslation } from 'react-i18next';

const options = ['Envoyer le contrat', 'Modifier le prêt', 'Nouveau paiement manuel', 'Nouveau rabais'];

function Loans(props) {

    const dispatch = Redux.useDispatch();
    const { t } = useTranslation('customersApp');
    //const nonLinkedRequests = Redux.useSelector(({ CustomerComp }) => CustomerComp.customer.data.nonLinkedRequests);
    const { form, handleChange, setForm } = props;
    //const isOpen = true;

    return (
        <Material.Card className="w-full mb-16">
            <Material.AppBar position="static" elevation={0} className="black_bg">
                <Material.Toolbar className="pl-16 pr-8">
                    <Material.Typography variant="subtitle2" color="inherit" className="flex-1">{t("LOAN_LIST")}</Material.Typography>
                    <NewLoanDialog form={form} setForm={setForm} />
                </Material.Toolbar>
            </Material.AppBar>
            <Material.CardContent className="">
                <ReactTable
                    data={form.loans}
                    collapseOnDataChange={false}
                    columns={[
                        {
                            Header: "",
                            width: 55,
                            expander: true,
                            Expander: ({ isExpanded, ...rest }) =>
                                <div>
                                    {isExpanded
                                        ? <Material.Icon>expand_less</Material.Icon>
                                        : <Material.Icon>remove_red_eye</Material.Icon>}
                                </div>
                        },
                        {
                            Header: t("LIST_LOAN_NO"),
                            width: 90,
                            accessor: 'loanNumber',
                            Cell: row => (
                                <div><strong>{row.value.toString().padStart(6, '0')}</strong></div>
                            )
                        },
                        {
                            Header: t("LIST_LOAN_AMOUNT"),
                            width: 120,
                            accessor: 'amount',
                            Cell: row => (
                                <div><strong>{row.value.toFixed(2)}</strong> $</div>
                            )
                        },
                        {
                            Header: t("LIST_LOAN_STATUS"),
                            width: 240,
                            accessor: 'status',
                            Cell: row => row.original.statusCode === "IP" || row.original.statusCode === "WFD" || row.original.statusCode === "LR"
                                ? <div className="w-full h-full inline-block pt-8 rounded-full truncate text-white text-center"
                                    style={{ backgroundColor: "#8bc34a" }}
                                >{row.original.status}</div>
                                : row.original.statusCode === "LS" || row.original.statusCode === "PA" || row.original.statusCode === "LD"
                                    ? <div className="w-full h-full inline-block pt-8 rounded-full truncate text-white text-center"
                                        style={{ backgroundColor: "#f44336" }}
                                    >{row.original.status}</div>
                                    : <div className="w-full h-full inline-block pt-8 rounded-full truncate text-white text-center"
                                        style={{ backgroundColor: "#ffa500" }}
                                    >{row.original.status}</div>
                        },
                        {
                            Header: t("LIST_LOAN_START_DATE"),
                            width: 120,
                            accessor: 'startDate',
                        },
                        {
                            Header: t("LIST_LOAN_END_DATE"),
                            width: 120,
                            accessor: 'endDate',
                        },
                        {
                            Header: t("LIST_LOAN_FREQUENCY"),
                            width: 200,
                            accessor: 'frequency',
                        },
                        {
                            Header: t("LIST_LOAN_NO_PAYMENTS"),
                            width: 160,
                            accessor: 'numberOfPayments',
                        },
                        {
                            Header: t("LIST_LOAN_CURRENT_BALANCE"),
                            width: 160,
                            accessor: 'currentDailyBalance',
                            Cell: row => (
                                <div><strong>{row.value.toFixed(2)}</strong> $</div>
                            )
                        },

                    ]}
                    resizable={false}
                    filterable={false}
                    defaultPageSize={5}
                    className="-striped white"
                    noDataText={"Aucuns prêts trouvés."}
                    getTrProps={(state, rowInfo, column) => {
                        return {
                            style: {
                                backgroundColor: 'rowInfo.row.status["color"].value'
                            },

                        }
                    }}
                    SubComponent={(v) =>
                        <PretDetails loan={v.row} form={form}></PretDetails>
                    }
                    pageText={t("REACT_TABLE_PAGE_TEXT")}
                    ofText={t("REACT_TABLE_OF_TEXT")}
                    rowsText={t("REACT_TABLE_ROWS_TEXT")}
                />
            </Material.CardContent>
        </Material.Card>

    );
}
export default Loans;