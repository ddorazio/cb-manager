import React, { useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Button, Card, CardContent, Icon, IconButton, Toolbar, Typography } from '@material-ui/core';
import ReactTable from "react-table";
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as Actions from '../../store/actions';
import { useTranslation } from 'react-i18next';

const ColorButton = withStyles(theme => ({
    root: {
        color: "#fff",
        backgroundColor: "#e41f25",
        '&:hover': {
            backgroundColor: "#000000",
        },
    },
}))(Button);

function IBV(props) {

    const { t } = useTranslation('customersApp');
    const dispatch = useDispatch();
    const form = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const ibv = useSelector(({ CustomerComp }) => CustomerComp.customer.data.ibvList);

    const [state, setState] = useState({
        loading: false,
    });

    useEffect(() => {
        setState({
            ...state,
            loading: form.actionStatus
        })
    }, [form]);

    function newIBV() {
        setState({
            ...state,
            loading: true
        })
        dispatch(Actions.createIBV(form.data));
    }

    return (
        <Card className="w-full mb-16">
            <AppBar position="static" elevation={0} className="black_bg">
                <Toolbar className="pl-16 pr-8">
                    <Typography variant="subtitle2" color="inherit" className="flex-1">{t("IBV_LIST")}</Typography>
                    <ColorButton variant="contained"
                        disabled={state.loading}
                        onClick={() => newIBV()}>
                        {state.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("NEW_IBV")} <img className="w-24 pl-8 square" src="assets/images/logos/ibv-check.png" />
                    </ColorButton>
                </Toolbar>
            </AppBar>
            <CardContent className="p-0">
                <ReactTable
                    data={ibv}
                    columns={[
                        {
                            Header: t("IBV_LIST_NO"),
                            width: 400,
                            style: {fontSize:"13px"},
                            accessor: 'guid',
                            filterable: false
                        },
                        {
                            Header: t("IBV_LIST_CREATED_DATE"),
                            accessor: 'createdDateFormat',
                            style: {fontSize:"13px"},
                            filterable: false
                        },
                        {
                            Header: t("IBV_LIST_STATUS"),
                            accessor: 'status',
                            filterable: false,
                            Cell: row => row.original.statusCode === "RS"
                                ? (<div class="inline text-12 rounded-full py-4 px-8 truncate bg-red text-black">{row.original.status}</div>)
                                : row.original.statusCode == "IP"
                                    ? (<div class="inline text-12 rounded-full py-4 px-8 truncate bg-yellow text-black">{row.original.status}</div>)
                                    : (<div class="inline text-12 rounded-full py-4 px-8 truncate bg-green text-black">{row.original.status}</div>)
                        },
                        {
                            Header: t("IBV_LIST_REPORT"),
                            width: 100,
                            className: "py-0",
                            Cell: row => (
                                row.original.statusCode === "COM"
                                    ?
                                    <div className="flex items-center">
                                        <IconButton onClick={() => window.open(row.original.url)}>
                                            <Icon>remove_red_eye</Icon>
                                        </IconButton>
                                    </div>
                                    : ""
                            )
                        },
                        {
                            Header: t("IBV_RESEND"),
                            width: 90,
                            Cell: row => row.original.statusCode === 'RS' || row.original.statusCode === 'IP'
                                ? <IconButton onClick={() => dispatch(Actions.resendIBV(row.original.id))}>
                                    <Icon>email</Icon>
                                </IconButton>
                                : null
                        }
                    ]}
                    defaultPageSize={5}
                    className="-striped white"
                    noDataText={"Aucune IBV trouvée."}
                    pageText={t("REACT_TABLE_PAGE_TEXT")}
                    ofText={t("REACT_TABLE_OF_TEXT")}
                    rowsText={t("REACT_TABLE_ROWS_TEXT")}
                />
            </CardContent>
        </Card>
    );
}

export default withRouter(IBV);