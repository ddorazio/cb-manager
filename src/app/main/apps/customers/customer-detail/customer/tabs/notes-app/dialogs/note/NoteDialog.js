import React from 'react';
import {Slide, Dialog} from '@material-ui/core';
import {useDebounce} from '@fuse/hooks';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from '../../store/actions';
import NoteForm from '../../note-form/NoteForm';
import NoteType from '../../NoteType'

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function NoteDialog(props)
{
    const dispatch = useDispatch();
    const notes = useSelector(({notesApp}) => {
        switch (props.type) {
            case NoteType.CLIENT: return notesApp.notes
            case NoteType.COLLECTION: return notesApp.collectionNotes
        }
        
    });

    function handleOnRemove()
    {
        switch (props.type) {
            case NoteType.CLIENT: {
                dispatch(Actions.removeNote(notes.noteDialogId));
                break
            }
            case NoteType.COLLECTION: {
                dispatch(Actions.removeCollectionNote(notes.noteDialogId));
                break
            }
        }
    }

    const handleClose = () => {
        switch (props.type) {
            case NoteType.CLIENT: {
                dispatch(Actions.closeNoteDialog())
                break
            }
            case NoteType.COLLECTION: {
                dispatch(Actions.closeCollectionNoteDialog())
                break
            }
        }
    }

    if ( !notes.entities )
    {
        return null;
    }

    return (
        <Dialog
            classes={{
                paper: "w-full m-24 rounded-8"
            }}
            TransitionComponent={Transition}
            onClose={ev => handleClose()}
            open={parseInt(notes.noteDialogId) >= 0}
        >
            <NoteForm
                {...props}
                note={notes.entities[notes.noteDialogId]}
                onClose={ev => handleClose()}
                onRemove={handleOnRemove}
            />
        </Dialog>
    );
}

export default NoteDialog;
