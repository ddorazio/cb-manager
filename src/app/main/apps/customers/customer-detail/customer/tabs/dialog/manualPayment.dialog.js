import React, { useRef, useState, useEffect } from 'react';
import { Button, ButtonGroup, ClickAwayListener, Dialog, DialogActions, DialogContent, DialogTitle, Grow, Icon, InputAdornment, MenuItem, MenuList, Paper, Popper, TextField } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../store/actions/loan.actions';
import { useTranslation } from 'react-i18next';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

export default function ManualPaymentDialog(props) {

    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const [fullWidth, setFullWidth] = React.useState(true);
    //const [isFormValid, setIsFormValid] = useState(false);

    const { loan, form } = props;
    const dispatch = useDispatch();

    const [payment, setPayment] = useState({
        loading: false,
        amount: null,
        effectiveDate: null
    });

    useEffect(() => {

        if (open) {
            resetForm();
        }

        onClose(initialState.actionStatus);
    }, [initialState]);

    function handleEffectiveDateChange(date) {
        setPayment({
            ...payment,
            effectiveDate: date
        });
    };

    function handleChangePaymentAmount(event) {
        setPayment({
            ...payment,
            amount: parseFloat(event.target.value) || 0
        });
    };

    // const [manualPaymentDate, setSelectedDate] = React.useState(new Date());
    // const handleDateChange = (date) => {
    //     setSelectedDate(date);
    // };

    function resetForm() {
        setPayment({
            ...payment,
            loading: false,
            amount: null,
            effectiveDate: null
        });
    }

    // Validate form submit
    function canBeSubmitted() {

        if (!payment.loading) {
            return payment.amount
                && payment.effectiveDate;
        }
    }

    const { onClose, open } = props;
    const handleClose = () => {
        onClose(false);
        resetForm();
    };

    function addManualPayment() {

        // Manual payments are not allowed after the last payment

        //manualPaymentNotAllowed();

        setPayment({
            ...payment,
            loading: true
        });

        dispatch(Actions.createPayment(loan, payment));
    }

    const setDateMin = () => {
        var today = new Date();
        var selectedPayments = loan._original.loanPayments.filter(x => new Date(x.paymentDate) <= today);
        //var lastPaymentDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);

        // if (selectedPayments.length != 0) {
        //     if (new Date(lastPaymentDate >= selectedPayments[0].paymentDate)) {
        //         return new Date(today.getFullYear(), today.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);
        //     }
        // }

        if (selectedPayments.length != 0) {
            var paymentDate = new Date(selectedPayments[selectedPayments.length - 1].paymentDate + 'T00:00:00');
            return new Date(paymentDate.getFullYear(), paymentDate.getMonth(), paymentDate.getDate(), 0, 0, 0);
        }

        return today;
    }

    // const manualPaymentNotAllowed = () => {

    //     dispatch(openDialog(
    //         {
    //             children: (
    //                 <React.Fragment>
    //                     <DialogTitle id="alert-dialog-title">{t("DELETE_LOAN_STATUS_CONFIRM_HEADER")}</DialogTitle>
    //                     <DialogContent>
    //                         <DialogContentText id="alert-dialog-description">
    //                             {t("DELETE_LOAN_STATUS_CONFIRM_MESSAGE")}
    //                         </DialogContentText>
    //                     </DialogContent>
    //                     <DialogActions>
    //                         <Button
    //                             className="whitespace-no-wrap normal-case"
    //                             variant="contained"
    //                             color="primary"
    //                             onClick={() => dispatch(closeDialog())}>
    //                             {t("DELETE_LOAN_STATUS_CONFIRM_CANCEL")}
    //                         </Button>
    //                     </DialogActions>
    //                 </React.Fragment >
    //             )
    //         })
    //     );
    // }

    return (
        <React.Fragment>
            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("MANUAL_PAYMENT_DIALOG_TITLE")}</DialogTitle>
                <DialogContent>
                    <TextField
                        className="mt-8 mb-16 bg-white"
                        id="manual_payment"
                        name="manual_payment"
                        label={t("MANUAL_PAYMENT_AMOUNT")}
                        type="number"
                        variant="outlined"
                        fullWidth
                        color="primary"
                        value={payment.amount}
                        onChange={handleChangePaymentAmount}
                        InputProps={{
                            endAdornment: <InputAdornment>$</InputAdornment>,
                        }}
                    />
                    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                        <KeyboardDatePicker
                            className="mt-8 mb-16 w-full"
                            minDate={setDateMin()}
                            //disableToolbar
                            //minDate={new Date()}
                            format="YYYY-MM-DD"
                            inputVariant="outlined"
                            id="manualPaymentDate"
                            name="manualPaymentDate"
                            autoOk
                            variant="inline"
                            inputVariant="outlined"
                            InputAdornmentProps={{ position: "start" }}
                            label={t("MANUAL_PAYMENT_PAYMENT_DATE")}
                            value={payment.effectiveDate}
                            onChange={handleEffectiveDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("MANUAL_PAYMENT_CANCEL_BUTTON")}
                    </Button>
                    <Button
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        disabled={!canBeSubmitted()}
                        onClick={() => addManualPayment()}
                    >
                        {payment.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("MANUAL_PAYMENT_SAVE_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
