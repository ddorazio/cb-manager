import {combineReducers} from 'redux';
import notes from './notes.reducer';
import labels from './labels.reducer';
import collectionNotes from './collectionNotes.reducer'

const reducer = combineReducers({
    notes,
    labels,
    collectionNotes
});

export default reducer;
