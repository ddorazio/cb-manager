import React, { useRef, useState, useEffect } from 'react';
import { Button, ButtonGroup, ClickAwayListener, Dialog, DialogActions, DialogContent, DialogTitle, Grow, Icon, InputAdornment, MenuItem, MenuList, Paper, Popper, TextField } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../../store/actions/loan.actions';
import { useTranslation } from 'react-i18next';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())


export default function StoppedLoanPaymentDialog(props) {

    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const [fullWidth, setFullWidth] = React.useState(true);
    //const [isFormValid, setIsFormValid] = useState(false);

    const { loan, form } = props;
    const dispatch = useDispatch();

    const [payment, setpayment] = useState({
        loading: false,
        amount: null,
        paymentDate: null
    });

    function handlepaymentDateChange(date) {
        setpayment({
            ...payment,
            paymentDate: date
        });
    };

    function handleChangepaymentAmount(event) {
        setpayment({
            ...payment,
            amount: parseFloat(event.target.value) || 0
        });
    };

    function resetForm() {
        setpayment({
            ...payment,
            loading: false,
            amount: null,
            paymentDate: null,
        });
    }

    // Validate form submit
    function canBeSubmitted() {

        if (!payment.loading) {
            return payment.amount
                && payment.paymentDate;
        }
    }

    const { onClose, open } = props;
    const handleClose = () => {
        onClose(false);
        resetForm();
    };

    useEffect(() => {

        if (open) {
            resetForm();
        }

        onClose(initialState.actionStatus);
    }, [initialState]);

    function addLoanpayment() {

        setpayment({
            ...payment,
            loading: true
        });

        dispatch(Actions.addPayment(loan, payment));
        //resetForm();
        //onClose(false);
    }

    const setDateMin = () => {
        // var today = new Date();
        // var selectedPayments = loan._original.loanPayments.filter(x => new Date(x.paymentDate) <= today);


        //if (selectedPayments.length != 0) {
        var paymentDate = new Date(loan._original.loanPayments[loan._original.loanPayments.length - 1].paymentDate + 'T00:00:00');
        return new Date(paymentDate.getFullYear(), paymentDate.getMonth(), paymentDate.getDate(), 0, 0, 0);
        //}

        //return today;
    }

    return (
        <React.Fragment>
            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("STOPPED_LOAN_PAYMENT_TITLE")}</DialogTitle>
                <DialogContent>
                    <TextField
                        className="mt-8 mb-16 bg-white"
                        id="manual_payment"
                        name="manual_payment"
                        label={t("STOPPED_LOAN_PAYMENT_AMOUNT")}
                        type="number"
                        variant="outlined"
                        color="primary"
                        onChange={handleChangepaymentAmount}
                        fullWidth
                        defaultValue={payment.amount}
                        value={payment.amount}
                        InputProps={{
                            endAdornment: <InputAdornment>$</InputAdornment>,
                        }}
                    />
                    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                        <KeyboardDatePicker
                            className="mt-8 mb-16 w-full"
                            minDate={setDateMin()}
                            //disableToolbar
                            //minDate={new Date()}
                            format="YYYY-MM-DD"
                            inputVariant="outlined"
                            id="newpaymentDate"
                            name="newpaymentDate"
                            autoOk
                            variant="inline"
                            inputVariant="outlined"
                            InputAdornmentProps={{ position: "start" }}
                            label={t("STOPPED_LOAN_PAYMENT_DATE")}
                            value={payment.paymentDate}
                            onChange={handlepaymentDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("STOPPED_LOAN_PAYMENT_CANCEL_BUTTON")}
                    </Button>
                    <Button
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        disabled={!canBeSubmitted()}
                        onClick={() => addLoanpayment()}
                    >
                        {payment.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("STOPPED_LOAN_PAYMENT_SAVE_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
