import React, { useRef, useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Button, ButtonGroup, Checkbox, ClickAwayListener, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormControlLabel, InputAdornment, Select, Switch, TextField, Typography } from '@material-ui/core';
import { FuseAnimateGroup } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
//import { useEffect } from 'react';
import * as Actions from '../../../store/actions';
import { useTranslation } from 'react-i18next';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

export default function RestartLoanDialog(props) {

    const { t } = useTranslation('customersApp');
    const initialState = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const { form, loan } = props;
    const [fullWidth, setFullWidth] = React.useState(true);

    const originalLoanStopDate = new Date(loan._original.stopLoanDate);
    //const minRestartDateToday = originalLoanStopDate.setDate(originalLoanStopDate.getDate() + 2);

    const setDateMin = () => {
        var today = new Date();
        if (form.nextPayDate && form.nextPayDate > today) {
            return new Date(form.nextPayDate.getFullYear(), form.nextPayDate.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);
        } else {
            return new Date(today.getFullYear(), today.getMonth(), form.lastPaymentDay + 1, 0, 0, 0);
        }
    }

    const dispatch = useDispatch();
    const [state, setState] = useState({
        loading: false,
        disableDates: false,
        checkFrequency: true,
        checkPayment: true,
        checkStopPayment: true,
        frequencyId: loan._original.frequencyId,
        newLoanAmount: loan._original.originalPaymentAmount,
        restartPaymentDate: setDateMin(),
        startDay: "",
        endDay: "",
        paymentAgreement: false,
        perceptech: false,
        interac: false
    });

    useEffect(() => {

        if (open) {
            resetForm();
        }

        onClose(initialState.actionStatus);
    }, [initialState]);

    const handleChangeLoanFrequency = (event) => {

        if (event.target.value === "4") {
            setState({
                ...state,
                frequencyId: event.target.value,
                disableDates: true
            });
        } else {
            setState({
                ...state,
                frequencyId: event.target.value,
                disableDates: false
            });
        }
    }

    // const handleChangeLoanFrequencyDate = (event) => {
    //     setState({
    //         ...state,
    //         frequencyDate: event
    //     });
    // };

    const handleChangeLoanAmount = (event) => {
        setState({
            ...state,
            newLoanAmount: parseFloat(event.target.value) || 0
        });
    };

    const handleChangeLoanStopPaymentDate = (event) => {
        setState({
            ...state,
            restartPaymentDate: event
        });
    };

    const handleChangeStartDate = (event) => {
        setState({
            ...state,
            startDay: event.target.value
        });
    };

    const handleChangeEndDate = (event) => {
        setState({
            ...state,
            endDay: event.target.value
        });
    };

    const handlePaymentAgreement = (event) => {
        setState({
            ...state,
            paymentAgreement: event.target.checked,
        });
    };

    const handleChangePerceptech = (event) => {
        setState({
            ...state,
            perceptech: event.target.checked,
            interac: false
        });
    };

    const handleChangeInterac = (event) => {
        setState({
            ...state,
            interac: event.target.checked,
            perceptech: false
        });
    };

    function resetForm() {
        setState({
            ...state,
            loading: false,
            disableDates: false,
            // checkFrequency: false,
            // checkPayment: false,
            // checkStopPayment: false,
            frequencyId: loan._original.frequencyId,
            //newLoanAmount: loan.originalPaymentAmount,
            restartPaymentDate: setDateMin(),
            newLoanAmount: loan._original.originalPaymentAmount,
            startDay: "",
            endDay: "",
            paymentAgreement: false,
            perceptech: false,
            interac: false
        });
    }

    const { open, onClose } = props;
    const handleClose = () => {
        onClose(false);
        resetForm();
    };

    function confirmRestartLoan() {
        setState({
            ...state,
            loading: true
        });
        dispatch(Actions.restartLoan(loan, state));
        onClose(false);
        resetForm();
    }

    // Validate form submit
    function canBeSubmitted() {

        if (!state.loading) {
            if (state.paymentAgreement) {
                return (state.perceptech
                    || state.interac)
                    && state.restartPaymentDate
                    && state.newLoanAmount
                    && state.frequencyId;;

            } else {
                return state.restartPaymentDate
                    && state.newLoanAmount
                    && state.frequencyId;
            }
        }
    }

    return (
        <React.Fragment>
            <Dialog onClose={handleClose} open={open} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                <DialogTitle id="form-dialog-title">{t("RESTART_LOAN_DIALOG_TITLE")}</DialogTitle>
                <DialogContent>
                    <div className="grid grid-cols-3 gap-16">
                        <div className="col-span-1">
                            <Typography className="subtitle1 mt-16" color="textPrimary">{t("RESTART_LOAN_RESTART_PAYMENTS")}</Typography>
                        </div>
                        <div className="col-span-2">
                            {state.checkStopPayment &&
                                (
                                    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                                        <KeyboardDatePicker
                                            className="w-full"
                                            //disableToolbar
                                            minDate={setDateMin()}
                                            format="YYYY-MM-DD"
                                            inputVariant="outlined"
                                            id="restartPaymentDate"
                                            name="restartPaymentDate"
                                            autoOk
                                            variant="inline"
                                            inputVariant="outlined"
                                            InputAdornmentProps={{ position: "start" }}
                                            label={t("RESTART_LOAN_FROM_DATE")}
                                            value={state.restartPaymentDate}
                                            onChange={handleChangeLoanStopPaymentDate}
                                            KeyboardButtonProps={{
                                                'aria-label': 'change date',
                                            }}
                                        />
                                    </MuiPickersUtilsProvider>
                                )
                            }
                        </div>
                        <div className="col-span-1">
                            <Typography className="subtitle1 mt-16" color="textPrimary">{t("RESTART_LOAN_CHANGE_FREQUENCY")}</Typography>
                        </div>
                        <div className="col-span-2">
                            {state.checkFrequency &&
                                (
                                    <React.Fragment>
                                        <FormControl className="w-full" variant="outlined" >
                                            <Select
                                                fullWidth
                                                native
                                                value={state.frequencyId}
                                                label="Fréquence des paiements"
                                                inputProps={{
                                                    name: 'frequence',
                                                    id: 'frequence-label',
                                                }}
                                                onChange={handleChangeLoanFrequency}
                                            >
                                                <option aria-label="None" value="" />
                                                {form.loanFrequencies.map((frequency, index) => (
                                                    <option key={index} value={frequency.id}>{frequency.name}</option>
                                                ))}

                                            </Select>
                                        </FormControl>
                                    </React.Fragment>
                                )
                            }
                        </div>
                        {state.disableDates &&
                            (
                                <div className="col-start-2 col-span-2 mb-16">
                                    <div class="grid grid-cols-2 gap-16">
                                        <div class="col-span-1">
                                            <TextField
                                                className="w-full"
                                                id="montant_pret"
                                                name="montant_pret"
                                                label={t("RESTART_LOAN_DAY_ONE")}
                                                type="number"
                                                variant="outlined"
                                                fullwidth
                                                onChange={handleChangeStartDate}
                                                //onChange={e => handleChangeStartDate(e.target.value)}
                                                defaultValue={state.startDay}
                                                value={state.startDay}
                                            />
                                        </div>
                                        <div class="col-span-1">
                                            <TextField
                                                className="w-full"
                                                id="montant_pret"
                                                name="montant_pret"
                                                label={t("RESTART_LOAN_DAY_TWO")}
                                                type="number"
                                                variant="outlined"
                                                fullwidth
                                                onChange={handleChangeEndDate}
                                                defaultValue={state.endDay}
                                                value={state.endDay}
                                            />
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        <div className="col-span-1">
                            <Typography className="subtitle1 mt-16" color="textPrimary">{t("RESTART_LOAN_CHANGE_AMOUNT")}</Typography>
                        </div>
                        <div className="col-span-2">
                            {state.checkPayment &&
                                (
                                    <React.Fragment>
                                        <FormControl className="w-full" variant="outlined" >
                                            <TextField
                                                // className="mt-8 bg-white"
                                                id="montant_pret"
                                                name="montant_pret"
                                                label={t("RESTART_LOAN_NEW_AMOUNT")}
                                                type="number"
                                                variant="outlined"
                                                fullWidth
                                                color="primary"
                                                onChange={handleChangeLoanAmount}
                                                defaultValue={state.newLoanAmount}
                                                value={state.newLoanAmount}
                                                InputProps={{
                                                    endAdornment: <InputAdornment>$</InputAdornment>,
                                                }}
                                            />
                                        </FormControl>
                                    </React.Fragment>
                                )
                            }
                        </div>

                        <div class="col-span-2">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={state.paymentAgreement}
                                        onChange={handlePaymentAgreement}
                                        name="paymentAgreement"
                                        color="secondary"
                                    />
                                }
                                label={t("RESTART_LOAN_PAYMENT_AGREEMENT")}
                            />
                        </div>
                        <div class="col-span-1">
                            {state.paymentAgreement &&
                                (
                                    <React.Fragment>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={state.perceptech}
                                                    onChange={handleChangePerceptech}
                                                    name="perceptech"
                                                    color="secondary"
                                                />
                                            }
                                            label={t("RESTART_LOAN_PAYMENT_PERCEPTECH")}
                                        />
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={state.interac}
                                                    onChange={handleChangeInterac}
                                                    name="interac"
                                                    color="secondary"
                                                />
                                            }
                                            label={t("RESTART_LOAN_PAYMENT_INTERAC")}
                                        />
                                    </React.Fragment>
                                )
                            }
                        </div>

                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("RESTART_LOAN_CANCEL_BUTTON")}
                    </Button>
                    <Button
                        //type="submit"
                        variant="contained"
                        color="secondary"
                        aria-label="Enregistrer"
                        onClick={() => confirmRestartLoan()}
                        disabled={!canBeSubmitted()}
                    >
                        {state.loading &&
                            <i
                                className="fa fa-refresh fa-spin"
                                style={{ marginRight: "5px" }}
                            />
                        }
                        {t("RESTART_LOAN_SAVE_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
