import React, { useRef, useState } from 'react';

import { AppBar, Button, Dialog, DialogTitle, DialogContent, DialogActions, FormControlLabel, Grow, Grid, Icon, InputLabel, Checkbox, TextField, InputAdornment, IconButton, MenuItem, MenuList, Paper, Popper, Table, TableBody, TableContainer, TableHead, TableRow, TableCell, Toolbar, Typography } from '@material-ui/core';

import { useTranslation } from 'react-i18next';


export default function CreditCheckDetailsDialog(props) {

    const { t } = useTranslation('customersApp');
    const [fullWidth, setFullWidth] = React.useState(true);
    const { row } = props;

    const { open, onClose } = props;
    const handleClose = () => {
        onClose(false);
    };

    return (
        <React.Fragment>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullWidth={fullWidth} maxWidth='sm'>
                {/* <DialogTitle id="form-dialog-title">{t("MODIFY_LOAN_PAYMENT_DIALOG_TITLE")}</DialogTitle> */}
                <DialogContent>

                    <div className="flex mb-16">
                        <div className="flex-1 mr-16">
                            <TableContainer component={Paper}>
                                <Table className="w-full" size="small">
                                    <TableBody>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_CLIENT")}</TableCell>
                                            <TableCell align="right">{row.client}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_TEL")}</TableCell>
                                            <TableCell align="right">{row.phone}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_EMAIL")}</TableCell>
                                            <TableCell align="right">{row.email}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_DOB")}</TableCell>
                                            <TableCell align="right">{row.dateOfBirth}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_LANGUAGE")}</TableCell>
                                            <TableCell align="right">{row.language}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_REQUEST_DATE")}</TableCell>
                                            <TableCell align="right">{row.requestDate}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_LENDER")}</TableCell>
                                            <TableCell align="right">{row.lender}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_AMOUNT")}</TableCell>
                                            <TableCell align="right">{row.amount} $</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_COMMENT")}</TableCell>
                                            <TableCell align="right">{row.comments}</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    </div>

                    <div className="flex mb-16">
                        <div className="flex-1 mr-16">
                            <TableContainer component={Paper}>
                                <Table className="w-full" size="small">
                                    <TableBody>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_ADDRESS")}</TableCell>
                                            <TableCell align="right">{row.address.streetAddressLine1}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_CITY")}</TableCell>
                                            <TableCell align="right">{row.address.city}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_COUNTRY")}</TableCell>
                                            <TableCell align="right">{row.address.country}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_PROVINCE")}</TableCell>
                                            <TableCell align="right">{row.address.stateProvince}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_POSTAL_CODE")}</TableCell>
                                            <TableCell align="right">{row.address.zipPostalCode}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_SINCE")}</TableCell>
                                            <TableCell align="right">{row.address.effectiveDate}</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    </div>

                    <div className="flex mb-16">
                        <div className="flex-1 mr-16">
                            <TableContainer component={Paper}>
                                <Table className="w-full" size="small">
                                    <TableBody>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_EMPLOYER")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.companyName : "N/A"}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_WORK_TEL")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.companyPhone : "N/A"}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_SUPERVISOR")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.supervisorName : "N/A"}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_OCCUPATION")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.jobTitle : "N/A"}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_PAY_FREQ")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.payFrequenc : "N/A"}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_NEXT_PAY")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.payNext : "N/A"}</TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_START_DATE")}</TableCell>
                                            <TableCell align="right">{row.job ? row.job.startDate : "N/A"}</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </div>
                    </div>

                    {row.references.map((item, index) => (
                        <div className="flex mb-16">
                            <div className="flex-1 mr-16">
                                <TableContainer component={Paper}>
                                    <Table className="w-full" size="small">
                                        <TableBody>
                                            <TableRow>
                                                <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_REF_FNAME") + (index + 1)}</TableCell>
                                                <TableCell align="right">{item.firstName}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_REF_LNAME") + (index + 1)}</TableCell>
                                                <TableCell align="right">{item.lastName}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_REF_TEL") + (index + 1)}</TableCell>
                                                <TableCell align="right">{item.phone}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_REF_EMAIL") + (index + 1)}</TableCell>
                                                <TableCell align="right">{item.email}</TableCell>
                                            </TableRow>
                                            <TableRow>
                                                <TableCell component="th" className="font-bold">{t("CREDIT_CHECK_REF_RELATIONSHIP") + (index + 1)}</TableCell>
                                                <TableCell align="right">{item.relationship}</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        </div>
                    ))}

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        {t("CREDIT_CHECK_CLOSE_BUTTON")}
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );
}
