import React, { useEffect } from 'react';
import { AppBar, Button, ButtonGroup, Card, CardContent, ClickAwayListener, DialogTitle, DialogContent, DialogContentText, DialogActions, FormControlLabel, Grow, Grid, Icon, IconButton, MenuItem, MenuList, Paper, Popper, Table, TableBody, TableContainer, TableHead, TableRow, TableCell, Toolbar, Typography } from '@material-ui/core';
import ReactTable from "react-table";
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import EditLoanPaymentDialog from './dialog/editLoanPayment.dialog'
import DelayLoanPaymentDialog from './dialog/delayLoanPayment.dialog'
import EditLoanDialog from './dialog/editLoan.dialog'
import RestartLoanDialog from './dialog/restartLoan.dialog'
import SendContractDialog from './dialog/sendContract.dialog'
import NewRebateDialog from './dialog/newRebate.dialog'
import GeneratePDFDialog from './dialog/generatePDF.dialog'
import ManualPaymentDialog from './dialog/manualPayment.dialog'
import StoppedLoanPaymentDialog from './dialog/stoppedLoanPayment.dialog'
import * as Actions from '../../store/actions';
import { useTranslation } from 'react-i18next';
import { showMessage, openDialog, closeDialog } from 'app/store/actions/fuse';
//import LoanAmounts from 'app/main/apps/configuration/loan/LoanAmounts';

const options = [
    'Envoyer le contrat',
    'Modifier le prêt',
    'Nouveau paiement manuel',
    'Nouveau rabais'
];

function PretsDetails(props) {

    const { t } = useTranslation('customersApp');
    const { loan, form } = props;
    const dispatch = useDispatch();

    const state = useSelector(({ CustomerComp }) => CustomerComp.customer);
    const loanConfiguration = useSelector(({ CustomerComp }) => CustomerComp.customer.data.loanConfiguration);
    //const [open, setOpen] = React.useState(false);
    //const anchorRef = React.useRef(null);
    const [selectedIndex, setSelectedIndex] = React.useState(1);

    useEffect(() => {
        setOpenNewRebateDialog(state.actionStatus);
    }, [state.actionStatus]);

    const handleMenuItemClick = (event, index, loan) => {
        //setOpen(false);
        setSelectedIndex(index);

        switch (index) {
            case 0: // Send Contract
                setOpenContractDialog(true);
                break;
            case 1: // Modify loan
                setOpenEditLoanDialog(true);
                break;
            case 2: // Manual payment
                setOpenManualPaymentDialog(true);
                break;
            case 3: // Rebate
                setOpenNewRebateDialog(true);
                break;
            case 4: // Delete Loan
                deleteLoanConfirmation(loan);
                break;
            case 5: // Restart loan
                setOpenEditLoanDialog(true);
                break;
            case 6: // 
                dispatch(Actions.openPDFGeneratorDialog())
                break;
            case 7: // Defer payment
                deferPaymentConfirmation(loan);
            case 8: // Add payment to Stopped Loan
                setOpenStoppedLoanPaymentDialog(true);
        }
    };

    const [openEditLoanDialog, setOpenEditLoanDialog] = React.useState(false);
    const handleEditLoanCloseDialog = (value) => {
        setOpenEditLoanDialog(value);
    };

    const [openContractDialog, setOpenContractDialog] = React.useState(false);
    const handleContractCloseDialog = (value) => {
        setOpenContractDialog(value);
    };

    const [openManualPaymentDialog, setOpenManualPaymentDialog] = React.useState(false);
    const handleManualPaymentCloseDialog = (value) => {
        setOpenManualPaymentDialog(value);
    };

    const [openNewRebateDialog, setOpenNewRebateDialog] = React.useState(false);
    const handleNewRebateCloseDialog = (value) => {
        setOpenNewRebateDialog(value);
    };

    const [openStoppedLoanPaymentDialog, setOpenStoppedLoanPaymentDialog] = React.useState(false);
    const handleStoppedLoanPaymentCloseDialog = (value) => {
        setOpenStoppedLoanPaymentDialog(value);
    };

    // const handleToggle = (event, index) => {
    //     setOpen((prevOpen) => !prevOpen);
    // };
    // const handleClose = (event) => {
    //     if (anchorRef.current && anchorRef.current.contains(event.target)) {
    //         return;
    //     }
    //     setOpen(false);
    // };

    function applyNsf(payment) {
        dispatch(Actions.createNSFPayment(loan, payment));
    }

    const nsfConfirmation = (payment) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("NSF_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("NSF_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("NSF_CONFIRM_CANCEL")}
                            </Button>
                            <Button onClick={() => applyNsf(payment)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("NSF_CONFIRM_CONFIRM")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    function removeNsf(payment) {
        dispatch(Actions.deleteNSFPayment(loan, payment));
    }

    const nsfRemoveConfirmation = (payment) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("NSF_REMOVE_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("NSF_REMOVE_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("NSF_REMOVE_CONFIRM_CANCEL")}
                            </Button>
                            <Button onClick={() => removeNsf(payment)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("NSF_REMOVE_CONFIRM_CONFIRM")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    function removeRebateAndManualPayment(payment) {
        dispatch(Actions.deleteRebate(loan, payment));
    }

    const removeRebateConfirmation = (payment) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("REBATE_REMOVE_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("REBATE_REMOVE_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("REBATE_REMOVE_CONFIRM_CANCEL")}
                            </Button>
                            <Button onClick={() => removeRebateAndManualPayment(payment)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("REBATE_REMOVE_CONFIRM_CONFIRM")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    const removeManualPaymentConfirmation = (payment) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("PAYMENT_REMOVE_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("PAYMENT_REMOVE_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("PAYMENT_REMOVE_CONFIRM_CANCEL")}
                            </Button>
                            <Button onClick={() => removeRebateAndManualPayment(payment)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("PAYMENT_REMOVE_CONFIRM_CONFIRM")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    function deleteLoan(loan) {
        dispatch(Actions.deleteLoan(loan));
    }

    const deleteLoanConfirmation = (loan) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("DELETE_LOAN_STATUS_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("DELETE_LOAN_STATUS_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("DELETE_LOAN_STATUS_CONFIRM_CANCEL")}
                            </Button>
                            <Button onClick={() => deleteLoan(loan)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("DELETE_LOAN_STATUS_CONFIRM_CONFIRM")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    function deferPayment(loan, row) {
        dispatch(Actions.deferPayment(loan, row));
    }

    const deferPaymentConfirmation = (loan, row) => {

        dispatch(openDialog(
            {
                children: (
                    <React.Fragment>
                        <DialogTitle id="alert-dialog-title">{t("DEFER_LOAN_PAYMENT_CONFIRM_HEADER")}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {t("DEFER_LOAN_PAYMENT_CONFIRM_MESSAGE")}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(closeDialog())}>
                                {t("DEFER_LOAN_PAYMENT_CANCEL_BUTTON")}
                            </Button>
                            <Button onClick={() => deferPayment(loan, row)}
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                autoFocus>
                                {t("DEFER_LOAN_PAYMENT_CONFIRM_BUTTON")}
                            </Button>
                        </DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    return (
        <Card className="px-16 my-16">
            <AppBar position="static" elevation={0}>
                <Toolbar className="pl-16 pr-8" style={{
                    backgroundColor: loan._original.statusCode === "IP" || loan._original.statusCode === "WFD" || loan._original.statusCode === "LR" ? "#8bc34a"
                        : loan._original.statusCode === "LS" || loan._original.statusCode === "PA" || loan._original.statusCode === "LD" ? "#f44336"
                            : "#ffa500"
                }}>
                    <Typography variant="subtitle2" color="inherit" className="flex-1">{t("LOAN_SUMMARY_HEADER")} #{loan._original.loanNumber.toString().padStart(6, '0')}</Typography>

                </Toolbar>
            </AppBar>
            <CardContent className="mb-16 pr-8" style={{ borderWidth: "1px", borderColor: loan._original.statusCode === "IP" ? "#8bc34a" : loan._original.statusCode === "LS" ? "#f44336" : "" }}>
                <div className="flex mb-16">
                    <div className="flex-1 mr-16">
                        <TableContainer component={Paper}>
                            <Table className="w-full" size="small">
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_AMOUNT")}</TableCell>
                                        <TableCell align="right">{loan.amount.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_INTEREST")} ({loan._original.interestRate} %) :</TableCell>
                                        <TableCell align="right">{loan._original.interestAmount.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_BROKERAGE")} :</TableCell>
                                        <TableCell align="right">{loan._original.brokerageFee.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_PAYMENT")} :</TableCell>
                                        <TableCell align="right">{loan._original.originalPaymentAmount.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_FREQUENCY")} :</TableCell>
                                        <TableCell align="right">{loan.frequency}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_CREATED_DATE")} :</TableCell>
                                        <TableCell align="right">{loan._original.createdDate}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_INITIAL_NUMBER_PAYMENTS")} :</TableCell>
                                        <TableCell align="right">{loan._original.initialNumberOfPayments != null ? loan._original.initialNumberOfPayments : "N/A"}</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                    <div className="flex-1">
                        <TableContainer component={Paper}>
                            <Table className="w-full" size="small">
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_NO_NSF")} :</TableCell>
                                        <TableCell align="right" className="font-bold">{loan._original.defaultPayments}</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_FEES")} :</TableCell>
                                        <TableCell align="right">{loan._original.fees.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_DAILY_BALANCE")} :</TableCell>
                                        <TableCell align="right">{loan._original.currentDailyBalance.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold text-red-400">{t("LOAN_SUMMARY_REBATE")} :</TableCell>
                                        <TableCell align="right" className="text-red-400">{loan._original.rebates.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_TOTAL")} :</TableCell>
                                        <TableCell align="right">{loan._original.totalAmountRemaining.toFixed(2)} $</TableCell>
                                    </TableRow>
                                    {/* <TableRow>
                                        <TableCell component="th" className="font-bold">{t("LOAN_SUMMARY_AGENT")} :</TableCell>
                                        <TableCell align="right">N/A</TableCell>
                                    </TableRow> */}
                                    {loan._original.contractSigned && loan._original.contractSignedDate != "" &&
                                        <TableRow>
                                            <TableCell component="th" className="font-bold">{t("LOAN_CONTRATC_SIGNED")} :</TableCell>
                                            <TableCell align="right">
                                                <IconButton
                                                    size="medium"
                                                    className="pt-0 pb-0"
                                                    onClick={() => window.open(loan._original.digitalUrl)}>
                                                    <Icon>file_copy</Icon>
                                                </IconButton>
                                                {loan._original.contractSignedDate}
                                            </TableCell>
                                        </TableRow>
                                    }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
                {/* <Typography variant="subtitle1" color="inherit" className="mb-8 text-center">Détails des Paiements :</Typography> */}
                <div className="mb-8">
                    {loan._original.isLoanStopped
                        ?
                        <RestartLoanDialog open={openEditLoanDialog} onClose={handleEditLoanCloseDialog} form={form} loan={loan}></RestartLoanDialog>
                        :
                        <EditLoanDialog open={openEditLoanDialog} onClose={handleEditLoanCloseDialog} form={form} loan={loan}></EditLoanDialog>
                    }
                    <SendContractDialog open={openContractDialog} onClose={handleContractCloseDialog} loan={loan}></SendContractDialog>
                    <ManualPaymentDialog open={openManualPaymentDialog} onClose={handleManualPaymentCloseDialog} loan={loan} form={form}></ManualPaymentDialog>
                    <NewRebateDialog open={openNewRebateDialog} onClose={handleNewRebateCloseDialog} loan={loan} form={form}></NewRebateDialog>
                    <StoppedLoanPaymentDialog open={openStoppedLoanPaymentDialog} onClose={handleStoppedLoanPaymentCloseDialog} loan={loan} form={form}></StoppedLoanPaymentDialog>
                    <GeneratePDFDialog loanId={loan._original.id}></GeneratePDFDialog>

                    {loan._original.paymentProvider == true && loan._original.beforeProvider == true
                        ?
                        < div className="mb-8"><span style={{ color: "red" }}>{t("BEFORE_PROVIDER")}</span></div>
                        :
                        null
                    }

                    <div className="mb-8">{loan._original.originalSubFeesPrice != null ? null
                        : loan._original.chargeMembershipFee == true ? <span style={{ color: "red" }}>{t("BEFORE_MEMBERSHIP")}</span> : null}</div>

                    <ButtonGroup color="secondary" size="medium">
                        {(loan._original.statusCode === "IP" || loan._original.statusCode === "PA" || loan._original.statusCode === "LD") &&
                            <Button variant="contained" onClick={(event) => { handleMenuItemClick(event, 1) }}>
                                {t("MODIFY_LOAN")}
                            </Button>
                        }
                        {loan._original.statusCode === "LS" &&

                            <Button variant="contained" onClick={(event) => { handleMenuItemClick(event, 5) }}>
                                {t("MODIFY_LOAN")}
                            </Button>
                        }
                        {(loan._original.statusCode === "LS" && loanConfiguration.loanStoppedPaymentAllowed) &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 8) }}>
                                {t("STOPPED_LOAN_PAYMENT")}
                            </Button>
                        }
                        {loan._original.statusCode !== "LS" && loan._original.statusCode !== "LR" &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 0) }}>
                                {t("SEND_CONTRACT")}
                            </Button>
                        }
                        {(loan._original.statusCode === "IP" || loan._original.statusCode === "PA" || loan._original.statusCode === "LD") &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 2) }}>
                                {t("MANUAL_PAYMENT")}
                            </Button>}
                        {(loan._original.statusCode === "IP" || loan._original.statusCode === "PA" || loan._original.statusCode === "LD") &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 3) }}>
                                {t("NEW_REBATE")}
                            </Button>
                        }
                        {loan._original.canDelete && loan._original.statusCode !== "LR" &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 4, loan) }}>
                                {t("DELETE_LOAN")}
                            </Button>
                        }
                        {(loan._original.statusCode === "IP" || loan._original.statusCode === "PA" || loan._original.statusCode === "LR" || loan._original.statusCode === "LD") &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 6) }}>
                                {t("ACCOUNT_STATEMENT_PDF")}
                            </Button>
                        }

                        {/* {loan._original.statusCode === "PEN" &&
                            <Button onClick={(event) => { handleMenuItemClick(event, 4) }}>
                                {t("START_LOAN")}
                            </Button>
                        } */}

                    </ButtonGroup>

                </div>
                <div className="pb-16 mt-16">
                    <ReactTable
                        data={loan._original.loanPayments}
                        columns={[
                            {
                                Header: t("LOAN_DETAIL_NO"),
                                width: 100,
                                sortable: false,
                                resizable: false,
                                accessor: 'paymentNo',
                                Cell: row => (
                                    <div><strong>{row.value.toString().padStart(7, '0')}</strong></div>
                                )
                            },
                            {
                                Header: t("LOAN_DETAIL_DATE"),
                                width: 110,
                                resizable: false,
                                accessor: 'paymentDate',
                            },
                            {
                                Header: t("LOAN_DETAIL_AMOUNT"),
                                width: 100,
                                accessor: 'amount',
                                sortable: false,
                                resizable: false,
                                Cell: row => (
                                    <div><strong>{row.value.toFixed(2)}</strong> $</div>
                                )
                            },
                            {
                                Header: t("LOAN_DETAIL_INTEREST"),
                                width: 100,
                                accessor: 'interest',
                                sortable: false,
                                resizable: false,
                                Cell: row => (
                                    <div><strong>{row.value.toFixed(2)}</strong> $</div>
                                )
                            },
                            {
                                Header: t("LOAN_DETAIL_CAPITAL"),
                                width: 100,
                                accessor: 'capital',
                                sortable: false,
                                resizable: false,
                                Cell: row => (
                                    <div><strong>{row.value.toFixed(2)}</strong> $</div>
                                )
                            },
                            {
                                Header: t("LOAN_DETAIL_BALANCE"),
                                width: 100,
                                accessor: 'balance',
                                sortable: false,
                                resizable: false,
                                Cell: row => (
                                    <div><strong>{row.value.toFixed(2)}</strong> $</div>
                                )
                            },
                            {
                                Header: t("LOAN_SUB_FEES"),
                                width: 100,
                                accessor: 'subFeesPayment',
                                sortable: false,
                                resizable: false,
                                show: (loan._original.originalSubFeesPrice != null && loan._original.chargeMembershipFee == true) ? true : false,
                                Cell: row => (
                                    <div><strong>{(row.value == null) ? "" : row.value.toFixed(2)}</strong>{(row.value == null) ? "" : "$"}</div>
                                )
                            },
                            {
                                Header: t("LOAN_DETAIL_STATUS"),
                                resizable: true,
                                accessor: 'status',
                                Cell: row => row.original.statusCode === "PEN" || row.original.statusCode === "PENI"
                                    ? <div className="w-full inline-block text-12 rounded-full py-4 px-8 truncate text-white text-center"
                                        style={{ backgroundColor: "#8bc34a" }}
                                    >{row.original.status}</div>
                                    : row.original.statusCode === "NSF" || row.original.statusCode === "SP"
                                        ? <div className="w-full inline-block text-12 rounded-full py-4 px-8 truncate text-white text-center"
                                            style={{ backgroundColor: "#f44336" }}
                                        >{row.original.status}</div>
                                        :
                                        <div className="w-full inline-block text-12 rounded-full py-4 px-8 truncate text-white text-center"
                                            style={{ backgroundColor: "#ffb300" }}
                                        >{row.original.status}</div>
                            },
                            {
                                Header: t("LOAN_DETAIL_ACTIONS"),
                                sortable: false,
                                width: 200,
                                resizable: false,
                                className: "py-8",
                                Cell: row =>
                                    (loan._original.statusCode === "IP" || loan._original.statusCode === "PA" || loan._original.statusCode === "LD") &&
                                    (
                                        (row.original.statusCode === "NSF" && row.original.canNSF)
                                            ?
                                            (
                                                <div className="items-center">
                                                    {/* {row.original.canEdit &&
                                                        (<EditLoanPaymentDialog loan={loan} row={row} form={form} />)
                                                    } */}

                                                    <Button
                                                        variant="outlined"
                                                        onClick={(ev) => {
                                                            ev.stopPropagation();
                                                            nsfRemoveConfirmation(row.original)
                                                        }}
                                                    //onClick={nsfConfirmation(row)}
                                                    > - NSF</Button>
                                                    {/* <IconButton><Icon>edit</Icon></IconButton> */}
                                                </div>
                                            )
                                            :
                                            (row.original.statusCode === "REB"
                                                ?
                                                (
                                                    <div className="items-center">
                                                        {row.original.canEdit &&
                                                            (<EditLoanPaymentDialog loan={loan} row={row} form={form} />)
                                                        }
                                                        <Button
                                                            variant="outlined"
                                                            onClick={(ev) => {
                                                                ev.stopPropagation();
                                                                removeRebateConfirmation(row.original)
                                                            }}
                                                        //onClick={nsfConfirmation(row)}
                                                        > - {t("LOAN_DETAIL_REBATE_BUTTON")}</Button>
                                                        {/* <IconButton><Icon>edit</Icon></IconButton> */}
                                                    </div>
                                                )
                                                :
                                                (row.original.statusCode === "MP"
                                                    ?
                                                    (
                                                        <div className="items-center">
                                                            {row.original.canEdit &&
                                                                (<EditLoanPaymentDialog loan={loan} row={row} form={form} />)
                                                            }
                                                            <Button
                                                                variant="outlined"
                                                                onClick={(ev) => {
                                                                    ev.stopPropagation();
                                                                    removeManualPaymentConfirmation(row.original)
                                                                }}
                                                            //onClick={nsfConfirmation(row)}
                                                            > - {t("LOAN_DETAIL_PAYMENT_BUTTON")}</Button>
                                                            {/* <IconButton><Icon>edit</Icon></IconButton> */}
                                                        </div>
                                                    )
                                                    :
                                                    (
                                                        <div className="items-center">
                                                            {row.original.canEdit &&
                                                                (<EditLoanPaymentDialog loan={loan} row={row} form={form} />)
                                                            }
                                                            {row.original.canEdit &&
                                                                <IconButton
                                                                    onClick={(ev) => {
                                                                        ev.stopPropagation();
                                                                        deferPaymentConfirmation(loan, row.original)
                                                                    }}>
                                                                    <Icon>calendar_today</Icon>
                                                                </IconButton>
                                                            }
                                                            {row.original.canNSF &&
                                                                <Button
                                                                    variant="outlined"
                                                                    onClick={(ev) => {
                                                                        ev.stopPropagation();
                                                                        nsfConfirmation(row.original)
                                                                    }}
                                                                > + NSF</Button>
                                                            }
                                                            {/* <IconButton><Icon>edit</Icon></IconButton> */}
                                                        </div>
                                                    )
                                                )
                                            )
                                    )
                            },
                            {
                                Header: t("LOAN_DETAIL_DESCRIPTION"),
                                accessor: 'description',
                                sortable: true,
                                resizable: true
                            }
                        ]}
                        className="white w-full min-w-900"
                        showPagination={false}
                        minRows={6}
                        defaultPageSize={200}
                        noDataText={"Aucun paiement trouvé."}
                        pageText={t("REACT_TABLE_PAGE_TEXT")}
                        ofText={t("REACT_TABLE_OF_TEXT")}
                        rowsText={t("REACT_TABLE_ROWS_TEXT")}
                    />
                </div>
            </CardContent>
        </Card >
    );
}

export default PretsDetails;
