import React, { useEffect, useState } from 'react';
import { AppBar, Button, Card, CardContent, Tab, Tabs, TextField, Icon, Typography, Toolbar } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/styles';
import { FuseAnimate, FuseAnimateGroup, FusePageCarded, FuseLoading } from '@fuse';
import { useForm } from '@fuse/hooks';
import clsx from 'clsx';
import _ from '@lodash';
import MaterialTable from 'material-table';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions/place.actions';
import reducer from '../store/reducers';
import { Translate } from "react-localize-redux";
import { withRouter } from 'react-router-dom';
import AccreditedPersonDialog from './AccreditedPersonDialog';

const useStyles = makeStyles(theme => ({
    placeImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    placetImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    placeImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $placeImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $placeImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $placeImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));


function Place(props) {

    const dispatch = useDispatch();
    const place = useSelector(({ placeApp }) => placeApp.place);
    const classes = useStyles(props);
    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);
       
    //********************************Accredited persons Data*********************************/

    const [accredited_state, setAccreditedState] = React.useState({
        columns: [
            { title: '', field: 'avatar' },
            { title: <Translate id="places.accredited_persons_title"></Translate>, field: 'title' },
            { title: <Translate id="global.first_name"></Translate>, field: 'firstname' },
            { title: <Translate id="global.last_name"></Translate>, field: 'lastname' },
            { title: <Translate id="places.accredited_persons_car"></Translate>, field: 'vehicle' },
            { title: <Translate id="global.note"></Translate>, field: 'note' },
            { title: <Translate id="places.accredited_persons_pass"></Translate>, field: 'privilegesList' }
        ],
        data: []
    });

    //update accredited table and catch new accredited ID
    useEffect(() => {

        const saveAccredited = (item) => {

            const data = [...accredited_state.data];

            var index = data.indexOf(item.oldData);

            if (typeof index !== undefined && index != null && index >= 0) {

                data[index] = item.newData;
                setAccreditedState({ ...accredited_state, data });
                setForm(_.set({ ...form }, `accreditedPersons`, data));
            } else {

            data.push(item.newData);
                setAccreditedState({ ...accredited_state, data });
                setForm(_.set({ ...form }, `accreditedPersons`, data));
            }
        };

        if ((place.actionType === "SAVE_ACCREDITED_PERSON" && place.actionData && place.actionStatus === 1) || (place.actionType === "SAVE_ACCREDITED_PERSON" && place.actionData && !place.id)) {
            saveAccredited(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }

    }, [form, setForm, accredited_state, place]);

    //delete accredited person from table view
    useEffect(() => {

        const deleteAccredited = (oldData) => {
            const data = [...accredited_state.data];
            data.splice(data.indexOf(oldData), 1);
            setAccreditedState({ ...accredited_state, data });
            setForm(_.set({ ...form }, `accreditedPersons`, data));
        };

        if ((place.actionType === "DELETE_ACCREDITED_PERSON" && place.actionData && place.actionStatus === 1) || (place.actionType === "DELETE_ACCREDITED_PERSON" && place.actionData && !place.id)) {
            deleteAccredited(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }
    }, [form, setForm, accredited_state, place]);
    //********************************Accredited persons Data*********************************/


    //********************************Locations Data*********************************/
    const [location_state, setLocationState] = React.useState({

        columns: [
            { title: 'Emplacements'/*<Translate id="places.locations"></Translate>*/, field: 'name' }
        ],
        data: []
    });

    //update location table and catch new location ID
    useEffect(() => {

        const saveLocation = (item) => {

            const data = [...location_state.data];
            var index = data.indexOf(item.oldData);

            if (typeof index !== undefined && index != null && index >= 0) {

                data[index] = item.newData;
                setLocationState({ ...location_state, data });
                setForm(_.set({ ...form }, `locations`, data));
            } else {

                data.push(item.newData);
                setLocationState({ ...location_state, data });
                setForm(_.set({ ...form }, `locations`, data));
            }
        };

        if ((place.actionType === "SAVE_LOCATION" && place.actionData && place.actionStatus === 1) || (place.actionType === "SAVE_LOCATION" && place.actionData && !place.data.id)) {
            saveLocation(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }
    }, [form, setForm, location_state, place]);

    //delete location from table view
    useEffect(() => {

        const deleteLocation = (oldData) => {
            const data = [...location_state.data];
            data.splice(data.indexOf(oldData), 1);
            setLocationState({ ...location_state, data });
            setForm(_.set({ ...form }, `locations`, data));
        };

        if ((place.actionType === "DELETE_LOCATION" && place.actionData && place.actionStatus === 1) || (place.actionType === "DELETE_LOCATION" && place.actionData && !place.data.id)) {
            deleteLocation(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }
    }, [form, setForm, location_state, place]);

    //********************************Locations Data*********************************/

    //********************************Taks Data*********************************/
    const [task_state, setTaskState] = React.useState({
        columns: [
            { title: 'Taches'/*<Translate id="places.tasks"></Translate>*/, field: 'name' }
        ],
        data: []
    });

    //update task table and catch new task ID
    useEffect(() => {

        const saveTask = (item) => {

            const data = [...task_state.data];
            var index = data.indexOf(item.oldData);

            if (typeof index !== undefined && index != null && index >= 0) {

                data[index] = item.newData;
                setTaskState({ ...task_state, data });
                setForm(_.set({ ...form }, `tasks`, data));
            } else {

                data.push(item.newData);
                setTaskState({ ...task_state, data });
                setForm(_.set({ ...form }, `tasks`, data));
            }
        };

        if ((place.actionType === "SAVE_TASK" && place.actionData && place.actionStatus === 1) || (place.actionType === "SAVE_TASK" && place.actionData && !place.data.id)) {
            saveTask(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }
    }, [form, setForm, task_state, place]);

    //delete task from the table view
    useEffect(() => {

        const deleteTask = (oldData) => {
            const data = [...task_state.data];
            data.splice(data.indexOf(oldData), 1);
            setTaskState({ ...task_state, data });
            setForm(_.set({ ...form }, `tasks`, data));
        };

        if ((place.actionType === "DELETE_TASK" && place.actionData && place.actionStatus === 1) || (place.actionType === "DELETE_TASK" && place.actionData && !place.data.id)) {
            deleteTask(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }
    }, [form, setForm, task_state, place]);


    //********************************Taks Data*********************************/

    //********************************Rates Data*********************************/
    const [rate_state, setRateState] = React.useState({
        columns: [
            { title: <Translate id="places.rate_position"></Translate>, field: 'position', editable: 'never' },
            { title: <Translate id="places.rate_default"></Translate>, field: 'defaultRate', editable: 'never' },
            { title: <Translate id="places.rate_client"></Translate>, field: 'clientRate' }
        ],
        data: []
    });

    //update rate table and catch new task ID
    useEffect(() => {

        const saveRate = (item) => {

            const data = [...rate_state.data];
            var index = data.indexOf(item.oldData);

            if (typeof index !== undefined && index != null && index >= 0) {

                data[index] = item.newData;
                setRateState({ ...rate_state, data });
                setForm(_.set({ ...form }, `rates`, data));
            }
        };

        if ((place.actionType === "SAVE_RATE" && place.actionData && place.actionStatus === 1) || (place.actionType === "SAVE_RATE" && place.actionData && !place.data.id)) {
            saveRate(place.actionData);
            place.actionData = null;
            place.actionStatus = null;
            place.actionType = null;
        }
    }, [form, setForm, rate_state, place]);


    //********************************Rates Data*********************************/

    useEffect(() => {

        if (place.actionType === "SAVE_PLACE" && place.actionData && place.data && place.data.id) {
               place.data = place.actionData;
               setForm(place.actionData);
               rate_state.data = place.actionData.rates;
               task_state.data = place.actionData.tasks;
               location_state.data = place.actionData.locations;
               accredited_state.data = place.actionData.accreditedPersons;
               place.actionData = null;
               place.actionStatus = null;
               place.actionType = null;             
               
           }
       }, [form, setForm, place, rate_state,task_state ,location_state , accredited_state]);

        //first load
    useEffect(() => {
        function updatePlaceState() {
            const params = props.match.params;
            const { placeId } = params;

            dispatch(Actions.getRef());

            if (placeId === 'new') {
                dispatch(Actions.newPlace(props.match.params.customerId));
            }
            else {
                dispatch(Actions.getPlace(props.match.params.placeId));                
            }
        }

        updatePlaceState();
    }, [dispatch, props.match.params]);

    //handle form and place.data
    useEffect(() => {

        if (
            (place.data && !form) ||
            (place.data && form && place.data.id !== form.id)
        ) {           
            setForm(place.data);            
            rate_state.data = place.data.rates;
            task_state.data = place.data.tasks;
            location_state.data = place.data.locations;
            accredited_state.data = place.data.accreditedPersons;
        }
    }, [form, place.data, setForm, rate_state.data, task_state.data, location_state.data, accredited_state.data]);

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    function deleteImage(media)
    {  
        var index = form.plans.indexOf(media);

        if (typeof index !== undefined && index != null && index >= 0) {
        
            media.isDeleted = true;   
            form.plans[index] =media;
            setForm(_.set({...form}, 'plans', form.plans));
        }      
    }

    function handleUploadChange(e) {
        
        const file = e.target.files[0];
        if (!file) {
            return;
        }
        const reader = new FileReader();
        reader.readAsBinaryString(file);

        reader.onload = () => {
            setForm(_.set({ ...form }, `plans`,
                [
                    {
                        'id': '',
                        'url': `data:${file.type};base64,${btoa(reader.result)}`,
                        'type': `${file.type}`,
                        'name': file.name,
                        'data': `${btoa(reader.result)}`
                    },
                    ...form.plans
                ]
            ));
        };

        if (e.target.files[0]) {
            e.target.value = "";
        }

        reader.onerror = function () {
            console.log("error on load image");
        };
    }

    function canBeSubmitted() {

        var imageWasDeleted =  form.plans && form.plans.filter(function(obj) {return obj.isDeleted}) && form.plans.filter(function(obj) {return obj.isDeleted}).length >0;
        return (
            form.general && form.general.name.length > 0 &&
            (!_.isEqual(place.data, form) || imageWasDeleted)
        );
    }

    if ((!place.data || (place.data && props.match.params.placeId !== place.data.id.toString())) && props.match.params.placeId !== 'new') {
        return <FuseLoading />;
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136",
                content: "dark_bg pt-0 ml-0"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" role="button" onClick={ev => props.history.push('/apps/customers/detail/' + props.match.params.customerId + '/')} color="inherit">
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    <Translate id="customers.return_to_customer"></Translate>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <FuseAnimate animation="transition.expandIn" delay={300} >
                                    {form.plans && form.plans.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 mr-8 sm:mr-16 rounded" src={_.find(form.plans, { id: form.featuredImageId }).url} alt={form.general.name} />
                                    ) : (
                                            <img className="w-32 sm:w-48 mr-8 sm:mr-16 rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={form.general.name} />
                                        )}
                                </FuseAnimate>
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Translate>
                                            {({ translate }) => (
                                                <Typography className="text-16 sm:text-20 truncate">
                                                    {props.match.params.placeId !== 'new' || form.general.name ? form.general.name : translate('places.new_place')}
                                                </Typography>
                                            )}
                                        </Translate>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption"><Translate id="places.place_detail"></Translate></Typography>
                                    </FuseAnimate>

                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>

                            <Button
                                className="primary"
                                variant="contained"
                                disabled={!canBeSubmitted()}
                                onClick={() => dispatch(Actions.savePlace(form))}
                            >
                                <Translate id="form.save"></Translate>
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            contentToolbar={
                <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                    indicatorColor="secondary"
                    textColor="secondary"
                    variant="scrollable"
                    scrollButtons="auto"
                    classes={{ root: "w-full h-64" }}
                >

                    <Tab className="h-64 w-auto px-10 normal-case" label={<Translate id="places.general"></Translate>} />
                    <Tab className="h-64 w-auto px-10 normal-case" label={<Translate id="places.locations_tasks"></Translate>} />
                    <Tab className="h-64 w-auto px-10 normal-case" label={<Translate id="places.accredited_persons"></Translate>} />
                    <Tab className="h-64 w-auto px-10 normal-case" label={<Translate id="places.rates"></Translate>} />
                    <Tab className="h-64 w-auto px-10 normal-case" label={<Translate id="places.contacts"></Translate>} />
                </Tabs>
            }
            content={
                form && (
                    <div className="p-16 sm:p-24">

                        {tabValue === 0 && /* Général */
                            (
                                <div className="md:flex">

                                    <div className="flex flex-col flex-1 md:pr-32">
                                        <FuseAnimateGroup
                                            enter={{
                                                animation: "transition.slideUpBigIn"
                                            }}
                                        >
                                            <Card className="w-full mb-16">
                                                <AppBar position="static" elevation={0} className="black_bg">
                                                    <Toolbar className="pl-16 pr-8">
                                                        <Typography variant="subtitle2" color="inherit" className="flex-1">
                                                            <Translate id='places.general_infos'></Translate>
                                                        </Typography>
                                                    </Toolbar>
                                                </AppBar>
                                                <CardContent>

                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    error={form.general.name === ''}
                                                                    required
                                                                    label={<Translate id="places.place_name"></Translate>}
                                                                    autoFocus
                                                                    id="general.name"
                                                                    name="general.name"
                                                                    value={form.general.name}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1"
                                                                    label={translate('global.email')}
                                                                    autoFocus
                                                                    id="general.email"
                                                                    name="general.email"
                                                                    value={form.general.email}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                    </div>
                                                    <Translate>
                                                        {({ translate }) => (
                                                            <TextField
                                                                className="mt-8 mb-16"
                                                                label={translate('global.address')}
                                                                autoFocus
                                                                id="general.address"
                                                                name="general.address"
                                                                value={form.general.address}
                                                                onChange={handleChange}
                                                                variant="standard"
                                                                fullWidth
                                                            />
                                                        )}
                                                    </Translate>
                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.city')}
                                                                    autoFocus
                                                                    id="general.city"
                                                                    name="general.city"
                                                                    value={form.general.city}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.country')}
                                                                    autoFocus
                                                                    id="general.country"
                                                                    name="general.country"
                                                                    value={form.general.country}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1"
                                                                    label={translate('global.postalCode')}
                                                                    autoFocus
                                                                    id="general.postalCode"
                                                                    name="general.postalCode"
                                                                    value={form.general.postalCode}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>

                                                    </div>
                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label="Longitude"
                                                                    autoFocus
                                                                    id="general.longitude"
                                                                    name="general.longitude"
                                                                    value={form.general.longitude}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1"
                                                                    label="Latitude"
                                                                    autoFocus
                                                                    id="general.latitude"
                                                                    name="general.latitude"
                                                                    value={form.general.latitude}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                    </div>
                                                    <Translate>
                                                        {({ translate }) => (
                                                            <TextField
                                                                className="mt-8 mb-16"
                                                                id="general.note"
                                                                name="general.note"
                                                                onChange={handleChange}
                                                                label="Note"
                                                                type="text"
                                                                value={form.general.note}
                                                                multiline
                                                                rows={2}
                                                                variant="outlined"
                                                                fullWidth
                                                            />
                                                        )}
                                                    </Translate>
                                                </CardContent>
                                            </Card>
                                        </FuseAnimateGroup>
                                    </div>
                                    <div className="flex flex-col flex-1 max-w-sm md:pr-32">
                                        <FuseAnimateGroup
                                            enter={{
                                                animation: "transition.slideUpBigIn"
                                            }}
                                        >
                                            <Card className="w-full mb-16">
                                                <AppBar position="static" elevation={0} className="black_bg">
                                                    <Toolbar className="pl-16 pr-8">
                                                        <Typography variant="subtitle2" color="inherit" className="flex-1">
                                                            <Translate id='places.images'></Translate>
                                                        </Typography>
                                                    </Toolbar>
                                                </AppBar>
                                                <CardContent>
                                                    <input
                                                        accept="image/*"
                                                        className="hidden"
                                                        id="button-file"
                                                        type="file"
                                                        onChange={handleUploadChange}
                                                    />
                                                    <div className="flex justify-center sm:justify-start flex-wrap">
                                                        <label
                                                            htmlFor="button-file"
                                                            className={
                                                                clsx(
                                                                    classes.placeImageUpload,
                                                                    "flex items-center justify-center relative w-128 h-128 rounded-4 mr-16 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5"
                                                                )}
                                                        >
                                                            <Icon fontSize="large" color="action">cloud_upload</Icon>
                                                        </label>

                                                        {form.general && form.plans && form.plans.length > 0 ? (

                                                            form.plans.filter(function(obj) {return !obj.isDeleted}).map(media => (

                                                                <div
                                                                    onClick={() => deleteImage(media)}
                                                                    className={
                                                                        clsx(
                                                                            classes.placesImageItem,
                                                                            "flex items-center justify-center relative w-128 h-128 rounded-4 mr-16 mb-16 overflow-hidden cursor-pointer shadow-1 hover:shadow-5",
                                                                            (media.id === form.featuredImageId) && 'featured')
                                                                    }
                                                                    key={media.id}
                                                                >
                                                                    <Icon className={classes.customerImageFeaturedStar}>remove_circle_outline</Icon>
                                                                    <img className="max-w-none w-auto h-full" src={media.url} alt="customer" />
                                                                </div>
                                                            ))
                                                        ) : ("")}

                                                    </div>
                                                </CardContent>
                                            </Card>
                                        </FuseAnimateGroup>
                                    </div>
                                </div>
                            )}

                        {tabValue === 1 && /* Emplacements & Taches */
                            (

                                <div className="md:flex">
                                    <div className="flex flex-col flex-1 md:pr-32">
                                        <MaterialTable
                                            title={<Translate id="places.list_locations"></Translate>}
                                            columns={location_state.columns}
                                            data={location_state.data}
                                            localization={{
                                                body: {
                                                    addTooltip: <Translate id="tooltip.add"></Translate>,
                                                    deleteTooltip: <Translate id="tooltip.delete"></Translate>,
                                                    editTooltip: <Translate id="tooltip.edit"></Translate>,
                                                    editRow: {
                                                        deleteText: <Translate id="places.location_deleteText"></Translate>,
                                                        cancelTooltip: <Translate id="tooltip.cancel"></Translate>,
                                                        saveTooltip: <Translate id="tooltip.save"></Translate>
                                                    }
                                                }
                                            }}
                                            options={{
                                                sorting: true,
                                                search: false
                                            }}
                                            editable={{

                                                onRowAdd: newData =>
                                                    new Promise(resolve => {
                                                        setTimeout(() => {
                                                            newData.placeId = place.data.id;
                                                            dispatch(Actions.saveLocation({ oldData: null, newData }));
                                                            resolve();
                                                        }, 600);
                                                    }),
                                                onRowUpdate: (newData, oldData) =>
                                                    new Promise(resolve => {
                                                        setTimeout(() => {
                                                            dispatch(Actions.saveLocation({ oldData, newData }));
                                                            resolve();
                                                        }, 600);
                                                    }),
                                                onRowDelete: oldData =>
                                                    new Promise(resolve => {
                                                        setTimeout(() => {
                                                            dispatch(Actions.deleteLocation(oldData));
                                                            resolve();
                                                        }, 600);
                                                    }),
                                            }}
                                        />
                                    </div>
                                    <div className="flex flex-col flex-1 md:pr-32">
                                        <MaterialTable
                                            title={<Translate id="places.list_tasks"></Translate>}
                                            columns={task_state.columns}
                                            data={task_state.data}
                                            localization={{
                                                body: {
                                                    addTooltip: <Translate id="tooltip.add"></Translate>,
                                                    deleteTooltip: <Translate id="tooltip.delete"></Translate>,
                                                    editTooltip: <Translate id="tooltip.edit"></Translate>,
                                                    editRow: {
                                                        deleteText: <Translate id="places.task_deleteText"></Translate>,
                                                        cancelTooltip: <Translate id="tooltip.cancel"></Translate>,
                                                        saveTooltip: <Translate id="tooltip.save"></Translate>
                                                    }
                                                }
                                            }}
                                            options={{
                                                sorting: true,
                                                search: false
                                            }}
                                            editable={{

                                                onRowAdd: newData =>
                                                    new Promise(resolve => {
                                                        setTimeout(() => {
                                                            resolve();
                                                            newData.placeId = place.data.id;
                                                            dispatch(Actions.saveTask({ oldData: null, newData }));
                                                            resolve();
                                                        }, 600);
                                                    }),
                                                onRowUpdate: (newData, oldData) =>
                                                    new Promise(resolve => {
                                                        setTimeout(() => {
                                                            resolve();
                                                            dispatch(Actions.saveTask({ oldData, newData }));
                                                        }, 600);
                                                    }),
                                                onRowDelete: oldData =>
                                                    new Promise(resolve => {
                                                        setTimeout(() => {
                                                            dispatch(Actions.deleteTask(oldData));
                                                            resolve();
                                                        }, 600);
                                                    }),
                                            }}
                                        />
                                    </div>
                                </div>

                            )}

                        {tabValue === 2 && /* Personnes accréditées */
                            (
                                <React.Fragment>
                                    <MaterialTable
                                        title={<Translate id="places.list_accredited_persons"></Translate>}
                                        columns={accredited_state.columns}
                                        data={accredited_state.data}
                                        localization={{
                                            body: {
                                                addTooltip: <Translate id="tooltip.add"></Translate>,
                                                deleteTooltip: <Translate id="tooltip.delete"></Translate>,
                                                editTooltip: <Translate id="tooltip.edit"></Translate>,
                                                editRow: {
                                                    deleteText: <Translate id="places.accredited_persons_deleteText"></Translate>,
                                                    cancelTooltip: <Translate id="tooltip.cancel"></Translate>,
                                                    saveTooltip: <Translate id="tooltip.save"></Translate>
                                                }
                                            }
                                        }}
                                        options={{
                                            sorting: true,
                                            search: true,
                                            paging: false,
                                            actionsColumnIndex: -1
                                        }}
                                        actions={[
                                            {
                                                icon: 'person_add',
                                                isFreeAction: true,
                                                tooltip: <Translate id="tooltip.add"></Translate>,
                                                onClick: rowData => {
                                                    dispatch(Actions.openAccreditedPersonDialog({ id: "new" }))
                                                },
                                                disabled: !place.data.id
                                            }, {
                                                icon: 'edit',
                                                tooltip: <Translate id="tooltip.edit"></Translate>,
                                                onClick: (event, rowData) => {
                                                    dispatch(Actions.openAccreditedPersonDialog(rowData))
                                                }
                                            }
                                        ]}
                                        editable={{
                                            /*
                                            onRowAdd: newData =>
                                                new Promise(resolve => {
                                                    setTimeout(() => {
                                                        resolve();
                                                        const data = [...accredited_state.data];
                                                        data.push(newData);
                                                        setState4({ ...accredited_state, data });
                                                    }, 600);
                                                }),
                                                onRowUpdate: (newData, oldData) =>
                                                new Promise(resolve => {
                                                    setTimeout(() => {
                                                        resolve();
                                                        dispatch(Actions.openAccreditedPersonDialog(oldData))
                                                    }, 600);
                                                }),*/
                                            onRowDelete: oldData =>
                                                new Promise(resolve => {
                                                    setTimeout(() => {
                                                        resolve();
                                                        dispatch(Actions.deleteAccreditedPerson(oldData))
                                                    }, 600);
                                                }),
                                        }}
                                    />
                                    <AccreditedPersonDialog />
                                </React.Fragment>

                            )}

                        {tabValue === 3 && /* Taux */
                            (
                                <MaterialTable
                                    title={<Translate id="places.list_rates"></Translate>}
                                    columns={rate_state.columns}
                                    data={rate_state.data}
                                    localization={{
                                        body: {
                                            addTooltip: <Translate id="tooltip.add"></Translate>,
                                            deleteTooltip: <Translate id="tooltip.delete"></Translate>,
                                            editTooltip: <Translate id="tooltip.edit"></Translate>,
                                            editRow: {
                                                deleteText: <Translate id="places.rate_deleteText"></Translate>,
                                                cancelTooltip: <Translate id="tooltip.cancel"></Translate>,
                                                saveTooltip: <Translate id="tooltip.save"></Translate>
                                            }
                                        }
                                    }}
                                    options={{
                                        sorting: false,
                                        paging: false,
                                        search: false,
                                        actionsColumnIndex: -1
                                    }}
                                    editable={{
                                        /*
                                        onRowAdd: newData =>
                                            new Promise(resolve => {
                                                setTimeout(() => {
                                                    resolve();
                                                    const data = [...rate_state.data];
                                                    data.push(newData);
                                                    setState3({ ...rate_state, data });
                                                }, 600);
                                            }),*/
                                        onRowUpdate: (newData, oldData) =>
                                            new Promise(resolve => {
                                                setTimeout(() => {
                                                    resolve();

                                                    if(place.data.id){
                                                        oldData.placeId=place.data.id;
                                                        newData.placeId=place.data.id;
                                                    }

                                                    dispatch(Actions.saveRate({ oldData, newData }));
                                                }, 600);
                                            }),/*
                                    onRowDelete: oldData =>
                                        new Promise(resolve => {
                                            setTimeout(() => {
                                                resolve();
                                                const data = [...rate_state.data];
                                                data.splice(data.indexOf(oldData), 1);
                                                setState3({ ...rate_state, data });
                                            }, 600);
                                        }),*/
                                    }}
                                />

                            )}
                        {tabValue === 4 && /* Contacts */
                            (
                                <div className="md:flex">

                                    <div className="flex flex-col flex-1 md:pr-32">
                                        <FuseAnimateGroup
                                            enter={{
                                                animation: "transition.slideUpBigIn"
                                            }}
                                        >
                                            <Card className="w-full mb-16">
                                                <AppBar position="static" elevation={0} className="black_bg">
                                                    <Toolbar className="pl-16 pr-8">
                                                        <Typography variant="subtitle2" color="inherit" className="flex-1">
                                                            <Translate id="places.contact"></Translate> 1
                                                            </Typography>
                                                    </Toolbar>
                                                </AppBar>
                                                <CardContent>
                                                    <Translate>
                                                        {({ translate }) => (
                                                            <TextField
                                                                className="mt-8 mb-16"
                                                                label={translate('places.contact_role')}
                                                                autoFocus
                                                                id="general.contact_1.role"
                                                                name="general.contact_1.role"
                                                                value={form.general.contact_1.role}
                                                                onChange={handleChange}
                                                                variant="standard"
                                                                fullWidth
                                                            />
                                                        )}
                                                    </Translate>

                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.first_name')}
                                                                    autoFocus
                                                                    id="general.contact_1.firstname"
                                                                    name="general.contact_1.firstname"
                                                                    value={form.general.contact_1.firstname}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1"
                                                                    label={translate('global.last_name')}
                                                                    autoFocus
                                                                    id="general.contact_1.lastname"
                                                                    name="general.contact_1.lastname"
                                                                    value={form.general.contact_1.lastname}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                    </div>
                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.email')}
                                                                    autoFocus
                                                                    id="general.contact_1.email"
                                                                    name="general.contact_1.email"
                                                                    value={form.general.contact_1.email}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.phone')}
                                                                    autoFocus
                                                                    id="general.contact_1.phone"
                                                                    name="general.contact_1.phone"
                                                                    value={form.general.contact_1.phone}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>

                                                    </div>
                                                </CardContent>
                                            </Card>
                                        </FuseAnimateGroup>
                                    </div>
                                    <div className="flex flex-col flex-1">
                                        <FuseAnimateGroup
                                            enter={{
                                                animation: "transition.slideUpBigIn"
                                            }}
                                        >
                                            <Card className="w-full mb-16">
                                                <AppBar position="static" elevation={0} className="black_bg">
                                                    <Toolbar className="pl-16 pr-8">
                                                        <Typography variant="subtitle2" color="inherit" className="flex-1">
                                                            <Translate id="places.contact"></Translate> 2
                                                            </Typography>
                                                    </Toolbar>
                                                </AppBar>
                                                <CardContent>
                                                    <Translate>
                                                        {({ translate }) => (
                                                            <TextField
                                                                className="mt-8 mb-16"
                                                                label={translate('places.contact_role')}
                                                                autoFocus
                                                                id="general.contact_2.role"
                                                                name="general.contact_2.role"
                                                                value={form.general.contact_2.role}
                                                                onChange={handleChange}
                                                                variant="standard"
                                                                fullWidth
                                                            />
                                                        )}
                                                    </Translate>

                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.first_name')}
                                                                    autoFocus
                                                                    id="general.contact_2.firstname"
                                                                    name="general.contact_2.firstname"
                                                                    value={form.general.contact_2.firstname}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1"
                                                                    label={translate('global.last_name')}
                                                                    autoFocus
                                                                    id="general.contact_2.lastname"
                                                                    name="general.contact_2.lastname"
                                                                    value={form.general.contact_2.lastname}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                    </div>
                                                    <div className="flex mt-8 mb-16 ">
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.email')}
                                                                    autoFocus
                                                                    id="general.contact_2.email"
                                                                    name="general.contact_2.email"
                                                                    value={form.general.contact_2.email}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>
                                                        <Translate>
                                                            {({ translate }) => (
                                                                <TextField
                                                                    className="flex-1 mr-16"
                                                                    label={translate('global.phone')}
                                                                    autoFocus
                                                                    id="general.contact_2.phone"
                                                                    name="general.contact_2.phone"
                                                                    value={form.general.contact_2.phone}
                                                                    onChange={handleChange}
                                                                    variant="standard"
                                                                />
                                                            )}
                                                        </Translate>

                                                    </div>
                                                </CardContent>
                                            </Card>
                                        </FuseAnimateGroup>
                                    </div>
                                </div>

                            )}

                    </div>

                )
            }
            innerScroll

        />
    )
}
export default withRouter(withReducer('placeApp', reducer)(Place));