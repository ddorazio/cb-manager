import * as Actions from '../actions/collection.actions';

const initialState = {
    loading: true,
    collectionId: null,
    data: {}
};

const collectionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.SELECT_COLLECITON:
            {
                return {
                    ...state,
                    collectionId: action.collectionId,
                    loading: true,
                    data: initialState.data
                }
            }
        case Actions.GET_COLLECTION:
            {
                return {
                    ...state,
                    collectionId: action.payload.id,
                    loading: false,
                    data: action.payload
                }
            }
        case Actions.SAVE_COLLECTION_COMMENT:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        comments: [
                            ...state.data.comments,
                            action.payload
                        ]
                    }
                }
            }
        case Actions.DELETE_COLLECTION_COMMENT:
            {
                const index = state.data.comments.findIndex(c => c.id === action.commentId);
                return {
                    ...state,
                    data: {
                        ...state.data,
                        comments: [
                            ...state.data.comments.slice(0, index),
                            ...state.data.comments.slice(index + 1)
                        ]
                    }
                }
            }
        case Actions.TOGGLE_PROCEDURE_STEP:
            {
                const index = state.data.procedure.findIndex(s => s.procedureStepId === action.payload.procedureStepId);
                return {
                    ...state,
                    data: {
                        ...state.data,
                        procedure: [
                            ...state.data.procedure.slice(0, index),
                            action.payload,
                            ...state.data.procedure.slice(index + 1)
                        ]
                    }
                }
            }
        default:
            {
                return state;
            }
    }
}

export default collectionsReducer;