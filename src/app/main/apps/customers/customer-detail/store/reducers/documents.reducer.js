import * as Actions from '../actions/documents.actions';

const initialState = {
    documents: [],
    loans: []
}

const documentsReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_CLIENT_DOCUMENTS:
            {
                return {
                    ...state,
                    documents: action.payload
                }
            }
        case Actions.CREATE_DOCUMENT:
            {
                return {
                    ...state,
                    documents: state.documents 
                        ? [
                            ...state.documents,
                            action.payload
                        ] : [ action.payload ]
                }
            }
        case Actions.DELETE_DOCUMENT:
            {
                const index = state.documents.findIndex(d => d.id === action.documentId);
                return {
                    ...state,
                    documents: [
                        ...state.documents.slice(0, index),
                        ...state.documents.slice(index + 1)
                    ]
                }
            }
        case Actions.GET_LOAN_NUMBERS:
            {
                return {
                    ...state,
                    loans: action.payload
                }
            }
        case Actions.UPDATE_DOCUMENT:
            {
                const index = state.documents.findIndex(d => d.id === action.payload.id);
                return {
                    ...state,
                    documents: [
                        ...state.documents.slice(0, index),
                        action.payload,
                        ...state.documents.slice(index +1)
                    ]
                }
            }
        case Actions.RESET_DOCUMENTS_LIST:
            {
                return initialState;
            }
        default:
            {
                return state;
            }
    }
}

export default documentsReducer;