import {combineReducers} from 'redux';
import customer from './customer.reducer';
import collection from './collection.reducer';

const reducer = combineReducers({
    customer,
    collection
});

export default reducer;
