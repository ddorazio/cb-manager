import * as Actions from '../actions';

const initialState = {
    data: {
        id: undefined,
        potentialMatch: false,
        clientMatchList: [],
        basicInfo: {
            personalInfo: {
                firstName: '',
                lastName: '',
                homePhoneNumber: '',
                cellPhoneNumber: '',
                faxNumber: '',
                emailAddress: '',
                genderId: '',
                languageId: '',
                statusId: '',
                socialInsuranceNumber: '',
                dateOfBirth: null
            },
            address: {
                civicNumber: '',
                streetName: '',
                apartment: '',
                postalCode: '',
                city: '',
                provinceId: '',
                country: '',
                startOfResidencyDate: null
            },
            revenueSource: {
                incomeTypeId: '',
                employmentInsuranceStart: null,
                employmentInsuranceNextPayDate: null,
                nextDepositDate: null,
                employmentNextPay: null,
                selfEmployedDirectDeposit: null,
                selfEmployedNumber: '',
                selfEmployedPayFrequencyId: '',
                selfEmployedStartDate: null
            },
            employmentStatus: {
                employerName: '',
                supervisorName: '',
                workPhoneNumber: '',
                workPhoneNumberExtension: '',
                workPhoneNumberHR: '',
                workPhoneNumberExtensionHR: '',
                occupation: '',
                hiringDate: null,
                payFrequencyId: ''
            },
            bankInfo: {
                transitNumber: '',
                institutionNumber: '',
                accountNumber: '',
                isBankrupt: false,
                bankruptEndDate: null,
            },
            financialStatus: {
                housingStatusCode: '',
                grossAnnualRevenue: null,
                monthlyHousingCost: null,
                monthlyUtilityCost: null,
                monthlyCarPayment: null,
                monthlyOtherLoanPayment: null
            },
            social: {
                facebookId: null,
                googleId: null,
                googlePhoto: null
            }
        },
        references: undefined,
        documents: [],
        ibvs: [],
        loans: [],
        requests: [],
        nonLinkedRequests: [],
        images: [],
        genders: [],
        status: [],
        languages: [],
        payFrequencies: [],
        provinces: [],
        history: [],
        activeCollections: [],
        creditBookChecks: [],
        pdfData: null,
        loanConfiguration: {
        }
    },
    sendPDF: false,
    actionStatus: false,
    totalAmountRemaining: null,
    mergedClientId: null,
    pdfGeneratorDialog: {
        open: false
    },

};

const customerReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_CUSTOMER:
            {
                return {
                    ...state,
                    actionType: "GET_CUSTOMER",
                    data: action.payload,
                    actionStatus: action.status
                };
            }
        case Actions.SAVE_CUSTOMER:
            {
                return {
                    ...state,
                    actionType: "SAVE_CUSTOMER",
                    data: action.payload,
                    actionStatus: action.status
                };
            }
        case Actions.NEW_CUSTOMER:
            {
                var init = initialState.data;
                init.languages = action.data.languages;
                init.genders = action.data.genders;
                init.payFrequencies = action.data.payFrequencies;
                init.provinces = action.data.provinces;

                return {
                    ...state,
                    data: init
                };
            }
        case Actions.NEW_IBV_SUCCESS:
            {
                var t = state.data.ibvList.concat(action.data);

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        ibvList: state.data.ibvList.concat(action.data)
                    }
                };
            }
        case Actions.GET_CLIENT_REFERENCES:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        references: action.references
                    }
                }
            }
        case Actions.UPDATE_CLIENT_REFERENCE:
            {
                var newRefs = state.data.references.slice();
                const index = newRefs.indexOf(newRefs.find(cref => cref.id === action.reference.id));
                newRefs[index] = action.reference;
                return {
                    ...state,
                    data: {
                        ...state.data,
                        references: newRefs
                    }
                }
            }
        case Actions.CREATE_CLIENT_REFERENCE:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        references: state.data.references
                            ? [...state.data.references, action.reference]
                            : [action.reference]
                    }
                }
            }
        case Actions.DELETE_CLIENT_REFERENCE:
            {
                const oldRefs = state.data.references;
                const index = oldRefs.indexOf(oldRefs.find(cref => cref.id === action.referenceId));
                return {
                    ...state,
                    data: {
                        ...state.data,
                        references: [
                            ...oldRefs.slice(0, index),
                            ...oldRefs.slice(index + 1)
                        ]
                    }
                }
            }
        case Actions.NEW_LOAN_SUCCESS:
            {
                const nonLinkedRequests = state.data.nonLinkedRequests;
                const index = nonLinkedRequests.indexOf(nonLinkedRequests.find(x => x.id === action.data.requestNo));
                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: state.data.loans.concat(action.data),
                        nonLinkedRequests: [
                            ...nonLinkedRequests.slice(0, index),
                            ...nonLinkedRequests.slice(index + 1)
                        ]
                    }
                }
            }
        case Actions.NSF_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.NSF_REMOVE_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.REBATE_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.REBATE_REMOVE_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.MANUAL_PAYMENT_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.MODIFY_PAYMENT_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.VALIDATE_TOTAL_SUCCESS:
            {
                return {
                    ...state,
                    totalAmountRemaining: action.data
                };
            }
        case Actions.MODIFY_LOAN_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.RESTART_LOAN_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.MODIFY_PAYMENT_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.VALIDATE_TOTAL_SUCCESS:
            {
                return {
                    ...state,
                    totalAmountRemaining: action.data
                };
            }
        case Actions.MODIFY_LOAN_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.RESTART_LOAN_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.VALIDATE_TOTAL_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.GET_CLIENT_HISTORY:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        history: action.history
                    }
                }
            }
        case Actions.GET_CLIENT_REQUESTS:
            {
                return {
                    ...state,
                    data: {
                        ...state.data,
                        requests: action.payload
                    }
                }
            }
        case Actions.UPDATE_CLIENT_REQUEST:
            {
                var newRequests = state.data.requests.slice();
                const index = newRequests.indexOf(newRequests.find(creq => creq.id === action.payload.id));
                newRequests[index] = action.payload;
                return {
                    ...state,
                    data: {
                        ...state.data,
                        requests: newRequests
                    }
                }
            }
        case Actions.CONTRACT_SENT:
            {
                return {
                    ...state,
                    actionStatus: false
                };
            }
        case Actions.SEARCH_CREDITBOOK_CHECK:
            {
                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        creditBookChecks: action.data
                    }
                };
            }

        case Actions.UPDATE_REQUEST_CHECKLIST:
            {
                var newRequests = state.data.requests.slice();
                const index = newRequests.indexOf(newRequests.find(x => x.id === action.data.id));
                newRequests[index] = action.data;
                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        requests: newRequests
                    }
                }
            }
        case Actions.MODIFY_LOAN_STATUS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.MODIFY_PAYMENT_DATE:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            }
        case Actions.MERGE_CLIENT_PROFILE:
            {
                return {
                    ...state,
                    mergedClientId: action.data,
                    actionStatus: true
                };
            }
        case Actions.DELETE_LOAN:
            {
                const loans = state.data.loans;
                const loanIndex = loans.indexOf(loans.find(x => x.id === action.data.id));

                const nonLinkedRequests = state.data.nonLinkedRequests;
                const index = nonLinkedRequests.indexOf(nonLinkedRequests.find(x => x.id === action.data.requestNo));

                return {
                    ...state,
                    data: {
                        ...state.data,
                        loans: [
                            ...loans.slice(0, loanIndex),
                            ...loans.slice(loanIndex + 1)
                        ],
                        nonLinkedRequests: action.payload
                    }
                }
            }
        case Actions.GET_PDF_DATA:
            {

                return {
                    ...state,
                    data: {
                        ...state.data,
                        pdfData: action.payload,
                    }
                }
            }
        case Actions.SEND_PDF:
            {
                return {
                    ...state,
                    sendPDF: action.payload
                }
            }
        case Actions.SEND_LOAN_PDF:
            {
                return {
                    ...state
                }
            }
        case Actions.OPEN_PDF_DIALOG:
            {
                return {
                    ...state,
                    pdfGeneratorDialog: {
                        open: true,
                    }
                };
            };
        case Actions.CLOSE_PDF_DIALOG:
            {
                return {
                    ...state,
                    pdfGeneratorDialog: {
                        open: false,
                    }
                };
            };
        case Actions.PAYMENT_SUCCESS:
            {
                let newLoans = state.data.loans.slice();
                let loanId = action.data.id;
                let loan = state.data.loans.find(x => x.id == loanId);
                let loanIndex = newLoans.indexOf(loan);
                newLoans[loanIndex] = action.data;

                return {
                    ...state,
                    actionStatus: false,
                    data: {
                        ...state.data,
                        loans: newLoans
                    }
                };
            };
        case Actions.LOAN_CONFIGURATION_SUCCESS:
            {
                return {
                    ...state,
                    data: {
                        loanConfiguration: action
                    }
                };
            };
        default:
            {
                return state;
            }
    }
};

export default customerReducer;
