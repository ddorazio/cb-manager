import { showMessage, closeDialog } from 'app/store/actions/fuse';
import LoanAPI from '../../../LoanAPI'
import CustomerAPI from '../../../CustomerAPI'
import i18n from 'i18n';
import { DELETE_LOAN_AMOUNT } from 'app/main/apps/configuration/store/actions';

export const NEW_LOAN_SUCCESS = 'NEW_LOAN_SUCCESS';
//export const NEW_LOAN_FAILED = 'NEW_LOAN_FAILED';

export const NSF_SUCCESS = 'NSF_SUCCESS';

export const NSF_REMOVE_SUCCESS = 'NSF_REMOVE_SUCCESS';

export const REBATE_SUCCESS = 'REBATE_SUCCESS';

export const REBATE_REMOVE_SUCCESS = 'REBATE_REMOVE_SUCCESS';

export const MANUAL_PAYMENT_SUCCESS = 'MANUAL_PAYMENT_SUCCESS';

export const MANUAL_PAYMENT_REMOVE_SUCCESS = 'MANUAL_PAYMENT_REMOVE_SUCCESS';

export const MODIFY_PAYMENT_SUCCESS = 'MODIFY_PAYMENT_SUCCESS';

export const VALIDATE_TOTAL_SUCCESS = 'VALIDATE_TOTAL_SUCCESS';

export const MODIFY_LOAN_SUCCESS = 'MODIFY_LOAN_SUCCESS';

export const RESTART_LOAN_SUCCESS = 'RESTART_LOAN_SUCCESS';

export const CONTRACT_SENT = "CONTRACT_SENT";

export const MODIFY_LOAN_STATUS = "MODIFY_LOAN_STATUS";

export const GET_CUSTOMER_REQUESTS = 'GET_CUSTOMER_REQUESTS';

export const MODIFY_PAYMENT_DATE = "MODIFY_PAYMENT_DATE";

export const DELETE_LOAN = "DELETE_LOAN";

export const GET_PDF_DATA = "GET_PDF_DATA";

export const SEND_PDF = "SEND_PDF";

export const SEND_LOAN_PDF = "SEND_LOAN_PDF";

export const PAYMENT_SUCCESS = "PAYMENT_SUCCESS";
export const LOAN_CONFIGURATION_SUCCESS = "LOAN_CONFIGURATION_SUCCESS";

export const createLoan = (form, loan) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.createLoan(form, loan);
            dispatch(showMessage({ message: i18n.t("LOAN_CREATED"), variant: 'success' }));
            return dispatch({
                type: NEW_LOAN_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const createNSFPayment = (loan, payment) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.createNSFPayment(loan, payment);
            dispatch(showMessage({ message: i18n.t("NSF_PAYMENT_CREATED"), variant: 'success' }));
            dispatch(closeDialog());
            return dispatch({
                type: NSF_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteNSFPayment = (loan, payment) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.deleteNSFPayment(loan, payment);
            dispatch(showMessage({ message: i18n.t("NSF_PAYMENT_DELETED"), variant: 'success' }));
            dispatch(closeDialog());
            return dispatch({
                type: NSF_REMOVE_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const addRebate = (loan, rebate) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.addRebate(loan, rebate);
            dispatch(showMessage({ message: i18n.t("REBATE_ADDED"), variant: 'success' }));
            return dispatch({
                type: REBATE_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const createPayment = (loan, payment) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.createPayment(loan, payment);
            dispatch(showMessage({ message: i18n.t("PAYMENT_CREATED"), variant: 'success' }));
            return dispatch({
                type: MANUAL_PAYMENT_SUCCESS,
                data: result.data
            });
        } catch (error) {

            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteRebate = (loan, payment) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.deleteRebate(loan, payment);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("REBATE_DELETED"), variant: 'success' }));
            return dispatch({
                type: REBATE_REMOVE_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updatePayment = (loan, row, changes) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.updatePayment(loan, row, changes);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("PAYMENT_UPDATED"), variant: 'success' }));
            return dispatch({
                type: MODIFY_PAYMENT_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const validateTotal = (loan, row, changes) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.validateTotal(loan, row, changes);
            return dispatch({
                type: VALIDATE_TOTAL_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateLoan = (loan, changes) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.updateLoan(loan, changes);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("LOAN_UPDATED"), variant: 'success' }));
            return dispatch({
                type: MODIFY_LOAN_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const restartLoan = (loan, changes) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.restartLoan(loan, changes);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("LOAN_RESTARTED"), variant: 'success' }));
            return dispatch({
                type: RESTART_LOAN_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const sendContract = (contract, loan) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.sendContract(contract, loan);
            dispatch(showMessage({ message: i18n.t("CONTRACT_SENT"), variant: 'success' }));
            return dispatch({
                type: CONTRACT_SENT
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateLoanStatus = (loanId) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.updateLoanStatus(loanId);
            dispatch(showMessage({ message: i18n.t("MODIFY_LOAN_STATUS"), variant: 'success' }));
            dispatch(closeDialog());
            return dispatch({
                type: MODIFY_LOAN_STATUS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getCustomerRequests = (params) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.getCustomerRequests(params);
            return dispatch({
                type: GET_CUSTOMER_REQUESTS,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

// NO LONGER USED
export const modifyPaymentDate = (loan, row, changes) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.modifyPaymentDate(loan, row, changes);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("MODIFY_PAYMENT_DATE"), variant: 'success' }));
            return dispatch({
                type: MODIFY_PAYMENT_DATE,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deferPayment = (loan, row) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.deferPayment(loan, row);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("MODIFY_PAYMENT_DATE"), variant: 'success' }));
            return dispatch({
                type: MODIFY_PAYMENT_DATE,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteLoan = (loan) => {
    return async (dispatch) => {
        try {
            var result = await LoanAPI.deleteLoan(loan._original.id);
            dispatch(closeDialog());
            dispatch(showMessage({ message: i18n.t("DELETED_LOAN"), variant: 'success' }));
            return dispatch({
                type: DELETE_LOAN,
                payload: result.data,
                data: loan._original
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getPDFData = (loanId) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.getPDFData(loanId);
            return dispatch({
                type: GET_PDF_DATA,
                payload: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const sendPDF = (isSending) => {
    return async (dispatch) => {
        try {
            return dispatch({
                type: SEND_PDF,
                payload: isSending
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const sendLoanPDF = (pdfString, email, typePDF, data) => {
    return async (dispatch) => {

        try {
            await LoanAPI.sendLoanPDF(pdfString, email, typePDF, data);
            dispatch(closeDialog());
            dispatch(showMessage({ message: "PDF envoyé", variant: 'success' }));
            return dispatch({
                type: SEND_LOAN_PDF,
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const addPayment = (loan, payment) => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.addPayment(loan, payment);
            dispatch(showMessage({ message: i18n.t("PAYMENT_ADDED"), variant: 'success' }));
            return dispatch({
                type: PAYMENT_SUCCESS,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const loadLoanConfiguration = () => {
    return async (dispatch) => {
        try {
            let result = await LoanAPI.loadLoanConfiguration();
            return dispatch({
                type: LOAN_CONFIGURATION_SUCCESS,
                data: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

