import { showMessage } from 'app/store/actions/fuse';
import CustomerAPI from '../../../CustomerAPI'
import RequestsAPI from '../../../../requests/RequestsAPI'
import { sendNotificationAll } from '../../../../notifications/store/actions/index'
import i18n from 'i18n';

export const NEW_CUSTOMER = 'NEW_CUSTOMER';
export const GET_CUSTOMER = 'GET CUSTOMER';
export const SAVE_CUSTOMER = 'SAVE CUSTOMER';

export const NEW_IBV_SUCCESS = 'NEW_IBV_SUCCESS';
export const NEW_IBV_FAILED = 'NEW_IBV_FAILED';

export const GET_CLIENT_REFERENCES = 'GET_CLIENT_REFERENCES';
export const UPDATE_CLIENT_REFERENCE = 'UPDATE_CLIENT_REFERENCE';

export const CREATE_CLIENT_REFERENCE = 'CREATE_CLIENT_REFERENCE';
export const DELETE_CLIENT_REFERENCE = 'DELETE_CLIENT_REFERENCE';

export const GET_CLIENT_HISTORY = 'GET_CLIENT_HISTORY';
export const GET_CLIENT_REQUESTS = 'GET_CLIENT_REQUESTS';
export const UPDATE_CLIENT_REQUEST = 'UPDATE_CLIENT_REQUESTS';

export const SEARCH_CREDITBOOK_CHECK = 'SEARCH_CREDITBOOK_CHECK';
export const UPDATE_REQUEST_CHECKLIST = 'UPDATE_REQUEST_CHECKLIST';
export const MERGE_CLIENT_PROFILE = 'MERGE_CLIENT_PROFILE';

export const getCustomer = (params) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.getCustomer(params);
            return dispatch({
                type: GET_CUSTOMER,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const saveCustomer = (form) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.saveCustomer(form);
            dispatch(showMessage({ message: i18n.t('CUSTOMER_SAVED'), variant: 'success' }));
            return dispatch({
                type: SAVE_CUSTOMER,
                payload: result,
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const newCustomer = () => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.createCustomer();
            return dispatch({
                type: NEW_CUSTOMER,
                data: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateCustomer = (data) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.updateCustomer(data);
            dispatch(showMessage({ message: i18n.t('CUSTOMER_SAVED'), variant: 'success' }));
            return dispatch({
                type: SAVE_CUSTOMER,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const createIBV = (data) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.createIBV(data);
            dispatch(showMessage({ message: i18n.t('CREATE_IBV_SUCCESS'), variant: 'success' }));
            return dispatch({
                type: NEW_IBV_SUCCESS,
                data: result.data
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const resendIBV = (id) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.resendIBV(id);
            dispatch(showMessage({ message: i18n.t('RESEND_IBV_SUCCESS'), variant: 'success' }));
        } catch (error) {
            dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const fetchCustomerReferences = (customerId) => {
    return async dispatch => {
        try {
            const result = await CustomerAPI.fetchCustomerReferences(customerId);
            return dispatch({
                type: GET_CLIENT_REFERENCES,
                references: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateClientReference = (cref, dry = false) => {
    if (dry) {
        return {
            type: UPDATE_CLIENT_REFERENCE,
            reference: cref
        };
    }
    return async dispatch => {
        try {
            const result = await CustomerAPI.updateCustomerReference(cref);
            return dispatch({
                type: UPDATE_CLIENT_REFERENCE,
                reference: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const createClientReference = (cref, dry = false) => {
    if (dry) {
        return {
            type: CREATE_CLIENT_REFERENCE,
            reference: cref
        };
    }
    return async dispatch => {
        try {
            const result = await CustomerAPI.createCustomerReference(cref);
            return dispatch({
                type: CREATE_CLIENT_REFERENCE,
                reference: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteClientReference = (refId, dry = false) => {
    if (dry) {
        return {
            type: DELETE_CLIENT_REFERENCE,
            referenceId: refId
        }
    }
    return async dispatch => {
        try {
            const result = await CustomerAPI.deleteCustomerReference(refId);
            return dispatch({
                type: DELETE_CLIENT_REFERENCE,
                referenceId: refId
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export function getClientHistory(clientId) {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.getClientHistory(clientId);
            return dispatch({
                type: GET_CLIENT_HISTORY,
                history: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    };
}

export function fetchClientRequests(clientId) {
    return async (dispatch) => {
        try {
            const result = await RequestsAPI.getClientRequests(clientId);
            return dispatch({
                type: GET_CLIENT_REQUESTS,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    };
}

export function updateRequest(request) {
    return async (dispatch) => {
        try {
            if(request.statusCode == "ACCEPTED" || request.statusCode == "PREAPPROVED") {
                dispatch(sendNotificationAll(request.statusCode, request.clientId))
            }
            const result = await RequestsAPI.updateRequest(request);
            return dispatch({
                type: UPDATE_CLIENT_REQUEST,
                payload: result
            })
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }))
        }
    }
}

export const searchCreditBookCheck = (data) => {
    return async (dispatch) => {
        try {
            let result = await CustomerAPI.searchCreditBookCheck(data);
            return dispatch({
                type: SEARCH_CREDITBOOK_CHECK,
                data: result.data
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateRequestChecklist = (data) => {
    return async dispatch => {
        try {
            const result = await CustomerAPI.updateRequestChecklist(data);
            return dispatch({
                type: UPDATE_REQUEST_CHECKLIST,
                data: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const mergeClientProfile = (customer, row) => {
    return async (dispatch) => {
        try {
            const result = await CustomerAPI.mergeClientProfile(customer, row);
            return dispatch({
                type: MERGE_CLIENT_PROFILE,
                data: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const OPEN_PDF_DIALOG = '[DIALOG APP] OPEN PDF DIALOG';
export const CLOSE_PDF_DIALOG = '[DIALOG APP] CLOSE PDF DIALOG';

export const openPDFGeneratorDialog = () => {
    return (dispatch) =>
    dispatch({
        type: OPEN_PDF_DIALOG
    })
}

export const closePDFGeneratorDialog = () => {
    return (dispatch) =>
    dispatch({
        type: CLOSE_PDF_DIALOG
    })
}