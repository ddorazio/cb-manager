import { showMessage } from 'app/store/actions/fuse';
import CollectionAPI from '../../../CollectionAPI';
import i18n from 'i18n';

export const SELECT_COLLECITON = 'SELECT_COLLECTION';
export const GET_COLLECTION = 'GET_COLLECTION';
export const SAVE_COLLECTION_COMMENT = 'SAVE_COLLECTION_COMMENT';
export const DELETE_COLLECTION_COMMENT = 'UPDATE_COLLECTION_COMMENT';
export const TOGGLE_PROCEDURE_STEP = 'TOGGLE_PROCEDURE_STEP';

export const selectCollection = (collectionId) => ({
    type: SELECT_COLLECITON,
    collectionId
});

export const getCollection = (collectionId) => {
    return async dispatch => {
        try {
            const result = await CollectionAPI.getCollection(collectionId);
            return dispatch({
                type: GET_COLLECTION,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateCollection = (collection) => {
    return async dispatch => {
        try {
            const result = await CollectionAPI.updateCollection(collection);
            dispatch(showMessage({ message: i18n.t('COLLECTION_SAVED'), variant: 'success' }));
            return dispatch({
                type: GET_COLLECTION,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getRejectionsOfTheDay = async (dispatch) => {
    try {
        const result = await CollectionAPI.getRejectionsOfTheDay();
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const getCollectionsWithFollowUp = async (dispatch) => {
    try {
        const result = await CollectionAPI.getCollectionsWithFollowUp();
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const getTransfersReceivableToDate = async (dispatch) => {
    try {
        const result = await CollectionAPI.getTransfersReceivableToDate();
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const getCollectionsAwaitingResponse = async (dispatch) => {
    try {
        const result = await CollectionAPI.getCollectionsAwaitingResponse();
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const saveComment = (comment) => {
    return async (dispatch) => {
        try {
            const result = await CollectionAPI.saveComment(comment);
            dispatch({
                type: SAVE_COLLECTION_COMMENT,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteComment = (commentId) => {
    return async (dispatch) => {
        try {
            await CollectionAPI.deleteComment(commentId);
            dispatch({
                type: DELETE_COLLECTION_COMMENT,
                commentId
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const toggleProcedureStep = (step) => {
    return async (dispatch) => {
        try {
            const result = await CollectionAPI.toggleProcedureStep(step);
            dispatch({
                type: TOGGLE_PROCEDURE_STEP,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}