import { showMessage } from 'app/store/actions/fuse';
import DocumentAPI from '../../../DocumentAPI'
import LoanAPI from '../../../LoanAPI'
import i18n from 'i18n';
import settingsConfig from 'app/fuse-configs/settingsConfig';

export const GET_CLIENT_DOCUMENTS = 'GET_CLIENT_DOCUMENTS';
export const CREATE_DOCUMENT = 'CREATE_DOCUMENT';
export const DELETE_DOCUMENT = 'DELETE_DOCUMENT';
export const GET_LOAN_NUMBERS = 'GET_LOAN_NUMBERS';
export const UPDATE_DOCUMENT = 'UPDATE_DOCUMENT';
export const RESET_DOCUMENTS_LIST = 'RESET_DOCUMENTS_LIST';

export const getClientDocuments = clientId => {
    return async dispatch => {
        try {
            const result = await DocumentAPI.getDocuments(clientId);
            return dispatch({
                type: GET_CLIENT_DOCUMENTS,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const createDocument = newDocument => {
    return async dispatch => {
        try {
            const result = await DocumentAPI.createDocument(newDocument);
            dispatch(showMessage({ message: i18n.t("DOCUMENT_CREATED"), variant: 'success' }));
            return dispatch({
                type: CREATE_DOCUMENT,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteDocument = guid => {
    return async dispatch => {
        try {
            const result = await DocumentAPI.deleteDocument(guid);
            dispatch(showMessage({ message: i18n.t("DOCUMENT_DELETED"), variant: 'success' }));
            return dispatch({
                type: GET_CLIENT_DOCUMENTS
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateDocument = data => {
    return async dispatch => {
        try {
            const result = await DocumentAPI.updateDocument(data);
            dispatch(showMessage({ message: i18n.t("DOCUMENT_UPDATED"), variant: 'success' }));
            return dispatch({
                type: UPDATE_DOCUMENT,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getLoanNumbers = clientId => {
    return async dispatch => {
        try {
            const result = await LoanAPI.getLoanNumbers(clientId);
            return dispatch({
                type: GET_LOAN_NUMBERS,
                payload: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const downloadDocument = async (document) => {
    await DocumentAPI.downloadDocument(document);
}