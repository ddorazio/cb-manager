import { getProtected, postProtected, putProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class CustomerAPI {
    static getCustomers = async () => {
        return await getProtected(new URL('/api/Client/List', settingsConfig.apiPath));
    }

    static deleteCustomer = async (customer) => {
        return await postProtected(new URL(`/api/Client/Delete/${customer.id}`, settingsConfig.apiPath));
    }

    static getCustomer = async (params) => {
        return await getProtected(new URL(`/api/Client/${params.customerId}`, settingsConfig.apiPath));
    }

    static saveCustomer = async (customer) => {
        return await postProtected(new URL('/api/Client/Save', settingsConfig.apiPath), customer);
    }

    static createCustomer = async () => {
        return await getProtected(new URL('/api/Client/NewClientData', settingsConfig.apiPath));
    }

    static updateCustomer = async (customer) => {
        return await postProtected(new URL('/api/Client/Update', settingsConfig.apiPath), customer);
    }

    static createIBV = async (customer) => {

        // Get Public IP
        let response = await fetch('https://api.ipify.org?format=json', { method: 'GET' });
        let data = await response.json();

        let model = {
            ClientId: customer.id,
            FirstName: customer.basicInfo.personalInfo.firstName,
            LastName: customer.basicInfo.personalInfo.lastName,
            Email: customer.basicInfo.personalInfo.emailAddress,
            PhoneNumber: customer.basicInfo.personalInfo.cellPhoneNumber,
            IPAddress: data.ip,
            BrowserName: "Mozilla"
        };
        return await postProtected(new URL('/api/IBV/NewIBVRequest', settingsConfig.apiPath), model);
    }

    static resendIBV = async (id) => {
        return await postProtected(new URL(`/api/IBV/Resend/${id}`, settingsConfig.apiPath), id);
    }

    static fetchCustomerReferences = async (customerId) => {
        return await getProtected(new URL(`/api/Client/${customerId}/References`, settingsConfig.apiPath));
    }

    static updateCustomerReference = async (data) => {
        return await postProtected(new URL("/api/Client/UpdateReference", settingsConfig.apiPath), data);
    }

    static createCustomerReference = async (data) => {
        return await postProtected(new URL("/api/Client/CreateReference", settingsConfig.apiPath), data);
    }

    static deleteCustomerReference = async (refId) => {
        return await deleteProtected(new URL(`/api/Client/DeleteReference/${refId}`, settingsConfig.apiPath));
    }

    static getClientHistory = async (clientId) => {
        return await getProtected(new URL(`/api/Client/GetClientHistory/${clientId}`, settingsConfig.apiPath), settingsConfig.apiPath);
    }

    static searchCreditBookCheck = async (form) => {

        let data = {
            firstName: form.basicInfo.personalInfo.firstName,
            lastName: form.basicInfo.personalInfo.lastName,
            phone: form.basicInfo.personalInfo.cellPhoneNumber,
            email: form.basicInfo.personalInfo.emailAddress
        };

        return await postProtected(new URL("/api/Client/SearchCreditBookCheck", settingsConfig.apiPath), data);
    }

    static updateRequestChecklist = async (form) => {
        return await putProtected(new URL("/api/LoanRequest/UpdateChecklist", settingsConfig.apiPath), form.data);
    }

    static getCustomerRequests = async (params) => {
        return await getProtected(new URL(`/api/GetClientRequest/${params.customerId}`, settingsConfig.apiPath));
    }

    static mergeClientProfile = async (customer, row) => {
        return await postProtected(new URL(`/api/Client/MergeClientProfile/${customer.id}/${row.id}`, settingsConfig.apiPath));
    }
}

export default CustomerAPI