import React from 'react';
import localStorageService from 'app/services/localStorageService';

const language = localStorageService.getLanguage()

const LoanStatus = (props) => {

    if (language == "fr") {
        switch (props.code) {
            case 'IP':
                {
                    return <span>En cours</span>
                }
            case 'LS':
                {
                    return <span>Prêt arrêté</span>
                }
            case 'WFD':
                {
                    return <span>En attente de dépôt</span>
                }
            case 'PEN':
                {
                    return <span>En attente</span>
                }
            case 'PA':
                {
                    return <span>Entente de paiement</span>
                }
            case 'LR':
                {
                    return <span>Prêt rembousé</span>
                }
            case 'LD':
                {
                    return <span>En souffrance</span>
                }
        }
    } else {
        switch (props.code) {
            case 'IP':
                {
                    return <span>In Progress</span>
                }
            case 'LS':
                {
                    return <span>Loan stopped</span>
                }
            case 'WFD':
                {
                    return <span>Waiting for deposit</span>
                }
            case 'PEN':
                {
                    return <span>Pending</span>
                }
            case 'PA':
                {
                    return <span>Payment agreement</span>
                }
            case 'LR':
                {
                    return <span>Loan refunded</span>
                }
            case 'LD':
                {
                    return <span>Loan defaulted</span>
                }
        }

    }
}

export default LoanStatus;