import { getProtected, putProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class DepositsAPI {
    static getDeposits = async () => {
        return await getProtected(new URL(`/api/Deposit/List`, settingsConfig.apiPath));
    }

    static updateDeposit = async (deposit) => {
        return await putProtected(new URL(`/api/Deposit/Update`, settingsConfig.apiPath), deposit);
    }
}

export default DepositsAPI;