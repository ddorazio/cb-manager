import React, { useState } from 'react';
import { Button, Dialog, DialogActions, TextField, InputLabel, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { updateDeposit, closeDepositDialog } from './store/actions/deposits.actions';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import localStorageService from 'app/services/localStorageService';

function DepositDialog(props) {

    const language = localStorageService.getLanguage()
    const { t } = useTranslation('depositsApp');
    const dispatch = useDispatch();
    const [interacCode, setInteracCode] = useState('');
    const dialog = useSelector(({ depositsApp }) => depositsApp.deposits.dialog)

    const closeDialog = () => {
        dispatch(closeDepositDialog());
    }

    const applyChange = () => {
        dispatch(updateDeposit({
            ...dialog.deposit,
            interacConfirmationCode: interacCode
        }));
        closeDialog();
    }

    const canSubmit = () => {
        switch (dialog.deposit.typeCode) {
            case 'DIRECT':
                return interacCode;
            default:
                return true;
        }
    }

    if (!dialog.deposit) return null;

    return (
        <Dialog
            {...dialog.props}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            {/* <DialogTitle id="alert-dialog-title">{t('DEPOSIT_DIALOG_TEXT')}</DialogTitle> */}
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {t('DEPOSIT_DIALOG_TEXT')
                        .replace('%AMT%', new Intl.NumberFormat(language + '-CA', { style: 'currency', currency: 'CAD' }).format(dialog.deposit.loanAmount))
                        .replace('%NAME%', dialog.deposit.firstName)
                        .replace('%TYPE%', dialog.option)}
                </DialogContentText>
                {dialog.deposit.typeCode === 'DIRECT' &&
                    <TextField
                        value={interacCode}
                        label={t('INTERAC_CONFIRMATION_CODE')}
                        onChange={(e) => setInteracCode(e.target.value)}
                    />}
            </DialogContent>
            <DialogActions>
                <Button onClick={closeDialog} color="primary">
                    {t('DEPOSIT_DIALOG_DISAGREE')}
                </Button>
                <Button onClick={applyChange} color="primary" autoFocus disabled={!canSubmit()}>
                    {t('DEPOSIT_DIALOG_AGREE')}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default DepositDialog