import React, { useEffect, useState } from 'react';
import { Button, Link } from '@material-ui/core';
import { FuseUtils, FuseAnimate, FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import DepositStatus from '../components/DepositStatus';
import { openDepositDialog } from './store/actions/deposits.actions';
import localStorageService from 'app/services/localStorageService';

import * as Fuse from 'app/store/actions/fuse';
import * as Material from '@material-ui/core';

function DepositsList(props) {

    const language = localStorageService.getLanguage()
    const { t } = useTranslation('depositsApp');
    const dispatch = useDispatch();
    const deposits = useSelector(({ depositsApp }) => depositsApp.deposits.entities)
    const searchText = useSelector(({ depositsApp }) => depositsApp.deposits.searchText);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchTet) {
            const arr = Object.keys(entities).map((id) => entities[id]);
            if (searchText.length === 0) {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if (deposits) {
            setFilteredData(getFilteredArray(deposits, searchText));
        }
    }, [deposits, searchText]);

    const applyDirectDeposit = (deposit) => {

        if (!deposit.depositPermitted) {
            handleDepositNotPermitted();
        }
        else {
            dispatch(openDepositDialog({
                ...deposit,
                statusCode: 'DEPOSITED',
                typeCode: 'DIRECT'
            }, t("DEPOSIT_TYPE_DIRECT")));
        }
    }

    const applyPerceptechDeposit = (deposit) => {
        if (!deposit.depositPermitted) {
            handleDepositNotPermitted();
        } else {
            dispatch(openDepositDialog({
                ...deposit,
                statusCode: 'DEPOSITED',
                typeCode: 'PERCEPTECH'
            }, t("DEPOSIT_TYPE_PERCEPTECH")));
        }
    }

    const handleDepositNotPermitted = () => {

        dispatch(Fuse.openDialog(
            {
                children: (
                    <React.Fragment>
                        <Material.DialogTitle id="alert-dialog-title">{t("DEPOSIT_REFUSED_HEADER")}</Material.DialogTitle>
                        <Material.DialogContent>
                            <Material.DialogContentText id="alert-dialog-description">
                                {t("DEPOSIT_REFUSED_MESSAGE")}
                            </Material.DialogContentText>
                        </Material.DialogContent>
                        <Material.DialogActions>
                            <Material.Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="primary"
                                onClick={() => dispatch(Fuse.closeDialog())}>
                                {t("DEPOSIT_REFUSED_CANCEL")}
                            </Material.Button>
                        </Material.DialogActions>
                    </React.Fragment >
                )
            })
        );
    }

    if (!filteredData) {
        return <FuseLoading />;
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                data={filteredData}
                columns={[
                    {
                        accessor: 'id',
                        show: false
                    },
                    {
                        id: "clientId",
                        Header: t("DEPOSIT_LIST_CLIENT_NO"),
                        accessor: row =>
                            <Link href="#" onClick={ev => {
                                ev.preventDefault()
                                props.history.push(`/apps/customers/detail/${row.clientId}`)
                            }}>
                                {row.clientId}
                            </Link>,
                        width: 100
                    },
                    {
                        Header: t("DEPOSIT_LIST_FIRST_NAME"),
                        accessor: "firstName"
                    },
                    {
                        Header: t("DEPOSIT_LIST_LAST_NAME"),
                        accessor: "lastName"
                    },
                    {
                        Header: t("DEPOSIT_LIST_BANK_ACCOUNT_NB"),
                        accessor: "bankAccountNumber",
                        width: 160
                    },
                    {
                        id: "loanAmount",
                        Header: t("DEPOSIT_LIST_AMOUNT"),
                        width: 100,
                        accessor: row => new Intl.NumberFormat(language + '-CA', { style: 'currency', currency: 'CAD' }).format(row.loanAmount)
                    },
                    {
                        Header: t("DEPOSIT_LIST_STATUS"),
                        id: 'statusCode',
                        accessor: row => <DepositStatus code={row.statusCode} />
                    },
                    {
                        Header: "Actions",
                        className: "pl-0",
                        width: 360,
                        Cell: row => {
                            return row.original.statusCode === 'WAITING' ? (
                                <div className="flex items-center">
                                    <Button
                                        color="secondary"
                                        variant="outlined"
                                        className="mr-4"
                                        onClick={e => applyDirectDeposit(row.original)}
                                    >
                                        {t("DEPOSIT_TYPE_DIRECT")}
                                    </Button>
                                    <Button
                                        color="secondary"
                                        variant="outlined"
                                        onClick={e => applyPerceptechDeposit(row.original)}
                                    >
                                        {t("DEPOSIT_TYPE_PERCEPTECH")}
                                    </Button>
                                </div>
                            ) : null
                        }
                    }
                ]}
                defaultPageSize={10}
                noDataText={t("DEPOSIT_LIST_NO_RESULTS_FOUND")}
                pageText={t("REACT_TABLE_PAGE_TEXT")}
                ofText={t("REACT_TABLE_OF_TEXT")}
                rowsText={t("REACT_TABLE_ROWS_TEXT")}
            />
        </FuseAnimate>
    );
}

export default withRouter(DepositsList);
