import {combineReducers} from 'redux';
import deposits from './deposits.reducer';

const reducer = combineReducers({
    deposits,
});

export default reducer;
