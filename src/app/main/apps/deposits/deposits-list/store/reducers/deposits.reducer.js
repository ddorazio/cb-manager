import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    entities: null,
    searchText: '',
    routeParams: {},
    dialog: {
        props: {
            open: false
        },
        deposit: null,
        option: null
    },
};

const depositsReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_DEPOSITS:
            {
                return {
                    ...state,
                    entities: action.payload,
                };
            }
        case Actions.SET_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText: action.searchText
                };
            }
        case Actions.OPEN_DEPOSIT_DIALOG:
            {
                return {
                    ...state,
                    dialog: {
                        props: {
                            open: true
                        },
                        deposit: action.deposit,
                        option: action.option
                    }
                };
            }
        case Actions.CLOSE_DEPOSIT_DIALOG:
            {
                return {
                    ...state,
                    dialog: {
                        props: {
                            open: false
                        },
                        deposit: null,
                        option: null
                    }
                };
            }
        case Actions.UPDATE_DEPOSIT:
            {
                var deposits = state.entities.slice();
                const index = deposits.indexOf(deposits.find(deposit => deposit.id === action.payload.id));
                deposits[index] = action.payload;
                return {
                    ...state,
                    entities: deposits
                }
            }
        default:
            {
                return state;
            }
    }
};

export default depositsReducer;

