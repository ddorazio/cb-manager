import { showMessage } from 'app/store/actions/fuse';
import DepositsAPI from '../../../DepositsAPI';
import i18n from 'i18n';

export const GET_DEPOSITS = '[DEPOSITS APP] GET DEPOSITS';
export const SET_SEARCH_TEXT = '[DEPOSITS APP] SET SEARCH TEXT';
export const UPDATE_DEPOSIT = '[DEPOSITS APP] UPDATE DEPOSIT';
export const OPEN_DEPOSIT_DIALOG = '[DEPOSITS APP] OPEN DEPOSIT DIALOG';
export const CLOSE_DEPOSIT_DIALOG = '[DEPOSITS APP] CLOSE DEPOSIT DIALOG';

export const getDeposits = () => {
    return async dispatch => {
        try {
            const deposits = await DepositsAPI.getDeposits();
            dispatch({
                type: GET_DEPOSITS,
                payload: deposits
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    };
}

export function setSearchText(event) {
    return {
        type: SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export const openDepositDialog = (deposit, option) => {
    return {
        type: OPEN_DEPOSIT_DIALOG,
        deposit: deposit,
        option: option
    }
}

export const closeDepositDialog = () => {
    return {
        type: CLOSE_DEPOSIT_DIALOG,
    }
}

export const updateDeposit = (deposit) => {
    return async dispatch => {
        try {
            const result = await DepositsAPI.updateDeposit(deposit);
            dispatch({
                type: UPDATE_DEPOSIT,
                payload: result
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    };
}