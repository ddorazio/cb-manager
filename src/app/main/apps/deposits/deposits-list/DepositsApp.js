import React, { useEffect, useRef } from 'react';
import { Icon, Tooltip, Fab } from '@material-ui/core';
import { FusePageSimple } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import { useTranslation } from 'react-i18next';
import DepositsList from './DepositsList';
import DepositsHeader from './DepositsHeader';
import DepositDialog from './DepositDialog';
import * as Actions from './store/actions';
import reducer from './store/reducers';

function DepositsApp(props) {

    const { t } = useTranslation('depositsApp');
    const pageLayout = useRef(null);
    const deposits = useSelector(({depositsApp}) => depositsApp.deposits)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(Actions.getDeposits());
    }, []);

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-20 sm:pb-20 h-full black_bg",
                    content: "flex flex-col h-full dark_bg",
                    leftSidebar: "w-256 border-0",
                    header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
                }}
                content={
                    <DepositsList />
                }
                header={
                    <DepositsHeader/>
                }
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
            <DepositDialog/>
        </React.Fragment>
    )
}

export default withReducer('depositsApp', reducer)(DepositsApp);