import React from 'react';
import { Redirect } from 'react-router-dom';
import i18next from 'i18next';

import en from './i18n/en';
import fr from './i18n/fr';

i18next.addResourceBundle('en', 'depositsApp', en);
i18next.addResourceBundle('fr', 'depositsApp', fr);

export const DepositsAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes: [
        {
            path: '/apps/deposits/:id',
            component: React.lazy(() => import('./deposits-list/DepositsApp'))
        },
        {
            path: '/apps/deposits',
            component: () => <Redirect to="/apps/deposits/all" />
        }
    ]
};