import React from 'react';
import { useTranslation } from 'react-i18next';

const DepositStatus = (props) => {
    const { t } = useTranslation('depositsApp');

    switch (props.code) {
        case 'WAITING':
            return <div className="inline text-12 rounded-full py-4 px-8 truncate bg-orange text-white">{t('DEPOSIT_WAITING')}</div>
        case 'DEPOSITED':
            return <div className="inline text-12 rounded-full py-4 px-8 truncate bg-green text-white">{t('DEPOSIT_DEPOSITED')}</div>
        case 'ERROR':
            return <div className="inline text-12 rounded-full py-4 px-8 truncate bg-red text-white">{t('DEPOSIT_ERROR')}</div>
        case 'AUTO_DECLINED':
            return <div className="inline text-12 rounded-full py-4 px-8 truncate bg-red text-white">{t('DEPOSIT_AUTO_DECLINED')}</div>
    }
}

export default DepositStatus;

