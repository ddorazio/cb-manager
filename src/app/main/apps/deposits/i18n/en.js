const locale = {
    DEPOSIT_HEADER: "Deposits",
    DEPOSIT_SEARCH: "Search",

    DEPOSIT_LIST_NO_RESULTS_FOUND: "No deposit found.",
    DEPOSIT_LIST_CLIENT_NO: "Client #",
    DEPOSIT_LIST_LAST_NAME: "Last Name",
    DEPOSIT_LIST_FIRST_NAME: "First Name",
    DEPOSIT_LIST_BANK_ACCOUNT_NB: "Bank Account No.",
    DEPOSIT_LIST_AMOUNT: "Amount",
    DEPOSIT_LIST_STATUS: "Status",

    DEPOSIT_TYPE_DIRECT: "Direct Deposit",
    DEPOSIT_TYPE_PERCEPTECH: "Perceptech Deposit",
    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "of",
    REACT_TABLE_ROWS_TEXT: "rows",

    DEPOSIT_DIALOG_TEXT: "Are you sure you want to deposit %AMT% to %NAME% by %TYPE% ?",
    DEPOSIT_DIALOG_AGREE: "Yes",
    DEPOSIT_DIALOG_DISAGREE: "No",

    DEPOSIT_WAITING: 'Waiting',
    DEPOSIT_DEPOSITED: 'Deposited',
    DEPOSIT_ERROR: 'Error',
    DEPOSIT_AUTO_DECLINED: 'Auto-declined',

    INTERAC_CONFIRMATION_CODE: 'Interac confirmation code',

    DEPOSIT_REFUSED_HEADER: "Message",
    DEPOSIT_REFUSED_CANCEL: "Close",
    DEPOSIT_REFUSED_MESSAGE: "The loan contains payment dates that have already been submitted for pre-authorization. A new loan must be created for this request.",
}

export default locale;