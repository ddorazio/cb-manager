const locale = {
    DEPOSIT_HEADER: "Dépots",
    DEPOSIT_SEARCH: "Rechercher",

    DEPOSIT_LIST_NO_RESULTS_FOUND: "Aucun dépôt trouvé.",
    DEPOSIT_LIST_CLIENT_NO: "# Client",
    DEPOSIT_LIST_LAST_NAME: "Nom",
    DEPOSIT_LIST_FIRST_NAME: "Prénom",
    DEPOSIT_LIST_BANK_ACCOUNT_NB: "Compte Bancaire",
    DEPOSIT_LIST_AMOUNT: "Montant",
    DEPOSIT_LIST_STATUS: "Status",

    DEPOSIT_TYPE_DIRECT: "Dépôt Direct",
    DEPOSIT_TYPE_PERCEPTECH: "Dépôt Perceptech",

    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "de",
    REACT_TABLE_ROWS_TEXT: "lignes",

    DEPOSIT_DIALOG_TEXT: "Êtes-vous sûr de vouloir faire le depôt de %AMT% à %NAME% par %TYPE% ?",
    DEPOSIT_DIALOG_AGREE: "Oui",
    DEPOSIT_DIALOG_DISAGREE: "Non",

    DEPOSIT_WAITING: 'En attente',
    DEPOSIT_DEPOSITED: 'Déposé',
    DEPOSIT_ERROR: 'Erreur',
    DEPOSIT_AUTO_DECLINED: 'Auto-refusé',

    INTERAC_CONFIRMATION_CODE: 'Code de confirmation Interac',

    DEPOSIT_REFUSED_HEADER: "Message",
    DEPOSIT_REFUSED_CANCEL: "Fermer",
    DEPOSIT_REFUSED_MESSAGE: "Le prêt contient des dates de paiement qui ont déjà été soumises pour une pré-autorisation. Un nouveau prêt doit être créé pour cette demande.",
}

export default locale;