import React, { useEffect, useRef } from 'react';
import { FusePageSimple } from '@fuse';
import {Box, Icon, Typography } from '@material-ui/core';
import { FuseAnimate } from '@fuse';

function SuperSetFrame(props) {

    const pageLayout = useRef(null);

    useEffect(() => {
        var myFrame = document.getElementById('superset_iframe');

        myFrame.contentWindow.foo = function(){
            console.log ("Look at me, executed inside an iframe!");
        }
    },[document.getElementById('superset_iframe')])

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-20 sm:pb-20 h-full black_bg",
                    content: "flex flex-col h-full dark_bg",
                    leftSidebar: "w-256 border-0",
                    header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
                }}
                header={
                    <div id="Superset-Header" className="flex flex-1 items-center justify-between p-8 sm:p-24">

                        <div className="flex flex-shrink items-center sm:w-224">
        
                            <div className="flex items-center">
                                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                    <Typography variant="h6" className="hidden sm:flex">Superset</Typography>
                                </FuseAnimate>
                            </div>
                        </div>
                    </div>
                }
                content={
                    <Box height="100%">
                        <iframe id="superset_iframe" src="https://superset.creditbook.ca/login/" title="Superset" style={{width:"100%", height:"100%"}}/>
                    </Box>
                }
                /*leftSidebarContent={
                    <CustomersSidebarContent/>
                }*/
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
            

        </React.Fragment>
        
    )
}

export default SuperSetFrame