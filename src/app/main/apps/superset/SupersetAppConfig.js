import React from 'react';
//import i18next from 'i18next';
import { Redirect } from 'react-router-dom';
// import enLoanSuperset from './i18n/enLoanSuperset';
// import frLoanSuperset from './i18n/frLoanSuperset';

// i18next.addResourceBundle('en', 'supersetApp', enLoanSuperset);
// i18next.addResourceBundle('fr', 'supersetApp', frLoanSuperset);

export const SupersetAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes: [
        {
            path: '/apps/superset',
            component: React.lazy(() => import('./Superset'))
        },

    ]
};
