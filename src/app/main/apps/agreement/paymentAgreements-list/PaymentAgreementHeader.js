import React from 'react';
import { Icon, Input, Paper, Typography } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import { FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';
import { useTranslation } from 'react-i18next';

function PaymentAgreementHeader(props) {
    const { t } = useTranslation('agreementApp');
    const dispatch = useDispatch();
    const mainTheme = useSelector(({ fuse }) => fuse.settings.mainTheme);
    const searchText = useSelector(({ agreementsApp }) => agreementsApp.agreement.searchText);
    return (
        <div className="flex flex-1 items-center justify-between p-8 sm:p-24">

            <div className="flex flex-shrink items-center sm:w-300">

                <div className="flex items-center">
                    <FuseAnimate animation="transition.expandIn" delay={300}>
                        <Icon className="text-32 mr-12">account_balance</Icon>
                    </FuseAnimate>
                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                        <Typography variant="h6" className="hidden sm:flex">{t("AGREEMENT_HEADER")}</Typography>
                    </FuseAnimate>
                </div>
            </div>

            <div className="flex items-center sm:w-512 mr-64">

                <ThemeProvider theme={mainTheme}>
                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                        <Paper className="flex p-4 items-center w-full max-w-512 px-8 py-4 pr-85" elevation={1}>

                            <Icon className="mr-8" color="action">search</Icon>
                            <Input
                                placeholder={t("AGREEMENT_SEARCH")}
                                className="flex flex-1"
                                disableUnderline
                                fullWidth
                                value={searchText}
                                inputProps={{
                                    'aria-label': 'Search'
                                }}
                            //onChange={ev => dispatch(Actions.setSearchText(ev))}
                            />
                        </Paper>
                    </FuseAnimate>
                </ThemeProvider>
            </div>
        </div>
    );
}

export default PaymentAgreementHeader;
