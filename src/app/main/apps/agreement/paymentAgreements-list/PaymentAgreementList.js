import React, { useEffect, useState } from 'react';
import { Button, Link } from '@material-ui/core';
import { FuseUtils, FuseAnimate, FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import { withRouter } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
// import DepositStatus from '../components/DepositStatus';
import { openDepositDialog } from '../store/actions/agreement.actions';
import localStorageService from 'app/services/localStorageService';
import * as Actions from '../store/actions';

function PaymentAgreementList(props) {

    const language = localStorageService.getLanguage()
    const { t } = useTranslation('agreementApp');
    const dispatch = useDispatch();
    const agreements = useSelector(({ agreementsApp }) => agreementsApp.agreement.agreements)
    //const searchText = useSelector(({ agreementsApp }) => agreementsApp.deposits.searchText);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        setFilteredData(agreements);
    }, [agreements]);

    // useEffect(() => {
    //     function getFilteredArray(entities, searchTet) {
    //         const arr = Object.keys(entities).map((id) => entities[id]);
    //         if (searchText.length === 0) {
    //             return arr;
    //         }
    //         return FuseUtils.filterArrayByString(arr, searchText);
    //     }

    //     if (deposits) {
    //         setFilteredData(getFilteredArray(deposits, searchText));
    //     }
    // }, [deposits, searchText]);

    const confirmDeposit = (agreement) => {
        dispatch(Actions.openAgreementDialog(agreement));
    }

    if (!filteredData) {
        return <FuseLoading />;
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                data={filteredData}
                columns={[
                    {
                        Header: t("AGREEMENT_LIST_FILE_NO"),
                        accessor: "idLoanPayment"
                    },
                    {
                        Header: t("AGREEMENT_LIST_FIRST_NAME"),
                        accessor: "firstName"
                    },
                    {
                        Header: t("AGREEMENT_LIST_LAST_NAME"),
                        accessor: "lastName"
                    },
                    {
                        Header: t("AGREEMENT_LIST_PAYMENT_DATE"),
                        accessor: "paymentDateFormatted"
                    },
                    {
                        Header: t("AGREEMENT_LIST_AMOUNT"),
                        Cell: row => `${row.original.paymentAmount} $`
                    },
                    {
                        Header: t("AGREEMENT_LIST_CONFIRMATION_DATE"),
                        accessor: "confirmationDate"
                    },
                    {
                        Header: "Actions",
                        className: "pl-0",
                        width: 240,
                        Cell: row => {
                            return row.original.confirmationDate === "" ? (
                                <div className="flex items-center">
                                    <Button
                                        color="secondary"
                                        variant="outlined"
                                        className="mr-4"
                                        onClick={e => confirmDeposit(row.original)}
                                    >
                                        {t("AGREEMENT_LIST_CONFIRM_DEPOSIT")}
                                    </Button>
                                </div>
                            ) : null
                        }
                    }
                ]}
                defaultPageSize={10}
                noDataText={t("AGREEMENT_LIST_NO_RESULTS_FOUND")}
                pageText={t("REACT_TABLE_PAGE_TEXT")}
                ofText={t("REACT_TABLE_OF_TEXT")}
                rowsText={t("REACT_TABLE_ROWS_TEXT")}
            />
        </FuseAnimate>
    );
}

export default withRouter(PaymentAgreementList);
