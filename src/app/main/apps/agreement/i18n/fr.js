const locale = {

    AGREEMENT_FILES: "Dossiers entente de paiement",
    AGREEMENT_CLIENTS: "Clients",
    AGREEMENT_FOLLOW_UP: "Faire suivis",
    AGREEMENT_PAYMENTS: "Virement à recevoir",

    AGREEMENT_HEADER: "Virement à recevoir",
    AGREEMENT_SEARCH: "Rechercher",

    AGREEMENT_LIST_NO_RESULTS_FOUND: "Aucun entente de paiement trouvé.",
    AGREEMENT_LIST_FILE_NO: "No Dossier",
    AGREEMENT_LIST_LAST_NAME: "Nom",
    AGREEMENT_LIST_FIRST_NAME: "Prénom",
    AGREEMENT_LIST_PAYMENT_DATE: "Date du paiment",
    AGREEMENT_LIST_AMOUNT: "Montant",
    AGREEMENT_LIST_CONFIRMATION_DATE: "Date de dépôt",

    AGREEMENT_LIST_CONFIRM_DEPOSIT: "Confirmer Dépôt",

    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "de",
    REACT_TABLE_ROWS_TEXT: "lignes",

    AGREEMENT_DEPOSIT_DIALOG_CONFIRM: "Veuillez confirmer la date du dépôt",
    AGREEMENT_DEPOSIT_DIALOG_DEPOSIT_DATE: "Date de dépôt",
    AGREEMENT_DEPOSIT_DIALOG_AGREE: "Confirmer",
    AGREEMENT_DEPOSIT_DIALOG_DISAGREE: "Annuler"
}

export default locale;