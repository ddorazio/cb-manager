const locale = {
    AGREEMENT_FILES: "Payment agreement files",
    AGREEMENT_CLIENTS: "Clients",
    AGREEMENT_FOLLOW_UP: "Need follow up",
    AGREEMENT_PAYMENTS: "Transfers to receive",

    AGREEMENT_HEADER: "Transfers to receive",
    AGREEMENT_SEARCH: "Search",

    AGREEMENT_LIST_NO_RESULTS_FOUND: "Aucun entente de paiement trouvé.",
    AGREEMENT_LIST_FILE_NO: "File No",
    AGREEMENT_LIST_LAST_NAME: "Last Name",
    AGREEMENT_LIST_FIRST_NAME: "First Name",
    AGREEMENT_LIST_PAYMENT_DATE: "Payment date",
    AGREEMENT_LIST_AMOUNT: "Amount",
    AGREEMENT_LIST_CONFIRMATION_DATE: "Deposited date",

    AGREEMENT_LIST_CONFIRM_DEPOSIT: "Confirm Deposit",

    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "of",
    REACT_TABLE_ROWS_TEXT: "rows",

    AGREEMENT_DEPOSIT_DIALOG_CONFIRM: "Please confirm the date for the deposit",
    AGREEMENT_DEPOSIT_DIALOG_DEPOSIT_DATE: "Deposited date",
    AGREEMENT_DEPOSIT_DIALOG_AGREE: "Confirm",
    AGREEMENT_DEPOSIT_DIALOG_DISAGREE: "Cancel"
}

export default locale;