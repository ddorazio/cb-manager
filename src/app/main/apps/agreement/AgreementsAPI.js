import { getProtected, putProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class AgreementsAPI {
    static getAgreements = async () => {
        return await getProtected(new URL(`/api/Agreement/List`, settingsConfig.apiPath));
    }

    static updateAgreement = async (agreement) => {
        return await putProtected(new URL(`/api/Agreement/Update`, settingsConfig.apiPath), agreement);
    }
}

export default AgreementsAPI;