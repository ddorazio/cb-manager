import React, { useState } from 'react';
import { Button, Dialog, DialogActions, TextField, InputLabel, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { updateDeposit, closeAgreementDialog } from '../store/actions/agreement.actions';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

/* DatePicker */
import localStorageService from 'app/services/localStorageService';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment'
import moment from "moment";
import "moment/locale/fr";

moment.locale(localStorageService.getLanguage())

function DepositConfirmationDialog(props) {

    const { t } = useTranslation('agreementApp');
    const dispatch = useDispatch();
    const [interacCode, setInteracCode] = useState('');
    const dialog = useSelector(({ agreementsApp }) => agreementsApp.agreement.dialog)

    const [state, setState] = useState({
        depositedDate: null
    });

    const closeDialog = () => {
        resetForm();
        dispatch(closeAgreementDialog());
    }

    const applyChange = () => {
        dispatch(updateDeposit({
            ...dialog.agreement,
            depositedDate: state.depositedDate
        }));
        closeDialog();
    }

    const canSubmit = () => {
        return state.depositedDate;
    }
    function resetForm() {
        setState({
            ...state,
            depositedDate: null
        });
    }

    return (
        <Dialog
            open={dialog.open}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
                {t('AGREEMENT_DEPOSIT_DIALOG_CONFIRM')
                    .replace('$0', `${dialog.paymentAmount}$`)
                    .replace('$1', dialog.firstName)}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                </DialogContentText>

                <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale={localStorageService.getLanguage()}>
                    <KeyboardDatePicker
                        className="w-full"
                        //disableToolbar
                        minDate={new Date()}
                        format="DD/MM/YYYY"
                        inputVariant="outlined"
                        id="frequency_date"
                        name="frequency_date"
                        autoOk
                        variant="inline"
                        inputVariant="outlined"
                        InputAdornmentProps={{ position: "start" }}
                        label={t("AGREEMENT_DEPOSIT_DIALOG_DEPOSIT_DATE")}
                        value={state.depositedDate}
                        onChange={e => setState({
                            ...state,
                            depositedDate: e
                        })}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                </MuiPickersUtilsProvider>
            </DialogContent>
            <DialogActions>
                <Button onClick={closeDialog} color="primary">
                    {t('AGREEMENT_DEPOSIT_DIALOG_DISAGREE')}
                </Button>
                <Button onClick={applyChange} color="secondary" autoFocus disabled={!canSubmit()}>
                    {t('AGREEMENT_DEPOSIT_DIALOG_AGREE')}
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default DepositConfirmationDialog