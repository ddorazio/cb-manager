import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    agreements: [],
    searchText: '',
    dialog: {
        open: false,
        agreement: null
    }
};

const agreementsReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_AGREEMENTS:
            {
                return {
                    ...state,
                    agreements: action.payload,
                };
            }
        case Actions.OPEN_AGREEMENT_DIALOG:
            {
                return {
                    ...state,
                    dialog: {
                        open: true,
                        agreement: action.agreement
                    }
                };
            }
        case Actions.CLOSE_AGREEMENT_DIALOG:
            {
                return {
                    ...state,
                    dialog: {
                        open: false
                    }
                };
            }
        case Actions.UPDATE_PAYMENT:
            {
                var agreements = state.agreements.slice();
                const index = agreements.indexOf(agreements.find(agr => agr.idLoanPayment === action.payload.idLoanPayment));
                agreements[index] = action.payload;
                return {
                    ...state,
                    agreements: agreements
                }
            }
        default:
            {
                return state;
            }
    }
};

export default agreementsReducer;

