import { combineReducers } from 'redux';
import agreement from './agreement.reducer';

const reducer = combineReducers({
    agreement,
});

export default reducer;
