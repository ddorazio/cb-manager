import { showMessage } from 'app/store/actions/fuse';
import AgreementsAPI from '../../AgreementsAPI';
import i18n from 'i18n';

export const GET_AGREEMENTS = 'GET_AGREEMENTS';
export const OPEN_AGREEMENT_DIALOG = 'OPEN_AGREEMENT_DIALOG';
export const CLOSE_AGREEMENT_DIALOG = 'CLOSE_AGREEMENT_DIALOG';
export const UPDATE_PAYMENT = 'UPDATE_PAYMENT';

export const getAgreements = () => {
    return async dispatch => {
        try {
            const agreements = await AgreementsAPI.getAgreements();
            dispatch({
                type: GET_AGREEMENTS,
                payload: agreements
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    };
}

export const openAgreementDialog = (agreement) => {
    return {
        type: OPEN_AGREEMENT_DIALOG,
        agreement: agreement
    }
}

export const closeAgreementDialog = () => {
    return {
        type: CLOSE_AGREEMENT_DIALOG,
    }
}

export const updateDeposit = (agreement) => {
    return async dispatch => {
        try {
            const result = await AgreementsAPI.updateAgreement(agreement);
            dispatch({
                type: UPDATE_PAYMENT,
                payload: result
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    };
}