import React from 'react';
import { Redirect } from 'react-router-dom';
import i18next from 'i18next';
import en from './i18n/en';
import fr from './i18n/fr';

i18next.addResourceBundle('en', 'agreementApp', en);
i18next.addResourceBundle('fr', 'agreementApp', fr);

export const AgreementAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes: [
        {
            path: '/apps/agreement/:id',
            component: React.lazy(() => import('./paymentAgreements-list/PaymentAgreementApp'))
        },
        {
            path: '/apps/agreement',
            component: () => <Redirect to="/apps/agreement/all" />
        },
    ]
};
