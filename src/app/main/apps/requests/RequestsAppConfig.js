import React from 'react';
import i18next from 'i18next';
import { Redirect } from 'react-router-dom';
import enLoanRequests from './i18n/enLoanRequests';
import frLoanRequests from './i18n/frLoanRequests';

i18next.addResourceBundle('en', 'requestsApp', enLoanRequests);
i18next.addResourceBundle('fr', 'requestsApp', frLoanRequests);

export const RequestsAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes: [
        {
            path: '/apps/requests/:id',
            component: React.lazy(() => import('./requests-list/RequestsApp'))
        },
        {
            path: '/apps/requests',
            component: () => <Redirect to="/apps/requests/all" />
        }
    ]
};
