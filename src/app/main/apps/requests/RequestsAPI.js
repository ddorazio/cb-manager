import { getProtected, postProtected, deleteProtected, putProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class RequestsAPI {
    static getRequests = async () => {
        return await getProtected(new URL("/api/LoanRequest/List", settingsConfig.apiPath));
    }

    static getClientRequests = async (clientId) => {
        return await getProtected(new URL(`/api/LoanRequest/ListProfile?clientId=${clientId}`, settingsConfig.apiPath));
    }

    static updateRequest = async (request) => {
        return await putProtected(new URL("/api/LoanRequest/Update", settingsConfig.apiPath), request)
    }

    static resendAllIBV = async () => {
        return await getProtected(new URL("/api/IBV/ResendAll", settingsConfig.apiPath));
    }
}

export default RequestsAPI