const locale = {
    LOAN_REQUESTS_HEADER: "Demandes de prêt",
    LOAN_SEARCH: "Rechercher",
    LOAN_ADD_NEW: "Ajouter un prêt",

    LOAN_SIDEBAR_PRE_APPROVED_REQUESTS: "Pré-approuvé",
    LOAN_SIDEBAR_ALL_REQUESTS: "Toutes les demandes",
    LOAN_SIDEBAR_ACCEPTED_REQUESTS: "Demandes approuvées",
    LOAN_SIDEBAR_NEW_REQUESTS: "Nouvelles demandes",
    LOAN_SIDEBAR_WAITING: "Demandes en attente",
    LOAN_SIDEBAR_DENIED: "Demandes refusées",
    LOAN_SIDEBAR_CANCELLED: "Demandes annulées",
    LOAN_SIDEBAR_AUTO_DECLINED: "Refus automatique",

    LOAN_LIST_NO_RESULTS_FOUND: "Aucune demandes trouvées.",
    LOAN_LIST_CREATED_DATE: "Date de création",
    LOAN_LIST_STATUS: "Statut",
    LOAN_LIST_FIRST_NAME: "Prénom",
    LOAN_LIST_FAMILY_NAME: "Nom",
    LOAN_LIST_CITY: "Ville",
    LOAN_LIST_PROVINCE: "Province",
    LOAN_LIST_TEL: "Téléphone",
    LOAN_LIST_CEL: "Cellulaire",
    LOAN_LIST_EMAIL: "Courriel",
    LOAN_AMOUNT: "Montant",
    LOAN_IBV: "IBV",
    LOAN_IBV_COMPLETED: "Terminé",

    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "de",
    REACT_TABLE_ROWS_TEXT: "lignes",

    STATUS: "Statut",

    IBV_RESEND_ALL : 'Rappel IBV',
    IBV_RESEND_ALL_SUCCESS : 'Toutes les confirmations IBV ont été renvoyé'
}

export default locale;