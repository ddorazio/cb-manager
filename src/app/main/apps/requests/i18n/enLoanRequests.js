const locale = {
    LOAN_REQUESTS_HEADER: "Loan Requests",
    LOAN_SEARCH: "Search",
    LOAN_ADD_NEW: "Add a loan",

    LOAN_SIDEBAR_PRE_APPROVED_REQUESTS: "Pre-approved",
    LOAN_SIDEBAR_ALL_REQUESTS: "All requests",
    LOAN_SIDEBAR_ACCEPTED_REQUESTS: "Accepted requests",
    LOAN_SIDEBAR_NEW_REQUESTS: "New requests",
    LOAN_SIDEBAR_WAITING: "Waiting requests",
    LOAN_SIDEBAR_DENIED: "Denied requests",
    LOAN_SIDEBAR_CANCELLED: "Cancelled requests",
    LOAN_SIDEBAR_AUTO_DECLINED: "Auto-decline",

    LOAN_LIST_NO_RESULTS_FOUND: "No requests found.",
    LOAN_LIST_CREATED_DATE: "Created date",
    LOAN_LIST_STATUS: "Status",
    LOAN_LIST_FIRST_NAME: "First name",
    LOAN_LIST_FAMILY_NAME: "Last name",
    LOAN_LIST_CITY: "City",
    LOAN_LIST_PROVINCE: "Province",
    LOAN_LIST_TEL: "Telephone",
    LOAN_LIST_CEL: "Cellular",
    LOAN_LIST_EMAIL: "Email",
    LOAN_AMOUNT: "Amount",
    LOAN_IBV: "IBV",
    LOAN_IBV_COMPLETED: "Completed",

    REACT_TABLE_PAGE_TEXT: "Page",
    REACT_TABLE_OF_TEXT: "of",
    REACT_TABLE_ROWS_TEXT: "rows",

    STATUS: "Status",

    IBV_RESEND_ALL : 'Resend IBV',
    IBV_RESEND_ALL_SUCCESS : 'All IBV confirmation have been resend'
}

export default locale;