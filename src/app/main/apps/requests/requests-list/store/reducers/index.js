import {combineReducers} from 'redux';
import requests from './requests.reducer';

const reducer = combineReducers({
    requests,
});

export default reducer;
