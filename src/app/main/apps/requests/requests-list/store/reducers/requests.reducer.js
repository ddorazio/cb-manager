import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
    entities: null,
    searchText: '',
    routeParams: {},
    actionType: '',
    actionData: '',
    actionStatus: ''
};

const requestsReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_REQUESTS:
            {
                return {
                    ...state,
                    entities: action.payload,
                    routeParams: action.routeParams
                };
            }
        // case Actions.TOGGLE_IN_SELECTED_REQUESTS:
        //     {

        //         const requestId = action.requestId;

        //         let selectedRequestIds = [...state.selectedRequestIds];

        //         if (selectedRequestIds.find(id => id === requestId) !== undefined) {
        //             selectedRequestIds = selectedRequestIds.filter(id => id !== requestId);
        //         }
        //         else {
        //             selectedRequestIds = [...selectedRequestIds, requestId];
        //         }

        //         return {
        //             ...state,
        //             selectedRequestIds: selectedRequestIds
        //         };
        //     }
        // case Actions.OPEN_NEW_REQUEST_DIALOG:
        //     {
        //         return {
        //             ...state,
        //             requestDialog: {
        //                 type: 'new',
        //                 props: {
        //                     open: true
        //                 },
        //                 data: null
        //             }
        //         };
        //     }
        // case Actions.CLOSE_NEW_REQUEST_DIALOG:
        //     {
        //         return {
        //             ...state,
        //             requestDialog: {
        //                 type: 'new',
        //                 props: {
        //                     open: false
        //                 },
        //                 data: null
        //             }
        //         };
        //     }
        // case Actions.OPEN_EDIT_REQUEST_DIALOG:
        //     {
        //         return {
        //             ...state,
        //             requestDialog: {
        //                 type: 'edit',
        //                 props: {
        //                     open: true
        //                 },
        //                 data: action.data
        //             }
        //         };
        //     }
        // case Actions.CLOSE_EDIT_REQUEST_DIALOG:
        //     {
        //         return {
        //             ...state,
        //             requestDialog: {
        //                 type: 'edit',
        //                 props: {
        //                     open: false
        //                 },
        //                 data: null
        //             }
        //         };
        //     }
        default:
            {
                return state;
            }
    }
};

export default requestsReducer;
