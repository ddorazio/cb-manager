import axios from 'axios';
import settingsConfig from 'app/fuse-configs/settingsConfig'
import RequestsAPI from '../../../RequestsAPI'
import FuseUtils from '@fuse/FuseUtils';
import { showMessage } from 'app/store/actions/fuse';
import i18n from 'i18n';

export const GET_REQUESTS = '[REQUESTS APP] GET REQUESTS';
export const SET_SEARCH_TEXT = '[REQUESTS APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_REQUESTS = '[REQUESTS APP] TOGGLE IN SELECTED REQUESTS';
export const SELECT_ALL_REQUESTS = '[REQUESTS APP] SELECT ALL REQUESTS';
export const DESELECT_ALL_REQUESTS = '[REQUESTS APP] DESELECT ALL REQUESTS';
export const OPEN_NEW_REQUEST_DIALOG = '[REQUESTS APP] OPEN NEW REQUEST DIALOG';
export const CLOSE_NEW_REQUEST_DIALOG = '[REQUESTS APP] CLOSE NEW REQUEST DIALOG';
export const OPEN_EDIT_REQUEST_DIALOG = '[REQUESTS APP] OPEN EDIT REQUEST DIALOG';
export const CLOSE_EDIT_REQUEST_DIALOG = '[REQUESTS APP] CLOSE EDIT REQUEST DIALOG';
export const TOGGLE_STARRED_REQUEST = '[REQUESTS APP] TOGGLE STARRED REQUEST';
export const TOGGLE_STARRED_REQUESTS = '[REQUESTS APP] TOGGLE STARRED REQUESTS';
export const SET_REQUESTS_STARRED = '[REQUESTS APP] SET REQUESTS STARRED ';
export const REMOVE_REQUEST = '[REQUESTS APP] REMOVE REQUEST ';

export function getRequests(routeParams)
{
    return async dispatch => {
        try {
            const requests = await RequestsAPI.getRequests();
            dispatch({
                type: GET_REQUESTS,
                payload: requests
            });
        } catch (error) {
            dispatch(showMessage({ message: error, variant: "error" }));
        }
    };
}

export const resendAllIBV = () => {
    return async (dispatch) => {
        try {
            const result = await RequestsAPI.resendAllIBV();
            dispatch(showMessage({ message: i18n.t('IBV_RESEND_ALL_SUCCESS'), variant: 'success' }));
        } catch (error) {
            dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

// export function removeRequest(data) {
//     const request = axios.post(settingsConfig.apiPath + '/main/DeleteRequest', data);

//     return (dispatch) =>
//         request.then((response) => {

//             if (response.data.status === 1) {

//                 dispatch(showMessage({ message: 'Client supprimé', variant: "success" }));
//                 return dispatch({
//                     type: REMOVE_REQUEST,
//                     payload: data,
//                     status: response.data.status
//                 })

//             } else {

//                 dispatch(showMessage({ message: response.data.message, variant: "error" }));

//                 return dispatch({
//                     type: REMOVE_REQUEST,
//                     status: response.data.status
//                 })
//             }
//         }
//         );
// }

// export function setSearchText(event)
// {
//     return {
//         type      : SET_SEARCH_TEXT,
//         searchText: event.target.value
//     }
// }

// export function toggleInSelectedRequests(requestId)
// {
//     return {
//         type: TOGGLE_IN_SELECTED_REQUESTS,
//         requestId
//     }
// }

// export function selectAllRequests()
// {
//     return {
//         type: SELECT_ALL_REQUESTS
//     }
// }

// export function deSelectAllRequests()
// {
//     return {
//         type: DESELECT_ALL_REQUESTS
//     }
// }

// export function openNewRequestDialog()
// {
//     return {
//         type: OPEN_NEW_REQUEST_DIALOG
//     }
// }

// export function closeNewRequestDialog()
// {
//     return {
//         type: CLOSE_NEW_REQUEST_DIALOG
//     }
// }

// export function openEditRequestDialog(data)
// {
//     return {
//         type: OPEN_EDIT_REQUEST_DIALOG,
//         data
//     }
// }

// export function closeEditRequestDialog()
// {
//     return {
//         type: CLOSE_EDIT_REQUEST_DIALOG
//     }
// }
