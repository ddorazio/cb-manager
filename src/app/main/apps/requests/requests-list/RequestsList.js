import React, { useEffect, useState } from 'react';
import localStorageService from 'app/services/localStorageService';

import { Avatar, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate, FuseLoading } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import * as Actions from './store/actions';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { getStatusColor, getStatusFullName } from '../RequestStatus'

function RequestsList(props) {

    const language = localStorageService.getLanguage()
    const { t } = useTranslation('requestsApp');
    const dispatch = useDispatch();
    const requests = useSelector(({ requestsApp }) => requestsApp.requests.entities);
    const searchText = useSelector(({ requestsApp }) => requestsApp.requests.searchText);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchText) {
            const arr = Object.keys(entities).map((id) => entities[id]);
            if (searchText.length === 0) {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if (requests) {
            const param = props.match.params.id
            if (param && param !== "all") {
                setFilteredData(getFilteredArray(requests.filter(request => request.statusCode.includes(param.toUpperCase()) && request.statusCode != 'POSSIBLE_RENEWAL'), searchText));
            } else {
                setFilteredData(getFilteredArray(requests, searchText));
            }
        }
    }, [requests, searchText]);

    function validateDate(date) {

        var testDate = new Date(date);
        var maxDt = new Date("2100-01-01");
        var minDt = new Date("1899-01-01");

        return (testDate < maxDt && testDate > minDt)

    }

    if (!filteredData) {
        return <FuseLoading />;
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        style: {
                            background: rowInfo !== undefined ? rowInfo.original.potentialMatch === true ? 'grey' : '' : '',
                        },
                        onClick: (e, handleOriginal) => {
                            if (rowInfo) {
                                if (rowInfo.original.potentialMatch === true) {
                                    props.history.push(`/apps/customers/detail/${rowInfo.original.clientId}/match`)
                                } else {
                                    props.history.push(`/apps/customers/detail/${rowInfo.original.clientId}/requests`)
                                }
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        id: "requestedDate",
                        width: 170,
                        Header: t("LOAN_LIST_CREATED_DATE"),
                        accessor: d => {
                            return d.requestedDateFormat
                            // if (d.requestedDate && validateDate(d.requestedDate)) {
                            //     return d.requestedDate //moment(d.requestedDate).format("YYYY-MM-DD hh:mm")
                            // } else {
                            //     return "";
                            // }
                        },
                        filterable: true
                    },
                    {
                        Header: t("LOAN_LIST_FIRST_NAME"),
                        accessor: "clientFirstName",
                        filterable: true
                    },
                    {
                        Header: t("LOAN_LIST_FAMILY_NAME"),
                        accessor: "clientLastName",
                        filterable: true
                    },
                    {
                        id: "loanAmount",
                        Header: t("LOAN_AMOUNT"),
                        filterable: true,
                        accessor: d => {
                            return new Intl.NumberFormat(language + '-CA', { style: 'currency', currency: 'CAD' }).format(d.loanAmount)
                        }
                    },
                    {
                        id: "ibvCompleted",
                        isVisible: true,
                        Header: t("LOAN_IBV"),
                        filterable: true,
                        Cell: row => (
                            row.original.ibvCompleted ?
                                <div className={`inline text-12 rounded-full py-4 px-8 truncate bg-green text-white`}>{t("LOAN_IBV_COMPLETED")}
                                    <IconButton
                                        size="medium"
                                        className="pt-0 pb-0"
                                        onClick={() => window.open(row.original.ibvReport)}
                                    >
                                        <Icon>remove_red_eye</Icon>
                                    </IconButton>
                                </div>
                                : null
                        )
                    },
                    {
                        id: "statusCode",
                        Header: t("STATUS"),
                        filterable: true,
                        Cell: row => (
                            <div className={`inline text-12 rounded-full py-4 px-8 truncate ${getStatusColor(row.original.statusCode)} text-white`}>{getStatusFullName(row.original.statusCode)}</div>
                        )
                    }
                    // {
                    //     Header: "",
                    //     width: 50,
                    //     className: "pl-0",
                    //     Cell: row => (
                    //         <div className="flex items-center">
                    //             <IconButton
                    //                 onClick={(ev) => {
                    //                     ev.stopPropagation();
                    //                     props.history.push(`/apps/customers/detail/${row.original.clientId}`)
                    //                 }}
                    //             >
                    //                 <Icon>remove_red_eye</Icon>
                    //             </IconButton>
                    //         </div>
                    //     )
                    // }
                ]}
                defaultPageSize={10}
                noDataText={t("LOAN_LIST_NO_RESULTS_FOUND")}
                pageText={t("REACT_TABLE_PAGE_TEXT")}
                ofText={t("REACT_TABLE_OF_TEXT")}
                rowsText={t("REACT_TABLE_ROWS_TEXT")}
            />
        </FuseAnimate>
    );
}

export default withRouter(RequestsList);
