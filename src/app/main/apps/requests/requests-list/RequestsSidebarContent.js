import React from 'react';
import { Avatar, Divider, Icon, List, ListItem, ListItemText, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { FuseAnimate, NavLinkAdapter } from '@fuse';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
    listItem: {
        color: 'inherit!important',
        textDecoration: 'none!important',
        height: 40,
        width: 'calc(100% - 16px)',
        borderRadius: '0 20px 20px 0',
        paddingLeft: 24,
        paddingRight: 12,
        '&.active': {
            backgroundColor: theme.palette.secondary.main,
            color: theme.palette.secondary.contrastText + '!important',
            pointerEvents: 'none',
            '& .list-item-icon': {
                color: 'inherit'
            }
        },
        '& .list-item-icon': {
            marginRight: 16
        }
    }
}));

function RequestSidebarContent(props) {
    const { t } = useTranslation('requestsApp');
    const user = useSelector(({ requestsApp }) => requestsApp.user);
    const classes = useStyles(props);

    return (
        <div className="p-0 lg:p-24 lg:ltr:pr-4 lg:rtl:pl-4">
            <FuseAnimate animation="transition.slideLeftIn" delay={200}>
                <Paper className="rounded-0 shadow-none lg:rounded-8 lg:shadow-1">
                    <List>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/all'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">assignment</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_ALL_REQUESTS")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/preapproved'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">access_alarm</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_PRE_APPROVED_REQUESTS")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/new'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">star</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_NEW_REQUESTS")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/accepted'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">check_circle</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_ACCEPTED_REQUESTS")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/waiting'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">account_circle</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_WAITING")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/denied'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">remove_circle_outline</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_DENIED")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/cancelled'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">remove_circle_outline</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_CANCELLED")} disableTypography={true} />
                        </ListItem>
                        <ListItem
                            button
                            component={NavLinkAdapter}
                            to={'/apps/requests/autodeclined'}
                            activeClassName="active"
                            className={classes.listItem}
                        >
                            <Icon className="list-item-icon text-16" color="action">cancel</Icon>
                            <ListItemText className="truncate" primary={t("LOAN_SIDEBAR_AUTO_DECLINED")} disableTypography={true} />
                        </ListItem>
                    </List>
                </Paper>
            </FuseAnimate>
        </div>
    );
}

export default RequestSidebarContent;

