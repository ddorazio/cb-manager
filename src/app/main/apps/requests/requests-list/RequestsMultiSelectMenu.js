import React, {useState} from 'react';
import {Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList} from '@material-ui/core';
import * as Actions from './store/actions';
import {useDispatch, useSelector} from 'react-redux';

function RequestsMultiSelectMenu(props)
{
    const dispatch = useDispatch();
    const selectedRequestIds = useSelector(({requestsApp}) => requestsApp.requests.selectedRequestIds);

    const [anchorEl, setAnchorEl] = useState(null);

    function openSelectedRequestMenu(event)
    {
        setAnchorEl(event.currentTarget);
    }

    function closeSelectedRequestsMenu()
    {
        setAnchorEl(null);
    }

    return (
        <React.Fragment>
            <IconButton
                className="p-0"
                aria-owns={anchorEl ? 'selectedRequestsMenu' : null}
                aria-haspopup="true"
                onClick={openSelectedRequestMenu}
            >
                <Icon>more_horiz</Icon>
            </IconButton>
            <Menu
                id="selectedRequestsMenu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={closeSelectedRequestsMenu}
            >
                <MenuList>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.removeRequests(selectedRequestIds));
                            closeSelectedRequestsMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>delete</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Remove"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.setRequestsStarred(selectedRequestIds));
                            closeSelectedRequestsMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>star</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Starred"/>
                    </MenuItem>
                    <MenuItem
                        onClick={() => {
                            dispatch(Actions.setRequestsUnstarred(selectedRequestIds));
                            closeSelectedRequestsMenu();
                        }}
                    >
                        <ListItemIcon className="min-w-40">
                            <Icon>star_border</Icon>
                        </ListItemIcon>
                        <ListItemText primary="Unstarred"/>
                    </MenuItem>
                </MenuList>
            </Menu>
        </React.Fragment>
    );
}

export default RequestsMultiSelectMenu;

