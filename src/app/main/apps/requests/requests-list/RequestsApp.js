import React, { useEffect, useRef } from 'react';
import { Icon, Tooltip, Fab } from '@material-ui/core';
import { FusePageSimple } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import RequestsList from './RequestsList';
import RequestsHeader from './RequestsHeader';
import RequestsSidebarContent from './RequestsSidebarContent';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import { useTranslation } from 'react-i18next';

function RequestsApp(props) {

    const { t } = useTranslation('requestsApp');
    const dispatch = useDispatch();
    const pageLayout = useRef(null);
    const requestsApp = useSelector(({ requestsApp }) => requestsApp.requests);

    useEffect(() => {
        dispatch(Actions.getRequests(props.match.params));
    }, [dispatch, props.match.params]);

    useEffect(() => {

        if (requestsApp.actionType === "REMOVE_REQUEST" && requestsApp.actionData && requestsApp.actionStatus === 1) {
            dispatch(Actions.getRequests(props.match.params));
            requestsApp.actionData = null;
            requestsApp.actionStatus = null;
            requestsApp.actionType = null;
        }
    }, [requestsApp, dispatch, props.match.params]);

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-20 sm:pb-20 h-full black_bg",
                    content: "flex flex-col h-full dark_bg",
                    leftSidebar: "w-256 border-0",
                    header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
                }}
                header={
                    <RequestsHeader pageLayout={pageLayout} />
                }
                content={
                    <RequestsList />
                }
                leftSidebarContent={
                    <RequestsSidebarContent />
                }
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
            {/* <Tooltip title={t("LOAN_ADD_NEW")}>
                <Fab
                    color="secondary"
                    aria-label="add"
                    className="new"
                >
                    <Icon>person_add</Icon>
                </Fab>
            </Tooltip> */}

        </React.Fragment>
    )
}

export default withReducer('requestsApp', reducer)(RequestsApp);
