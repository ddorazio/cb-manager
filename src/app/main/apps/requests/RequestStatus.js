import i18n from 'i18n';

export const requestStatus = [
    {
        statusCode: "NEW",
        name: i18n.t('NEW'),
        color: "bg-blue"
    },
    {
        statusCode: "PREAPPROVED",
        name: i18n.t('PREAPPROVED'),
        color: "bg-green"
    },
    {
        statusCode: "ACCEPTED",
        name: i18n.t('ACCEPTED'),
        color: "bg-green"
    },
    {
        statusCode: "WAITING_MISSING_CONTRACT",
        name: i18n.t('WAITING_MISSING_CONTRACT'),
        color: "bg-orange",
    },
    {
        statusCode: "WAITING_MISSING_IBV",
        name: i18n.t('WAITING_MISSING_IBV'),
        color: "bg-orange",
    },
    {
        statusCode: "WAITING_MISSING_INFO",
        name: i18n.t('WAITING_MISSING_INFO'),
        color: "bg-orange",
    },
    {
        statusCode: "IN_COLLECTIONS",
        name: i18n.t('IN_COLLECTIONS'),
        color: "bg-red",
    },
    {
        statusCode: "DENIED",
        name: i18n.t('DENIED'),
        color: "bg-red",
        subStatus: [
            {
                statusCode: "DENIED_EXCESS_LOANS",
                name: i18n.t('DENIED_EXCESS_LOANS')
            }, {
                statusCode: "DENIED_ALREADY_REJECTED",
                name: i18n.t('DENIED_ALREADY_REJECTED')
            }, {
                statusCode: "DENIED_EXCESS_REQUESTS",
                name: i18n.t('DENIED_EXCESS_REQUESTS')
            }, {
                statusCode: "DENIED_NO_JOB",
                name: i18n.t('DENIED_NO_JOB')
            }, {
                statusCode: "DENIED_EXCESS_NSF",
                name: i18n.t('DENIED_EXCESS_NSF')
            }, {
                statusCode: "DENIED_COLLECTION",
                name: i18n.t('DENIED_COLLECTION')
            }, {
                statusCode: "DENIED_OTHER",
                name: i18n.t('DENIED_OTHER')
            },
            {
                statusCode: "DENIED_NO_CAPACITY",
                name: i18n.t('DENIED_NO_CAPACITY')
            },
            {
                statusCode: "DENIED_LOAN_IN_PROGRESS",
                name: i18n.t('DENIED_LOAN_IN_PROGRESS')
            },
            {
                statusCode: "DENIED_STOPPED_PAYMENTS",
                name: i18n.t('DENIED_STOPPED_PAYMENTS')
            }
        ]
    }, {
        statusCode: "CANCELLED_BY_CLIENT",
        name: i18n.t('CANCELLED_BY_CLIENT'),
        color: "bg-red"
    },
    {
        statusCode: "DUPLICATE",
        name: i18n.t('DUPLICATE'),
        color: "bg-red"
    },
    {
        statusCode: "AUTODECLINED",
        name: i18n.t('AUTO_DECLINED'),
        color: "bg-red"
    },

    {
        statusCode: "IN_PAYMENT_AGREEMENT",
        name: i18n.t('IN_PAYMENT_AGREEMENT'),
        color: "bg-red"
    },
    {
        statusCode: "BANKRUPTCY",
        name: i18n.t('BANKRUPTCY'),
        color: "bg-red"
    },
    {
        statusCode: "LOAN_PAY_IN_FULL",
        name: i18n.t('LOAN_PAY_IN_FULL'),
        color: "bg-red"
    },
    {
        statusCode: "FRAUDSTER",
        name: i18n.t('FRAUDSTER'),
        color: "bg-red"
    },
    {
        statusCode: "POSSIBLE_RENEWAL",
        name: i18n.t('POSSIBLE_RENEWAL'),
        color: "bg-red"
    },
    {
        statusCode: "WARNING",
        name: i18n.t('WARNING'),
        color: "bg-red"
    },

]

export const getStatusFullName = (statusCode) => {
    const status = requestStatus.find(el => el.statusCode == statusCode)
    if (status) return status.name
    const parentStatus = requestStatus.find(el => el.subStatus && el.subStatus.find(el2 => el2.statusCode == statusCode))
    if (parentStatus) {
        return parentStatus.name + " - " + parentStatus.subStatus.find(el => el.statusCode == statusCode).name
    }
    return ""
}

export const getStatusColor = (statusCode) => {
    const status = requestStatus.find(el => el.statusCode == statusCode || (el.subStatus && el.subStatus.find(el2 => el2.statusCode == statusCode)))
    if (status) return status.color
    return ""
}
