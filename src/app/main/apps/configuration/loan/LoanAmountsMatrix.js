import React, { useEffect } from 'react';
import { FuseAnimateGroup, FuseLoading } from '@fuse';
import MaterialTable from 'material-table';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Delete from '@material-ui/icons/Delete';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';
import { getLoanConfigurations, createLoanConfiguration, updateLoanConfiguration, deleteLoanConfiguration } from '../store/actions/configuration.actions';
import { Paper } from '@material-ui/core';
import { LoanFrequencyDesc, LoanFrequencySelect } from './LoanFrequency';
import { showMessage } from 'app/store/actions/fuse';

function LoanAmountsMatrix() {
    const { t } = useTranslation('configApp');

    const store = useSelector(({ configuration }) => configuration.loan.matrix);
    const dispatch = useDispatch();

    // Get a distinct list of amounts for adding new rows
    const amounts = [...new Set(store.data.map(config => config.loanAmount))];

    // We can delete a row only if there are more than one configured payment numbers
    // For the same amount/frequency combo
    const isRowDeletable = (rowData) => {
        var matches = store.data.filter(config =>
            config.loanFrequencyCode === rowData.loanFrequencyCode &&
            config.loanAmount === rowData.loanAmount
        );
        return matches.length >= 2;
    }

    const showError = message => {
        dispatch(showMessage({
            message: message,
            variant: 'error'
        }));
    }

    useEffect(() => {
        // Load initial data
        if (!store.loaded) {
            dispatch(getLoanConfigurations);
        }
    });

    if (!store.loaded) {
        return <FuseLoading />
    }

    return (
        <Paper>
            <MaterialTable
                className="w-full mb-16"
                title={t("AMOUNT_MATRIX")}
                columns={[
                    { title: '', field: 'id', hidden: true },
                    {
                        title: t("AMOUNT"),
                        field: 'loanAmount',
                        type: 'currency',
                        sorting: true,
                        defaultSort: 'asc',
                        cellStyle: { textAlign: 'left' },
                        editable: 'onAdd',
                        editComponent: props =>
                            <FormControl>
                                <InputLabel>{t("AMOUNT")}</InputLabel>
                                <Select
                                    style={{ width: 120 }}
                                    onChange={e => props.onChange(e.target.value)}
                                >
                                    {amounts.map(amount => <MenuItem value={amount}>${amount}</MenuItem>)}
                                </Select>
                            </FormControl>
                    },
                    {
                        title: t("FREQUENCY"),
                        field: 'loanFrequencyCode',
                        editable: 'onAdd',
                        render: rowData => <LoanFrequencyDesc code={rowData.loanFrequencyCode} />,
                        editComponent: props =>
                            <LoanFrequencySelect
                                style={{ width: 200 }}
                                onChange={e => props.onChange(e.target.value)}
                            />
                    },
                    {
                        title: t("NUMBER_OF_PAYMENT"),
                        field: 'totalPayments',
                        emptyValue: () => <span style={{ color: "#E41F26" }}>{t("CHOOSE_VALUE")}</span>
                    }
                ]}
                data={store.data}
                options={{
                    sorting: true,
                    search: false,
                    paging: false,
                    actionsColumnIndex: -1,
                    toolbar: true,
                    draggable: false
                }}

                editable={{
                    onRowAdd: (newData) =>
                        new Promise((resolve, reject) => {
                            var errorMessage = undefined;
                            if (!newData.loanAmount || !newData.loanFrequencyCode || !newData.totalPayments) {
                                errorMessage = t('MISSING_REQUIRED');
                            }
                            if (!parseInt(newData.totalPayments) || parseInt(newData.totalPayments) <= 0) {
                                errorMessage = 'Nombre de paiements invalide';
                            }
                            if (errorMessage) {
                                showError(errorMessage);
                                reject();
                                return;
                            }
                            setTimeout(() => {
                                dispatch(createLoanConfiguration(newData));
                                resolve();
                            }, 600);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                            var errorMessage = undefined;
                            if (!parseInt(newData.totalPayments) || parseInt(newData.totalPayments) <= 0) {
                                errorMessage = 'Nombre de paiements invalide';
                            }
                            if (errorMessage) {
                                showError(errorMessage);
                                reject();
                                return;
                            }
                            if (!newData.id) {
                                dispatch(createLoanConfiguration(newData));
                            } else {
                                dispatch(updateLoanConfiguration(newData));
                            }
                            resolve();
                        })
                }}

                actions={[
                    rowData => ({
                        icon: () => <Delete />,
                        hidden: !isRowDeletable(rowData),
                        onClick: (oldData) =>
                            new Promise(resolve => {
                                setTimeout(() => {
                                    dispatch(deleteLoanConfiguration(rowData.id));
                                    resolve();
                                }, 600);
                            })
                    })
                ]}

                localization={{
                    body: {
                        emptyDataSourceMessage: t("emptyDataSourceMessage"),
                        addTooltip: t("addTooltip"),
                        deleteTooltip: t("deleteTooltip"),
                        editTooltip: t("editTooltip"),
                        editRow: {
                            deleteText: t("deleteText"),
                            cancelTooltip: t("cancelTooltip"),
                            saveTooltip: t("saveTooltip"),
                        }
                    },
                    toolbar: {
                        nRowsSelected: t("nRowsSelected"),
                        searchTooltip: t("searchTooltip"),
                        searchPlaceholder: t("searchPlaceholder"),
                    },
                    pagination: {
                        labelDisplayedRows: t("labelDisplayedRows"),
                        labelRowsSelect: t("labelRowsSelect"),
                        labelRowsPerPage: t("labelRowsPerPage"),
                        firstTooltip: t("firstTooltip"),
                        previousTooltip: t("previousTooltip"),
                        nextTooltip: t("nextTooltip"),
                        lastTooltip: t("lastTooltip"),
                    }
                }}
            />
        </Paper>
    );
}

export default withReducer('configuration', reducer)(LoanAmountsMatrix);