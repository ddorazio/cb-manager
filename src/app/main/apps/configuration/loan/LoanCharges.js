import React, { useEffect } from 'react';
import { forwardRef } from 'react';
import AddCircle from '@material-ui/icons/AddCircle';
import { FuseAnimateGroup, FuseLoading } from '@fuse';
import withReducer from 'app/store/withReducer';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import CurrencyField from 'app/components/CurrencyField';
import MaterialTable from 'material-table';
import { Paper, Select, FormControl, InputLabel, MenuItem, TextField, Icon } from '@material-ui/core';
import reducer from '../store/reducers';
import { showMessage } from 'app/store/actions/fuse';
import {
    getLoanChargeConfigurations,
    createLoanChargeConfiguration,
    deleteLoanChargeConfiguration
} from '../store/actions/configuration.actions';
import localStorageService from 'app/services/localStorageService'
const language = localStorageService.getLanguage();

function ChargeType(props) {
    switch (props.code) {
        case 'nsf':
            return language === "fr" ? <span>NSF</span> : <span>NSF</span>
        case 'delayed':
            return language === "fr" ? <span>Paiement reporté</span> : <span>Delayed payment</span>
        case 'interac_deposit':
            return language === "fr" ? <span>Dépôt Interac</span> : <span>Interact Deposit</span>
        case 'auto_withdrawal_amount':
            return language === "fr" ? <span>Prélèvements</span> : <span>Auto Withdrawal</span>
    }
}

function EffectiveDate(props) {
    var isValid = validateEffectiveDate(props.value);
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    return <TextField
        required
        type="date"
        value={props.value}
        //defaultValue={new Date()}
        inputProps={{ min: yyyy + '-' + mm + '-' + dd }}
        onChange={e => props.onChange(e.target.value)}
        error={!isValid}
        InputProps={{
            startAdornment: (
                <Icon>date_range</Icon>
            )
        }}
        InputLabelProps={{
            shrink: true
        }}
    />
}

function validateEffectiveDate(date) {
    // Date should be in the future
    try {
        return new Date(date) > new Date();
    } catch {
        return false;
    }
}


function LoanCharges(props) {
    const { t } = useTranslation('configApp');
    const dispatch = useDispatch();
    const store = useSelector(({ configuration }) => configuration.loan.charges);

    useEffect(() => {
        if (!store.loaded) {
            dispatch(getLoanChargeConfigurations);
        }
    });

    function ChargeTypeSelectList(props) {
        return <FormControl>
            <InputLabel>Type</InputLabel>
            <Select
                style={{ width: 200 }}
                {...props}
            >
                <MenuItem value='nsf'>{t('CHARGES_NSF')}</MenuItem>
                <MenuItem value='delayed'>{t('CHARGES_DEFER_PAYMENT')}</MenuItem>
                <MenuItem value='interac_deposit'>{t('CHARGES_INTERAC')}</MenuItem>
                <MenuItem value='auto_withdrawal_amount'>{t('CHARGES_AUTO_WITHDRAWAL')}</MenuItem>
            </Select>
        </FormControl>
    }

    /*     if (!store.loaded) {
            return <FuseLoading />
        } */

    return (
        <Paper className="mb-16 mr-16">
            <MaterialTable
                className="w-full mb-16"
                title={t('FEES_CONFIG')}
                columns={[
                    { field: 'id', hidden: true },
                    {
                        title: 'Type',
                        field: 'code',
                        render: rowData => <ChargeType code={rowData.code} />,
                        editComponent: props =>
                            <ChargeTypeSelectList onChange={e => props.onChange(e.target.value)} />
                    },
                    {
                        title: t('AMOUNT'),
                        type: 'currency',
                        field: 'amountCharged',
                        cellStyle: { textAlign: 'left' },
                        editComponent: CurrencyField
                    },
                    {
                        title: t("EFFECTIVE_DATE"),
                        field: 'effectiveDate',
                        editComponent: EffectiveDate
                    }
                ]}
                data={store.data}
                options={{
                    paging: false,
                    actionsColumnIndex: -1,
                    draggable: false,
                    search: false
                }}
                icons={{
                    Add: forwardRef((props, ref) => <AddCircle {...props} ref={ref} color="secondary" />),

                }}
                editable={{
                    onRowAdd: newData => new Promise((resolve, reject) => {
                        setTimeout(() => {
                            if (!newData.code || !newData.amountCharged || !newData.effectiveDate) {
                                dispatch(showMessage({
                                    message: t('MISSING_REQUIRED'),
                                    variant: 'error'
                                }));
                                reject();
                                return;
                            }
                            if (!validateEffectiveDate(newData.effectiveDate)) {
                                dispatch(showMessage({
                                    message: t('MISSING_DATE_MESSAGE'),
                                    variant: 'error'
                                }));
                                reject();
                                return;
                            }
                            dispatch(createLoanChargeConfiguration(newData));
                            resolve();
                        }, 600);
                    })
                    // onRowDelete: newData => new Promise(resolve => {
                    //     setTimeout(() => {
                    //         dispatch(deleteLoanChargeConfiguration(newData.id));
                    //         resolve();
                    //     }, 600);
                    // })
                }}
                localization={{
                    body: {
                        emptyDataSourceMessage: t("emptyDataSourceMessage"),
                        addTooltip: t("addTooltip"),
                        deleteTooltip: t("deleteTooltip"),
                        editTooltip: t("editTooltip"),
                        editRow: {
                            deleteText: t("deleteText"),
                            cancelTooltip: t("cancelTooltip"),
                            saveTooltip: t("saveTooltip"),
                        }
                    },
                }}
            />
        </Paper>
    )
}

export default withReducer('configuration', reducer)(LoanCharges);