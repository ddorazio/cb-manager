import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import {FuseAnimateGroup} from '@fuse';
import MaterialTable from 'material-table';
import {useTranslation } from 'react-i18next';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';
import { LoanFrequencyDesc } from './LoanFrequency';
import { getLoanFrequencies, updateLoanFrequency } from '../store/actions';
import { showMessage } from 'app/store/actions';

function LoanTermsList() {
    const { t } = useTranslation('configApp');
    const store = useSelector(({ configuration }) => configuration.loan.terms);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!store.loaded) {
            dispatch(getLoanFrequencies);
        }
    });
 
    return (
        <Paper className="mb-16 mr-16">
                <MaterialTable
                    className="w-full mb-16 mr-16"
                    title={t("PAYMENT_START_DATE")}
                    columns={[
                        { title: '', field: 'id', hidden: true },
                        { 
                            title: t("FREQUENCY"),
                            field: 'code', 
                            editable: 'never' ,
                            render: rowData => <LoanFrequencyDesc code={rowData.code} />
                        },
                        // { 
                        //     title: t("PAYMENT_DELAY"), 
                        //     field: 'minimumDaysBeforeDeposit' 
                        // }
                    ]}
                    data={store.data}
                    options={{
                        sorting: true,
                        search: false,
                        paging: false,
                        actionsColumnIndex: -1,
                        toolbar: true,
                        draggable: false
                    }}

                   /* editable={{
                    
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve, reject) => {
                                setTimeout(() => {
                                    if (!newData.minimumDaysBeforeDeposit) {
                                        dispatch(showMessage({
                                            message: t('MISSING_REQUIRED'),
                                            variant: 'error'
                                        }));
                                        reject();
                                        return;
                                    }
                                    if (!parseInt(newData.minimumDaysBeforeDeposit) || parseInt(newData.minimumDaysBeforeDeposit) <= 0) {
                                        dispatch(showMessage({
                                            message: 'Délai invalide',
                                            variant: 'error'
                                        }));
                                        reject();
                                        return;
                                    }
                                    dispatch(updateLoanFrequency(newData));
                                    resolve();
                                }, 600);
                            })
                    }}*/
                    localization={{
                        body: {
                            emptyDataSourceMessage: t("emptyDataSourceMessage"),
                            addTooltip: t("addTooltip"),
                            deleteTooltip: t("deleteTooltip"),
                            editTooltip: t("editTooltip"),
                            editRow: {
                                deleteText: t("deleteText"),
                                cancelTooltip: t("cancelTooltip"),
                                saveTooltip: t("saveTooltip"),
                            }
                        }
                    }}
                />
        </Paper>
    );
}

export default withReducer('configuration', reducer)(LoanTermsList);