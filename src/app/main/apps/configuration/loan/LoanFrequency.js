import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { useTranslation } from 'react-i18next';

export function LoanFrequencyDesc(props) {
    const { t } = useTranslation('configApp');
    switch (props.code) {
        case 'weekly':
            return <span>{t('WEEKLY')}</span>
        case 'biweekly':
            return <span>{t('BIWEEKLY')}</span>
        case 'monthly':
            return <span>{t('MONTHLY')}</span>
        case 'bimonthly':
            return <span>{t('BIMONTHLY')}</span>
    }
}

export function LoanFrequencySelect(props) {
    const { t } = useTranslation('configApp');
    return <FormControl>
        <InputLabel>{t('FREQUENCY')}</InputLabel>
        <Select {...props} >
            {['weekly', 'biweekly', 'monthly', 'bimonthly'].map(
                freq => <MenuItem value={freq}>
                    <LoanFrequencyDesc code={freq} />
                </MenuItem>
            )}
        </Select>
    </FormControl>
}