import React, { useEffect } from 'react';
import { forwardRef } from 'react';
import AddCircle from '@material-ui/icons/AddCircle';
import { useSelector, useDispatch } from 'react-redux';
import withReducer from 'app/store/withReducer';
import { FuseAnimateGroup, FuseLoading } from '@fuse';
import {Icon, Input, InputAdornment} from '@material-ui/core';
import CurrencyField from 'app/components/CurrencyField';
import MaterialTable from 'material-table';
import { useTranslation } from 'react-i18next';
import reducer from '../store/reducers';
import { getLoanAmounts, createLoanAmount, deleteLoanAmount } from '../store/actions';
import { showMessage } from 'app/store/actions';

function LoanAmounts(props) {
    const { t } = useTranslation('configApp');
    const dispatch = useDispatch();
    const store = useSelector(({configuration}) => configuration.loan.amounts);

    useEffect(() => {
        // Load initial data
        if (!store.loaded) {
            dispatch(getLoanAmounts);
        }
    });

   /*  if (!store.loaded) {
        return <FuseLoading />
    } */

    return <div className="mb-16 mr-16">
            <MaterialTable
                className="w-full mb-16 mr-16"
                title={t('AMOUNT_OFFERED')}
                columns={[
                    { title: '', field: 'id', hidden: true },
                    {
                        title: t('AMOUNT'),
                        field: 'amount',
                        type: 'currency',
                        sorting: true,
                        defaultSort: 'asc',
                        cellStyle: { textAlign: 'left' }
                    }
                ]}
                data={store.data}
                options={{
                    paging: false,
                    actionsColumnIndex: -1,
                    draggable: false,
                    search: false
                }}
                    icons={{
                    Add: forwardRef((props, ref) => <AddCircle {...props} ref={ref} color="secondary" />),
                    
                }} 
                editable={{
                    onRowAdd: newData => new Promise((resolve, reject) => {
                        if (!parseFloat(newData.amount) || parseFloat(newData.amount) <= 0) {
                            dispatch(showMessage({
                                message: 'Montant invalide',
                                variant: 'error'
                            }))
                            reject();
                            return;
                        }
                        if (store.data.some(row => parseFloat(row.amount) === parseFloat(newData.amount))) {
                            dispatch(showMessage({
                                message: 'Le montant existe déjà',
                                variant: 'error'
                            }));
                            reject();
                            return;
                        }
                        setTimeout(() => {
                            dispatch(createLoanAmount(newData));
                            resolve();
                        }, 1000);
                    }),
                    onRowDelete: oldData => new Promise((resolve, reject) => {
                        setTimeout(() => {
                            dispatch(deleteLoanAmount(oldData.id));
                            resolve();
                        }, 1000);
                    })
                }}
                localization={{
                    body: {
                        emptyDataSourceMessage: t("emptyDataSourceMessage"),
                        addTooltip: t("addTooltip"),
                        deleteTooltip: t("deleteTooltip"),
                        editTooltip: t("editTooltip"),
                        editRow: {
                            deleteText: t("deleteText"),
                            cancelTooltip: t("cancelTooltip"),
                            saveTooltip: t("saveTooltip"),
                        }
                    },
                }}
            />
        </div>
}

export default withReducer('configuration', reducer)(LoanAmounts);