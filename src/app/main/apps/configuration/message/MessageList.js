import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { forwardRef } from 'react';
import AddCircle from '@material-ui/icons/AddCircle';
import SettingsIcon from '@material-ui/icons/Settings';
import { useSelector, useDispatch } from 'react-redux';
import withReducer from 'app/store/withReducer';
import { FuseAnimateGroup, FuseLoading } from '@fuse';
import MaterialTable from 'material-table';
import { useTranslation } from 'react-i18next';
import reducer from '../store/reducers';
import {getMessages} from '../store/actions';
import Edit from '@material-ui/icons/Edit';
import { useEditor } from 'slate-react';

function MessageList(props) {
    const { t } = useTranslation('configApp');
    const store = useSelector(({configuration}) => configuration.messages);
    const dispatch = useDispatch();
    

    useEffect(() => {
        dispatch(getMessages);
    }, []);


    const editMessage = (data) => {
        props.history.push(`/apps/configuration/message/detail/${data.id}`)
    }

   /*  if (!store.loaded) {
        return <FuseLoading />
    } */

    return <div className="">
            <FuseAnimateGroup
                enter={{
                    animation: "transition.slideUpBigIn"
                }}
            >
                <MaterialTable
                    className="w-full mb-16 mr-16"
                    title={t('MESSAGES')}
                    columns={[
                        { title: '', field: 'id', hidden: true },
                        { title: t('TITLE'), field: 'title', accessor: 'title'},
                        { title: t('DESCRIPTION'), field: 'description', accessor: 'description'}
                    ]}
                    data={store.data}
                    options={{
                        paging: false,
                        actionsColumnIndex: -1,
                        draggable: false,
                        search: false
                    }}
                    actions={[
                        rowData => ({
                            icon: () => <Edit />,
                            onClick: (data) =>
                                editMessage(rowData)
                        }),
                        {
                            icon: () => <SettingsIcon color="secondary"/>,
                            isFreeAction: true,
                            onClick: () => {
                                props.history.push('/apps/configuration/messages/configuration')
                            }
                        },
                        {
                            icon: () => <AddCircle color="secondary"/>,
                            isFreeAction: true,
                            onClick: () => {
                                props.history.push('/apps/configuration/message/detail/new')
                            }
                        }
                    ]}
                    localization={{
                        body: {
                            emptyDataSourceMessage: t("emptyDataSourceMessage"),
                            addTooltip: t("addTooltip"),
                            deleteTooltip: t("deleteTooltip"),
                            editTooltip: t("editTooltip"),
                            editRow: {
                                deleteText: t("deleteText"),
                                cancelTooltip: t("cancelTooltip"),
                                saveTooltip: t("saveTooltip"),
                            }
                        },
                    }}
                />
            </FuseAnimateGroup>
        </div>
}

export default withRouter(withReducer('configuration', reducer)(MessageList)) ;