import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import withReducer from 'app/store/withReducer';
import { withRouter } from 'react-router-dom';
import { useForm } from '@fuse/hooks';
import _ from '@lodash';
import { Link } from 'react-router-dom';
import { FuseAnimate, FuseAnimateGroup, FusePageCarded, FuseLoading } from '@fuse';
import { Button, Icon, Typography, AppBar, Card, CardContent, TextField, Toolbar } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import CustomSlate from '../../../../components/RichTextEditor/CustomSlate';

function MessageDetail(props) {
    const { t } = useTranslation('configApp');
    const dispatch = useDispatch();
    const theme = useTheme();

    const message = useSelector(({ configuration }) => configuration.message);
    const [submitDisabled, setSubmitDisabled] = useState(true);

    const { form, handleChange, setForm } = useForm(null);

    const handleFormChange = e => {
        handleChange(e);
        if (submitDisabled && canBeSubmitted()) {
            setSubmitDisabled(false);
        }
    }

    function handleContentChange(key, value) {
        setForm({
            ...form,
            [key]: value
        });
        if (submitDisabled && canBeSubmitted()) {
            setSubmitDisabled(false);
        }
    }

    const dropdown = [
        {
            token: "$amount_due",
            value: t('AMOUNT_DUE')
        },
        {
            token: "$balance",
            value: t('BALANCE')
        },
        {
            token: "$due_date",
            value: t('DUE_DATE')
        },
        {
            token: "$first_name",
            value: t('FIRST_NAME')
        },
        {
            token: "$last_name",
            value: t('LAST_NAME')
        },
        {
            token: "$ibv_link",
            value: t('IBV_LINK')
        },
        {
            token: "$email_document",
            value: t('EMAIL_DOCUMENT')
        },
        {
            token: "$email_contact",
            value: t('EMAIL_CONTACT')
        },
        {
            token: "$loaner_name",
            value: t('LOANER_NAME')
        },
        {
            token: "$loaner_amount",
            value: t('LOAN_AMOUNT')
        },
        {
            token: "$interac_answer",
            value: t('INTERAC_ANSWER')
        },
        {
            token: "$phone",
            value: t('CONTACT_PHONE')
        },
        {
            token: "$website",
            value: t('WEBSITE')
        },
        {
            token: "$digital",
            value: t('DIGITAL')
        }
    ]

    useEffect(() => {
        if (props.match.params.id !== "new") {
            dispatch(Actions.getMessage(props.match.params.id));
        }
    }, []);

    useEffect(() => {
        if (message) {
            setForm(message);
        }
    }, [message]);

    // Validate Save and Update
    function canBeSubmitted() {

        if (message.id != 28 || message.id != 30) {
            return form.title
                && form.description
                && form.subjectFr
                && form.subjectEn
                && form.emailContentFr
                && form.emailContentEn;

        } else {
            return form.title
                && form.description
                && form.subjectFr
                && form.subjectEn
                && form.emailContentFr
                && form.emailContentEn
                && form.smsContentFr
                && form.smsContentEn;
        }
    }

    function saveMessage() {
        dispatch(Actions.saveMessage(form));
        setSubmitDisabled(true);
    }

    if (!message && props.match.params.id !== 'new') {
        return <FuseLoading />;
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">
                        <div className="flex flex-col items-start max-w-full">
                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/configuration/messages/all" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">{t("ALL_MESSAGES")}</span>
                                </Typography>
                            </FuseAnimate>
                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0 mx-8 mr-64 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {message.title ? `${message.title}` : t("NEW_MESSAGE")}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimateGroup animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption"> {form.id ? form.id.toString().padStart(6, '0') : ""} </Typography>
                                        {/* <Chip size="small" label="Actif" className="ml-16 mr-16 MuiChip green" />
                                        <Chip size="small" label="Prêt #002 en défaut" className="MuiChip orange" /> */}
                                    </FuseAnimateGroup>
                                </div>
                            </div>
                        </div>
                        <FuseAnimateGroup animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={submitDisabled}
                                onClick={() => saveMessage()}
                            >
                                {(props.match.params.id === 'new') ? t("SAVE") : t("UPDATE")}

                            </Button>
                        </FuseAnimateGroup>
                    </div>
                )
            }
            content={
                form && (
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <Icon>message</Icon>
                                <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("MESSAGE")}
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <CardContent>
                            <div>
                                <div className="flex">
                                    <TextField
                                        className="mt-8 mb-16 mr-16"
                                        required
                                        label={t("TITLE")}
                                        autoFocus
                                        id="title"
                                        name="title"
                                        value={form.title}
                                        onChange={handleFormChange}
                                        variant="outlined"
                                        error={form.title ? false : true}
                                        fullWidth
                                    />
                                    <TextField
                                        className="mt-8 mb-16"
                                        required
                                        label={t("DESCRIPTION")}
                                        id="description"
                                        name="description"
                                        value={form.description}
                                        onChange={handleFormChange}
                                        variant="outlined"
                                        error={form.description ? false : true}
                                        fullWidth
                                    />
                                </div>
                                <div className="flex">
                                    <TextField
                                        className="mt-8 mb-16 mr-16"
                                        required
                                        label={t("SUBJECT_FR")}
                                        id="subjectFr"
                                        name="subjectFr"
                                        value={form.subjectFr}
                                        onChange={handleFormChange}
                                        variant="outlined"
                                        error={form.subjectFr ? false : true}
                                        fullWidth
                                    />
                                    <TextField
                                        className="mt-8 mb-16"
                                        required
                                        label={t("SUBJECT_EN")}
                                        id="subjectEn"
                                        name="subjectEn"
                                        value={form.subjectEn}
                                        onChange={handleFormChange}
                                        variant="outlined"
                                        error={form.subjectEn ? false : true}
                                        fullWidth
                                    />
                                </div>
                                <div className="flex mb-16 -mx-2">
                                    <div className="w-1/2 mr-16">
                                        <Typography variant="body1">{t('FRENCH_EMAIL')}</Typography>
                                        <CustomSlate
                                            placeholder={t('MESSAGE_PLACEHOLDER')}
                                            onChange={handleContentChange}
                                            value={message.emailContentFr}
                                            name="emailContentFr"
                                            dropdown={dropdown}
                                            richText={true}
                                        />
                                    </div>
                                    <div className="w-1/2">
                                        <Typography variant="body1">{t('ENGLISH_EMAIL')}</Typography>
                                        <CustomSlate
                                            placeholder={t('MESSAGE_PLACEHOLDER')}
                                            onChange={handleContentChange}
                                            value={message.emailContentEn}
                                            name="emailContentEn"
                                            dropdown={dropdown}
                                            richText={true}
                                        />
                                    </div>
                                </div>
                                {(message.id != 28 && message.id != 30) &&
                                    <div className="flex mb-16 -mx-2">
                                        <div className="w-1/2 mr-16">
                                            <Typography variant="body1">{t('FRENCH_SMS')}</Typography>
                                            <CustomSlate
                                                placeholder={t('MESSAGE_PLACEHOLDER')}
                                                onChange={handleContentChange}
                                                value={message.smsContentFr}
                                                name="smsContentFr"
                                                dropdown={dropdown}
                                            />
                                        </div>
                                        <div className="w-1/2">
                                            <Typography variant="body1">{t('ENGLISH_SMS')}</Typography>
                                            <CustomSlate
                                                placeholder={t('MESSAGE_PLACEHOLDER')}
                                                onChange={handleContentChange}
                                                value={message.smsContentEn}
                                                name="smsContentEn"
                                                dropdown={dropdown}
                                            />
                                        </div>
                                    </div>
                                }

                            </div>
                        </CardContent>
                    </Card>
                )
            }
        />
    )
}

export default withReducer('configuration', reducer)(MessageDetail);