import React, { useEffect, useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import withReducer from 'app/store/withReducer';
import clsx from 'clsx';
import { useForm } from '@fuse/hooks';
import _ from '@lodash';
import { Link } from 'react-router-dom';
import { FuseAnimate, FuseAnimateGroup, FusePageCarded, FuseLoading } from '@fuse';
import { Button, Icon, Typography, AppBar, Card, CardContent, TextField, Toolbar } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { orange } from '@material-ui/core/colors';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import CustomSlate from '../../../../components/RichTextEditor/CustomSlate';

const useStyles = makeStyles(theme => ({
    customerImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    customerImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    customerImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $customerImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $customerImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $customerImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));

function MessageDetail(props) {
    const { t } = useTranslation('configApp');
    const classes = useStyles(props);
    const dispatch = useDispatch(); 
    const theme = useTheme();

    const messageConfiguration = useSelector(({configuration}) => configuration.messageConfiguration);

    const { form, handleChange, setForm } = useForm(null);

    useEffect(() => {
        dispatch(Actions.getMessageConfiguration);
    }, []);

    useEffect(() => {
        if (messageConfiguration) {
            setForm(messageConfiguration);
        }
    }, [messageConfiguration]);

    function canBeSubmitted() {
        return !_.isEqual(messageConfiguration, form)
            && form.headerContentFr
            && form.headerContentEn
            && form.footerContentFr
            && form.footerContentEn
    }

    function saveMessageConfiguration() {
        return dispatch(Actions.saveMessageConfiguration(form))
    }

    if (!messageConfiguration) {
        return <FuseLoading />;
    }

    function handleContentChange(key, value) {
        setForm({
            ...form,
            [key]: value
        })
    }

    function handleUploadChange(e) {
        const file = e.target.files[0];

        if (!file) {
            return;
        }

        setForm({
            ...form,
            logo: file
        });
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">
                        <div className="flex flex-col items-start max-w-full">
                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/configuration/messages/all" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">{t("ALL_MESSAGES")}</span>
                                </Typography>
                            </FuseAnimate>
                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0 mx-8 mr-64 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {t("MESSAGE_CONFIGURATION")}
                                        </Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimateGroup animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!canBeSubmitted()}
                                onClick={() => saveMessageConfiguration()}
                            >
                                {t("UPDATE")}

                            </Button>
                        </FuseAnimateGroup>
                    </div>
                )
            }
            content={
                form && (
                    <Card className="w-full mb-16">
                        <AppBar position="static" elevation={0} className="black_bg">
                            <Toolbar className="pl-16 pr-8">
                                <Icon>settings</Icon>
                                <Typography variant="subtitle2" color="inherit" className="flex-1"> {t("CONFIGURATION")}
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <CardContent>
                            <div>
                                <div className="flex">
                                    <input
                                        /* ref={fileInput => (this.fileInput = fileInput)}*/
                                        accept="image/*"
                                        className="hidden"
                                        id="button-file"
                                        type="file"
                                        onChange={handleUploadChange}
                                    //disabled={form.images && form.images.filter(function(obj) {return !obj.isDeleted}).length > 0}
                                    />

                                    <div className="mt-8 mb-16 mr-16">
                                        <label
                                            htmlFor="button-file"
                                            className={
                                                clsx(
                                                    classes.customerImageUpload,
                                                    "flex items-center justify-center relative w-50 h-50 pt-8 overflow-hidden cursor-pointer"
                                                )}
                                        >
                                            <Icon fontSize="large" color="action">add_a_photo</Icon>
                                        </label>
                                    </div>
                                </div>

                                {/* Have to pass the image url to Custom Slate as a prop */}
                                <div className="flex mb-16 -mx-2">
                                    <div className="w-1/2 mr-16">
                                        <Typography variant="body1">{t('FRENCH_HEADER')}</Typography>
                                        <CustomSlate placeholder={t('MESSAGE_PLACEHOLDER')} onChange={handleContentChange} value={messageConfiguration.headerContentFr} name="headerContentFr" richText dropdown={[]} imageEnabled imageURL={messageConfiguration.logo}/>
                                    </div>
                                    <div className="w-1/2">
                                        <Typography variant="body1">{t('ENGLISH_HEADER')}</Typography>
                                        <CustomSlate placeholder={t('MESSAGE_PLACEHOLDER')} onChange={handleContentChange} value={messageConfiguration.headerContentEn} name="headerContentEn" richText dropdown={[]} imageEnabled imageURL={messageConfiguration.logo}/>
                                    </div>
                                </div>
                                <div className="flex mb-16 -mx-2">
                                    <div className="w-1/2 mr-16">
                                        <Typography variant="body1">{t('FRENCH_FOOTER')}</Typography>
                                        <CustomSlate placeholder={t('MESSAGE_PLACEHOLDER')} onChange={handleContentChange} value={messageConfiguration.footerContentFr} name="footerContentFr" richText dropdown={[]} imageEnabled imageURL={messageConfiguration.logo}/>
                                    </div>
                                    <div className="w-1/2">
                                        <Typography variant="body1">{t('ENGLISH_FOOTER')}</Typography>
                                        <CustomSlate placeholder={t('MESSAGE_PLACEHOLDER')} onChange={handleContentChange} value={messageConfiguration.footerContentEn} name="footerContentEn" richText dropdown={[]} imageEnabled imageURL={messageConfiguration.logo}/>
                                    </div>
                                </div>
                            </div>
                        </CardContent>
                    </Card>
                )
            }
        />
    )
}

export default withReducer('configuration', reducer)(MessageDetail);