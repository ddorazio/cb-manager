import React from 'react';
import {Redirect} from 'react-router-dom';
import i18next from 'i18next';
import en from './i18n/en';
import fr from './i18n/fr';

i18next.addResourceBundle('en', 'configApp', en);
i18next.addResourceBundle('fr', 'configApp', fr);

export const ConfigurationAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/apps/configuration/loan',
            component: React.lazy(() => import('./ConfigurationApp'))
        },
        {
            path     : '/apps/configuration/message/detail/:id',
            component: React.lazy(() => import('./message/MessageDetail'))
        },
        {
            path     : '/apps/configuration/messages/all',
            component: React.lazy(() => import('./ConfigurationApp'))
        },
        {
            path     : '/apps/configuration/messages/configuration',
            component: React.lazy(() => import('./message/MessageConfiguration'))
        },
        {
            path     : '/apps/configuration',
            component: () => <Redirect to="/apps/configuration/loan"/>
        },
    ]
};
