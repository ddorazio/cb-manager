import React, { useRef, useState, useEffect } from 'react';
import {FusePageSimple, FuseAnimate} from '@fuse';
import { Icon, Tab, Tabs, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import LoanTermsList from './loan/LoanTermsList';
import LoanAmountsMatrix from './loan/LoanAmountsMatrix';
import LoanAmounts from './loan/LoanAmounts';
import LoanCharges from './loan/LoanCharges';
import MessageList from './message/MessageList';

const useStyles = makeStyles(theme => ({ 
    layoutHeader: {
        height: 140,
        minHeight: 140,
        [theme.breakpoints.down('md')]: {
            height: 240,
            minHeight: 240
        }
    }
}));

function ConfigurationApp(props) {
    const { t } = useTranslation('configApp');
    const pageLayout = useRef(null);
    const classes = useStyles();
    const [selectedTab, setSelectedTab] = useState(0);

    function handleTabChange(event, value) {
        setSelectedTab(value);
        switch (value) {
            case 0:
                props.history.push('/apps/configuration/loan');
                break;
            case 1:
                props.history.push('/apps/configuration/messages/all');
                break;
        }
    }

    useEffect(() => {
        if (props.location.pathname == "/apps/configuration/messages/all") {
            setSelectedTab(1);
        }
    }, []);


    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    header: classes.layoutHeader,
                    toolbar: "px-16 sm:px-24"
                }}
                header={
                    <div className="p-16 flex flex-1 flex-col items-center justify-center md:flex-row">
                        <div className="flex flex-1 flex-col items-center justify-center md:flex-row md:items-center md:justify-start">
                            <FuseAnimate animation="transition.expandIn" delay={300}>
                                <Icon className="text-32 mr-12">settings</Icon>
                            </FuseAnimate>
                            <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                <Typography variant="h6" className="hidden sm:flex">{t('SETTINGS')}</Typography>
                            </FuseAnimate>
                        </div>
                    </div>
                }
                contentToolbar={
                    <Tabs
                        value={selectedTab}
                        onChange={handleTabChange}
                        indicatorColor="secondary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        classes={{ root: "flex w-full items-center justify-between" }}
                    >
                        <Tab className="h-64 flex-1 px-10 normal-case text-base" label={t("LOANS_MANAGEMENT")} />
                        <Tab className="h-64 flex-1 px-10 normal-case text-base" label={t("MESSAGES_MANAGEMENT")} />
                        {/* <Tab className="h-64 flex-1 px-10 normal-case" label="Date de départ des paiements" />
                        <Tab className="h-64 flex-1 px-10 normal-case" label="Matrice de montant" />
                        <Tab className="h-64 flex-1 px-10 normal-case" label="Liste des montants offerts" />
                        <Tab className="h-64 flex-1 px-10 normal-case" label="Configurations des frais" /> */}
                    </Tabs>
                }
                content={
                    <div className="p-16 sm:p-24">
                         {selectedTab === 0 && 
                            <React.Fragment>
                                <div className="flex">
                                    <div className="flex-1">
                                        <LoanAmounts />
                                        <LoanTermsList />
                                        <LoanCharges />
                                    </div>
                                    <div className="flex-1">
                                        <LoanAmountsMatrix />
                                    </div>
                                </div>
                            </React.Fragment>
                        } 
                        {selectedTab === 1 && 
                            <MessageList />
                        }
                        {/* 
                        {selectedTab === 1 && <LoanAmountsMatrix />}
                        {selectedTab === 2 && <LoanAmounts />}
                        {selectedTab === 3 && <LoanCharges />} 
                        */}
                    </div>
                }
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
        </React.Fragment>
    )
}

export default ConfigurationApp;
