import ConfigurationAPI from '../../ConfigurationAPI'
import { showMessage } from 'app/store/actions/fuse';
import i18n from 'i18n';

export const GET_LOAN_AMOUNTS = 'GET_LOAN_AMOUNTS';
export const CREATE_LOAN_AMOUNT = 'CREATE_LOAN_AMOUNT';
export const DELETE_LOAN_AMOUNT = 'DELETE_LOAN_AMOUNT';
export const GET_LOAN_CONFIGURATIONS = 'GET_LOAN_CONFIGURATIONS';
export const CREATE_LOAN_CONFIGURATION = 'CREATE_LOAN_CONFIGURATION';
export const UPDATE_LOAN_CONFIGURATION = 'UPDATE_LOAN_CONFIGURATION';
export const DELETE_LOAN_CONFIGURATION = 'DELETE_LOAN_CONFIGURATION';
export const GET_LOAN_CHARGE_CONFIGURATIONS = 'GET_LOAN_CHARGE_CONFIGURATIONS';
export const CREATE_LOAN_CHARGE_CONFIGURATION = 'CREATE_LOAN_CHARGE_CONFIGURATION';
export const DELETE_LOAN_CHARGE_CONFIGURATION = 'DELETE_LOAN_CHARGE_CONFIGURATION';
export const GET_LOAN_FREQUENCIES = 'GET_LOAN_FREQUENCIES';
export const UPDATE_LOAN_FREQUENCY = 'UPDATE_LOAN_FREQUENCY';


export const getLoanAmounts = async (dispatch) => {
    try {
        const result = await ConfigurationAPI.getLoanAmounts();
        return dispatch({
            type: GET_LOAN_AMOUNTS,
            amounts: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const createLoanAmount = amount => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.createLoanAmount(amount);
            dispatch(showMessage({ message: i18n.t('LOAN_AMOUNT_CREATED'), variant: 'success' }));
            return dispatch({
                type: CREATE_LOAN_AMOUNT,
                amount: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteLoanAmount = amountId => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.deleteLoanAmount(amountId);
            dispatch(showMessage({ message: i18n.t('LOAN_AMOUNT_DELETED'), variant: 'success' }));
            return dispatch({
                type: DELETE_LOAN_AMOUNT,
                amountId
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getLoanConfigurations = async (dispatch) => {
    try {
        const result = await ConfigurationAPI.getLoanConfigurations();
        return dispatch({
            type: GET_LOAN_CONFIGURATIONS,
            configurations: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const createLoanConfiguration = configuration => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.createLoanConfiguration(configuration);
            dispatch(showMessage({ message: i18n.t('LOAN_CONFIGURATION_CREATED'), variant: 'success' }));
            return dispatch({
                type: CREATE_LOAN_CONFIGURATION,
                configuration: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const updateLoanConfiguration = configuration => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.updateLoanConfiguration(configuration);
            dispatch(showMessage({ message: i18n.t('LOAN_CONFIGURATION_UPDATED'), variant: 'success' }));
            return dispatch({
                type: UPDATE_LOAN_CONFIGURATION,
                configuration: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteLoanConfiguration = configurationId => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.deleteLoanConfiguration(configurationId);
            dispatch(showMessage({ message: i18n.t('LOAN_CONFIGURATION_DELETED'), variant: 'success' }));
            return dispatch({
                type: DELETE_LOAN_CONFIGURATION,
                configurationId
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getLoanChargeConfigurations = async (dispatch) => {
    try {
        const result = await ConfigurationAPI.getLoanChargeConfigurations();
        return dispatch({
            type: GET_LOAN_CHARGE_CONFIGURATIONS,
            charges: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const createLoanChargeConfiguration = charge => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.createLoanChargeConfiguration(charge);
            dispatch(showMessage({ message: i18n.t('LOAN_CHARGE_CREATED'), variant: 'success' }));
            return dispatch({
                type: CREATE_LOAN_CHARGE_CONFIGURATION,
                charge: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const deleteLoanChargeConfiguration = chargeId => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.deleteLoanChargeConfiguration(chargeId);
            dispatch(showMessage({ message: i18n.t('LOAN_CHARGE_DELETED'), variant: 'success' }));
            return dispatch({
                type: DELETE_LOAN_CHARGE_CONFIGURATION,
                chargeId
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getLoanFrequencies = async (dispatch) => {
    try {
        const result = await ConfigurationAPI.getLoanFrequencies();
        return dispatch({
            type: GET_LOAN_FREQUENCIES,
            frequencies: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const updateLoanFrequency = frequency => {
    return async dispatch => {
        try {
            const result = await ConfigurationAPI.updateLoanFrequency(frequency);
            dispatch(showMessage({ message: i18n.t('LOAN_FREQUENCY_UPDATED'), variant: 'success' }));
            return dispatch({
                type: UPDATE_LOAN_FREQUENCY,
                frequency: result
            });
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}




