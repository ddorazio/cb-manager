import { getProtected, putProtected } from 'app/main/api/apiWrapper';
import { showMessage } from 'app/store/actions/fuse';
import settingsConfig from 'app/fuse-configs/settingsConfig';
import i18n from 'i18n';

export const GET_MESSAGE = 'GET_MESSAGE';
export const SAVE_MESSAGE = 'SAVE_MESSAGE';
export const GET_EMAIL_TEMPLATE = 'GET_EMAIL_TEMPLATE';
export const SAVE_EMAIL_TEMPLATE = 'SAVE_EMAIL_TEMPLATE';

export const getMessage = messageId => {
    return async dispatch => {
        const response = await getProtected(`${settingsConfig.apiPath}/api/Configuration/GetMessageTemplate/${messageId}`)
        dispatch({
            type: GET_MESSAGE,
            payload: response
        });
    }
}

export const saveMessage = message => {
    return async dispatch => {
        const response = await putProtected(`${settingsConfig.apiPath}/api/Configuration/UpdateMessageTemplate`, message);
        dispatch(showMessage({
            message: i18n.t("MESSAGE_SAVED"),
            variant: 'success'
        }));
        dispatch({
            type: SAVE_MESSAGE,
            payload: response.data
        });
    }
}
