import settingsConfig from 'app/fuse-configs/settingsConfig';
import localStorageService from 'app/services/localStorageService';
import { getProtected, putProtected } from 'app/main/api/apiWrapper';
import { showMessage } from 'app/store/actions';
import i18n from 'i18n';

export const GET_MESSAGE_CONFIGURATION = 'GET_MESSAGE_CONFIGURATION';
export const SAVE_MESSAGE_CONFIGURATION = 'SAVE_MESSAGE_CONFIGURATION';


export const getMessageConfiguration = async (dispatch) => {
    const response = await getProtected(`${settingsConfig.apiPath}/api/Configuration/GetEmailConfiguration`);
    dispatch({
        type: GET_MESSAGE_CONFIGURATION,
        payload: response
    });
}

export const saveMessageConfiguration = template => {
    return async dispatch => {
        if (template.logo) {
            // Start by uploading the logo and setting the url
            template.logoUrl = await saveLogo(template.logo);
        }
        const response = await putProtected(`${settingsConfig.apiPath}/api/Configuration/SaveEmailConfiguration`, template);
        dispatch({
            type: SAVE_MESSAGE_CONFIGURATION,
            payload: response
        });

        dispatch(showMessage({
            message: i18n.t("MESSAGE_SAVED"),
            variant: 'success'
        }));
    }
}

const saveLogo = async (logo) => {
    const formData = new FormData();
    formData.append('file', logo);
    const response = await fetch(`${settingsConfig.apiPath}/api/Configuration/SaveLogo`, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${localStorageService.getAccessToken()}`
        },
        body: formData
    });
    return await response.text();
}