import { getProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig';

export const GET_MESSAGES = 'GET_MESSAGES';

export const getMessages = async dispatch => {
    var response = await getProtected(`${settingsConfig.apiPath}/api/Configuration/GetMessageTemplateList`);
    // var result = await response.json();
    dispatch({
        type: GET_MESSAGES,
        payload: response
    });
}