import * as Actions from '../actions';

const initialState = {
    id: '',
    title: '',
    description: '',
    subjectFr: '',
    subjectEn: '',
    emailContentFr: '',
    emailContentEn: '',
    smsContentFr: '',
    smsContentEn: ''
}

const message = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_MESSAGE:
            {
                return {...action.payload}
                
            }
        default: {
            return state
        }
    }
}
export default message;