import * as Actions from '../actions';

const initialState = {
    loaded: false,
    amounts: {
        loaded: false,
        data: []
    },
    matrix: {
        loaded: false,
        data: []
    },
    charges: {
        loaded: false,
        data: []
    },
    terms: {
        loaded: false,
        data: []
    },
}

const loan = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_LOAN_AMOUNTS:
            {
                return {
                    ...state,
                    amounts: {
                        loaded: true,
                        data: action.amounts
                    }
                }
            }
        case Actions.CREATE_LOAN_AMOUNT:
            {
                return {
                    ...state,
                    amounts: {
                        ...state.amounts,
                        data: [
                            ...state.amounts.data,
                            action.amount
                        ]
                    },
                    // This will trigger the matrix component to reload data
                    matrix: {
                        ...initialState.matrix
                    }
                }
            }
        case Actions.DELETE_LOAN_AMOUNT:
            {
                const index = state.amounts.data.findIndex(a => a.id === action.amountId);
                return {
                    ...state,
                    amounts: {
                        ...state.amounts,
                        data: [
                            ...state.amounts.data.slice(0, index),
                            ...state.amounts.data.slice(index + 1)
                        ]
                    },
                    matrix: {
                        ...initialState.matrix
                    }
                }
            }
        case Actions.GET_LOAN_CONFIGURATIONS:
            {
                return {
                    ...state,
                    matrix: {
                        loaded: true,
                        data: action.configurations
                    }
                }
            }
        case Actions.CREATE_LOAN_CONFIGURATION:
            {
                // If there was no configured data we want to replace the existing row
                // Otherwise we just add a new row
                const oldData = state.matrix.data;
                const row = oldData.find(conf => 
                    conf.loanAmount === action.configuration.loanAmount &&
                    conf.loanFrequencyCode === action.configuration.loanFrequencyCode);
                const index = oldData.indexOf(row);
                
                var newData = row.id // If this has no id then there's no existing data
                    ? [ ...oldData, action.configuration ]
                    : [ ...oldData.slice(0, index), action.configuration, ...oldData.slice(index + 1) ]
                
                return {
                    ...state,
                    matrix: {
                        ...state.matrix,
                        data: newData
                    }
                }
            }
        case Actions.UPDATE_LOAN_CONFIGURATION:
            {
                const oldData = state.matrix.data;
                const index = action.configuration.id = oldData.findIndex(c => c.id === action.configuration.id);
                return {
                    ...state,
                    matrix: {
                        ...state.matrix,
                        data: [
                            ...oldData.slice(0, index),
                            action.configuration,
                            ...oldData.slice(index + 1)
                        ]
                    }
                }
            }
        case Actions.GET_LOAN_CHARGE_CONFIGURATIONS:
            {
                return {
                    ...state,
                    charges: {
                        loaded: true,
                        data: action.charges
                    }
                }
            }
        case Actions.CREATE_LOAN_CHARGE_CONFIGURATION:
            {
                return {
                    ...state,
                    charges: {
                        ...state.charges,
                        data: [
                            ...state.charges.data,
                            action.charge
                        ]
                    }
                }
            }
        case Actions.DELETE_LOAN_CHARGE_CONFIGURATION:
            {
                const index = state.charges.data.findIndex(c => c.id === action.chargeId);
                return {
                    ...state,
                    charges: {
                        ...state.charges,
                        data: [
                            ...state.charges.data.slice(0, index),
                            ...state.charges.data.slice(index + 1)
                        ]
                    }
                }
            }
        case Actions.GET_LOAN_FREQUENCIES:
            {
                return {
                    ...state,
                    terms: {
                        loaded: true,
                        data: action.frequencies
                    }
                }
            }
        case Actions.UPDATE_LOAN_FREQUENCY:
            {
                const index = state.terms.data.findIndex(c => c.id === action.frequency.id);
                return {
                    ...state,
                    terms: {
                        ...state.terms,
                        data: [
                            ...state.terms.data.slice(0, index),
                            action.frequency,
                            ...state.terms.data.slice(index + 1)
                        ]
                    }
                }
            }
        case Actions.DELETE_LOAN_CONFIGURATION:
            {
                const oldData = state.matrix.data;
                const index = oldData.findIndex(c => c.id === action.configurationId);
                return {
                    ...state,
                    matrix: {
                        ...state.matrix,
                        data: [
                            ...oldData.slice(0, index),
                            ...oldData.slice(index + 1)
                        ]
                    }
                }
            }
        default:
            {
                return state;
            }
    }
}

export default loan;