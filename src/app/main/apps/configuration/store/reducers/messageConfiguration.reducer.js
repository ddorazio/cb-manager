import * as Actions from '../actions';

const initialState = {
    headerContentFr: '',
    headerContentEn: '',
    footerContentFr: '',
    footerContentEn: '',
    logo: undefined,
    logoUrl: ''
}

const messageConfiguration = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_MESSAGE_CONFIGURATION:
            {
                return {
                    ...state,
                    ...action.payload || []
                }
            }
        default: {
            return state
        }
    }
}
export default messageConfiguration;