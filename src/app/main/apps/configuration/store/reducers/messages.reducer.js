import * as Actions from '../actions';

const initialState = {
    loaded: false,
    data: [],
    routeParams: {},
}

const messages = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_MESSAGES:
            {
                return {
                    ...state,
                    loaded: true,
                    data: action.payload
                }
            }
        case Actions.SAVE_MESSAGE:
            {
                return {
                    ...state,
                    data: action.payload,
                    
                }
                // const index = state.data.findIndex(message => message.id === action.payload.id);
                // if (index) {
                //     return {
                //         ...state,
                //         data: [
                //             ...state.data.slice(0, index),
                //             action.payload,
                //             ...state.data.slice(index + 1)
                //         ]
                //     }
                // } else {
                //     return {
                //         ...state,
                //         data: [
                //             ...state.data,
                //             action.payload
                //         ]
                //     }
                // }
            }
        default: {
            return state
        }
    }
}
export default messages;