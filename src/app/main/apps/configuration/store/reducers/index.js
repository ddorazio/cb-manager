import {combineReducers} from 'redux';
import loan from './configuration.reducer'
import messages from './messages.reducer'
import message from './message.reducer'
import messageConfiguration from './messageConfiguration.reducer'

const reducer = combineReducers({
    loan,
    messages,
    message,
    messageConfiguration
});

export default reducer;
