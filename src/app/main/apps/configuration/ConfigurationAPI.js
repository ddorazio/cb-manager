import { getProtected, postProtected, deleteProtected, putProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class ConfigurationAPI {
    static getLoanAmounts = async () => {
        return await getProtected(new URL("/api/Configuration/GetLoanAmounts", settingsConfig.apiPath));
    }

    static createLoanAmount = async (data) => {
        return await postProtected(new URL("/api/Configuration/CreateLoanAmount", settingsConfig.apiPath), data);
    }

    static deleteLoanAmount = async (id) => {
        return await deleteProtected(new URL(`/api/Configuration/DeleteLoanAmount/${id}`, settingsConfig.apiPath));
    }

    static getLoanConfigurations = async () => {
        return await getProtected(new URL("/api/Configuration/GetLoanConfigurations", settingsConfig.apiPath));
    }

    static createLoanConfiguration = async (data) => {
        return await postProtected(new URL("/api/Configuration/CreateLoanConfiguration", settingsConfig.apiPath), data);
    }

    static updateLoanConfiguration = async (data) => {
        return await putProtected(new URL("/api/Configuration/UpdateLoanConfiguration", settingsConfig.apiPath), data);
    }

    static getLoanChargeConfigurations = async () => {
        return await getProtected(new URL("/api/Configuration/GetLoanChargeConfigurations", settingsConfig.apiPath));
    }

    static createLoanChargeConfiguration = async (data) => {
        return await postProtected(new URL("/api/Configuration/CreateLoanChargeConfiguration", settingsConfig.apiPath), data);
    }

    static deleteLoanChargeConfiguration = async (id) => {
        return await deleteProtected(new URL(`/api/Configuration/DeleteLoanChargeConfiguration/${id}`, settingsConfig.apiPath));
    }

    static getLoanFrequencies = async () => {
        return await getProtected(new URL("/api/Configuration/GetLoanFrequencies", settingsConfig.apiPath));
    }

    static updateLoanFrequency = async (data) => {
        return await putProtected(new URL("/api/Configuration/UpdateLoanFrequency", settingsConfig.apiPath), data);
    }

    static deleteLoanConfiguration = async (id) => {
        return await deleteProtected(new URL(`/api/Configuration/DeleteLoanConfiguration/${id}`, settingsConfig.apiPath));
    }

    static getMessages = async () => {
        return await getProtected(new URL('/api/Configuration/GetMessageTemplateList', settingsConfig.apiPath));
    }

    static getMessage = async (messageId) => {
        return await getProtected(new URL(`/api/Configuration/GetMessageTemplate/${messageId}`, settingsConfig.apiPath));
    }

    static saveMessage = async (message) => {
        return await putProtected(new URL(`/api/Configuration/UpdateMessageTemplate`, settingsConfig.apiPath), message);
    }

    static getMessageConfiguration = async (messageConfiguration) => {
        return await getProtected(`${settingsConfig.apiPath}/api/Configuration/GetEmailConfiguration`);
    }

    static saveMessageConfiguration = async (template) => {
        return await putProtected(`${settingsConfig.apiPath}/api/Configuration/SaveEmailConfiguration`, template);
    }
}

export default ConfigurationAPI