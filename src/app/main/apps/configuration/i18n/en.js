const locale = {
    SAVE: "Save",
    UPDATE: "Update",
    SETTINGS: "Settings",
    LOANS_MANAGEMENT: "Loans Management",

    PAYMENT_START_DATE: "Payments start date",
    FREQUENCY: "Frequency",
    PAYMENT_DELAY: "Delay (days)",
    AMOUNT_MATRIX: "Amount matrix",
    AMOUNT: "Amount",
    NUMBER_OF_PAYMENT: "Number of payment",
    CHOOSE_VALUE: "Choose a value ...",
    FEES_CONFIG: "Fees configurations",
    EFFECTIVE_DATE: 'Date',
    MISSING_REQUIRED: 'Missing required fields',
    MISSING_DATE_MESSAGE: 'The effective date should be in the future.',
    AMOUNT_OFFERED: "Amounts offered",

    // Frequencies
    WEEKLY: 'Weekly',
    MONTHLY: 'Monthly',
    BIWEEKLY: 'Every two weeks',
    BIMONTHLY: 'Twice a month',

    // MATERIAL TABLE
    emptyDataSourceMessage: "Nothing to display",
    addTooltip: "Add",
    deleteTooltip: "Delete",
    editTooltip: "Edit",
    deleteText: "Are you sure you want to delete this?",
    cancelTooltip: "Cancel",
    saveTooltip: "Save",
    nRowsSelected: "{0} row (s) selected",
    searchTooltip: "Search",
    searchPlaceholder: "Search",
    labelDisplayedRows: "{from} - {to} of {count}",
    labelRowsSelect: "rows",
    labelRowsPerPage: "Rows per page",
    firstTooltip: "First Page",
    previousTooltip: "Previous page",
    nextTooltip: "Next page",
    lastTooltip: "Last Page",

    // MESSAGES
    MESSAGES_MANAGEMENT: "Messages management",
    MESSAGES: 'Messages',
    TITLE: 'Title',
    DESCRIPTION: 'Description',

    // MESSAGE DETAILS
    MESSAGE_SAVED: "Message saved",
    ALL_MESSAGES: "All messages",
    NEW_MESSAGE: "New message",
    FRENCH_EMAIL: "French email",
    ENGLISH_EMAIL: "English email",
    MESSAGE: "Message",
    MESSAGE_PLACEHOLDER: "Write here...",
    SUBJECT_FR: "French subject",
    SUBJECT_EN: "English subject",
    FRENCH_SMS: "French sms message",
    ENGLISH_SMS: "English sms message",

    // MESSAGE DROPDOWN
    AMOUNT_DUE: "Amount due",
    BALANCE: "Balance",
    DUE_DATE: "Due date",
    FIRST_NAME: "Client first name",
    LAST_NAME: "Client last name",
    IBV_LINK: "IBV link",
    EMAIL_DOCUMENT: "Document email",
    EMAIL_CONTACT: "Contact email",
    LOANER_NAME: "Company name",
    LOAN_AMOUNT: "Loan amount",
    INTERAC_ANSWER: "Interac answer",
    CONTACT_PHONE: "Phone number",
    WEBSITE: "website",
    DIGITAL: "Digital",

    // MESSAGE CONFIGURATION
    MESSAGE_CONFIGURATION: "Message configuration",
    CONFIGURATION: "Configuration",
    FRENCH_HEADER: "French header",
    ENGLISH_HEADER: "English header",
    FRENCH_FOOTER: "French footer",
    ENGLISH_FOOTER: "English footer",

    // Payment Charges
    CHARGES_NSF: "NSF",
    CHARGES_DEFER_PAYMENT: "Delayed payment",
    CHARGES_INTERAC: "Interact Deposit",
    CHARGES_AUTO_WITHDRAWAL: "Auto Withdrawal",

}

export default locale;
