const locale = {
    SAVE: "Enregistrer",
    UPDATE: "Mettre à jour",
    SETTINGS: "Configuration",
    LOANS_MANAGEMENT: "Gestion des prêts",

    PAYMENT_START_DATE: "Date de départ des paiements",
    FREQUENCY: "Fréquence",
    PAYMENT_DELAY: "Délai (jours)",
    AMOUNT_MATRIX: "Matrice de montants",
    AMOUNT: "Montant",
    NUMBER_OF_PAYMENT: "Nombre de paiements",
    CHOOSE_VALUE: "Choisissez une valeur...",
    FEES_CONFIG: "Configurations des frais",
    EFFECTIVE_DATE: 'Date',
    MISSING_REQUIRED: 'Champs obligatoires manquants',
    MISSING_DATE_MESSAGE: 'La date effective devrait être dans le futur',
    AMOUNT_OFFERED: "Montants offerts",

    // Frequencies
    WEEKLY: 'Chaque semaine',
    MONTHLY: 'Chaque mois',
    BIWEEKLY: 'Toutes les deux semaines',
    BIMONTHLY: 'Deux fois par mois',

    // MATERIAL TABLE
    emptyDataSourceMessage: "Aucune information à afficher",
    addTooltip: "Ajouter",
    deleteTooltip: "Supprimer",
    editTooltip: "Modifier",
    deleteText: "Êtes-vous sûr de vouloir supprimer ce champ ?",
    cancelTooltip: "Annuler",
    saveTooltip: "Enregistrer",
    nRowsSelected: "{0} rangée(s) selectionnées",
    searchTooltip: "Rechercher",
    searchPlaceholder: "Rechercher",
    labelDisplayedRows: "{from}-{to} de {count}",
    labelRowsSelect: "rangées",
    labelRowsPerPage: "Rangées par page",
    firstTooltip: "Première Page",
    previousTooltip: "Page précédante",
    nextTooltip: "Page suivante",
    lastTooltip: "Dernière Page",

    // MESSAGES
    MESSAGES_MANAGEMENT: "Gestion des messages",
    MESSAGES: 'Messages',
    TITLE: 'Titre',
    DESCRIPTION: 'Description',


    // MESSAGE DETAIL
    MESSAGE_SAVED: "Message sauvegardé",
    ALL_MESSAGES: "Tout les messages",
    NEW_MESSAGE: "Nouveau message",
    FRENCH_EMAIL: "Courriel français",
    ENGLISH_EMAIL: "Courriel anglais",
    MESSAGE: "Message",
    MESSAGE_PLACEHOLDER: "Écrivez ici...",
    SUBJECT_FR: "Sujet français",
    SUBJECT_EN: "Sujet anlais",
    FRENCH_SMS: "Texto français",
    ENGLISH_SMS: "Texto anglais",

    // MESSAGE DROPDOWN
    AMOUNT_DUE: "Montant dû",
    BALANCE: "Balance",
    DUE_DATE: "Date d'échéance",
    FIRST_NAME: "Prénom du client",
    LAST_NAME: "Nom du client",
    IBV_LINK: "Lien IBV",
    EMAIL_DOCUMENT: "Courriel pour documents",
    EMAIL_CONTACT: "Courriel pour contact",
    LOANER_NAME: "Nom de la compagnie",
    LOAN_AMOUNT: "Montant du prêt",
    INTERAC_ANSWER: "Réponse Interac",
    CONTACT_PHONE: "Téléphone",
    WEBSITE: "site web",
    DIGITAL: "Digital",

    // MESSAGE CONFIGURATION
    MESSAGE_CONFIGURATION: "Configuration des messages",
    CONFIGURATION: "Configuration",
    FRENCH_HEADER: "Entête français",
    ENGLISH_HEADER: "Entête anglais",
    FRENCH_FOOTER: "Bas de page français",
    ENGLISH_FOOTER: "Bas de page anglais",

    // Payment Charges
    CHARGES_NSF: "NSF",
    CHARGES_DEFER_PAYMENT: "Paiement reporté",
    CHARGES_INTERAC: "Dépôt Interac",
    CHARGES_AUTO_WITHDRAWAL: "Prélèvements",
}

export default locale;
