import React from 'react';
import { useTranslation } from 'react-i18next';

const CollectionDegree = (props) => {
    const { t } = useTranslation('collectionsApp');

    switch (props.code) {
        case 'FIRST_NSF':
            {
                return <span>{t('FIRST_NSF')}</span>
            }
        case 'SECOND_NSF_CONSECUTIVE':
            {
                return <span>{t('SECOND_NSF_CONSECUTIVE')}</span>
            }
        case 'THIRD_NSF_CONSECUTIVE':
            {
                return <span>{t('THIRD_NSF_CONSECUTIVE')}</span>
            }
        case 'MANUAL_NSF':
            {
                return <span>{t('MANUAL_NSF')}</span>
            }
        case 'ACCOUNT_CLOSED':
            {
                return <span>{t('ACCOUNT_CLOSED')}</span>
            }
        default:
            {
                return <span>N/A</span>
            }
    }
}

export default CollectionDegree;