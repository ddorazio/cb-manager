import React from 'react';
import { useTranslation } from 'react-i18next';

const NonPaymentReason = (props) => {
    const { t } = useTranslation('collectionsApp');

    switch (props.code) {
        case 'BORROWED_TOO_MUCH':
            {
                return <span>{t('BORROWED_TOO_MUCH')}</span>
            }
        case 'OTHER':
            {
                return <span>{t('OTHER')}</span>
            }
        case 'TOO_MANY_NSF_PAYMENTS':
            {
                return <span>{t('TOO_MANY_NSF_PAYMENTS')}</span>
            }
        case 'UNEMPLOYED':
            {
                return <span>{t('UNEMPLOYED')}</span>
            }
    }
}

export default NonPaymentReason;