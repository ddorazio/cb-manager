import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Checkbox, IconButton, Icon } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { FuseLoading } from '@fuse';
import ReactTable from 'react-table';
import CollectionDegree from 'app/main/apps/collections/components/CollectionDegree';
import { useTranslation } from 'react-i18next';
import { getTransfersReceivable, sendInteracCollectionReminders } from '../store/actions/collections.actions.js';

function CollectionsList_payments(props) {
    const { t } = useTranslation('collectionsApp');
    const dispatch = useDispatch();
    const receivables = useSelector(store => store.collections.list.receivables);

    useEffect(() => {
        dispatch(getTransfersReceivable);
    }, []);

    if(!receivables) {
        return (
            <div style={{position:"absolute",top:"45%", marginLeft:"0", marginRight:"auto", left:"0",right:"0"}}>
                <FuseLoading ></FuseLoading> 
            </div>
           
        )
    }

    return (
        <React.Fragment>
            <Button
                variant="outlined"
                color="secondary"
                className="mb-16"
                startIcon={<Icon>send</Icon>}
                onClick={() => {
                    var collectionIds = receivables.map(rcv => rcv.id);
                    dispatch(sendInteracCollectionReminders(collectionIds));
                }}
            >
                {t("SEND_REMINDER")}
            </Button>
            <ReactTable
                className="-striped -highlight h-full w-full"
                data={receivables}
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick: (e, handleOriginal) => {
                            if (rowInfo) {
                                props.history.push('/apps/customers/detail/' + rowInfo.original.clientId);
                            }
                        }
                    }
                }}
                columns={[
                    {
                        Header: "",
                        width: 50,
                        sortable: false,
                        className: "no-pad-left",
                        Cell: row => (
                            <div className="flex items-center">
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        props.history.push('/apps/customers/detail/' + row.original.clientId);
                                        //dispatch(Actions.removeCustomer(row.original));
                                    }}
                                >
                                    <Icon>remove_red_eye</Icon>
                                </IconButton>
                            </div>
                        )
                    },
                    {
                        Header: t("COLLECTION_DEGREE"),
                        accessor: "collectionDegreeCode",
                        width: 170,
                        Cell: ({ row }) => <CollectionDegree code={row.collectionDegreeCode} />,
                        style: { 'whiteSpace': 'unset' }
                    },
                    {
                        Header: t("TRANSFER_DATE"),
                        accessor: "interacPaymentDate",
                        width: 210,
                    },
                    {
                        Header: t("AMOUNT"),
                        accessor: "amount",
                        width: 220,
                        style: { 'whiteSpace': 'unset' }
                    },

                    {
                        Header: t("SURNAME"),
                        accessor: "clientFirstName",
                        width: 170
                    },
                    {
                        Header: t("NAME"),
                        accessor: "clientLastName",
                        width: 170
                    },
                    {
                        Header: t("EMAIL"),
                        accessor: "clientEmailAddress",
                        style: { 'whiteSpace': 'unset' }
                    }
                ]}
                resizable={false}
                filterable={false}
                defaultPageSize={10}
                noDataText={t("emptyDataSourceMessage")}
            />
        </React.Fragment>
    )
}

export default withRouter(CollectionsList_payments);