import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers/';
import { FuseLoading } from '@fuse';
import { IconButton, Icon } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import ReactTable from 'react-table';
import CollectionDegree from 'app/main/apps/collections/components/CollectionDegree';
import { useTranslation } from 'react-i18next';
import { getRejectionsOfTheDay } from '../store/actions/collections.actions';

function CollectionsList(props) {
    const dispatch = useDispatch();
    const rejections = useSelector(store => { 
        return store.collections.list.rejections
    });
    const { t } = useTranslation('collectionsApp');

    useEffect(() => {
        dispatch(getRejectionsOfTheDay);
    }, []);

    if(!rejections) {
        return (
            <div style={{position:"absolute",top:"45%", marginLeft:"0", marginRight:"auto", left:"0",right:"0"}}>
                <FuseLoading ></FuseLoading> 
            </div>
           
        )
    }

    return (

        <ReactTable
            className="-striped -highlight h-full w-full"
            data={rejections}
            getTrProps={(state, rowInfo, column) => {
                return {
                    className: "cursor-pointer",
                    onClick: (e, handleOriginal) => {
                        if (rowInfo) {
                            props.history.push('/apps/customers/detail/' + rowInfo.original.clientId);
                        }
                    }
                }
            }}
            columns={[
                {
                    Header: "",
                    width: 50,
                    sortable: false,
                    className: "no-pad-left",
                    Cell: row => (
                        <div className="flex items-center">
                            <IconButton
                                onClick={(ev) => {
                                    ev.stopPropagation();
                                    props.history.push('/apps/customers/detail/' + row.original.clientId);
                                }}
                            >
                                <Icon>remove_red_eye</Icon>
                            </IconButton>
                        </div>
                    )
                },
                {
                    Header: t("COLLECTION_DEGREE"),
                    accessor: "collectionDegreeCode",
                    width: 170,
                    Cell: ({ row }) => <CollectionDegree code={row.collectionDegreeCode} />,
                    style: { 'whiteSpace': 'unset' }
                },
                {
                    id: "amount",
                    accessor: "amount",
                    Header: t("AMOUNT"),
                    width: 120,
                    Cell: row => (<div className="inline-block text-center text-18">{(Math.round(row.value * 100) / 100) + "$"}</div>)
                },
                {
                    Header: t("LAST_PAYMENT_DATE"),
                    accessor: "lastPaymentDate",
                    width: 190,
                },

                {
                    Header: t("SURNAME"),
                    accessor: "clientFirstName",
                    width: 140
                },
                {
                    Header: t("NAME"),
                    accessor: "clientLastName",
                    width: 140
                },
                {
                    Header: t("PHONE"),
                    accessor: "clientPhoneNumber",
                    width: 130
                },
                {
                    Header: t("PHONE_CELL"),
                    accessor: "clientCellPhoneNumber",
                    width: 130
                },
                {
                    Header: t("EMAIL"),
                    accessor: "clientEmailAddress",
                    style: { 'whiteSpace': 'unset' }
                }
            ]}
            resizable={false}
            filterable={false}
            defaultPageSize={10}
            noDataText={t("emptyDataSourceMessage")}
        />
    )
}

export default withRouter(withReducer('collections', reducer)(CollectionsList));