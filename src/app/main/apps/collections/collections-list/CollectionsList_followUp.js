import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IconButton, Icon } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import { FuseLoading } from '@fuse';
import ReactTable from 'react-table';
import { useTranslation } from 'react-i18next';
import { getFollowUps } from '../store/actions/collections.actions';
import CollectionDegree from 'app/main/apps/collections/components/CollectionDegree';
import NonPaymentReason from 'app/main/apps/collections/components/NonPaymentReason';

function CollectionsList_followUp(props) {
    const { t } = useTranslation('collectionsApp');
    const dispatch = useDispatch();
    const followUps = useSelector(store => store.collections.list.followUps);
    const { form, handleChange, setForm } = props;
    const isOpen = true;

    useEffect(() => {
        dispatch(getFollowUps);
    }, []);

    if(!followUps) {
        return (
            <div style={{position:"absolute",top:"45%", marginLeft:"0", marginRight:"auto", left:"0",right:"0"}}>
                <FuseLoading ></FuseLoading> 
            </div>
           
        )
    }

    return (

        <ReactTable
            className="-striped -highlight h-full w-full"
            data={followUps}
            getTrProps={(state, rowInfo, column) => {
                return {
                    className: "cursor-pointer",
                    onClick: (e, handleOriginal) => {
                        if (rowInfo) {
                            props.history.push('/apps/customers/detail/' + rowInfo.original.clientId);
                        }
                    }
                }
            }}
            columns={[
                {
                    Header: "",
                    width: 50,
                    sortable: false,
                    className: "no-pad-left",
                    Cell: row => (
                        <div className="flex items-center">
                            <IconButton
                                onClick={(ev) => {
                                    ev.stopPropagation();
                                    props.history.push('/apps/customers/detail/' + row.original.clientId);
                                }}
                            >
                                <Icon>remove_red_eye</Icon>
                            </IconButton>
                        </div>
                    )
                },
                {
                    Header: t("COLLECTION_DEGREE"),
                    accessor: "collectionDegreeCode",
                    width: 170,
                    Cell: ({ row }) => <CollectionDegree code={row.collectionDegreeCode} />,
                    style: { 'whiteSpace': 'unset' }
                },
                {
                    Header: t("FOLLOW_UP_DATE"),
                    accessor: "followUpDate",
                    width: 210,
                },
                {
                    Header: t("REASON_OF_NO_PAYMENT"),
                    accessor: "nonPaymentReasonCode",
                    width: 220,
                    Cell: ({ row }) => row.nonPaymentReasonCode == null ? '' : <NonPaymentReason code={row.nonPaymentReasonCode} />,
                    style: { 'whiteSpace': 'unset' }
                },
                {
                    Header: t("SURNAME"),
                    accessor: "clientFirstName",
                    width: 170
                },
                {
                    Header: t("NAME"),
                    accessor: "clientLastName",
                    width: 170
                },
                {
                    Header: t("LATEST_NOTE"),
                    accessor: "lastComment",
                    style: { 'whiteSpace': 'unset' }
                }
            ]}
            resizable={false}
            filterable={false}
            defaultPageSize={10}
            noDataText={t("emptyDataSourceMessage")}
        />
    )
}

export default withRouter(CollectionsList_followUp);