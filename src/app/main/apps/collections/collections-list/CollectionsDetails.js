import React, { useEffect } from 'react';
import { AppBar, Button, ButtonGroup, Card, CardContent, ClickAwayListener, DialogTitle, DialogContent, DialogContentText, DialogActions, FormControlLabel, Grow, Grid, Icon, IconButton, MenuItem, MenuList, Paper, Popper, Table, TableBody, TableContainer, TableHead, TableRow, TableCell, Toolbar, Typography } from '@material-ui/core';
import ReactTable from "react-table";
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
/* import EditLoanPaymentDialog from './dialog/editLoanPayment.dialog'
import EditLoanDialog from './dialog/editLoan.dialog'
import RestartLoanDialog from './dialog/restartLoan.dialog'
import SendContractDialog from './dialog/sendContract.dialog'
import NewRebateDialog from './dialog/newRebate.dialog'
import ManualPaymentDialog from './dialog/manualPayment.dialog' */
import { useTranslation } from 'react-i18next';
import { showMessage, openDialog, closeDialog } from 'app/store/actions/fuse';
import LoanAmounts from 'app/main/apps/configuration/amounts/LoanAmounts';

//import NewFormDialog from './dialog/newLoan.dialog'
//import * as Actions from '../../store/actions';

const options = [
    'Envoyer le contrat',
    'Modifier le prêt',
    'Nouveau paiement manuel',
    'Nouveau rabais'
];

// const options = ['Envoyer le contrat', 'Modifier le prêt', 'Nouveau paiement manuel', 'Nouveau rabais'];
function CollectionsDetails(props) {

    const { t } = useTranslation('customersApp');
    const { loan, form, setForm } = props;
    const dispatch = useDispatch();

    //const state = useSelector(({ CustomerComp }) => CustomerComp.customer);

    const [open, setOpen] = React.useState(false);

    const anchorRef = React.useRef(null);
    return (
        <Card className="px-16 my-16">
            <AppBar position="static" elevation={0}>
                <Toolbar className="pl-16 pr-8">
                    <Typography variant="subtitle2" color="inherit" className="flex-1">Informations sur le prêt</Typography>
                    
                </Toolbar>
            </AppBar>
            <CardContent className="mb-16 pr-8" style={{ borderWidth: "1px", borderColor: "#e5e5e5"}}>
                <div className="flex mb-16">
                    <div className="flex-1 mr-16">
                        <TableContainer component={Paper}>
                            <Table className="w-full" size="small">
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Montant du prêt :</TableCell>
                                        <TableCell align="right">10 000 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Intérêts (22 %) :</TableCell>
                                        <TableCell align="right">62.00 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Frais de courtage :</TableCell>
                                        <TableCell align="right">32.00 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Paiement :</TableCell>
                                        <TableCell align="right">134.00 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Fréquence :</TableCell>
                                        <TableCell align="right">Hebdomadaire</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Date de création :</TableCell>
                                        <TableCell align="right"></TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                    <div className="flex-1">
                        <TableContainer component={Paper}>
                            <Table className="w-full" size="small">
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Paiements en défaut :</TableCell>
                                        <TableCell align="right" className="font-bold">2</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Frais :</TableCell>
                                        <TableCell align="right">100 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Solde du jour :</TableCell>
                                        <TableCell align="right">450 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold text-red-400">Rabais :</TableCell>
                                        <TableCell align="right" className="text-red-400">0 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Total dû :</TableCell>
                                        <TableCell align="right">20000 $</TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell component="th" className="font-bold">Agent :</TableCell>
                                        <TableCell align="right">N/A</TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </div>
            </CardContent>
        </Card >
    );
}

export default CollectionsDetails;
