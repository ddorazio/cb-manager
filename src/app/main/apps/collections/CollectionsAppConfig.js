import React from 'react';
import i18next from 'i18next';
import en from './i18n/en';
import fr from './i18n/fr';

i18next.addResourceBundle('en', 'collectionsApp', en);
i18next.addResourceBundle('fr', 'collectionsApp', fr);

export const CollectionsAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/apps/collections',
            component: React.lazy(() => import('./CollectionsApp'))
        }
    ]
};
