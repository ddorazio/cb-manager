import React, { useRef, useState } from 'react';
import {Divider, List, ListItem, ListItemText, Paper, Tab, Tabs, Typography, Icon } from '@material-ui/core';
import {FuseAnimate, FusePageCarded, FusePageSimple, NavLinkAdapter } from '@fuse';
import { useForm } from '@fuse/hooks';
import { useTranslation } from 'react-i18next';
import {makeStyles} from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from 'app/store/actions';
import reducer from 'app/store/reducers';
import CollectionsList from './collections-list/CollectionsList';
import CollectionsList_followUp from './collections-list/CollectionsList_followUp';
import CollectionsList_payments from './collections-list/CollectionsList_payments';
import CollectionsList_waiting from './collections-list/CollectionsList_waiting';

const useStyles = makeStyles(theme => ({
    listItem: {
        color              : 'inherit!important',
        textDecoration     : 'none!important',
        height             : 40,
        width              : 'calc(100% - 16px)',
        borderRadius       : '0 20px 20px 0',
        paddingLeft        : 24,
        paddingRight       : 12,
        '&.active'         : {
            backgroundColor    : theme.palette.secondary.main,
            color              : theme.palette.secondary.contrastText + '!important',
            pointerEvents      : 'none',
            '& .list-item-icon': {
                color: 'inherit'
            }
        },
        '& .list-item-icon': {
            marginRight: 16
        }
    }
}));



function CollectionsApp(props) {
    const classes = useStyles(props);
    const { t } = useTranslation('collectionsApp');
    const dispatch = useDispatch();
    const customer = useSelector(({ CollectionComp }) => CollectionComp.customer);
    const [tabValue, setTabValue] = useState(0);
    const { form, handleChange, setForm } = useForm(null);
    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    return (
            <FusePageCarded
                classes={{
                    toolbar: "h-84",
                    header: "min-h-72 h-72 sm:h-136 sm:min-h-136 title"
                }}
                header={
                    <div className="p-16 flex flex-1 flex-col items-center md:flex-row">
                        <FuseAnimate animation="transition.expandIn" delay={300}>
                            <Icon className="text-32 mr-12">warning</Icon>
                        </FuseAnimate>
                        <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                            <Typography variant="h6" className="hidden sm:flex">{t("COLLECTION_FILES")}</Typography>
                        </FuseAnimate>
                    </div>
                }
                contentToolbar={
                    <Tabs
                        value={tabValue}
                        onChange={handleChangeTab}
                        indicatorColor="secondary"
                        textColor="primary"
                        variant="scrollable"
                        className="w-full"
                    >
                        <Tab className="normal-case" icon={<Icon>star</Icon>} label={t("REJECTION_OF_THE_DAY")} />
                        <Tab className="normal-case" icon={<Icon>check_circle</Icon>} label={t("FOLLOW_UP")} />
                        <Tab className="normal-case" icon={<Icon>move_to_inbox</Icon>} label={t("TRANSFERS_RECEIVABLE")} />
                        <Tab className="normal-case" icon={<Icon>remove_circle_outline</Icon>} label={t("WAITING_RESPONSE")} />
    
                    </Tabs>
                }

                content={
                        <div className="p-24">
                            {tabValue === 0 &&
                                (
                                    <CollectionsList form={form} handleChange={handleChange} setForm={setForm} customer={customer} />
                                )
                            }
                            {tabValue === 1 &&
                                (
                                    <CollectionsList_followUp form={form} handleChange={handleChange} setForm={setForm} customer={customer} />
                                )
                            }
                            {tabValue === 2 &&
                                (
                                    <CollectionsList_payments form={form} handleChange={handleChange} setForm={setForm} customer={customer} />
                                )
                            }
                            {tabValue === 3 &&
                                (
                                    <CollectionsList_waiting form={form} handleChange={handleChange} setForm={setForm} customer={customer} />
                                )
                            }

                        

                        </div>
                }
                innerScroll
            />
    )
}

export default withReducer('CollectionComp', reducer)(CollectionsApp);
