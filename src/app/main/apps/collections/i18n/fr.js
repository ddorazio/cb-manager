const locale = {
    REJECTION_OF_THE_DAY: "Rejets du jour",
    FOLLOW_UP: "Faire suivis",
    TRANSFERS_RECEIVABLE: "Virement à recevoir",
    WAITING_RESPONSE: "En attente",
    SEND_REMINDER: "Envoyer un rappel à toute la liste",
    AMOUNT: "Montant dû",
    COLLECTION_FILES: "Dossiers de collection",
    STATUS: "Statut",
    COLLECTION_DEGREE: "Degré de collection",
    REASON_OF_NO_PAYMENT: "Raison de non-paiement",
    LAST_PAYMENT_DATE: "Date dernier paiement",
    SINCE: "Depuis",
    SURNAME: "Prénom",
    NAME: "Nom",
    PHONE: "Téléphone",
    PHONE_CELL: "Cellulaire",
    EMAIL: "Courriel",
    SAVE: "Enregistrer",
    UPDATE: "Mettre à jour",
    LATEST_NOTE: "Dernière note",
    NO_COLLECTION_FILES: "Aucun dossier de collection.",
    DAYS: "jours",
    ALL_FILES: "Tous les dossiers",
    FOLLOW_UP_DATE: "Date de suivi",
    TRANSFER_DATE: "Date du virement prévu",


    // MATERIAL TABLE
    emptyDataSourceMessage: "Aucune information à afficher",
    addTooltip: "Ajouter",
    deleteTooltip: "Supprimer",
    editTooltip: "Modifier",
    deleteText: "Êtes-vous sûr de vouloir supprimer ce champ ?",
    cancelTooltip: "Annuler",
    saveTooltip: "Enregistrer",
    nRowsSelected: "{0} rangée(s) selectionnées",
    searchTooltip: "Rechercher",
    searchPlaceholder: "Rechercher",
    labelDisplayedRows: "{from}-{to} de {count}",
    labelRowsSelect: "rangées",
    labelRowsPerPage: "Rangées par page",
    firstTooltip: "Première Page",
    previousTooltip: "Page précédante",
    nextTooltip: "Page suivante",
    lastTooltip: "Dernière Page",

    // Collection degrees
    FIRST_NSF: 'Premier NSF',
    SECOND_NSF_CONSECUTIVE: 'Deuxième NSF consécutif',
    THIRD_NSF_CONSECUTIVE: 'TroisièmeNSF consécutif',
    MANUAL_NSF: 'NSF manuel',
    ACCOUNT_CLOSED: "Compte fermé",

    // Non-payment reasons
    BORROWED_TOO_MUCH: "Trop d'empruns",
    OTHER: "Autre",
    TOO_MANY_NSF_PAYMENTS: "Trop de paiements NSF",
    UNEMPLOYED: 'Sans emploi'
}

export default locale;
