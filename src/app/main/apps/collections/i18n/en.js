const locale = {
    REJECTION_OF_THE_DAY: "Rejections of the day",
    FOLLOW_UP: "Need follow up",
    TRANSFERS_RECEIVABLE: "Transfers receivable to date",
    WAITING_RESPONSE: "Waiting response",
    SEND_REMINDER: "Send a reminder to the whole list",
    AMOUNT: "Amount",
    COLLECTION_FILES: "Collection Files",
    STATUS: "Status",
    COLLECTION_DEGREE: "Collection degree",
    REASON_OF_NO_PAYMENT: "Reason of non-payment",
    LAST_PAYMENT_DATE: "Last payment date",
    SINCE: "Since",
    SURNAME: "First Name",
    NAME: "Last Name",
    PHONE: "Phone",
    PHONE_CELL: "Cellphone",
    EMAIL: "Email",
    SAVE: "Save",
    UPDATE: "Update",
    LATEST_NOTE: "Latest Note",
    NO_COLLECTION_FILES: "No collections files yet",
    DAYS: "days",
    ALL_FILES: "All files",
    FOLLOW_UP_DATE: "Follow up date",
    TRANSFER_DATE: "Transfer date",
    SEND_SMS: "Send an SMS reminder the same day",
    ADD_NOTE: "Add a note",


    // MATERIAL TABLE
    emptyDataSourceMessage: "Nothing to display",
    addTooltip: "Add",
    deleteTooltip: "Delete",
    editTooltip: "Edit",
    deleteText: "Are you sure you want to delete this?",
    cancelTooltip: "Cancel",
    saveTooltip: "Save",
    nRowsSelected: "{0} row (s) selected",
    searchTooltip: "Search",
    searchPlaceholder: "Search",
    labelDisplayedRows: "{from} - {to} of {count}",
    labelRowsSelect: "rows",
    labelRowsPerPage: "Rows per page",
    firstTooltip: "First Page",
    previousTooltip: "Previous page",
    nextTooltip: "Next page",
    lastTooltip: "Last Page",

    // Collection degrees
    FIRST_NSF: 'First NSF',
    SECOND_NSF_CONSECUTIVE: 'Second consecutive NSF',
    THIRD_NSF_CONSECUTIVE: 'Third consecutive NSF',
    MANUAL_NSF: 'Manual NSF',
    ACCOUNT_CLOSED: "Account closed",

    // Non-payment reasons
    BORROWED_TOO_MUCH: "Borrowed too much",
    OTHER: "Other",
    TOO_MANY_NSF_PAYMENTS: "Too many NSF payments",
    UNEMPLOYED: 'Unemployed'
}

export default locale;
