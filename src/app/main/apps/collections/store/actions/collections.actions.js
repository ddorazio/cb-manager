import { showMessage } from 'app/store/actions/fuse';
import CollectionAPI from '../../../customers/CollectionAPI';
import i18n from 'i18n';

export const GET_REJECTIONS_OF_THE_DAY = 'GET_REJECTIONS_OF_THE_DAY';
export const GET_FOLLOW_UPS = 'GET_FOLLOW_UPS';
export const GET_TRANSFERS_RECEIVABLE = 'GET_TRANSFERS_RECEIVABLE';
export const GET_COLLECTIONS_AWAITING_RESPONSE = 'GET_COLLECTIONS_AWAITING_RESPONSE';
export const SEND_INTERAC_COLLECTION_REMINDERS = 'SEND_INTERAC_COLLECTION_REMINDERS';

export const getRejectionsOfTheDay = async (dispatch) => {
    try {
        const result = await CollectionAPI.getRejectionsOfTheDay();
        dispatch({
            type: GET_REJECTIONS_OF_THE_DAY,
            payload: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const getFollowUps = async (dispatch) => {
    try {
        const result = await CollectionAPI.getCollectionsWithFollowUp();
        dispatch({
            type: GET_FOLLOW_UPS,
            payload: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const getTransfersReceivable = async (dispatch) => {
    try {
        const result = await CollectionAPI.getTransfersReceivableToDate();
        dispatch({
            type: GET_TRANSFERS_RECEIVABLE,
            payload: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const getCollectionsAwaitingResponse = async (dispatch) => {
    try {
        const result = await CollectionAPI.getCollectionsAwaitingResponse();
        dispatch({
            type: GET_COLLECTIONS_AWAITING_RESPONSE,
            payload: result
        });
    } catch (error) {
        return dispatch(showMessage({ message: error, variant: 'error' }));
    }
}

export const sendInteracCollectionReminders = (collectionIds) => {
    return async (dispatch) => {
        try {
            await CollectionAPI.sendInteracCollectionReminders(collectionIds);
            dispatch({
                type: SEND_INTERAC_COLLECTION_REMINDERS,
                collectionIds
            });
            dispatch(showMessage({
                message: i18n.t('MESSAGE_SENT'),
                variant: 'success'
            }));
        } catch (error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}