import * as Actions from '../actions/collections.actions';

const initialState = {
    rejections: null,
    receivables: null,
    onHolds: null,
    followUps: null
};

const collectionsReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.GET_REJECTIONS_OF_THE_DAY:
            {
                return {
                    ...state,
                    rejections: action.payload
                }
            }
        case Actions.GET_FOLLOW_UPS:
            {
                return {
                    ...state,
                    followUps: action.payload
                }
            }
        case Actions.GET_TRANSFERS_RECEIVABLE:
            {
                return {
                    ...state,
                    receivables: action.payload
                }
            }
        case Actions.GET_COLLECTIONS_AWAITING_RESPONSE:
            {
                return {
                    ...state,
                    onHolds: action.payload
                }
            }
        default:
            {
                return state;
            }
    }
}

export default collectionsReducer;