import {combineReducers} from 'redux';
import list from './collections.reducer';

const reducer = combineReducers({
    list
});

export default reducer;
