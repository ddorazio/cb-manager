import React from 'react';
import enNotifications from './i18n/enNotifications';
import frNotifications from './i18n/frNotifications';
import i18next from 'i18next';

i18next.addResourceBundle('en', 'notificationsApp', enNotifications);
i18next.addResourceBundle('fr', 'notificationsApp', frNotifications);

export const NotificationsAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/apps/notifications/:type',
            component: React.lazy(() => import('./NotificationsApp'))
        }
    ]
};
