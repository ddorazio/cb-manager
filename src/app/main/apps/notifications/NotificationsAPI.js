import { getProtected, putProtected, postProtected, deleteProtected } from 'app/main/api/apiWrapper';
import settingsConfig from 'app/fuse-configs/settingsConfig'

class NotificationAPI {

    static sendNotificationAll = async (type, idClient, path) => {
        path = (path === undefined) ? window.location.pathname : path
        const data = {
            type : type,
            path: window.location.pathname,
            idClient : idClient
        }
        return await postProtected(new URL('/api/Notification/NewNotification', settingsConfig.apiPath), data);
    }

    static getNotifications = async (type) => {
        return await getProtected(new URL('/api/Notification/GetNotifications?type='+type, settingsConfig.apiPath));
    }

    static setNotificationRead = async (id) => {
        return await putProtected(new URL('/api/Notification/setNotificationRead?id='+id, settingsConfig.apiPath));
    }
}

export default NotificationAPI