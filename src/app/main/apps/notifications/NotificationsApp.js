import React, {useEffect, useRef, useState} from 'react';
import {FusePageSimple} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from './store/actions';
import reducer from './store/reducers';
import NotificationList from './NotificationsList'


function NotificationsApp(props)
{
    const dispatch = useDispatch();
    const pageLayout = useRef(null);
    const notifications = useSelector(({notificationsApp}) => notificationsApp.notif.notifications);
    const notifType = useSelector(({notificationsApp}) => notificationsApp.notif.notifType);
    const [notifs, setNotifs] = useState([])

    useEffect(() => {
        dispatch(Actions.setNotifType(props.match.params.type))
    })

    useEffect(() => {  
        if(notifType) {
            dispatch(Actions.getNotifications(notifType)) 
        }     
    }, [notifType]);

    useEffect(() => {  
        if(notifType && notifications) {
            loadData()   
        }     
    }, [notifications]);

    const loadData = () => {
        if(notifType == "PREAPPROVED") {
            setNotifs(notifications.preapproved)
        } 
        else if (notifType == "ACCEPTED") {
            setNotifs(notifications.accepted)
        } 
        else if (notifType == "CONTRACT") {
            setNotifs(notifications.contract)
        }
        else if (notifType == "IBV") {
            setNotifs(notifications.ibv)
        }
        else if (notifType == "REJECT") {
            setNotifs(notifications.reject)
        }
    }

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-80 sm:pb-80 h-full",
                    content       : "flex flex-col h-full",
                    leftSidebar   : "w-256 border-0",
                    header        : "min-h-72 h-72 sm:h-136 sm:min-h-136"
                }}
                content={
                    <NotificationList notifications={notifs} notifType={notifType}/>
                }
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
        </React.Fragment>
    )
}

export default withReducer('notificationsApp', reducer)(NotificationsApp);
