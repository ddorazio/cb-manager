import NotificationAPI from '../../NotificationsAPI'
import { showMessage } from 'app/store/actions/fuse';

export const SEND_NOTIFICATION_ALL = 'SEND_NOTIFICATION_ALL';
export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS';
export const SET_NOTIFICATION_READ = 'SET_NOTIFICATION_READ';

export const SET_NOTIF_TYPE = "SET_NOTIF_TYPE";


export const sendNotificationAll = (type, idClient, path) => {
    return async (dispatch) => {
        try {          
            await NotificationAPI.sendNotificationAll(type, idClient, path);
        } catch(error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const getNotifications = (notifType) => {
    return async (dispatch) => {
        try {          
            const result = await NotificationAPI.getNotifications(notifType);

            return dispatch({
                type: GET_NOTIFICATIONS,
                payload: result.data,
                notifType: notifType
            });
        } catch(error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const setNotificationRead = (id) => {
    return async (dispatch) => {
        try {     
            await NotificationAPI.setNotificationRead(id);

        } catch(error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}

export const setNotifType = (notifType) => {
    return async (dispatch) => {
        try {          

            return dispatch({
                type:SET_NOTIF_TYPE,
                notifType: notifType
            });
        } catch(error) {
            return dispatch(showMessage({ message: error, variant: 'error' }));
        }
    }
}