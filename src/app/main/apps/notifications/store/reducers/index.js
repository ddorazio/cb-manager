import {combineReducers} from 'redux';
import notif from './notification.reducer';

const reducer = combineReducers({
    notif
});

export default reducer;