import * as Actions from '../actions/notification.actions';

const initialState = {
    notifications: {
        preapproved : [],
        accepted: [],
        contract: [],
        ibv: [],
        reject: []
    },
    notifType: "",

};

const notificationReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.SEND_NOTIFICATION_ALL:
            {   
                return {
                    ...state,
                }
            }
        case Actions.GET_NOTIFICATIONS:
            {   
                if(action.notifType == "PREAPPROVED") {
                    return {
                        ...state,
                        notifications : {
                            ...state.notifications,
                            preapproved: action.payload
                        }  
                    }
                } 
                else if (action.notifType == "ACCEPTED") {
                    return {
                        ...state,
                        notifications : {
                            ...state.notifications,
                            accepted: action.payload
                        }  
                    }
                } 
                else if (action.notifType == "CONTRACT") {
                    return {
                        ...state,
                        notifications : {
                            ...state.notifications,
                            contract: action.payload
                        }  
                    }
                }
                else if (action.notifType == "IBV") {
                    return {
                        ...state,
                        notifications : {
                            ...state.notifications,
                            ibv: action.payload
                        }  
                    }
                }
                else if (action.notifType == "REJECT") {
                    return {
                        ...state,
                        notifications : {
                            ...state.notifications,
                            reject: action.payload
                        }  
                    }
                }
            }
        case Actions.SET_NOTIF_TYPE:
            {   
    
                return {
                    ...state,
                    notifType : action.notifType
                }
            }
        case Actions.SET_NOTIFICATION_READ:
            {   
                return {
                    ...state,
                }
            }            
        default:
            {
                return state;
            }
    }
}

export default notificationReducer;