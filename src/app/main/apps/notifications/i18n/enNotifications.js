const locale = {
    ACCEPTED: "Approved requests",
    PREAPPROVED: "Pre-Approved requests",
    CONTRACT: "Signed contracts",
    REJECT: "Daily rejections",
    IBV: "Completed IBV",

    NO_NOTIFICATIONS: "There are no notifications!",
    NO_READ_DATE: "Click to read the notifications",
    DATE_READ: "Reading date",
    DATE_CREATED: "Creation date"
}

export default locale;