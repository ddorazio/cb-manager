const locale = {
    ACCEPTED: "Demandes approuvées",
    PREAPPROVED: "Demandes pré-approuvées",
    CONTRACT: "Contrats signés",
    REJECT: "Rejets du jour",
    IBV: "IBV terminé",

    NO_NOTIFICATIONS: "Aucune notification!",
    NO_READ_DATE: "Cliquez pour lire la notification",
    DATE_READ: "Date de lecture",
    DATE_CREATED: "Date de création"
}

export default locale;