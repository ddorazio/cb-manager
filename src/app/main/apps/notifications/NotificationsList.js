import React, {useEffect, useState} from 'react';
import {Typography} from '@material-ui/core';
import {FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import MaterialTable from "material-table";
import ReactTable from "react-table";
import * as Actions from './store/actions';
import { useHistory } from "react-router-dom";
import {makeStyles} from '@material-ui/styles';
import { useTranslation } from 'react-i18next';
import MaterialTableTraduction from 'app/i18n/materialTable/MaterialTableTraduction'

const useStyles = makeStyles({});

function NotificationsList(props)
{
    const localeTable = MaterialTableTraduction()
    const dispatch = useDispatch();
    const classes = useStyles(props);
    const notifications = props.notifications
    const notifType = props.notifType
    const history = useHistory()
    const { t } = useTranslation('notificationsApp');
    
    const columns=[
        {
            title    : t("DATE_CREATED"),
            field  : "dateCreatedFormat",
            className : "font-bold"
        },
        {
            title    : "Message",
            field  : "message",
        },
        {
            title : t("DATE_READ"),
            field : "dateReadFormat",
            render : rowData => {
                if(rowData.dateReadFormat == "") {
                    return <p>{t("NO_READ_DATE")}</p>
                } else {
                    return <p>{rowData.dateReadFormat}</p>
                }
            }
        }
    ]

    if ( !notifications || (notifications && notifications.length === 0))
    {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                   {t("NO_NOTIFICATIONS")}
                </Typography>
            </div>
        );
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
           
            <MaterialTable
                data={notifications}
                columns={columns}
                title={t(notifType)}
                
                onRowClick={(state, rowData) => { 
                    dispatch(Actions.setNotificationRead(rowData.id))
                    history.push(rowData.path)
                }}
                localization={localeTable}
                options={{
                    pageSize:13,
                    pageSizeOptions:[13],
                    rowStyle: rowData => ({
                        backgroundColor: (rowData.read == true) ? "grey"  : "white",
                        color: (rowData.read == true) ? "white"  : "black",
                    })
                }}
                noDataText={t("NO_NOTIFICATIONS")}
            />
        </FuseAnimate>
    );
}

export default NotificationsList;
