import React, {useEffect, useState} from 'react';
import { Button, Typography} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import withReducer from 'app/store/withReducer';
import { useHistory } from "react-router-dom";
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles({
    notifButton: {
        marginRight:"8px",
        fontWeight:"600",
        fontSize:"13px",
        backgroundColor:"#e5e5e5",
       "&:hover" : {
           cursor:"pointer"
       }
    },
    numberIcon: {
        position:"relative",
        top:"-15px",
        left:"-15px",
        color: "white",
        width:"25px",
        height:"25px",
        fontSize:"11px",
        fontWeight:"700",
        display:"flex",
        justifyContent:"center",
        alignItems:"center",
        borderRadius:"50%"
    },
    redNotif : {
        backgroundColor:"red",
    },
    noNotif : {
        backgroundColor:"grey",
    }
});

function ButtonNotif(props)
{
    const dispatch = useDispatch();
    const history = useHistory()
    const classes = useStyles(props);

    const notifType = props.notifType
    const notifs = props.notifications
    const numberRead = props.numberRead
    const buttonName = props.buttonName

    const openNotification = (notifType) => {
        dispatch(Actions.setNotifType(notifType))
        history.push("/apps/notifications/"+notifType)
    }

    return (
        <React.Fragment>
                <Button className={classes.notifButton} onClick={() => openNotification(notifType)}>
                    {buttonName}
                </Button>
                {numberRead == 0 ?
                    <Typography className={`${classes.numberIcon} ${classes.noNotif}`}>{numberRead}</Typography>
                :           
                    <Typography className={`${classes.numberIcon} ${classes.redNotif}`}>{numberRead}</Typography>
                }
        </React.Fragment>
    );
}

export default withReducer('notificationsApp', reducer)(ButtonNotif);
