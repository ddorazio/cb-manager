import React, {useEffect, useState} from 'react';
import { IconButton, Button} from '@material-ui/core';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import withReducer from 'app/store/withReducer';
import { useTranslation } from 'react-i18next';
import { useHistory } from "react-router-dom";
import {makeStyles} from '@material-ui/styles';

import ButtonNotif from './ButtonNotif'

const useStyles = makeStyles({
    notifContainer:{
        marginLeft:"25px",
        display:"flex",
        alignItems:"center"
    }
});

function ButtonNotifContainer(props)
{
    const dispatch = useDispatch();
    const classes = useStyles(props);
    const { t } = useTranslation('notificationsApp');

    const notifications = useSelector(({notificationsApp}) => notificationsApp.notif.notifications);

    useEffect(() => {
        loadData();  
    }, [dispatch]);

    useEffect(() => {
    }, [notifications])

    const loadData = () => {
        dispatch(Actions.getNotifications("PREAPPROVED"));
        dispatch(Actions.getNotifications("ACCEPTED"));
        dispatch(Actions.getNotifications("CONTRACT"));
        dispatch(Actions.getNotifications("IBV"));
        dispatch(Actions.getNotifications("REJECT"));
    }

    const getNumberRead = (notifs) => {
        return notifs.filter(notif => notif.read == false).length
    }

    return (
        <div className={classes.notifContainer}>
                <ButtonNotif 
                    notifications={notifications.preapproved} 
                    notifType={"PREAPPROVED"} 
                    numberRead={getNumberRead(notifications.preapproved)}
                    buttonName={t("PREAPPROVED")}
                    />
                <ButtonNotif 
                    notifications={notifications.accepted} 
                    notifType={"ACCEPTED"} 
                    numberRead={getNumberRead(notifications.accepted)} 
                    buttonName={t("ACCEPTED")}
                    />
                <ButtonNotif 
                    notifications={notifications.contract} 
                    notifType={"CONTRACT"} 
                    numberRead={getNumberRead(notifications.contract)}
                    buttonName={t("CONTRACT")} 
                    />
                <ButtonNotif 
                    notifications={notifications.ibv} 
                    notifType={"IBV"} 
                    numberRead={getNumberRead(notifications.ibv)}
                    buttonName={t("IBV")} 
                    />
                <ButtonNotif 
                    notifications={notifications.reject} 
                    notifType={"REJECT"} 
                    numberRead={getNumberRead(notifications.reject)} 
                    buttonName={t("REJECT")}
                    />
        </div>
    );
}

export default withReducer('notificationsApp', reducer)(ButtonNotifContainer);
