import { AnalyticsDashboardAppConfig } from './dashboards/analytics/AnalyticsDashboardAppConfig';
import { ProjectDashboardAppConfig } from './dashboards/project/ProjectDashboardAppConfig';
import { MailAppConfig } from './mail/MailAppConfig';
import { CustomersAppConfig } from './customers/CustomersAppConfig';
import { RequestsAppConfig } from './requests/RequestsAppConfig';
import { ContactsAppConfig } from './contacts/ContactsAppConfig';
import { CalendarAppConfig } from './calendar/CalendarAppConfig';
import { ConfigurationAppConfig } from './configuration/ConfigurationAppConfig';
import { FuseComponentsConfig } from '../../../@fuse/components/FuseComponentsConfig';
import { CollectionsAppConfig } from './collections/CollectionsAppConfig';
import { DepositsAppConfig } from './deposits/DepositsAppConfig'
import { AgreementAppConfig } from './agreement/AgreementAppConfig'
import { SupersetAppConfig } from './superset/SupersetAppConfig'

export const appsConfigs = [
    AnalyticsDashboardAppConfig,
    ProjectDashboardAppConfig,
    MailAppConfig,
    ContactsAppConfig,
    CustomersAppConfig,
    RequestsAppConfig,
    CalendarAppConfig,
    ConfigurationAppConfig,
    FuseComponentsConfig,
    CollectionsAppConfig,
    DepositsAppConfig,
    AgreementAppConfig,
    SupersetAppConfig

];